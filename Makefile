build:
	RUSTFLAGS=--cfg=web_sys_unstable_apis wasm-pack build apps/demo-web --dev --target web --no-typescript
	cp apps/demo-web/src/index.html apps/demo-web/pkg/

build-posh:
	powershell $$env:RUSTFLAGS = \'--cfg=web_sys_unstable_apis\'
	powershell wasm-pack build apps/demo-web --dev --target web --no-typescript
	powershell cp apps/demo-web/src/index.html apps/demo-web/pkg/

profile:
	RUSTFLAGS=--cfg=web_sys_unstable_apis wasm-pack build apps/demo-web --profiling --target web --no-typescript
	cp apps/demo-web/src/index.html apps/demo-web/pkg/

release:
	RUSTFLAGS=--cfg=web_sys_unstable_apis wasm-pack build apps/demo-web --release --target web --no-typescript
	cp apps/demo-web/src/index.html apps/demo-web/pkg/

clean:
	rm -rfv apps/demo-web/pkg
