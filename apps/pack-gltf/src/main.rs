// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::Result;
use clap::Parser;
use russimp::scene::{PostProcess, Scene};

/// Convert a shape in any format to a GLTF with expected packing
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// Source file to import
    #[arg(short, long)]
    source: String,
}

fn main() -> Result<()> {
    let args = Args::parse();

    let scene = Scene::from_file(
        &args.source,
        vec![
            PostProcess::CalculateTangentSpace,
            // PostProcess::FixOrRemoveInvalidData,
            PostProcess::GenerateUVCoords,
            // PostProcess::TransformUVCoords,
            // PostProcess::FindInstances,
            PostProcess::JoinIdenticalVertices,
            PostProcess::MakeLeftHanded,
            PostProcess::Triangulate,
            PostProcess::GenerateNormals,
            PostProcess::SortByPrimitiveType,
            PostProcess::ValidateDataStructure,
            PostProcess::ImproveCacheLocality,
            // PostProcess::OptimizeMeshes,
            // PostProcess::OptimizeGraph,
            PostProcess::RemoveRedundantMaterials,
            PostProcess::GenerateBoundingBoxes,
        ],
    )?;
    // println!("And Scene: {scene:#?}");
    // println!("materials: {}", scene.materials.len());
    // println!("meshes: {}", scene.meshes.len());

    for mat in &scene.materials {
        println!("tex: {:?}", mat.properties[1].data);
    }
    for mesh in &scene.meshes {
        println!(
            "mesh: {}, verts: {}, faces: {}",
            mesh.name,
            mesh.vertices.len(),
            mesh.faces.len()
        );
    }
    // println!("{:#?}", scene.root);

    Ok(())
}
