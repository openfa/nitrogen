// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use nitrogen_main::{shared_main, NitrogenOpts};
use wasm_bindgen::prelude::*;

#[wasm_bindgen(start)]
pub async fn main_js() -> Result<(), JsValue> {
    shared_main(NitrogenOpts::default(), "Nitrogen Web Demo")
        .await
        .map_err(|e| js_sys::Error::new(&e.to_string()))?;
    Ok(())
}
