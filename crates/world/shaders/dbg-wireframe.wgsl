// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <camera/include/camera.wgsl>
//!bindings [0] <camera/bindings/camera.wgsl>

struct FragmentOutput {
    @location(0) color: vec4<f32>,
}

@fragment
fn main_frag() -> FragmentOutput {
    return FragmentOutput(vec4(1., 0., 1., 1.));
}

struct VertexOutput {
    @builtin(position) position: vec4<f32>,
}

@vertex
fn main_vert(
    @location(0) v_surface_position_eye: vec3<f32>,
    @location(1) v_position_eye: vec3<f32>,
    @location(2) v_normal_eye: vec3<f32>,
    @location(3) v_graticule: vec2<f32>
) -> VertexOutput {
    return VertexOutput(camera.perspective_m * vec4(v_position_eye, 1.));
}
