// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <shader_shared/include/consts.wgsl>
//!include <shader_shared/include/fullscreen.wgsl>
//!include <shader_shared/include/quaternion.wgsl>
//!include <atmosphere/include/global.wgsl>
//!include <atmosphere/include/library.wgsl>
//!include <camera/include/camera.wgsl>
//!include <orrery/include/orrery.wgsl>
//!include <terrain/include/terrain_render.wgsl>
//!include <world/include/world.wgsl>
//!include <stars/include/stars.wgsl>

//!bindings [0] <stars/bindings/stars.wgsl>
//!bindings [0] <atmosphere/bindings/atmosphere.wgsl>
//!bindings [0] <orrery/bindings/orrery.wgsl>
//!bindings [1] <terrain/bindings/layout_composite.wgsl>
//!bindings [2] <camera/bindings/camera.wgsl>

struct VertexOutput {
    @location(0) v_ray_world: vec3<f32>,
    @location(1) ndc: vec2<f32>,
    @location(2) v_tc_idx: vec2<f32>,
    @builtin(position) position: vec4<f32>,
}

@vertex
fn vert_common_main(@builtin(vertex_index) i: u32) -> VertexOutput {
    let input = fullscreen_input(i);
    let v_ray_world = raymarching_view_ray(input.clip, camera.inverse_perspective_m, camera.inverse_view_m);

    // We need a screen uv version for texelFetch for integer textures.
    let v_tc_idx = input.frame * vec2(
        f32(camera.render_width),
        f32(camera.render_height)
    );

    // Assignment of clip space will produce ndc on the other side.
    // But since w is 1, clip == ndc in this case.
    return VertexOutput(v_ray_world, input.ndc.xy, v_tc_idx, input.clip);
}

struct FragOut {
    @location(0) color: vec4<f32>,
}

fn fullscreen_to_world_km(fullscreen: vec2<f32>, z_ndc: f32) -> vec4<f32>
{
    // Use our depth coordinate in ndc to find w. We know the z component of clip is always the near depth
    // because of how we constructed our perspective matrix. We multiplied by perspective to get clip and
    // assigned to gl_Position. Internally this will have produced a fragment in ndc by dividing by w, which
    // is more or less -z, again because of how we constructed our perspective matrix.
    // z{ndc} = z{clip} / w
    // w = z{clip} / z{ndc}
    let w = camera.z_near_m / z_ndc;

    // Now that we have w, we can reconstruct the clip space we passed into gl_Position. The fullscreen position
    // we pass through the vertex shader is technically in clip space, but because we also set w to 1 on position
    // that is equal to the ndc that would have been present for the z ndc coordinate we looked up in the depth
    // buffer at the equivalent texture coordinate. Thus we can reverse the fullscreen clip as if it were the
    // terrain's ndc.
    // x{ndc} = x{clip} / w
    // x{clip} = x{ndc} * w
    // z{clip} = z{ndc} * w
    let clip = vec4(
        fullscreen.x * w,
        fullscreen.y * w,
        camera.z_near_m,
        w
    );

    // Now that we have the clip that was passed to gl_Position in draw_deferred, we can
    // invert the relevant transforms to get back to world space.
    // V{clip} = M{proj}*M{scale}*M{view}*V{wrld}
    // M{-view}*M{-scale}*M{-proj}*V{clip} = V{wrld}
    let eye_m = camera.inverse_perspective_m * clip;
    let wrld_m = camera.inverse_view_m * eye_m;
    return wrld_m / 1000.;
}

@fragment
fn world_main(
    @location(0) v_ray_world: vec3<f32>,
    @location(1) v_ndc: vec2<f32>,
    @location(2) v_tc_idx: vec2<f32>,
) -> FragOut {
    let depth_sample = textureLoad(terrain_deferred_depth, vec2<i32>(v_tc_idx), 0);
    let ground_intersect_w_km = fullscreen_to_world_km(v_ndc, depth_sample).xyz;

    let camera_position_w_km = camera.position_km.xyz;
    let sun_direction_w = orrery.sun_direction.xyz;
    let camera_direction_w = normalize(v_ray_world);

    // Sky and stars
    let sky = get_sky_radiance(
        atmosphere,
        transmittance_texture,
        transmittance_sampler,
        scattering_texture,
        scattering_sampler,
        single_mie_scattering_texture,
        single_mie_scattering_sampler,
        camera_position_w_km,
        camera_direction_w,
        sun_direction_w,
    );
    let sky_radiance = sky.radiance;
    let transmittance = sky.transmittance;
    let sun_lums = get_solar_luminance(
        atmosphere.sun_irradiance.xyz,
        atmosphere.sun_angular_radius,
        atmosphere.sun_spectral_radiance_to_luminance
    );

    // Compute ground alpha and radiance.
    let ground_albedo = terrain_ground_diffuse_color(terrain_color_acc_texture, vec2<i32>(v_tc_idx));
    let ground_normal_w = terrain_ground_normal_world(terrain_deferred_texture, terrain_normal_acc_texture, vec2<i32>(v_tc_idx));

    let ground_radiance = radiance_at_point(
        atmosphere,
        transmittance_texture,
        transmittance_sampler,
        irradiance_texture,
        irradiance_sampler,
        scattering_texture,
        scattering_sampler,
        single_mie_scattering_texture,
        single_mie_scattering_sampler,
        ground_intersect_w_km,
        ground_normal_w,
        ground_albedo.rgb,
        sun_direction_w,
        camera_position_w_km,
        camera_direction_w,
    );

    // FIXME: API could just return transparent stars instead of alpha here...
    let star = show_stars(camera_direction_w, hour_angle);

    var radiance = sky_radiance + star.radiance * star.alpha;

    let ray_intersects_sun = dot(camera_direction_w, sun_direction_w) - cos(atmosphere.sun_angular_radius) > 0.;
    if (ray_intersects_sun) {
        radiance = sun_lums;
    }

    if (depth_sample > 0.0) {
        radiance = ground_radiance;
    }

    let color = tone_mapping(radiance, atmosphere.whitepoint.xyz, camera.exposure, camera.tone_gamma);

    return FragOut(vec4(color, 1.));
}

@fragment
fn dbg_deferred_frag_main(
    @location(0) v_ray_world: vec3<f32>,
    @location(1) v_fullscreen: vec2<f32>,
    @location(2) v_tc_idx: vec2<f32>,
) -> FragOut {
    let grat_ll = textureLoad(terrain_deferred_texture, vec2<i32>(v_tc_idx), 0).xy;
    let depth = textureLoad(terrain_deferred_depth, vec2<i32>(v_tc_idx), 0);
    var color = vec4(0., 0., 0., 1.);
    if (depth > 0.) {
        color = vec4(0.5, 0.5, 1., 1.);
    }
    return FragOut(color);
}

@fragment
fn dbg_depth_frag_main(
    @location(0) v_ray_world: vec3<f32>,
    @location(1) v_fullscreen: vec2<f32>,
    @location(2) v_tc_idx: vec2<f32>,
) -> FragOut {
    let zp = textureLoad(terrain_deferred_depth, vec2<i32>(v_tc_idx), 0);

    // Invert the depth projection to get to the real eye space depth.
    var z = 1. / (zp * 0.5);

    // Cast back into 0->1 on an earth scale.
    z = z / EARTH_RADIUS_M;

    // Enhance middle distance depths so we can clearly see what's going on.
    z = sqrt(sqrt(z));

    return FragOut(vec4(z, z, z, 1.));
}

@fragment
fn dbg_color_frag_main(
    @location(0) v_ray_world: vec3<f32>,
    @location(1) v_fullscreen: vec2<f32>,
    @location(2) v_tc_idx: vec2<f32>,
) -> FragOut {
    return FragOut(terrain_ground_diffuse_color(terrain_color_acc_texture, vec2<i32>(v_tc_idx)));
}

@fragment
fn dbg_local_normal_frag_main(
    @location(0) v_ray_world: vec3<f32>,
    @location(1) v_fullscreen: vec2<f32>,
    @location(2) v_tc_idx: vec2<f32>,
) -> FragOut {
    let depth = textureLoad(terrain_deferred_depth, vec2<i32>(v_tc_idx), 0);
    let local_normal = terrain_ground_normal_local(terrain_normal_acc_texture, vec2<i32>(v_tc_idx));
    var color = vec4(0., 0., 0., 1.);
    if (depth > 0.) {
        color = vec4((local_normal + 1.) / 2., 1.);
    }
    return FragOut(color);
}

@fragment
fn dbg_world_normal_frag_main(
    @location(0) v_ray_world: vec3<f32>,
    @location(1) v_fullscreen: vec2<f32>,
    @location(2) v_tc_idx: vec2<f32>,
) -> FragOut {
    let depth = textureLoad(terrain_deferred_depth, vec2<i32>(v_tc_idx), 0);
    let global_normal = terrain_ground_normal_world(terrain_deferred_texture, terrain_normal_acc_texture, vec2<i32>(v_tc_idx));
    var color = vec4(0., 0., 0., 1.);
    if (depth > 0.) {
        color = vec4((global_normal + 1.) / 2., 1.);
    }
    return FragOut(color);
}
