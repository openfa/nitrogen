// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

fn radiance_at_point(
    atmosphere: AtmosphereParameters,
    transmittance_texture: texture_2d<f32>,
    transmittance_sampler: sampler,
    irradiance_texture: texture_2d<f32>,
    irradiance_sampler: sampler,
    scattering_texture: texture_3d<f32>,
    scattering_sampler: sampler,
    single_mie_scattering_texture: texture_3d<f32>,
    single_mie_scattering_sampler: sampler,
    point_w_km: vec3<f32>,
    normal_w: vec3<f32>,
    solid_diffuse_color: vec3<f32>,
    sun_direction_w: vec3<f32>,
    camera_position_w_km: vec3<f32>,
    camera_direction_w: vec3<f32>,
) -> vec3<f32> {
    // Get sun and sky irradiance at the ground point and modulate
    // by the ground albedo.
    let irr = get_sun_and_sky_irradiance(
        atmosphere,
        transmittance_texture,
        irradiance_texture,
        point_w_km,
        normal_w,
        sun_direction_w,
    );
    let sky_irradiance = irr.sky_irradiance;
    let sun_irradiance = irr.sun_irradiance;

    // FIXME: this ground albedo scaling factor is arbitrary and dependent on our source material
    let ground_radiance = solid_diffuse_color * 2. * (
        // Todo: proper shadow maps so we can get sun visibility
        sun_irradiance * get_sun_visibility(point_w_km, sun_direction_w) +
        sky_irradiance * get_sky_visibility(point_w_km)
    );

    // Fade the radiance on the ground by the amount of atmosphere
    // between us and that point and brighten by ambient in-scatter
    // to the camera on that path.
    let rv = get_sky_radiance_to_point(
        atmosphere,
        transmittance_texture,
        transmittance_sampler,
        scattering_texture,
        scattering_sampler,
        single_mie_scattering_texture,
        single_mie_scattering_sampler,
        camera_position_w_km,
        point_w_km,
        camera_direction_w,
        sun_direction_w,
    );
    let total_radiance = ground_radiance * rv.transmittance + rv.in_scattering;

    return total_radiance;
}

fn tone_mapping(radiance: vec3<f32>, whitepoint: vec3<f32>, camera_exposure: f32, tone_gamma: f32) -> vec3<f32> {
    return pow(
        vec3(1.0) - exp(-radiance / whitepoint * MAX_LUMINOUS_EFFICACY * camera_exposure),
        vec3(1.0 / tone_gamma)
    );
}
