// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::Result;
use atmosphere::Atmosphere;
use bevy_ecs::prelude::*;
use camera::{CameraStep, ScreenCamera};
use composite::CompositeStep;
use gui::GuiStep;
use log::trace;
use mantle::{BindingsBuilder, CurrentEncoder, Gpu, GpuStep, LayoutBuilder};
use marker::MarkersStep;
use nitrous::{inject_nitrous_resource, method, NitrousResource};
use orrery::{Orrery, OrreryStep};
use runtime::{Extension, Runtime};
use stars::Stars;
use terrain::{Terrain, TerrainStep, TerrainVertex};

#[derive(Debug)]
enum DebugMode {
    None,
    Deferred,
    Depth,
    Color,
    NormalLocal,
    NormalGlobal,
}

impl DebugMode {
    pub fn from_str(s: &str) -> Self {
        match s {
            "deferred" => Self::Deferred,
            "depth" => Self::Depth,
            "color" => Self::Color,
            "normal_local" => Self::NormalLocal,
            "normal_global" | "normal" => Self::NormalGlobal,
            _ => Self::None,
        }
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum WireframeMode {
    None,
    Patch,
}

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum WorldStep {
    Render,
}

#[derive(NitrousResource)]
pub struct World {
    // Debug and normal pipelines
    composite_bind_group: wgpu::BindGroup,
    composite_pipeline: wgpu::RenderPipeline,
    dbg_deferred_pipeline: wgpu::RenderPipeline,
    dbg_depth_pipeline: wgpu::RenderPipeline,
    dbg_color_pipeline: wgpu::RenderPipeline,
    dbg_normal_local_pipeline: wgpu::RenderPipeline,
    dbg_normal_global_pipeline: wgpu::RenderPipeline,
    wireframe_pipeline: Option<wgpu::RenderPipeline>,

    // Render Mode
    wireframe_mode: WireframeMode,
    debug_mode: DebugMode,
}

// 1) Render tris to an offscreen buffer, collecting (a) grat, (b) norm, (c) depth per pixel
// 2) Clear diffuse color and normal accumulation buffers
// 3) For each layer, for each pixel of the offscreen buffer, accumulate the color and normal
// 4) For each pixel of the accumulator and depth, compute lighting, skybox, stars, etc.

impl Extension for World {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        let world = World::new(
            runtime.resource::<Terrain>(),
            runtime.resource::<Atmosphere>(),
            runtime.resource::<Orrery>(),
            runtime.resource::<Stars>(),
            runtime.resource::<Gpu>(),
        )?;
        runtime.inject_resource(world)?;
        runtime.add_frame_system(
            Self::sys_render_world
                .in_set(WorldStep::Render)
                .after(CameraStep::HandleDisplayChange)
                .after(CameraStep::UploadToGpu)
                .after(OrreryStep::UploadToGpu)
                .after(TerrainStep::AccumulateNormalsAndColor)
                .after(GpuStep::CreateCommandEncoder)
                .before(MarkersStep::Render)
                .before(CompositeStep::Render)
                .before(GpuStep::SubmitCommands)
                // Note: pin the encode order for consistency
                .before(GuiStep::EndFrame)
                .before(MarkersStep::UploadGeometry),
        );
        Ok(())
    }
}

#[inject_nitrous_resource]
impl World {
    pub fn new(
        terrain: &Terrain,
        atmosphere: &Atmosphere,
        orrery: &Orrery,
        stars: &Stars,
        gpu: &Gpu,
    ) -> Result<Self> {
        trace!("WorldRenderPass::new");

        let shader = gpu.compile_shader(
            "fullscreen-shared.wgsl",
            include_str!("../target/fullscreen-shared.wgsl"),
        );
        let combined_bind_group_layout = LayoutBuilder::default()
            .with_label("world-composite-bind-group-layout")
            .with_layout::<Stars>()
            .with_layout::<Atmosphere>()
            .with_layout::<Orrery>()
            .build(gpu);
        let composite_bind_group = BindingsBuilder::new(&combined_bind_group_layout)
            .with_label("world-composite-bind-group")
            .with_bindings(stars)
            .with_bindings(atmosphere)
            .with_bindings(orrery)
            .build(gpu);
        let fullscreen_layout =
            gpu.device()
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: Some("world-deferred-pipeline-layout"),
                    push_constant_ranges: &[],
                    bind_group_layouts: &[
                        // These are all uploaded once, or updated in place
                        &combined_bind_group_layout,
                        // Terrain needs to regen bind groups on window changes
                        terrain.gbuffer_bind_group_layout(),
                        // Camera may change multiple times per frame
                        &ScreenCamera::info_bind_group_layout(gpu),
                    ],
                });

        let composite_pipeline = Self::make_common_pipeline(
            "world-composite-pipeline",
            gpu.device(),
            &fullscreen_layout,
            &shader,
            "world_main",
        );
        let dbg_deferred_pipeline = Self::make_common_pipeline(
            "world-dbg-pipeline-deferred",
            gpu.device(),
            &fullscreen_layout,
            &shader,
            "dbg_deferred_frag_main",
        );
        let dbg_depth_pipeline = Self::make_common_pipeline(
            "world-dbg-pipeline-depth",
            gpu.device(),
            &fullscreen_layout,
            &shader,
            "dbg_depth_frag_main",
        );
        let dbg_color_pipeline = Self::make_common_pipeline(
            "world-dbg-pipeline-color",
            gpu.device(),
            &fullscreen_layout,
            &shader,
            "dbg_color_frag_main",
        );
        let dbg_normal_local_pipeline = Self::make_common_pipeline(
            "world-dbg-pipeline-normals-local",
            gpu.device(),
            &fullscreen_layout,
            &shader,
            "dbg_local_normal_frag_main",
        );
        let dbg_normal_global_pipeline = Self::make_common_pipeline(
            "world-dbg-pipeline-normals-global",
            gpu.device(),
            &fullscreen_layout,
            &shader,
            "dbg_world_normal_frag_main",
        );

        let mut wireframe_pipeline = None;
        if gpu.has_polygon_mode_line() {
            let wireframe_shader = gpu.compile_shader(
                "dbg-wireframe.wgsl",
                include_str!("../target/dbg-wireframe.wgsl"),
            );
            wireframe_pipeline = Some(gpu.device().create_render_pipeline(
                &wgpu::RenderPipelineDescriptor {
                    label: Some("world-wireframe-pipeline"),
                    layout: Some(&gpu.device().create_pipeline_layout(
                        &wgpu::PipelineLayoutDescriptor {
                            label: Some("world-wireframe-pipeline-layout"),
                            push_constant_ranges: &[],
                            bind_group_layouts: &[&ScreenCamera::info_bind_group_layout(gpu)],
                        },
                    )),
                    vertex: wgpu::VertexState {
                        module: &wireframe_shader,
                        entry_point: "main_vert",
                        buffers: &[TerrainVertex::descriptor()],
                        compilation_options: wgpu::PipelineCompilationOptions {
                            constants: &Default::default(),
                            zero_initialize_workgroup_memory: false,
                        },
                    },
                    fragment: Some(wgpu::FragmentState {
                        module: &wireframe_shader,
                        entry_point: "main_frag",
                        targets: &[Some(wgpu::ColorTargetState {
                            format: ScreenCamera::COLOR_FORMAT,
                            blend: None,
                            write_mask: wgpu::ColorWrites::ALL,
                        })],
                        compilation_options: wgpu::PipelineCompilationOptions {
                            constants: &Default::default(),
                            zero_initialize_workgroup_memory: false,
                        },
                    }),
                    primitive: wgpu::PrimitiveState {
                        topology: wgpu::PrimitiveTopology::LineList,
                        strip_index_format: None,
                        front_face: wgpu::FrontFace::Cw,
                        cull_mode: None,
                        unclipped_depth: false,
                        polygon_mode: wgpu::PolygonMode::Line,
                        conservative: false,
                    },
                    depth_stencil: Some(wgpu::DepthStencilState {
                        format: ScreenCamera::DEPTH_FORMAT,
                        depth_write_enabled: false,
                        depth_compare: wgpu::CompareFunction::Always,
                        stencil: wgpu::StencilState {
                            front: wgpu::StencilFaceState::IGNORE,
                            back: wgpu::StencilFaceState::IGNORE,
                            read_mask: 0,
                            write_mask: 0,
                        },
                        bias: wgpu::DepthBiasState {
                            constant: 0,
                            slope_scale: 0.0,
                            clamp: 0.0,
                        },
                    }),
                    multisample: wgpu::MultisampleState {
                        count: 1,
                        mask: !0,
                        alpha_to_coverage_enabled: false,
                    },
                    multiview: None,
                },
            ));
        };

        Ok(Self {
            composite_bind_group,
            composite_pipeline,
            dbg_deferred_pipeline,
            dbg_depth_pipeline,
            dbg_color_pipeline,
            dbg_normal_local_pipeline,
            dbg_normal_global_pipeline,
            wireframe_pipeline,

            wireframe_mode: WireframeMode::None,
            debug_mode: DebugMode::None,
        })
    }

    pub fn make_common_pipeline(
        name: &'static str,
        device: &wgpu::Device,
        layout: &wgpu::PipelineLayout,
        shader: &wgpu::ShaderModule,
        frag_entry_point: &str,
    ) -> wgpu::RenderPipeline {
        device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some(name),
            layout: Some(layout),
            vertex: wgpu::VertexState {
                module: shader,
                entry_point: "vert_common_main",
                buffers: &[],
                compilation_options: wgpu::PipelineCompilationOptions {
                    constants: &Default::default(),
                    zero_initialize_workgroup_memory: false,
                },
            },
            fragment: Some(wgpu::FragmentState {
                module: shader,
                entry_point: frag_entry_point,
                targets: &[Some(wgpu::ColorTargetState {
                    format: ScreenCamera::COLOR_FORMAT,
                    blend: None,
                    write_mask: wgpu::ColorWrites::ALL,
                })],
                compilation_options: wgpu::PipelineCompilationOptions {
                    constants: &Default::default(),
                    zero_initialize_workgroup_memory: false,
                },
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: Some(wgpu::Face::Back),
                unclipped_depth: true,
                polygon_mode: wgpu::PolygonMode::Fill,
                conservative: false,
            },
            depth_stencil: Some(wgpu::DepthStencilState {
                format: ScreenCamera::DEPTH_FORMAT,
                depth_write_enabled: true,
                // depth_compare: wgpu::CompareFunction::GreaterEqual,
                depth_compare: wgpu::CompareFunction::Always,
                stencil: wgpu::StencilState {
                    front: wgpu::StencilFaceState::IGNORE,
                    back: wgpu::StencilFaceState::IGNORE,
                    read_mask: 0,
                    write_mask: 0,
                },
                bias: wgpu::DepthBiasState {
                    constant: 0,
                    slope_scale: 0.0,
                    clamp: 0.0,
                },
            }),
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            multiview: None,
        })
    }

    #[method]
    pub fn change_wireframe_mode(&mut self) {
        if self.wireframe_pipeline.is_some() {
            self.wireframe_mode = match self.wireframe_mode {
                WireframeMode::None => WireframeMode::Patch,
                WireframeMode::Patch => WireframeMode::None,
            };
        }
        println!("Wireframe Mode is now: {:?}", self.wireframe_mode);
    }

    #[method]
    pub fn change_debug_mode(&mut self) {
        self.debug_mode = match self.debug_mode {
            DebugMode::None => DebugMode::Deferred,
            DebugMode::Deferred => DebugMode::Depth,
            DebugMode::Depth => DebugMode::Color,
            DebugMode::Color => DebugMode::NormalLocal,
            DebugMode::NormalLocal => DebugMode::NormalGlobal,
            DebugMode::NormalGlobal => DebugMode::None,
        };
        println!("Debug Mode is now: {:?}", self.debug_mode);
    }

    #[method]
    pub fn set_debug_mode(&mut self, value: &str) {
        self.debug_mode = DebugMode::from_str(value);
        println!("Debug Mode is now: {:?}", self.debug_mode);
    }

    fn sys_render_world(
        world: Res<World>,
        terrain: Res<Terrain>,
        gpu: Res<Gpu>,
        mut camera: ResMut<ScreenCamera>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) {
        camera.ensure_layer("color", ScreenCamera::COLOR_FORMAT, &gpu);
        camera.ensure_layer("depth", ScreenCamera::DEPTH_FORMAT, &gpu);

        if let Some(encoder) = maybe_encoder.get_encoder_mut() {
            let rpass = camera.begin_render_pass("color", Some("depth"), true, encoder);
            let _rpass = world.render_world(rpass, &camera, &terrain);
        }
    }

    pub fn render_world<'a>(
        &'a self,
        mut rpass: wgpu::RenderPass<'a>,
        camera: &'a ScreenCamera,
        terrain: &'a Terrain,
    ) -> wgpu::RenderPass<'a> {
        match self.debug_mode {
            DebugMode::None => rpass.set_pipeline(&self.composite_pipeline),
            DebugMode::Deferred => rpass.set_pipeline(&self.dbg_deferred_pipeline),
            DebugMode::Depth => rpass.set_pipeline(&self.dbg_depth_pipeline),
            DebugMode::Color => rpass.set_pipeline(&self.dbg_color_pipeline),
            DebugMode::NormalLocal => rpass.set_pipeline(&self.dbg_normal_local_pipeline),
            DebugMode::NormalGlobal => rpass.set_pipeline(&self.dbg_normal_global_pipeline),
        }
        rpass.set_bind_group(0, &self.composite_bind_group, &[]);
        rpass.set_bind_group(1, camera.bind_group(Terrain::GBUFFER_BINDING_NAME), &[]);
        rpass.set_bind_group(2, camera.info_bind_group(), &[]);
        rpass.draw(0..3, 0..1);

        if self.wireframe_mode == WireframeMode::Patch {
            if let Some(wireframe_pipeline) = self.wireframe_pipeline.as_ref() {
                rpass.set_pipeline(wireframe_pipeline);
                rpass.set_bind_group(0, camera.info_bind_group(), &[]);
                rpass.set_vertex_buffer(0, terrain.vertex_buffer());
                for i in 0..terrain.num_patches() {
                    let winding = terrain.patch_winding(i);
                    let base_vertex = terrain.patch_vertex_buffer_offset(i);
                    rpass.set_index_buffer(
                        terrain.wireframe_index_buffer(winding),
                        wgpu::IndexFormat::Uint32,
                    );
                    rpass.draw_indexed(terrain.wireframe_index_range(winding), base_vertex, 0..1);
                }
            }
        }

        rpass
    }
}
