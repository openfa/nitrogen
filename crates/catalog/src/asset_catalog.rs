// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
#[cfg(not(target_arch = "wasm32"))]
use crate::directory_provider::DirectoryProvider;
use crate::{
    asset_provider::{AssetProvider, FileInfo, Search},
    CatalogOpts,
};
use anyhow::{anyhow, bail, ensure, Result};
use nitrous::{inject_nitrous_resource, method, NitrousResource};
use runtime::{Extension, Runtime};

pub trait FileSystem {
    fn search(&self, search: Search) -> Result<Vec<FileInfo>>;
    fn lookup(&self, name: &'_ str) -> Result<FileInfo>;
}

#[derive(Debug)]
struct ProviderEntry {
    priority: i64,
    name: String,
    provider: Box<dyn AssetProvider>,
}

/// A collection of asset providers.
#[derive(Debug)]
pub struct AssetCollection {
    priority: i64,
    name: String,
    providers: Vec<ProviderEntry>,
}

impl AssetCollection {
    fn new(priority: i64, name: &str) -> Self {
        Self {
            priority,
            name: name.to_owned(),
            providers: Vec::new(),
        }
    }

    pub fn add_provider<T: AssetProvider + Send + Sync + 'static>(
        &mut self,
        name: &str,
        priority: i64,
        mut provider: T,
    ) -> Result<()> {
        provider.set_context(
            (self.priority, self.name.clone()),
            (priority, name.to_owned()),
        );
        ensure!(
            self.get_provider(name).is_none(),
            "attempt to add duplicate provider '{name}'",
        );
        self.providers.push(ProviderEntry {
            priority,
            name: name.to_owned(),
            provider: Box::new(provider) as Box<dyn AssetProvider>,
        });
        self.providers.sort_by_key(|c| c.priority);
        Ok(())
    }

    /// Return the named provider.
    pub fn get_provider(&self, name: &'_ str) -> Option<&dyn AssetProvider> {
        self.providers
            .iter()
            .find(|entry| entry.name == name)
            .map(|entry| entry.provider.as_ref())
    }

    /// Return the named provider mutably.
    pub fn get_provider_mut(&mut self, name: &'_ str) -> Option<&mut dyn AssetProvider> {
        self.providers
            .iter_mut()
            .find(|entry| entry.name == name)
            .map(|entry| entry.provider.as_mut())
    }

    /// Return the named provider.
    pub fn provider(&self, name: &'_ str) -> Result<&dyn AssetProvider> {
        self.get_provider(name)
            .ok_or_else(|| anyhow!("no such provider: {name}"))
    }

    /// Return the named provider mutably.
    pub fn provider_mut(&mut self, name: &'_ str) -> Result<&mut dyn AssetProvider> {
        self.get_provider_mut(name)
            .ok_or_else(|| anyhow!("no such provider: {name}"))
    }

    pub fn provider_priority(&self, name: &str) -> Result<i64> {
        self.providers
            .iter()
            .find(|entry| entry.name == name)
            .map(|entry| entry.priority)
            .ok_or_else(|| anyhow!("no such provider: {name}"))
    }

    pub fn all_providers(&self) -> impl Iterator<Item = (&str, i64, &dyn AssetProvider)> {
        self.providers
            .iter()
            .map(|entry| (entry.name.as_ref(), entry.priority, entry.provider.as_ref()))
    }
}

impl FileSystem for AssetCollection {
    fn search(&self, mut search: Search) -> Result<Vec<FileInfo>> {
        // Note: pass through without must_match because that should only apply at the top level.
        let must_match = search.reset_must_match();

        let mut out = Vec::new();
        for entry in &self.providers {
            if let Some(name) = search.provider_name() {
                if name != entry.name {
                    continue;
                }
            }
            out.extend(entry.provider.search(&search)?.into_iter());
        }

        if must_match && out.is_empty() {
            bail!("Search::must_match and no files!")
        }

        out.sort();

        Ok(out)
    }

    fn lookup(&self, name: &'_ str) -> Result<FileInfo> {
        for entry in &self.providers {
            if let Some(info) = entry.provider.lookup(name) {
                return Ok(info);
            }
        }
        bail!("no such file: {name}")
    }
}

#[derive(Debug)]
struct CollectionEntry {
    priority: i64,
    name: String,
    collection: AssetCollection,
}

/// Map files from various media into one or more asset collections.
#[derive(Debug, Default, NitrousResource)]
#[resource(name = "catalog")]
pub struct AssetCatalog {
    collections: Vec<CollectionEntry>,
}

impl Extension for AssetCatalog {
    type Opts = CatalogOpts;

    fn init(runtime: &mut Runtime, _opts: CatalogOpts) -> Result<()> {
        // FIXME: re-organize this; I think we can make it denser
        #[cfg(not(target_arch = "wasm32"))]
        let catalog = {
            let mut catalog = Self::default();
            catalog.add_collection(Self::BUILTIN_COLLECTION, i64::MAX)?;
            for (i, d) in _opts.extra_paths.iter().enumerate() {
                catalog.collection_mut("builtin")?.add_provider(
                    "",
                    1000i64 + i as i64,
                    DirectoryProvider::new(d)?,
                )?;
            }
            catalog
        };
        #[cfg(target_arch = "wasm32")]
        let catalog = {
            let mut catalog = Self::default();
            catalog.add_collection(Self::BUILTIN_COLLECTION, i64::MAX)?;
            catalog
        };
        runtime.inject_resource(catalog)?;
        Ok(())
    }
}

#[inject_nitrous_resource]
impl AssetCatalog {
    pub const BUILTIN_COLLECTION: &'static str = "builtin";

    #[method]
    fn list_collections(&self) -> Result<String> {
        let mut out = String::new();
        for collection in &self.collections {
            out += &format!("{} prio:{}\n", collection.name, collection.priority);
            for provider in &collection.collection.providers {
                out += &format!("    {} prio:{}\n", provider.name, provider.priority);
            }
        }
        Ok(out)
    }

    pub fn add_collection(&mut self, name: &str, priority: i64) -> Result<()> {
        ensure!(
            self.get_collection(name).is_none(),
            "attempt to add duplicate collection: {name}"
        );
        self.collections.push(CollectionEntry {
            priority,
            name: name.to_owned(),
            collection: AssetCollection::new(priority, name),
        });
        self.collections.sort_by_key(|c| c.priority);
        Ok(())
    }

    pub fn get_collection(&self, name: &'_ str) -> Option<&AssetCollection> {
        for entry in &self.collections {
            if entry.name == name {
                return Some(&entry.collection);
            }
        }
        None
    }

    pub fn get_collection_mut(&mut self, name: &'_ str) -> Option<&mut AssetCollection> {
        for entry in &mut self.collections {
            if entry.name == name {
                return Some(&mut entry.collection);
            }
        }
        None
    }

    pub fn collection(&self, name: &'_ str) -> Result<&AssetCollection> {
        self.get_collection(name)
            .ok_or_else(|| anyhow!("no such collection: {name}"))
    }

    pub fn collection_mut(&mut self, name: &'_ str) -> Result<&mut AssetCollection> {
        self.get_collection_mut(name)
            .ok_or_else(|| anyhow!("no such collection: {name}"))
    }

    pub fn update_collection_priority(&mut self, name: &'_ str, priority: i64) -> Result<()> {
        {
            let entry = self
                .collections
                .iter_mut()
                .find(|entry| entry.name == name)
                .ok_or_else(|| anyhow!("no such collection: {name}"))?;
            entry.priority = priority;
        }
        self.collections.sort_by_key(|entry| entry.priority);
        Ok(())
    }

    pub fn collection_priority(&self, name: &str) -> Result<i64> {
        self.collections
            .iter()
            .find(|entry| entry.name == name)
            .map(|entry| entry.priority)
            .ok_or_else(|| anyhow!("no such collection {name}"))
    }

    pub fn primary_collection_name(&self) -> &str {
        &self.collections[0].name
    }

    pub fn primary(&self) -> &AssetCollection {
        &self.collections[0].collection
    }

    pub fn primary_mut(&mut self) -> &mut AssetCollection {
        &mut self.collections[0].collection
    }
}

impl FileSystem for AssetCatalog {
    fn search(&self, mut search: Search) -> Result<Vec<FileInfo>> {
        // Note: pass through without must_match because that should only apply at the top level.
        let must_match = search.reset_must_match();

        let mut out = Vec::new();
        for entry in &self.collections {
            if !search.collection_names().is_empty()
                && !search.collection_names().contains(&entry.name)
            {
                continue;
            }
            out.extend(entry.collection.search(search.clone())?.into_iter());
        }

        if must_match && out.is_empty() {
            bail!("Search::must_match and no files!")
        }

        Ok(out)
    }

    fn lookup(&self, name: &'_ str) -> Result<FileInfo> {
        for entry in &self.collections {
            if let Ok(info) = entry.collection.lookup(name) {
                return Ok(info);
            }
        }
        bail!("no such file: {name}")
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::directory_provider::DirectoryProvider;
    use std::path::Path;

    #[test]
    fn test_provider_masking() -> Result<()> {
        let mut cat = AssetCatalog::default();

        // See if we can get b/a.txt first.
        cat.add_collection("test", 1)?;
        cat.collection_mut("test")?.add_provider(
            "b",
            1,
            DirectoryProvider::new(Path::new("masking_test_data/b"))?,
        )?;
        let meta = cat.collection("test")?.lookup("a.txt")?;
        assert_eq!(b"world".as_slice(), meta.data()?.as_ref());
        assert_eq!(1, cat.collection("test")?.search(Search::default())?.len());

        // Add a provider that masks in the test collection.
        cat.collection_mut("test")?.add_provider(
            "a",
            0,
            DirectoryProvider::new(Path::new("masking_test_data/a"))?,
        )?;
        let meta = cat.collection("test")?.lookup("a.txt")?;
        assert_eq!(b"hello".as_slice(), meta.data()?.as_ref());
        assert_eq!(2, cat.collection("test")?.search(Search::default())?.len());

        Ok(())
    }

    #[test]
    fn test_collection_masking() -> Result<()> {
        let mut cat = AssetCatalog::default();
        cat.add_collection("b", 1)?;
        cat.collection_mut("b")?.add_provider(
            "test",
            0,
            DirectoryProvider::new(Path::new("masking_test_data/b"))?,
        )?;
        let meta = cat.lookup("a.txt")?;
        assert_eq!(b"world".as_slice(), meta.data()?.as_ref());
        assert_eq!(1, cat.search(Search::default())?.len());

        cat.add_collection("a", 0)?;
        cat.collection_mut("a")?.add_provider(
            "test",
            0,
            DirectoryProvider::new(Path::new("masking_test_data/a"))?,
        )?;
        let meta = cat.lookup("a.txt")?;
        assert_eq!(b"hello".as_slice(), meta.data()?.as_ref());
        assert_eq!(2, cat.search(Search::default())?.len());

        Ok(())
    }
}
