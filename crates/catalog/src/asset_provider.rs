// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::Result;
use glob::{MatchOptions, Pattern};
use itertools::Itertools;
use smallvec::SmallVec;
use std::{
    borrow::Cow,
    cmp::Ordering,
    fmt::Debug,
    fmt::{Display, Formatter},
};

#[derive(Clone, Copy, Debug, Default)]
pub enum Order {
    #[default]
    None,
    Asc,
    Desc,
}

/// Expresses how to search a FileSystem.
#[derive(Clone, Debug, Default)]
pub struct Search {
    filters: SmallVec<[Filter; 2]>,
    in_collections: SmallVec<[String; 2]>,
    in_provider: Option<String>,
    sort_order: Order,
    must_match: bool,
}

impl Search {
    pub fn for_glob<S: ToString>(glob: S) -> Self {
        Self::default().with_glob(glob)
    }

    pub fn for_extension<S: ToString>(ext: S) -> Self {
        Self::default().with_extension(ext)
    }

    pub fn for_input(ext: &str, input: &str) -> Self {
        Self::for_extension(ext).maybe_with_collection(input)
    }

    pub fn for_any_input(input: &str) -> Self {
        Self::default().maybe_with_collection(input)
    }

    fn maybe_with_collection(self, input: &str) -> Self {
        if let Some((collection, files_glob)) = input.split_once(':') {
            self.with_glob(files_glob).in_collection(collection)
        } else {
            self.with_glob(input)
        }
    }

    /// Require that matched files match the given glob.
    pub fn with_glob<S: ToString>(mut self, glob: S) -> Self {
        self.filters.push(Filter::new_glob(glob));
        self
    }

    /// Require that matched files have the given extension. This is
    /// currently implemented with a case-insensitive ends_with.
    pub fn with_extension<S: ToString>(mut self, ext: S) -> Self {
        self.filters.push(Filter::new_extension(ext));
        self
    }

    /// Require that matched files are from the specified collection.
    pub fn in_collection<S: ToString>(mut self, name: S) -> Self {
        self.in_collections.push(name.to_string());
        self
    }

    /// Require that matched files are from the specified collections.
    pub fn in_collections<S: ToString, const N: usize>(mut self, names: [S; N]) -> Self {
        self.in_collections
            .extend(names.iter().map(|s| s.to_string()));
        self
    }

    /// Require that matched files are from the specified provider.
    pub fn in_provider<S: ToString>(mut self, name: S) -> Self {
        self.in_provider = Some(name.to_string());
        self
    }

    pub fn sort(mut self, ord: Order) -> Self {
        self.sort_order = ord;
        self
    }

    /// If there are no matching files, throw an error instead of returning an
    /// empty results-set. This is useful for tests to ensure that we don't pass
    /// because the test data got lost.
    pub fn must_match(mut self) -> Self {
        self.must_match = true;
        self
    }

    /// Reset the must_match requirement and return the prior value.
    pub fn reset_must_match(&mut self) -> bool {
        let out = self.must_match;
        self.must_match = false;
        out
    }

    /// Required collection
    pub fn collection_names(&self) -> &SmallVec<[String; 2]> {
        &self.in_collections
    }

    /// Required provider
    pub fn provider_name(&self) -> Option<&str> {
        self.in_provider.as_ref().map(|s| s.as_ref())
    }

    /// Required sort order
    pub fn sort_order(&self) -> Order {
        self.sort_order
    }

    pub fn filter_iterator<'query, 'provider>(
        &'query self,
        mut iter: Box<dyn Iterator<Item = &'provider str> + 'provider>,
    ) -> Result<Box<dyn Iterator<Item = &'provider str> + 'provider>> {
        for filter in &self.filters {
            match &filter {
                Filter::Ext(ext) => {
                    let pattern = ".".to_owned() + ext;
                    let pattern_upper = pattern.to_uppercase();
                    let pattern_lower = pattern.to_lowercase();
                    iter = Box::new(iter.filter(move |s: &&str| {
                        s.ends_with(&pattern_upper) || s.ends_with(&pattern_lower)
                    }));
                }
                Filter::Glob(glob) => {
                    let opts = MatchOptions {
                        case_sensitive: false,
                        require_literal_leading_dot: false,
                        require_literal_separator: true,
                    };
                    let pattern = Pattern::new(glob)?;
                    iter = Box::new(iter.filter(move |s: &&str| pattern.matches_with(s, opts)));
                }
            }
        }
        iter = match self.sort_order {
            Order::None => iter,
            Order::Asc => Box::new(iter.sorted()),
            Order::Desc => Box::new(iter.sorted().rev()),
        };
        Ok(iter)
    }
}

/// A filter to apply in a search.
#[derive(Clone, Debug)]
enum Filter {
    Glob(String),
    Ext(String),
}

impl Filter {
    fn new_extension<S: ToString>(ext: S) -> Self {
        let s = ext.to_string();
        debug_assert!(!s.starts_with('.'));
        Self::Ext(s)
    }

    fn new_glob<S: ToString>(glob: S) -> Self {
        Self::Glob(glob.to_string())
    }
}

#[derive(Clone, Copy)]
pub struct FileInfo<'a> {
    collection_info: (i64, &'a str),
    provider_info: (i64, &'a str),
    provider: &'a dyn AssetProvider,
    name: &'a str,
}

impl PartialEq for FileInfo<'_> {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
            && self.provider_info == other.provider_info
            && self.collection_info == other.collection_info
    }
}

impl Eq for FileInfo<'_> {}

impl PartialOrd for FileInfo<'_> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for FileInfo<'_> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.collection_info.cmp(&other.collection_info).then(
            self.provider_info
                .cmp(&other.provider_info)
                .then(self.name.cmp(other.name)),
        )
    }
}

impl<'a> FileInfo<'a> {
    pub fn new(
        provider: &'a dyn AssetProvider,
        name: &'a str,
        collection_context: &'a (i64, String),
        provider_context: &'a (i64, String),
    ) -> Self {
        Self {
            collection_info: (collection_context.0, collection_context.1.as_ref()),
            provider_info: (provider_context.0, provider_context.1.as_ref()),
            provider,
            name,
        }
    }

    pub fn name(&self) -> &str {
        self.name
    }

    pub fn data(&self) -> Result<Cow<[u8]>> {
        self.provider.read(self.name)
    }

    pub fn collection_name(&self) -> &str {
        self.collection_info.1
    }

    pub fn provider_name(&self) -> &str {
        self.collection_info.1
    }

    pub fn stat(&self) -> Result<FileStat> {
        self.provider.stat(self.name)
    }
}

impl Display for FileInfo<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let s = format!(
            "{}:{}:{}",
            self.collection_info.1, self.provider_info.1, self.name,
        );
        Display::fmt(&s, f)
    }
}

// Extra information about a file that we can look up.
#[derive(Debug)]
pub struct FileStat {
    compression_type: &'static str,
    compressed_size: u64,
    size: u64,
}

impl FileStat {
    pub fn new(compression_type: &'static str, compressed_size: u64, size: u64) -> Self {
        Self {
            compression_type,
            compressed_size,
            size,
        }
    }

    pub fn compression_type(&self) -> &'static str {
        self.compression_type
    }

    pub fn compressed_size(&self) -> u64 {
        self.compressed_size
    }

    pub fn size(&self) -> u64 {
        self.size
    }
}

/// Implement this to plug in a new kind of asset source to the catalog.
pub trait AssetProvider: Debug + Send + Sync + 'static {
    fn set_context(&mut self, collection: (i64, String), provider: (i64, String));
    fn search(&self, search: &Search) -> Result<Vec<FileInfo>>;
    fn lookup(&self, name: &'_ str) -> Option<FileInfo>;
    fn stat(&self, name: &str) -> Result<FileStat>;
    fn read(&self, name: &str) -> Result<Cow<[u8]>>;
    fn write(&mut self, name: &str, data: &[u8]) -> Result<()>;
    fn remove(&mut self, name: &str) -> Result<()>;
}
