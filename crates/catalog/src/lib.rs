use std::path::PathBuf;

// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
mod asset_catalog;
mod asset_provider;
#[cfg(not(target_arch = "wasm32"))]
mod directory_provider;

use structopt::StructOpt;

pub use crate::{
    asset_catalog::{AssetCatalog, AssetCollection, FileSystem},
    asset_provider::{AssetProvider, FileInfo, FileStat, Order, Search},
};

#[cfg(not(target_arch = "wasm32"))]
pub use crate::directory_provider::DirectoryProvider;

#[derive(Clone, Debug, Default, StructOpt)]
pub struct CatalogOpts {
    /// Extra directories to make available
    #[allow(unused)]
    #[structopt(short = "-l", long)]
    extra_paths: Vec<PathBuf>,
}

impl CatalogOpts {
    pub fn from_extra_paths(extra_paths: Vec<PathBuf>) -> Self {
        Self { extra_paths }
    }
}
