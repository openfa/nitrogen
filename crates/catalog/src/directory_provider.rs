// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::asset_provider::{AssetProvider, FileInfo, FileStat, Search};
use anyhow::{ensure, Result};
use std::{
    borrow::Cow,
    collections::HashSet,
    fs,
    path::{Path, PathBuf},
};

/// An asset provider that takes resources from the given directory.
#[derive(Debug)]
pub struct DirectoryProvider {
    collection_context: (i64, String),
    provider_context: (i64, String),

    path: PathBuf,
    index: HashSet<String>,
}

impl DirectoryProvider {
    pub fn new(path: &Path) -> Result<Self> {
        let path = PathBuf::from(path).canonicalize()?;
        let mut index = HashSet::new();
        for entry in fs::read_dir(&path)? {
            let entry = entry?;
            if !entry.file_type()?.is_file() {
                continue;
            }
            if let Some(raw_name) = entry.path().file_name() {
                let name = raw_name.to_string_lossy().to_string();
                index.insert(name);
            }
        }
        Ok(Self {
            collection_context: (0, "".to_owned()),
            provider_context: (0, "".to_owned()),
            path,
            index,
        })
    }
}

impl AssetProvider for DirectoryProvider {
    fn set_context(&mut self, collection: (i64, String), provider: (i64, String)) {
        self.collection_context = collection;
        self.provider_context = provider;
    }

    fn search(&self, search: &Search) -> Result<Vec<FileInfo>> {
        Ok(search
            .filter_iterator(Box::new(self.index.iter().map(|a| a.as_ref())))?
            .map(|name| FileInfo::new(self, name, &self.collection_context, &self.provider_context))
            .collect::<Vec<_>>())
    }

    fn lookup(&self, name: &'_ str) -> Option<FileInfo> {
        self.index
            .get(name)
            .map(|key| FileInfo::new(self, key, &self.collection_context, &self.provider_context))
    }

    fn stat(&self, name: &str) -> Result<FileStat> {
        ensure!(self.index.contains(name), "file not found: {name}");
        let mut path = self.path.clone();
        path.push(name);
        let buf = fs::metadata(path)?;
        Ok(FileStat::new("none", buf.len(), buf.len()))
    }

    fn read(&self, name: &str) -> Result<Cow<[u8]>> {
        ensure!(self.index.contains(name), "file not found: {name}");
        let mut global_path = self.path.clone();
        global_path.push(name);
        Ok(Cow::from(fs::read(global_path)?))
    }

    fn write(&mut self, name: &str, data: &[u8]) -> Result<()> {
        let mut path = self.path.clone();
        path.push(name);
        fs::write(path, data)?;
        Ok(())
    }

    fn remove(&mut self, name: &str) -> Result<()> {
        let mut path = self.path.clone();
        path.push(name);
        fs::remove_file(path)?;
        Ok(())
    }
}
