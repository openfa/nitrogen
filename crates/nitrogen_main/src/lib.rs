// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
mod demo;
mod options;

// use crate::demo::Demo;
pub use crate::options::NitrogenOpts;

use crate::demo::Demo;
use animate::Timeline;
use anyhow::Result;
use arcball::ArcBallController;
use atmosphere::Atmosphere;
use camera::{ScreenCamera, ScreenCameraController};
use catalog::AssetCatalog;
use composite::Composite;
use event_mapper::EventMapper;
use geodb::GeoDb;
use gui::Gui;
use mantle::Core;
use marker::Markers;
use orrery::Orrery;
use phase::Frame;
use physics::Physics;
use runtime::{Runtime, StartupOpts, StdPaths, StdPathsOpts};
use spog::{Dashboard, Terminal};
use stars::Stars;
use terrain::{Terrain, TerrainOpts};
use tracelog::TraceLog;
use vehicle::{
    AirbrakeEffector, BayEffector, FlapsEffector, GearEffector, HookEffector, PitchInceptor,
    PowerSystem, RollInceptor, ThrustVectorPitchControl, ThrustVectorPitchEffector,
    ThrustVectorYawControl, ThrustVectorYawEffector, YawInceptor,
};
use world::World;

const PRELUDE: &str = r#"
// Default camera bindings
bindings.bind("+mouseLeft", "@camera.arcball.pan_view($pressed)");
bindings.bind("+mouseRight", "@camera.arcball.move_view($pressed)");
bindings.bind("mouseMotion", "@camera.arcball.handle_mousemotion($dx, $dy)");
bindings.bind("mouseWheel", "@camera.arcball.handle_mousewheel($vertical_delta)");
bindings.bind("+Shift+Up", "@camera.arcball.target_up_fast($pressed)");
bindings.bind("+Shift+Down", "@camera.arcball.target_down_fast($pressed)");
bindings.bind("+Up", "@camera.arcball.target_up($pressed)");
bindings.bind("+Down", "@camera.arcball.target_down($pressed)");
bindings.bind("LBracket", "globals.decrease_gamma()");
bindings.bind("RBracket", "globals.increase_gamma()");

bindings.bind("+PageUp", "camera.increase_fov($pressed)");
bindings.bind("+PageDown", "camera.decrease_fov($pressed)");
bindings.bind("Shift+LBracket", "camera.decrease_exposure()");
bindings.bind("Shift+RBracket", "camera.increase_exposure()");

// Debugging bindings
bindings.bind("F3", "dashboard.toggle()");
bindings.bind("F2", "foobar.call_missing()");
bindings.bind("+mouseMiddle", "orrery.move_sun($pressed)");
bindings.bind("mouseMotion", "orrery.handle_mousemove($dx)");
bindings.bind("Space", "demo.spawn()");

// By default start out pointing at Mt Everest.
// let $location <- "Everest";
// @camera.arcball.set_target(@camera.arcball.notable_location($location));
// @camera.arcball.set_bearing(@camera.arcball.bearing_for_notable_location($location));
// @camera.arcball.set_pitch(@camera.arcball.pitch_for_notable_location($location));
// @camera.arcball.set_distance(@camera.arcball.distance_for_notable_location($location));
// orrery.set_date_time(1964, 2, 24, 0, 0, 0);
"#;

pub async fn shared_main(opts: NitrogenOpts, window_title: &str) -> Result<()> {
    Core::boot(opts.display_opts.clone(), opts, window_title, setup_runtime).await?;
    Ok(())
}

fn setup_runtime(runtime: &mut Runtime, opt: NitrogenOpts) -> Result<()> {
    runtime
        .load_extension_with::<TraceLog>(opt.tracelog_opts)?
        .load_extension_with::<StdPaths>(StdPathsOpts::new("nitrogen"))?
        .load_extension_with::<AssetCatalog>(opt.catalog_opts)?
        .load_extension::<EventMapper>()?
        .load_extension_with::<GeoDb>(opt.geo_db_opts)?
        .load_extension::<Gui>()?
        .load_extension::<Dashboard>()?
        .load_extension::<Terminal>()?
        .load_extension::<Atmosphere>()?
        .load_extension::<Orrery>()?
        .load_extension::<Stars>()?
        .load_extension_with::<Terrain>(TerrainOpts::from_detail(
            opt.detail_opts.cpu_detail(),
            opt.detail_opts.gpu_detail(),
        ))?
        .load_extension::<World>()?
        .load_extension::<Markers>()?
        .load_extension::<Composite>()?
        .load_extension::<Demo>()?
        .load_extension::<Timeline>()?
        .load_extension::<ScreenCamera>()?
        .load_extension::<ArcBallController>()?
        .load_extension::<Physics>()?
        .load_extension::<PitchInceptor>()?
        .load_extension::<RollInceptor>()?
        .load_extension::<YawInceptor>()?
        .load_extension::<PowerSystem>()?
        .load_extension::<ThrustVectorPitchControl>()?
        .load_extension::<ThrustVectorYawControl>()?
        .load_extension::<ThrustVectorPitchEffector>()?
        .load_extension::<ThrustVectorYawEffector>()?
        .load_extension::<AirbrakeEffector>()?
        .load_extension::<BayEffector>()?
        .load_extension::<FlapsEffector>()?
        .load_extension::<GearEffector>()?
        .load_extension::<HookEffector>()?
        .load_extension_with::<StartupOpts>(opt.startup_opts.with_prelude(PRELUDE))?;

    // We need at least one entity with a camera controller for the screen camera
    // before the sim is fully ready to run.
    let mut arcball = ArcBallController::default();
    let name = "Everest";
    let target = arcball.notable_location(name)?;
    let bearing = arcball.bearing_for_notable_location(name);
    let pitch = arcball.pitch_for_notable_location(name);
    let distance = arcball.distance_for_notable_location(name);
    arcball.set_target_imm_unsafe(target);
    arcball.set_bearing(bearing);
    arcball.set_pitch(pitch);
    arcball.set_distance(distance)?;
    let _player_ent = runtime
        .spawn("camera")?
        .inject(Frame::default())?
        .inject(arcball)?
        .inject(ScreenCameraController)?
        .id();

    Ok(())
}
