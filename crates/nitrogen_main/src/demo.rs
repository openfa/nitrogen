// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use anyhow::Result;
use arcball::ArcBallController;
use bevy_ecs::prelude::*;
use camera::{CameraStep, ScreenCamera, ScreenCameraController};
use geodb::{GeoDb, GeoDbStep, MapKind};
use geodesy::{Bearing, PitchCline};
use geometry::{Aabb3, Arrow};
use gui::{egui_color, Gui, GuiStep};
use marker::{Markers, MarkersStep};
use nitrous::{inject_nitrous_resource, method, HeapMut, NitrousResource};
use orrery::{Orrery, OrreryStep};
use phase::{BodyMotion, CollisionBuilder, Frame};
use physics::{PhysicsMailbox, PhysicsStep, RigidBodyDynamics};
use planck::{rgb, rgba};
use runtime::{Extension, Runtime};
use terrain::TerrainStep;

#[derive(SystemSet, Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub enum DemoStep {
    DrawOverlay,
    DrawMarkers,
}

#[derive(Debug, NitrousResource)]
pub(crate) struct Demo {}

impl Extension for Demo {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        let demo = Demo::new()?;
        runtime.inject_resource(demo)?;
        runtime.add_frame_system(
            Self::sys_draw_overlay
                .in_set(DemoStep::DrawOverlay)
                .after(GuiStep::StartFrame)
                .before(CameraStep::UploadToGpu)
                .before(GuiStep::EndFrame)
                .before(TerrainStep::OptimizePatches)
                .after(OrreryStep::UploadToGpu)
                .after(CameraStep::MoveCameraToFrame)
                .after(GeoDbStep::CheckDownloads),
        );
        runtime.add_frame_system(
            Self::sys_draw_markers
                .in_set(DemoStep::DrawMarkers)
                .after(CameraStep::MoveCameraToFrame)
                .after(DemoStep::DrawOverlay)
                .before(MarkersStep::UploadGeometry)
                // Non-conflicting use of camera; position vs gbuffers
                .ambiguous_with(PhysicsStep::DisplayDebug)
                .ambiguous_with(TerrainStep::OptimizePatches),
        );
        runtime.run_string(
            r#"
                bindings.bind("Escape", "window.request_quit()");
                bindings.bind("q", "window.request_quit()");
            "#,
        )?;
        Ok(())
    }
}

#[inject_nitrous_resource]
impl Demo {
    pub fn new() -> Result<Self> {
        Ok(Self {})
    }

    #[method]
    fn spawn(&mut self, mut heap: HeapMut) -> Result<()> {
        // Spawn at the arcball target
        let arcball = heap.get::<ArcBallController>("camera");
        let frame = Frame::from_geodetic_bearing_and_pitch(
            arcball.target(),
            arcball.bearing(),
            arcball.pitch(),
        );

        // Spawn something roughly f22 shaped.
        let (x, y, z) = (feet!(22.25), feet!(8.3), feet!(31));
        // let (x, y, z) = (feet!(500), feet!(500), feet!(500));
        let aabb = Aabb3::<Meters>::from_bounds(
            Pt3::new(meters!(-x), meters!(-y), meters!(-z)),
            Pt3::new(meters!(x), meters!(y), meters!(z)),
        );
        let collider = CollisionBuilder::for_airplane(&aabb, meters!(2)).build();

        let mut ent = heap.spawn_numbered("demo-collider")?;
        ent.inject(frame)?
            // .inject(InertiaImpostor::for_collision(&collider, kilograms!(4000)))?
            .inject(RigidBodyDynamics::default())?
            .inject(collider)?
            .inject(BodyMotion::default())?
            .inject(PhysicsMailbox::default())?;

        Ok(())
    }

    fn sys_draw_markers(
        mut markers: ResMut<Markers>,
        query: Query<(&ArcBallController, &ScreenCameraController)>,
        orrery: Res<Orrery>,
    ) {
        let (arcball, _) = query.single();
        markers.draw_point(arcball.target().pt3(), meters!(10_f64), rgb!(0xFF00FF));
        markers.draw_frame(
            Frame::from_geodetic_bearing_and_pitch(
                arcball.target(),
                Bearing::north(),
                PitchCline::level(),
            ),
            meters!(100_f64),
        );
        markers.draw_arrow(
            rgb!(0xCF6100),
            &Arrow::new(
                arcball.target().pt3(),
                orrery.sun_direction(),
                meters!(1000_f64),
                meters!(10_f64),
            ),
        );
    }

    fn sys_draw_overlay(
        gui: Res<Gui>,
        geodb: Res<GeoDb>,
        mut query: Query<(&mut ArcBallController, &ScreenCameraController)>,
        mut camera: ResMut<ScreenCamera>,
        mut orrery: ResMut<Orrery>,
    ) {
        if !geodb.is_ready(MapKind::Heights) || !geodb.is_ready(MapKind::Colors) {
            Self::draw_loading_progress(&gui, &geodb);
        } else {
            let (mut arcball, _) = query.single_mut();
            Self::draw_info_area(&gui, &mut orrery, &mut arcball, &mut camera);
        }
    }

    fn draw_loading_progress(gui: &Gui, geodb: &GeoDb) {
        const WIDTH: f32 = 200.;
        const HEIGHT: f32 = 50.;
        egui::CentralPanel::default()
            .frame(
                egui::Frame::default()
                    .inner_margin(egui::Margin {
                        left: (gui.screen().width() - WIDTH) / 2.,
                        top: (gui.screen().height() - HEIGHT) / 2.,
                        ..Default::default()
                    })
                    .fill(egui_color(rgba!(0x303040A0))),
            )
            .show(gui.screen().ctx(), |ui| {
                ui.vertical(|ui| {
                    ui.label("Downloading terrain index...");
                    ui.add(
                        egui::ProgressBar::new(
                            (geodb.startup_progress(MapKind::Heights)
                                + geodb.startup_progress(MapKind::Colors))
                                as f32
                                / 2f32,
                        )
                        .desired_width(WIDTH)
                        .animate(true)
                        .show_percentage(),
                    );
                });
            });
    }

    fn draw_info_area(
        gui: &Gui,
        orrery: &mut Orrery,
        arcball: &mut ArcBallController,
        camera: &mut ScreenCamera,
    ) {
        egui::Area::new(egui::Id::new("info"))
            .pivot(egui::Align2::RIGHT_TOP)
            .fixed_pos([gui.screen().width(), 0.])
            .show(gui.screen().ctx(), |ui| {
                egui::Frame::none()
                    .fill(egui_color(rgba!(0x202020A0)))
                    .stroke(egui::Stroke::new(2., egui::Color32::BLACK))
                    .inner_margin(egui::Margin {
                        left: 10.,
                        bottom: 6.,
                        top: 4.,
                        right: 4.,
                    })
                    .rounding(egui::Rounding {
                        sw: 10.,
                        ..Default::default()
                    })
                    .show(ui, |ui| {
                        egui::CollapsingHeader::new("Nitrogen")
                            .show_background(false)
                            .default_open(false)
                            .show(ui, |ui| {
                                egui::Grid::new("controls_grid")
                                    .num_columns(2)
                                    .spacing([4.0, 2.0])
                                    .striped(false)
                                    .show(ui, |ui| {
                                        Self::fill_info_grid(ui, orrery, arcball, camera);
                                    });
                            });
                    });
            });
    }

    fn fill_info_grid(
        ui: &mut egui::Ui,
        orrery: &mut Orrery,
        arcball: &mut ArcBallController,
        camera: &mut ScreenCamera,
    ) {
        ui.label("Time Zone");
        let zone = orrery.lookup_time_zone(*arcball.target().geode());
        ui.label(zone);
        ui.end_row();

        ui.label("Local Time");
        let local_time = orrery.get_local_civil(*arcball.target().geode()).unwrap();
        ui.label(format!("{} {}", local_time.date(), local_time.time()));
        ui.end_row();

        ui.label("Latitude");
        let mut lat = arcball.target().lat::<Degrees>().f32();
        ui.add(egui::Slider::new(&mut lat, -90.0..=90.0).suffix("°"))
            .on_hover_text("Right click + drag for fine control");
        arcball.target_mut().set_lat(degrees!(lat));
        ui.end_row();

        ui.label("Longitude");
        let mut lon = arcball.target().lon::<Degrees>().f32();
        ui.add(egui::Slider::new(&mut lon, -180.0..=180.0).suffix("°"))
            .on_hover_text("Right click + drag for fine control");
        arcball.target_mut().set_lon(degrees!(lon));
        ui.end_row();

        ui.label("Altitude");
        let mut altitude = arcball.target().asl::<Feet>().f32();
        ui.add(egui::Slider::new(&mut altitude, 0.0..=40_000.0).suffix("'"))
            .on_hover_text("Up + Down arrows for fine control");
        arcball.target_mut().set_asl(feet!(altitude));
        ui.end_row();

        ui.label("FoV").on_hover_text("Field of View");
        let mut fov = degrees!(camera.fov_y::<Radians>()).f32();
        ui.add(egui::Slider::new(&mut fov, 5.0..=120.0).suffix("°"))
            .on_hover_text("Change the field of view");
        camera.set_fov_y(radians!(degrees!(fov)));
        ui.end_row();
    }
}
