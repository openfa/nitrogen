// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

use catalog::CatalogOpts;
use geodb::GeoDbOpts;
use runtime::StartupOpts;
use structopt::StructOpt;
// use terminal_size::{terminal_size, Width};
use mantle::{DetailLevelOpts, DisplayOpts};
use tracelog::TraceLogOpts;

/// Demonstrate the capabilities of the Nitrogen engine
#[derive(Clone, Default, Debug, StructOpt)]
// #[structopt(name = "Nitrogen", set_term_width = if let Some((Width(w), _)) = terminal_size() { w as usize } else { 80 })]
#[structopt(name = "Nitrogen")]
pub struct NitrogenOpts {
    #[structopt(flatten)]
    pub catalog_opts: CatalogOpts,

    #[structopt(flatten)]
    pub detail_opts: DetailLevelOpts,

    #[structopt(flatten)]
    pub geo_db_opts: GeoDbOpts,

    #[structopt(flatten)]
    pub display_opts: DisplayOpts,

    #[structopt(flatten)]
    pub startup_opts: StartupOpts,

    #[structopt(flatten)]
    pub tracelog_opts: TraceLogOpts,
}
