// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
mod panel;

pub use crate::panel::Panel;

use anyhow::Result;
#[cfg(not(target_arch = "wasm32"))]
#[cfg(not(test))]
use arboard::Clipboard;
use bevy_ecs::prelude::*;
use camera::CameraStep;
use egui::{
    Color32, CursorIcon, Event, Key, Modifiers, MouseWheelUnit, PointerButton, Pos2, RawInput,
    Rect, Vec2,
};
use egui_wgpu::ScreenDescriptor;
use log::info;
use log::trace;
use mantle::{
    CurrentEncoder, Gpu, GpuStep, InputEvent, InputEvents, SystemEvent, SystemEvents, Window,
};
use nitrous::{inject_nitrous_resource, NitrousResource};
use planck::Color;
use runtime::{report_errors, Extension, Runtime, TimeStep};
use shader_shared::layout::frag;
use stars::StarsStep;
use std::collections::HashMap;
use winit::{
    event::{ElementState, MouseButton},
    keyboard::{KeyCode, ModifiersState, PhysicalKey},
    window,
};

/// Convert a planck Color to an egui Color32
pub fn egui_color(color: Color) -> Color32 {
    let clr = color.to_array();
    Color32::from_rgba_premultiplied(clr[0], clr[1], clr[2], clr[3])
}

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum GuiStep {
    CollectInputEvents,
    CollectSystemEvents,
    HandleDisplayChange,
    StartFrame,
    EndFrame,
}

#[derive(NitrousResource)]
pub struct Gui {
    input: RawInput,
    mouse_position: Pos2,
    pixels_per_point: f32,
    #[cfg(not(target_arch = "wasm32"))]
    #[cfg(not(test))]
    clipboard: Clipboard,
    screen_panel: Panel,
    panels: HashMap<String, Panel>,
    layout: wgpu::BindGroupLayout,
    last: std::time::Instant,
}

impl Extension for Gui {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        let gui = Gui::new(
            runtime.resource::<TimeStep>(),
            runtime.resource::<Window>(),
            runtime.resource::<Gpu>(),
        )?;
        runtime.inject_resource(gui)?;

        runtime.add_input_systems((
            Self::sys_collect_system_events.in_set(GuiStep::CollectSystemEvents),
            Self::sys_collect_input_events
                .in_set(GuiStep::CollectInputEvents)
                .ambiguous_with(GuiStep::CollectSystemEvents),
        ));

        runtime.add_frame_system(
            Self::sys_handle_display_config_change
                .in_set(GuiStep::HandleDisplayChange)
                .after(GpuStep::HandleDisplayChange)
                .before(GpuStep::CreateTargetSurface),
        );
        runtime.add_frame_system(
            Self::sys_start_frame
                .in_set(GuiStep::StartFrame)
                .after(GuiStep::HandleDisplayChange)
                .after(GpuStep::CreateTargetSurface),
        );
        runtime.add_frame_system(
            Self::sys_end_frame
                .pipe(report_errors)
                .in_set(GuiStep::EndFrame)
                .after(GpuStep::CreateCommandEncoder)
                .after(GuiStep::StartFrame)
                .before(GpuStep::SubmitCommands)
                // Note: pin encoder ordering so we get fixed inter-run behavior on serial systems.
                .after(CameraStep::UploadToGpu)
                .after(StarsStep::UpdateHourAngle),
        );
        Ok(())
    }
}

#[inject_nitrous_resource]
impl Gui {
    pub fn new(timestep: &TimeStep, window: &Window, gpu: &Gpu) -> Result<Self> {
        let layout = gpu
            .device()
            .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                label: Some("gui-bind-group-layout"),
                entries: &[frag::texture(0), frag::sampler(1)],
            });

        let mut gui = Self {
            input: RawInput {
                screen_rect: Some(Rect {
                    min: Default::default(),
                    max: Pos2 {
                        x: window.width() as f32 / window.scale_factor() as f32,
                        y: window.height() as f32 / window.scale_factor() as f32,
                    },
                }),
                max_texture_side: Some(gpu.device().limits().max_texture_dimension_2d as usize),
                time: Some(timestep.real_time_elapsed_since_boot().f64()),
                predicted_dt: 1_f32 / 60.,
                focused: true,
                ..Default::default()
            },
            pixels_per_point: window.scale_factor() as f32,
            mouse_position: Pos2 { x: 0., y: 0. },
            #[cfg(not(target_arch = "wasm32"))]
            #[cfg(not(test))]
            clipboard: Clipboard::new()?,
            screen_panel: Panel::new(
                gpu,
                &layout,
                ScreenDescriptor {
                    size_in_pixels: [
                        (window.window_size().width as f64 / window.scale_factor()) as u32,
                        (window.window_size().height as f64 / window.scale_factor()) as u32,
                    ],
                    pixels_per_point: window.scale_factor() as f32,
                },
            ),
            panels: HashMap::new(),
            layout,
            last: std::time::Instant::now(),
        };
        let mut style = (*gui.screen_panel.ctx().style()).clone();
        style.visuals = egui::Visuals::dark();
        gui.screen_panel.ctx_mut().set_style(style);
        Ok(gui)
    }

    pub fn screen(&self) -> &Panel {
        &self.screen_panel
    }

    pub fn input(&self) -> &RawInput {
        &self.input
    }

    // For using the offscreen texture
    pub fn bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        &self.layout
    }

    pub fn screen_bind_group(&self) -> &wgpu::BindGroup {
        self.screen_panel.bind_group()
    }

    // TODO: non-screen panel bind groups for rendering to geometry in a scene

    pub fn sys_handle_display_config_change(
        window: Res<Window>,
        gpu: Res<Gpu>,
        mut gui: ResMut<Gui>,
    ) {
        if window.updated_config().is_some() {
            gui.handle_render_extent_changed(&gpu);
        }
    }

    fn handle_render_extent_changed(&mut self, gpu: &Gpu) {
        self.screen_panel
            .handle_render_extent_changed(gpu, &self.layout);
    }

    // Per input/sim cycle
    fn sys_collect_input_events(mut gui: ResMut<Gui>, events: Res<InputEvents>) {
        for event in events.iter() {
            match event {
                InputEvent::KeyboardKey {
                    physical_key: PhysicalKey::Code(key_code),
                    press_state,
                    modifiers_state,
                    ..
                } => {
                    let modifiers = Self::modstate_to_mods(modifiers_state);
                    gui.input.modifiers = modifiers;

                    let (maybe_event, maybe_text, maybe_key) =
                        gui.vkey_to_key(key_code, &modifiers, press_state);
                    trace!("event: {maybe_event:?}, {maybe_text:?}, {maybe_key:?}");

                    if let Some(event) = maybe_event {
                        gui.input.events.push(event);
                    }
                    if let Some(key) = maybe_key {
                        gui.input.events.push(Event::Key {
                            key,
                            // FIXME: once we have winit next, we should be able to plumb this through
                            physical_key: None,
                            modifiers,
                            pressed: Self::press_state_is_pressed(press_state),
                            repeat: false,
                        });
                    }
                    if *press_state == ElementState::Pressed
                        && !modifiers_state.control_key()
                        && !modifiers_state.alt_key()
                        && !modifiers_state.super_key()
                    {
                        if let Some(text) = maybe_text {
                            gui.input.events.push(Event::Text(text));
                        }
                    }
                }
                InputEvent::MouseButton {
                    button,
                    press_state,
                    modifiers_state,
                    ..
                } => {
                    if let Some(button) = Self::button_id_to_button(button) {
                        let pos = gui.mouse_position;
                        gui.input.events.push(Event::PointerButton {
                            pos,
                            button,
                            pressed: Self::press_state_is_pressed(press_state),
                            modifiers: Self::modstate_to_mods(modifiers_state),
                        });
                    }
                }
                InputEvent::MouseMotion { .. } => {}
                InputEvent::CursorMove { pixel_position, .. } => {
                    let pos = Pos2 {
                        x: pixel_position.0 as f32 / gui.pixels_per_point,
                        y: pixel_position.1 as f32 / gui.pixels_per_point,
                    };
                    gui.mouse_position = pos;
                    gui.input.events.push(Event::PointerMoved(pos));
                }
                InputEvent::MouseWheel {
                    horizontal_delta,
                    vertical_delta,
                    modifiers_state,
                    ..
                } => {
                    #[cfg(target_family = "windows")]
                    let (x, y) = {
                        (
                            *horizontal_delta as f32 * 64.,
                            -*vertical_delta as f32 * 64.,
                        )
                    };
                    #[cfg(not(target_family = "windows"))]
                    let (x, y) = { (*horizontal_delta as f32, -*vertical_delta as f32) };
                    // gui.input.events.push(Event::Scroll(Vec2 { x, y }));
                    gui.input.events.push(Event::MouseWheel {
                        unit: MouseWheelUnit::Line,
                        delta: Vec2 { x, y },
                        modifiers: Modifiers {
                            alt: modifiers_state.alt_key(),
                            ctrl: modifiers_state.control_key(),
                            shift: modifiers_state.shift_key(),
                            mac_cmd: modifiers_state.super_key(),
                            command: modifiers_state.super_key(),
                        },
                    });
                }
                _ => {}
            }
        }
    }

    // Per frame events
    fn sys_collect_system_events(mut gui: ResMut<Gui>, events: Res<SystemEvents>) {
        for event in events.iter() {
            match event {
                SystemEvent::ScaleFactorChanged { scale } => {
                    info!("scale changed from {} to {scale}", gui.pixels_per_point);
                    gui.pixels_per_point = *scale as f32;
                    gui.screen_panel.update_scaling(*scale as f32)
                }
                SystemEvent::WindowResized { width, height } => {
                    gui.input.screen_rect.as_mut().unwrap().max.x =
                        *width as f32 / gui.pixels_per_point;
                    gui.input.screen_rect.as_mut().unwrap().max.y =
                        *height as f32 / gui.pixels_per_point;
                    gui.screen_panel.update_size(*width, *height);
                }
                SystemEvent::Quit => {}
            }
        }
    }

    fn sys_start_frame(mut gui: ResMut<Gui>, timestep: Res<TimeStep>) {
        let now = std::time::Instant::now();
        let last_frame_duration = now - gui.last;
        gui.last = now;
        gui.input.predicted_dt = last_frame_duration.as_secs_f32();
        gui.input.time = Some(timestep.real_time_elapsed_since_boot().f64());
        let frame_input = gui.input.clone();
        gui.screen_panel.begin_frame(frame_input.clone());
        for panel in gui.panels.values() {
            panel.begin_frame(frame_input.clone());
        }
        gui.input.events.clear();
        gui.input.dropped_files.clear();
    }

    fn sys_end_frame(
        mut gui: ResMut<Gui>,
        gpu: Res<Gpu>,
        mut window: ResMut<Window>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) -> Result<()> {
        if let Some(encoder) = maybe_encoder.get_encoder_mut() {
            gui.end_frame(&gpu, &mut window, encoder)?;
        }
        Ok(())
    }

    fn end_frame(
        &mut self,
        gpu: &Gpu,
        window: &mut Window,
        encoder: &mut wgpu::CommandEncoder,
    ) -> Result<()> {
        let platform_output = self.screen_panel.end_frame(gpu, encoder);
        for panel in self.panels.values_mut() {
            let _ = panel.end_frame(gpu, encoder);
        }

        // Handle platform outputs
        if matches!(platform_output.cursor_icon, CursorIcon::None) {
            window.set_cursor_visible(false);
        } else {
            window.set_cursor_visible(true);
            window.set_cursor_icon(Self::cursor_icon_for(platform_output.cursor_icon));
        }
        #[cfg(not(target_arch = "wasm32"))]
        #[cfg(not(test))]
        if !platform_output.copied_text.is_empty() {
            self.clipboard.set_text(&platform_output.copied_text)?;
        }

        Ok(())
    }

    fn modstate_to_mods(mods: &ModifiersState) -> Modifiers {
        Modifiers {
            alt: mods.alt_key(),
            ctrl: mods.control_key(),
            shift: mods.shift_key(),
            mac_cmd: mods.super_key(),
            command: mods.super_key(),
        }
    }

    fn press_state_is_pressed(press_state: &ElementState) -> bool {
        matches!(press_state, ElementState::Pressed)
    }

    fn button_id_to_button(button: &MouseButton) -> Option<PointerButton> {
        Some(match *button {
            MouseButton::Left => PointerButton::Primary,
            MouseButton::Middle => PointerButton::Middle,
            MouseButton::Right => PointerButton::Secondary,
            MouseButton::Back => PointerButton::Extra1,
            MouseButton::Forward => PointerButton::Extra2,
            _ => return None,
        })
    }

    fn cursor_icon_for(icon: CursorIcon) -> window::CursorIcon {
        match icon {
            CursorIcon::Default => window::CursorIcon::Default,
            CursorIcon::None => window::CursorIcon::Default,
            CursorIcon::ContextMenu => window::CursorIcon::ContextMenu,
            CursorIcon::Help => window::CursorIcon::Help,
            CursorIcon::PointingHand => window::CursorIcon::Grab,
            CursorIcon::Progress => window::CursorIcon::Progress,
            CursorIcon::Wait => window::CursorIcon::Wait,
            CursorIcon::Cell => window::CursorIcon::Cell,
            CursorIcon::Crosshair => window::CursorIcon::Crosshair,
            CursorIcon::Text => window::CursorIcon::Text,
            CursorIcon::VerticalText => window::CursorIcon::VerticalText,
            CursorIcon::Alias => window::CursorIcon::Alias,
            CursorIcon::Copy => window::CursorIcon::Copy,
            CursorIcon::Move => window::CursorIcon::Move,
            CursorIcon::NoDrop => window::CursorIcon::NoDrop,
            CursorIcon::NotAllowed => window::CursorIcon::NotAllowed,
            CursorIcon::Grab => window::CursorIcon::Grab,
            CursorIcon::Grabbing => window::CursorIcon::Grabbing,
            CursorIcon::AllScroll => window::CursorIcon::AllScroll,
            CursorIcon::ResizeHorizontal => window::CursorIcon::EwResize,
            CursorIcon::ResizeNeSw => window::CursorIcon::NeswResize,
            CursorIcon::ResizeNwSe => window::CursorIcon::NwseResize,
            CursorIcon::ResizeVertical => window::CursorIcon::NsResize,
            CursorIcon::ResizeEast => window::CursorIcon::EResize,
            CursorIcon::ResizeSouthEast => window::CursorIcon::SeResize,
            CursorIcon::ResizeSouth => window::CursorIcon::SResize,
            CursorIcon::ResizeSouthWest => window::CursorIcon::SwResize,
            CursorIcon::ResizeWest => window::CursorIcon::WResize,
            CursorIcon::ResizeNorthWest => window::CursorIcon::NwResize,
            CursorIcon::ResizeNorth => window::CursorIcon::NResize,
            CursorIcon::ResizeNorthEast => window::CursorIcon::NeResize,
            CursorIcon::ResizeColumn => window::CursorIcon::NsResize,
            CursorIcon::ResizeRow => window::CursorIcon::EwResize,
            CursorIcon::ZoomIn => window::CursorIcon::ZoomIn,
            CursorIcon::ZoomOut => window::CursorIcon::ZoomOut,
        }
    }

    fn vkey_to_key(
        &mut self,
        vkey: &KeyCode,
        mods: &Modifiers,
        press_state: &ElementState,
    ) -> (Option<Event>, Option<String>, Option<Key>) {
        fn b(base: char) -> Option<String> {
            Some(base.to_string())
        }
        fn s(base: char, shifted: char, mods: &Modifiers) -> Option<String> {
            Some(if mods.shift { shifted } else { base }.to_string())
        }
        let (t, k) = match vkey {
            KeyCode::Digit1 => (s('1', '!', mods), Some(Key::Num1)),
            KeyCode::Digit2 => (s('2', '@', mods), Some(Key::Num2)),
            KeyCode::Digit3 => (s('3', '#', mods), Some(Key::Num3)),
            KeyCode::Digit4 => (s('4', '$', mods), Some(Key::Num4)),
            KeyCode::Digit5 => (s('5', '%', mods), Some(Key::Num5)),
            KeyCode::Digit6 => (s('6', '^', mods), Some(Key::Num6)),
            KeyCode::Digit7 => (s('7', '&', mods), Some(Key::Num7)),
            KeyCode::Digit8 => (s('8', '*', mods), Some(Key::Num8)),
            KeyCode::Digit9 => (s('9', '(', mods), Some(Key::Num9)),
            KeyCode::Digit0 => (s('0', ')', mods), Some(Key::Num0)),
            KeyCode::KeyA => (s('a', 'A', mods), Some(Key::A)),
            KeyCode::KeyB => (s('b', 'B', mods), Some(Key::B)),
            KeyCode::KeyC => (s('c', 'C', mods), Some(Key::C)),
            KeyCode::KeyD => (s('d', 'D', mods), Some(Key::D)),
            KeyCode::KeyE => (s('e', 'E', mods), Some(Key::E)),
            KeyCode::KeyF => (s('f', 'F', mods), Some(Key::F)),
            KeyCode::KeyG => (s('g', 'G', mods), Some(Key::G)),
            KeyCode::KeyH => (s('h', 'H', mods), Some(Key::H)),
            KeyCode::KeyI => (s('i', 'I', mods), Some(Key::I)),
            KeyCode::KeyJ => (s('j', 'J', mods), Some(Key::J)),
            KeyCode::KeyK => (s('k', 'K', mods), Some(Key::K)),
            KeyCode::KeyL => (s('l', 'L', mods), Some(Key::L)),
            KeyCode::KeyM => (s('m', 'M', mods), Some(Key::M)),
            KeyCode::KeyN => (s('n', 'N', mods), Some(Key::N)),
            KeyCode::KeyO => (s('o', 'O', mods), Some(Key::O)),
            KeyCode::KeyP => (s('p', 'P', mods), Some(Key::P)),
            KeyCode::KeyQ => (s('q', 'Q', mods), Some(Key::Q)),
            KeyCode::KeyR => (s('r', 'R', mods), Some(Key::R)),
            KeyCode::KeyS => (s('s', 'S', mods), Some(Key::S)),
            KeyCode::KeyT => (s('t', 'T', mods), Some(Key::T)),
            KeyCode::KeyU => (s('u', 'U', mods), Some(Key::U)),
            KeyCode::KeyV => (s('v', 'V', mods), Some(Key::V)),
            KeyCode::KeyW => (s('w', 'W', mods), Some(Key::W)),
            KeyCode::KeyX => (s('x', 'X', mods), Some(Key::X)),
            KeyCode::KeyY => (s('y', 'Y', mods), Some(Key::Y)),
            KeyCode::KeyZ => (s('z', 'Z', mods), Some(Key::Z)),
            KeyCode::Escape => (None, Some(Key::Escape)),
            KeyCode::F1 => (None, Some(Key::F1)),
            KeyCode::F2 => (None, Some(Key::F2)),
            KeyCode::F3 => (None, Some(Key::F3)),
            KeyCode::F4 => (None, Some(Key::F4)),
            KeyCode::F5 => (None, Some(Key::F5)),
            KeyCode::F6 => (None, Some(Key::F6)),
            KeyCode::F7 => (None, Some(Key::F7)),
            KeyCode::F8 => (None, Some(Key::F8)),
            KeyCode::F9 => (None, Some(Key::F9)),
            KeyCode::F10 => (None, Some(Key::F10)),
            KeyCode::F11 => (None, Some(Key::F11)),
            KeyCode::F12 => (None, Some(Key::F12)),
            KeyCode::F13 => (None, Some(Key::F13)),
            KeyCode::F14 => (None, Some(Key::F14)),
            KeyCode::F15 => (None, Some(Key::F15)),
            KeyCode::F16 => (None, Some(Key::F16)),
            KeyCode::F17 => (None, Some(Key::F17)),
            KeyCode::F18 => (None, Some(Key::F18)),
            KeyCode::F19 => (None, Some(Key::F19)),
            KeyCode::F20 => (None, Some(Key::F20)),
            KeyCode::F21 => (None, None),
            KeyCode::F22 => (None, None),
            KeyCode::F23 => (None, None),
            KeyCode::F24 => (None, None),
            KeyCode::Insert => (None, Some(Key::Insert)),
            KeyCode::Home => (None, Some(Key::Home)),
            KeyCode::Delete => (None, Some(Key::Delete)),
            KeyCode::End => (None, Some(Key::End)),
            KeyCode::PageDown => (None, Some(Key::PageDown)),
            KeyCode::PageUp => (None, Some(Key::PageUp)),
            KeyCode::ArrowLeft => (None, Some(Key::ArrowLeft)),
            KeyCode::ArrowUp => (None, Some(Key::ArrowUp)),
            KeyCode::ArrowRight => (None, Some(Key::ArrowRight)),
            KeyCode::ArrowDown => (None, Some(Key::ArrowDown)),
            KeyCode::Backspace => (None, Some(Key::Backspace)),
            KeyCode::Enter => (None, Some(Key::Enter)),
            KeyCode::Space => (b(' '), Some(Key::Space)),
            // KeyCode::Compose => (None, None),
            // KeyCode::Caret => (b('^'), None),
            KeyCode::NumLock => (None, None),
            KeyCode::Numpad0 => (b('0'), Some(Key::Num0)),
            KeyCode::Numpad1 => (b('1'), Some(Key::Num1)),
            KeyCode::Numpad2 => (b('2'), Some(Key::Num2)),
            KeyCode::Numpad3 => (b('3'), Some(Key::Num3)),
            KeyCode::Numpad4 => (b('4'), Some(Key::Num4)),
            KeyCode::Numpad5 => (b('5'), Some(Key::Num5)),
            KeyCode::Numpad6 => (b('6'), Some(Key::Num6)),
            KeyCode::Numpad7 => (b('7'), Some(Key::Num7)),
            KeyCode::Numpad8 => (b('8'), Some(Key::Num8)),
            KeyCode::Numpad9 => (b('9'), Some(Key::Num9)),
            KeyCode::Abort => (None, None),
            KeyCode::Suspend => (None, None),
            KeyCode::Resume => (None, None),
            KeyCode::Turbo => (None, None),
            KeyCode::Hyper => (None, None),
            KeyCode::NumpadAdd => (b('+'), None),
            KeyCode::NumpadDivide => (b('/'), None),
            KeyCode::NumpadDecimal => (b('.'), None),
            KeyCode::NumpadComma => (b(','), None),
            KeyCode::NumpadEnter => (None, Some(Key::Enter)),
            KeyCode::NumpadEqual => (b('='), None),
            KeyCode::NumpadMultiply => (b('*'), None),
            KeyCode::NumpadSubtract => (b('-'), None),
            KeyCode::Quote => (s('\'', '"', mods), None),
            // KeyCode::Apps => (None, None),
            // KeyCode::Asterisk => (b('*'), None),
            // KeyCode::At => (b('@'), None),
            // KeyCode::Ax => (None, None),
            KeyCode::Backslash => (s('\\', '|', mods), None),
            KeyCode::LaunchApp2 => (None, None),
            KeyCode::CapsLock => (None, None),
            KeyCode::Comma => (s(',', '<', mods), None),
            KeyCode::Convert => (None, None),
            KeyCode::Equal => (s('=', '+', mods), None),
            KeyCode::Backquote => (s('`', '~', mods), None),
            KeyCode::KanaMode => (None, None),
            // KeyCode::Kanji => (None, None),
            KeyCode::AltLeft => (None, None),
            KeyCode::BracketLeft => (s('[', '{', mods), None),
            KeyCode::ControlLeft => (None, None),
            KeyCode::ShiftLeft => (None, None),
            KeyCode::SuperLeft => (None, None),
            KeyCode::LaunchMail => (None, None),
            KeyCode::MediaSelect => (None, None),
            KeyCode::MediaStop => (None, None),
            KeyCode::Minus => (s('-', '_', mods), None),
            KeyCode::AudioVolumeMute => (None, None),
            KeyCode::LaunchApp1 => (None, None),
            KeyCode::MediaTrackNext => (None, None),
            KeyCode::NonConvert => (None, None),
            // KeyCode::OEM102 => (None, None),
            KeyCode::Period => (s('.', '>', mods), None),
            KeyCode::MediaPlayPause => (None, None),
            // KeyCode::Plus => (b('+'), None),
            KeyCode::Power => (None, None),
            KeyCode::MediaTrackPrevious => (None, None),
            KeyCode::AltRight => (None, None),
            KeyCode::BracketRight => (s(']', '}', mods), None),
            KeyCode::ControlRight => (None, None),
            KeyCode::ShiftRight => (None, None),
            KeyCode::SuperRight => (None, None),
            KeyCode::Semicolon => (s(';', ':', mods), None),
            KeyCode::Slash => (s('/', '?', mods), None),
            KeyCode::Sleep => (None, None),
            KeyCode::PrintScreen => (None, None),
            KeyCode::Tab => (None, Some(Key::Tab)),
            // KeyCode::Underline => (None, None),
            // KeyCode::Unlabeled => (None, None),
            KeyCode::AudioVolumeDown => (None, None),
            KeyCode::AudioVolumeUp => (None, None),
            KeyCode::WakeUp => (None, None),
            KeyCode::BrowserBack => (None, None),
            KeyCode::BrowserFavorites => (None, None),
            KeyCode::BrowserForward => (None, None),
            KeyCode::BrowserHome => (None, None),
            KeyCode::BrowserRefresh => (None, None),
            KeyCode::BrowserSearch => (None, None),
            KeyCode::BrowserStop => (None, None),
            KeyCode::IntlYen => (None, None),
            KeyCode::Copy => return (Some(Event::Cut), None, None),
            KeyCode::Paste => (None, None),
            KeyCode::Cut => return (Some(Event::Cut), None, None),
            // KeyCode::Snapshot => (None, None),
            KeyCode::ScrollLock => (None, None),
            KeyCode::Pause => (None, None),
            _ => (None, None),
        };

        // TODO: Key combos feel like they should have something more elaborate than just this
        let mut event = None;
        if Self::press_state_is_pressed(press_state) && mods.ctrl && !mods.shift && !mods.alt {
            match k {
                Some(Key::C) => event = Some(Event::Copy),
                Some(Key::X) => event = Some(Event::Cut),
                Some(Key::V) =>
                {
                    #[cfg(not(target_arch = "wasm32"))]
                    #[cfg(not(test))]
                    if let Ok(s) = self.clipboard.get_text() {
                        event = Some(Event::Paste(s))
                    }
                }
                _ => {}
            }
        }
        (event, t, k)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use mantle::Core;

    #[test]
    fn it_works() -> Result<()> {
        let mut runtime = Core::for_test()?;
        runtime.load_extension::<Gui>()?;
        Ok(())
    }
}
