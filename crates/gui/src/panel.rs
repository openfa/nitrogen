// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use egui::{Context, FullOutput, PlatformOutput, RawInput};
use egui_wgpu::{Renderer, ScreenDescriptor};
use mantle::Gpu;
use shader_shared::binding;

pub struct Panel {
    desc: ScreenDescriptor,
    ctx: Context,
    renderer: Renderer,
    offscreen_color: (wgpu::Texture, wgpu::TextureView),
    sampler: wgpu::Sampler,
    bind_group: wgpu::BindGroup,
}

impl Panel {
    pub(crate) fn new(gpu: &Gpu, layout: &wgpu::BindGroupLayout, desc: ScreenDescriptor) -> Self {
        let offscreen_color = gpu.make_screen_target("ui-panel-color");
        let sampler = gpu.device().create_sampler(&wgpu::SamplerDescriptor {
            label: Some("gui-panel-sampler"),
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Linear,
            mipmap_filter: wgpu::FilterMode::Linear,
            lod_min_clamp: 0.0,
            lod_max_clamp: 0.0,
            compare: None,
            anisotropy_clamp: 1,
            border_color: None,
        });
        let bind_group = Self::_make_bind_group(gpu.device(), layout, &offscreen_color.1, &sampler);
        let ctx = Context::default();
        Self {
            desc,
            ctx,
            renderer: Renderer::new(gpu.device(), Gpu::SCREEN_FORMAT, None, 1),
            offscreen_color,
            sampler,
            bind_group,
        }
    }

    fn _make_bind_group(
        device: &wgpu::Device,
        layout: &wgpu::BindGroupLayout,
        color: &wgpu::TextureView,
        sampler: &wgpu::Sampler,
    ) -> wgpu::BindGroup {
        device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("gui-panel-bind-group"),
            layout,
            entries: &[binding::texture(0, color), binding::sampler(1, sampler)],
        })
    }

    pub fn ctx(&self) -> &Context {
        &self.ctx
    }

    pub fn ctx_mut(&mut self) -> &mut Context {
        &mut self.ctx
    }

    pub fn width(&self) -> f32 {
        self.desc.size_in_pixels[0] as f32 / self.desc.pixels_per_point
    }

    pub fn height(&self) -> f32 {
        self.desc.size_in_pixels[1] as f32 / self.desc.pixels_per_point
    }

    pub fn bind_group(&self) -> &wgpu::BindGroup {
        &self.bind_group
    }

    pub(crate) fn update_scaling(&mut self, scale: f32) {
        self.desc.pixels_per_point = scale;
    }

    pub(crate) fn update_size(&mut self, width: u32, height: u32) {
        self.desc.size_in_pixels[0] = width;
        self.desc.size_in_pixels[1] = height;
    }

    pub(crate) fn handle_render_extent_changed(
        &mut self,
        gpu: &Gpu,
        layout: &wgpu::BindGroupLayout,
    ) {
        self.offscreen_color = gpu.make_screen_target("ui-panel-color");
        self.desc.size_in_pixels[0] = gpu.render_extent().width;
        self.desc.size_in_pixels[1] = gpu.render_extent().height;
        self.bind_group =
            Self::_make_bind_group(gpu.device(), layout, &self.offscreen_color.1, &self.sampler);
    }

    pub(crate) fn begin_frame(&self, mut raw_input: RawInput) {
        raw_input
            .viewports
            .get_mut(&raw_input.viewport_id)
            .unwrap()
            .native_pixels_per_point = Some(self.desc.pixels_per_point);
        self.ctx.begin_frame(raw_input);
    }

    pub(crate) fn end_frame(
        &mut self,
        gpu: &Gpu,
        encoder: &mut wgpu::CommandEncoder,
    ) -> PlatformOutput {
        let FullOutput {
            textures_delta,
            shapes,
            platform_output,
            ..
        } = self.ctx.end_frame();

        // Upload texture changes
        for (tex_id, image) in &textures_delta.set {
            self.renderer
                .update_texture(gpu.device(), gpu.queue(), *tex_id, image);
        }
        for tex_id in &textures_delta.free {
            self.renderer.free_texture(tex_id);
        }

        // Upload vertices and indices
        let paint_jobs = self.ctx.tessellate(shapes, self.desc.pixels_per_point);
        self.renderer
            .update_buffers(gpu.device(), gpu.queue(), encoder, &paint_jobs, &self.desc);

        // Encode a render pass
        let (color_attachments, depth_stencil_attachment) = self.offscreen_target();
        let render_pass_desc_ref = wgpu::RenderPassDescriptor {
            label: Some(concat!("gui-render-pass",)),
            color_attachments: &color_attachments,
            depth_stencil_attachment,
            timestamp_writes: None,
            occlusion_query_set: None,
        };
        let mut rpass = encoder.begin_render_pass(&render_pass_desc_ref);

        self.renderer
            .render(&mut rpass, paint_jobs.as_slice(), &self.desc);

        platform_output
    }

    fn offscreen_target(
        &self,
    ) -> (
        [Option<wgpu::RenderPassColorAttachment>; 1],
        Option<wgpu::RenderPassDepthStencilAttachment>,
    ) {
        (
            [Some(wgpu::RenderPassColorAttachment {
                view: &self.offscreen_color.1,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color::TRANSPARENT),
                    store: wgpu::StoreOp::Store,
                },
            })],
            None,
        )
    }
}
