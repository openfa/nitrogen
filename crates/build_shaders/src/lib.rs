// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Scripts should be put in files like: <project>/shaders/<name>.<type>.glsl
 * Outputs get put in the project target dir: <project>/target/<name>.<type>.spir
 *
 * Compiler Options:
 *     DUMP_SPIRV=1   Dump disassembled code next to bytecode.
 *     DEBUG=1        Compile with debug settings.
 */
use anyhow::{anyhow, ensure, Result};
use log::trace;
use once_cell::sync::Lazy;
use regex::Regex;
use std::{
    collections::HashMap,
    env, fs,
    path::{Path, PathBuf, MAIN_SEPARATOR},
    sync::Mutex,
};
use zerocopy::IntoBytes;

fn bindings_for(group: u32, binding_offset: u32, module_path: &str) -> Result<(String, u32)> {
    // load module in bindings dir.
    let path = find_included_path(module_path)?;
    let raw = fs::read_to_string(path)?;

    // Replace GROUP with `group`
    let mut s = raw.replace("GROUP", &group.to_string());

    // Replace each B{N} with N+binding_offset
    let mut binding_no = 0u32;
    let mut binding_rep = binding_offset;
    loop {
        let s1 = s.replace(&format!("B{binding_no}"), &binding_rep.to_string());
        if s == s1 {
            break;
        }
        s = s1;
        binding_no += 1;
        binding_rep += 1;
    }
    Ok((s, binding_no))
}

fn make_output_dirs() -> PathBuf {
    let project_cargo_root = env::var("CARGO_MANIFEST_DIR").unwrap();
    let target_dir = Path::new(&project_cargo_root).join("target");
    trace!("creating directory: {:?}", target_dir);
    fs::create_dir_all(&target_dir).expect("a directory");
    target_dir
}

fn stage_name(stage: naga::ShaderStage) -> &'static str {
    match stage {
        naga::ShaderStage::Compute => "comp",
        naga::ShaderStage::Vertex => "vert",
        naga::ShaderStage::Fragment => "frag",
    }
}

fn output_for_wgsl(name: &str) -> String {
    assert!(name.ends_with(".wgsl"));
    assert!(name.len() > 5);
    let file_name = format!("{}.wgsl", &name[..name.len() - 5]);
    make_output_dirs()
        .join(file_name)
        .to_str()
        .unwrap()
        .to_owned()
}

fn output_for_name_wgsl_spirv(name: &str, stage: naga::ShaderStage) -> String {
    assert!(name.ends_with(".wgsl"));
    assert!(name.len() > 5);
    let file_name = format!("{}.{}.spirv", &name[..name.len() - 5], stage_name(stage));
    make_output_dirs()
        .join(file_name)
        .to_str()
        .unwrap()
        .to_owned()
}

fn find_included_path(name: &str) -> Result<PathBuf> {
    // This is not the manifest dir, it is the project dir. Walk up to find the libs directory.
    let project_cargo_root = env::var("CARGO_MANIFEST_DIR").unwrap();
    let mut libs_dir = Path::new(&project_cargo_root);
    while libs_dir.file_stem().unwrap() != "crates" {
        libs_dir = libs_dir.parent().expect("non-root");
    }
    assert_eq!(libs_dir.file_stem().expect("non-root"), "crates");
    libs_dir = libs_dir.parent().expect("at-root");

    // The directories that will act as the base for #include<> directives.
    let mut include_dirs = vec![libs_dir.join("crates")];

    // The manifest dir may or may not be in the nitrogen project; nitrogen might instead be
    // linked in under the libs dir. If so, add nitrogen to the includes, so that nitrogen
    // includes are available to the client.
    if libs_dir.join("nitrogen").is_dir() {
        include_dirs.push(libs_dir.join("crates").join("nitrogen"));
    }

    let input_path = &name.split(MAIN_SEPARATOR).collect::<PathBuf>();
    trace!("Using include dirs: {:?}", include_dirs);
    for path in &include_dirs {
        let attempt = path.join(input_path);
        trace!("Checking: {:?}", attempt);
        if attempt.is_file() {
            let resolved_name = attempt.to_str().expect("a path").to_owned();
            println!("cargo:rerun-if-changed={resolved_name}");
            return Ok(attempt);
        }
    }
    Err(anyhow!("did not find included file"))
}

pub fn build() -> Result<()> {
    env_logger::init();
    println!("cargo:rerun-if-env-changed=DUMP_SPIRV");
    println!("cargo:rerun-if-env-changed=DEBUG");
    let shaders_dir = env::current_dir()?.as_path().join("shaders");
    if shaders_dir.is_dir() {
        println!(
            "cargo:rerun-if-changed={}/",
            shaders_dir.to_str().expect("a path")
        );
    }
    let include_dir = env::current_dir()?.as_path().join("include");
    if include_dir.is_dir() {
        println!(
            "cargo:rerun-if-changed={}/",
            include_dir.to_str().expect("a path")
        );
    }

    let shader_dir = Path::new("shaders/");
    if !shader_dir.is_dir() {
        return Ok(());
    }

    for entry in fs::read_dir(shader_dir)? {
        let entry = entry?;
        let pathbuf = entry.path();
        if !pathbuf.is_file() {
            continue;
        }
        let dump_spirv = env::var("DUMP_SPIRV").unwrap_or_else(|_| "0".to_owned()) == "1";
        build_wgsl_from_path(&pathbuf, dump_spirv)?;
    }

    Ok(())
}

static INCLUDE_REGEX: Lazy<Regex> = Lazy::new(|| Regex::new(r"//!include\s*<(.*\.wgsl)>").unwrap());
static BINDINGS_REGEX: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"//!bindings\s*\[(\d+)\]\s+<([^\s]+)>").unwrap());
static BINDING_OFFSET: Lazy<Mutex<HashMap<u32, u32>>> = Lazy::new(|| Mutex::new(HashMap::new()));
pub fn read_wgsl_shader(path: &Path) -> Result<String> {
    let raw_content = fs::read_to_string(path)?;

    let bound_content = BINDINGS_REGEX.replace_all(&raw_content, move |cap: &regex::Captures| {
        let group = cap[1].parse().expect("a number");
        let binding_offset = *BINDING_OFFSET.lock().unwrap().entry(group).or_default();
        let module_path = &cap[2];
        let (s, binding_cnt) = bindings_for(group, binding_offset, module_path).unwrap();
        BINDING_OFFSET
            .lock()
            .unwrap()
            .entry(group)
            .and_modify(|cnt| *cnt += binding_cnt);
        s
    });

    let flat_content = INCLUDE_REGEX.replace_all(&bound_content, move |cap: &regex::Captures| {
        let rel_name = &cap[1];
        let inc_path = find_included_path(rel_name).unwrap();
        read_wgsl_shader(&inc_path).unwrap()
    });

    Ok(flat_content.to_string())
}

pub fn build_wgsl_from_path(path: &Path, dump_spirv: bool) -> Result<()> {
    ensure!(path.extension().unwrap() == "wgsl");
    *BINDING_OFFSET.lock().unwrap() = HashMap::new();

    let shader_content = read_wgsl_shader(path)?;
    let path_str = path.file_name().unwrap().to_str().unwrap();
    fs::write(output_for_wgsl(path_str), shader_content.as_bytes())?;
    println!(
        "cargo:rerun-if-changed={}",
        path.canonicalize()?.to_string_lossy()
    );

    let mut parser = naga::front::wgsl::Frontend::new();
    let module = parser.parse(&shader_content)?;
    let mut validator = naga::valid::Validator::new(
        naga::valid::ValidationFlags::all(),
        naga::valid::Capabilities::all(),
    );
    let info = validator.validate(&module)?;
    if dump_spirv {
        for entry in &module.entry_points {
            let mut back = naga::back::spv::Writer::new(&naga::back::spv::Options::default())?;
            let mut out = Vec::new();
            back.write(
                &module,
                &info,
                Some(&naga::back::spv::PipelineOptions {
                    shader_stage: entry.stage,
                    entry_point: entry.name.clone(),
                }),
                &Some(naga::back::spv::DebugInfo {
                    source_code: &shader_content,
                    file_name: path,
                }),
                &mut out,
            )?;

            let target_path = output_for_name_wgsl_spirv(
                path.file_name()
                    .expect("a file name")
                    .to_str()
                    .expect("a string"),
                entry.stage,
            );
            fs::write(&target_path, out.as_bytes())?;

            let dot_content = naga::back::dot::write(
                &module,
                Some(&info),
                naga::back::dot::Options { cfg_only: false },
            )?;
            fs::write(format!("{target_path}.module"), format!("{module:#?}"))?;
            fs::write(format!("{target_path}.dot"), dot_content)?;
        }
    }

    Ok(())
}
