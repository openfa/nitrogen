// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
mod detail;
mod generic_event;
mod gpu;
mod window;

// Exports
pub use crate::{
    detail::{CpuDetailLevel, DetailLevelOpts, GpuDetailLevel},
    generic_event::{
        test_make_input_events, InputEvent, InputEvents, MouseAxis, SystemEvent, SystemEvents,
    },
    gpu::{
        texture_format_sample_type, texture_format_size, BindingCollection, BindingsBuilder,
        CurrentEncoder, CurrentFrame, Gpu, GpuStep, LayoutBuilder, LayoutProvider, RenderConfig,
    },
    window::{DisplayConfig, DisplayOpts, Window},
};

// Re-exports
pub use winit::dpi::PhysicalSize;

use crate::generic_event::{
    wrap_event, GlobalInputState, InputEventVec, InputMethodErrata, SystemEventVec,
};
use anyhow::Result;
use log::{info, trace};
use runtime::{Runtime, TimeStep};
use std::sync::Arc;
#[cfg(not(target_arch = "wasm32"))]
use std::{backtrace::Backtrace, panic, sync::Mutex};
use winit::{
    dpi::LogicalSize,
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop, EventLoopBuilder},
    window::{Window as OsWindow, WindowBuilder as OsWindowBuilder},
};

#[cfg(not(target_arch = "wasm32"))]
static PANIC_CAPTURE: Mutex<Option<(String, Backtrace)>> = Mutex::new(None);

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum MetaEvent {
    Stop,
}

/// System bits that cannot be easily modularized, or at least where the
/// effort distracts from the core mission. This storage is main-thread-only
/// and the entry point is the only async entry point until we have the
/// bevy task framework available.
pub struct Core {}

impl Core {
    #[cfg(target_arch = "wasm32")]
    fn platform_pre_init() {
        console_error_panic_hook::set_once();
        console_log::init_with_level(log::Level::Trace).expect("Couldn't initialize logger");
    }

    #[cfg(not(target_arch = "wasm32"))]
    fn platform_pre_init() {
        env_logger::init();
    }

    #[cfg(target_arch = "wasm32")]
    fn platform_window_hacks(
        os_window: Arc<OsWindow>,
        _event_loop: &mut EventLoop<MetaEvent>,
        _input_state: &mut GlobalInputState,
    ) {
        // Cannot use css for this part. Eventually we'll get size
        // events from winit that will let us hug the window.
        let _ = os_window.request_inner_size(winit::dpi::PhysicalSize::new(640, 480));

        use winit::platform::web::WindowExtWebSys;
        let canvas = os_window.canvas().expect("missing canvas");
        canvas.style().set_css_text("background-color: #0061cf;");

        web_sys::window()
            .expect("no window")
            .document()
            .expect("no document")
            .get_element_by_id("holder")
            .expect("did not find 'holder' element")
            .append_child(&web_sys::Element::from(canvas))
            .expect("failed to append child to 'holder'");
    }

    #[cfg(unix)]
    #[cfg(not(target_arch = "wasm32"))]
    fn platform_window_hacks(
        _os_window: Arc<OsWindow>,
        _event_loop: &mut EventLoop<MetaEvent>,
        _input_state: &mut GlobalInputState,
    ) {
        use winit::platform::wayland::EventLoopWindowTargetExtWayland;
        if _event_loop.is_wayland() {
            _input_state.add_errata(InputMethodErrata::UseWindowScrollEvents);
            _input_state.add_errata(InputMethodErrata::InvertVerticalScrollEvents);
        }
        use winit::platform::x11::EventLoopWindowTargetExtX11;
        if _event_loop.is_x11() {
            _input_state.add_errata(InputMethodErrata::InvertVerticalScrollEvents);
        }
    }

    #[cfg(windows)]
    #[cfg(not(target_arch = "wasm32"))]
    fn platform_window_hacks(
        _os_window: Arc<OsWindow>,
        _event_loop: &mut EventLoop<MetaEvent>,
        _input_state: &mut GlobalInputState,
    ) {
        _input_state.add_errata(InputMethodErrata::InvertVerticalScrollEvents);
    }

    /// Does the following for us:
    ///   * Initializes logging and other very low level capabilities
    ///   * Creates the event loop, window, and gpu device
    ///   * Creates a Runtime and calls the setup function to populate it
    ///   * Installs core Gpu, Window, Input types on the runtime
    ///   * Causes the the frame function to be called each frame
    ///   * Returns
    pub async fn boot<O, M>(
        display_opts: DisplayOpts,
        opt: O,
        window_title: &str,
        setup_runtime: M,
    ) -> Result<()>
    where
        O: Clone + Send + Sync + 'static,
        M: 'static + Send + FnMut(&mut Runtime, O) -> Result<()>,
    {
        #[cfg(target_arch = "wasm32")]
        {
            Self::boot_inner(display_opts, opt, window_title, setup_runtime).await
        }
        #[cfg(not(target_arch = "wasm32"))]
        {
            // Safety: We're not going to stick around after a panic, we just want
            //         to know when we unwound so that we can report the stack we
            //         captured in the panic handler.
            panic::set_hook(Box::new(|info| {
                *PANIC_CAPTURE.lock().unwrap() = Some((info.to_string(), Backtrace::capture()));
            }));
            let panout = panic::catch_unwind(panic::AssertUnwindSafe(|| {
                pollster::block_on(Self::boot_inner(
                    display_opts,
                    opt,
                    window_title,
                    setup_runtime,
                ))
            }));
            panout.unwrap_or_else(|e| {
                let mut capture = None;
                std::mem::swap(&mut *PANIC_CAPTURE.lock().unwrap(), &mut capture);
                let message = match capture {
                    None => {
                        format!(
                            "failed to capture panic, all we have is: {}",
                            e.downcast::<&str>().unwrap_or_else(|_| Box::new("nothing"))
                        )
                    }
                    Some((message, bt)) => {
                        println!("{bt}");
                        message
                    }
                };
                println!("{message}");
                msgbox::create(
                    "Don't Panic!",
                    &format!("{window_title} encountered more errors than it was prepared to handle.\n{message}"),
                    msgbox::IconType::Error,
                ).ok();
                Ok(())
            })
        }
    }

    async fn boot_inner<O, M>(
        display_opts: DisplayOpts,
        opt: O,
        window_title: &str,
        mut setup_runtime: M,
    ) -> Result<()>
    where
        O: Clone + Send + Sync + 'static,
        M: 'static + Send + FnMut(&mut Runtime, O) -> Result<()>,
    {
        Self::platform_pre_init();
        trace!("pre-init");

        let mut input_state = GlobalInputState::new();
        let mut event_loop_builder = EventLoopBuilder::<MetaEvent>::with_user_event();
        #[cfg(unix)]
        {
            use winit::platform::x11::EventLoopBuilderExtX11;
            event_loop_builder.with_any_thread(true);
        }
        #[cfg(windows)]
        {
            use winit::platform::windows::EventLoopBuilderExtWindows;
            event_loop_builder.with_any_thread(true);
        }
        let mut event_loop = event_loop_builder.build()?;
        let os_window = Arc::new(
            OsWindowBuilder::new()
                .with_title(window_title)
                .with_inner_size(LogicalSize::new(1280, 720))
                .build(&event_loop)?,
        );
        Self::platform_window_hacks(os_window.clone(), &mut event_loop, &mut input_state);
        let mut proxy = event_loop.create_proxy();
        trace!("system up");

        // Create and set up the runtime
        let mut runtime = Runtime::new()?;
        let mut window = Window::new(&display_opts, os_window.clone());

        let gpu = Gpu::new(
            &mut runtime,
            os_window.clone(),
            &mut window,
            display_opts.use_web_limits(),
        )
        .await?;

        runtime.inject_resource(InputEvents::default())?;
        runtime.inject_resource(SystemEvents::default())?;
        runtime.inject_resource(window)?;
        runtime.inject_resource(gpu)?;
        runtime.load_extension::<TimeStep>()?;
        setup_runtime(&mut runtime, opt)?;
        trace!("runtime up");

        // Run startup systems
        runtime.run_startup();

        // Kick off the event loop
        event_loop.run(move |event, target| {
            target.set_control_flow(ControlFlow::Poll);
            if event == Event::UserEvent(MetaEvent::Stop) {
                trace!("user event stop");
                target.exit();
                return;
            }

            // Capture this event into the changes to propagate with the next frame.
            Self::process_event(&event, &mut input_state, os_window.clone(), &mut runtime);

            // Delegate frame processing.
            if matches!(
                event,
                Event::WindowEvent {
                    event: WindowEvent::RedrawRequested,
                    ..
                }
            ) {
                // Note: we do simulation _before_ showing the current simulation state.
                // Thus, we need to step our frame time before actually beginning hte frame
                // systems properly. Sim stepping otoh happens inline as a normal system.
                runtime
                    .resource_mut::<TimeStep>()
                    .advance_frame_times_one_step();

                // Catch monotonic sim time up to frame time (compression is accounted for
                // when stepping the frame times above).
                while runtime.resource::<TimeStep>().need_sim_step() {
                    runtime.run_sim_once();
                    runtime.resource_mut::<InputEvents>().clear();
                    runtime.resource_mut::<SystemEvents>().clear();
                }

                // Display a frame
                runtime.run_frame_once();

                // Clear per frame communication resources.
                runtime
                    .resource_mut::<Window>()
                    .process_per_frame(os_window.clone(), &mut proxy);
            }
        })?;
        trace!("leaving boot");
        Ok(())
    }

    fn process_event(
        event: &Event<MetaEvent>,
        input_state: &mut GlobalInputState,
        os_window: Arc<OsWindow>,
        runtime: &mut Runtime,
    ) {
        let mut input_events = InputEventVec::new();
        let mut system_events = SystemEventVec::new();
        wrap_event(event, input_state, &mut input_events, &mut system_events);
        for event in &system_events {
            match event {
                SystemEvent::ScaleFactorChanged { scale } => {
                    runtime
                        .resource_mut::<Window>()
                        .set_window_scale_factor(*scale);
                }
                SystemEvent::WindowResized { width, height } => {
                    Self::on_window_resized(*width, *height, os_window.clone(), runtime);
                }
                SystemEvent::Quit => {
                    runtime.resource_mut::<Window>().request_quit();
                }
            }
        }
        runtime.resource_mut::<InputEvents>().append(input_events);
        runtime.resource_mut::<SystemEvents>().append(system_events);
    }

    fn on_window_resized(width: u32, height: u32, os_window: Arc<OsWindow>, runtime: &mut Runtime) {
        info!(
            "received resize event: {}x{}; cached: {}x{}",
            width,
            height,
            os_window.inner_size().width,
            os_window.inner_size().height,
        );
        let new_size = PhysicalSize { width, height };

        // On X11 (maybe others?), the w/h pair we get in the change event maybe has not
        // made it to / been fully processed by, the window, so try to make sure the window
        // knows what size the window is. :facepalm:
        #[cfg(unix)]
        {
            let _ = os_window.request_inner_size(new_size);
        }

        // note: the OS doesn't always give us the option to set the exact window size,
        // so use whatever is real, regardless of what happened above. It is possible
        // (AwesomeWM, X11) that the size change event reflects the full usable area
        // and not the ultimate client size, in which case using the new numbers passed
        // in the change event will cause us to resize every frame. :facepalm:
        #[cfg(unix)]
        {
            let new_size = os_window.inner_size();
            info!(
                "after resize, size is: {}x{}",
                new_size.width, new_size.height
            );
        }

        runtime.resource_mut::<Window>().set_window_size(new_size);
    }

    #[cfg(not(target_arch = "wasm32"))]
    pub fn for_test() -> Result<Runtime> {
        use pollster::FutureExt as _;

        Self::platform_pre_init();
        trace!("pre-init");

        let mut input_state = GlobalInputState::new();
        let mut event_loop_builder = EventLoopBuilder::<MetaEvent>::with_user_event();
        #[cfg(unix)]
        {
            use winit::platform::{
                wayland::EventLoopBuilderExtWayland, x11::EventLoopBuilderExtX11,
            };
            EventLoopBuilderExtWayland::with_any_thread(&mut event_loop_builder, true);
            EventLoopBuilderExtX11::with_any_thread(&mut event_loop_builder, true);
        }
        #[cfg(windows)]
        {
            use winit::platform::windows::EventLoopBuilderExtWindows;
            event_loop_builder.with_any_thread(true);
        }
        let mut event_loop = event_loop_builder.build()?;
        let os_window = Arc::new(
            OsWindowBuilder::new()
                .with_title("Core::for_test")
                .build(&event_loop)?,
        );
        Self::platform_window_hacks(os_window.clone(), &mut event_loop, &mut input_state);
        trace!("system up");

        // Create and set up the runtime
        let mut runtime = Runtime::new()?;
        let mut window = Window::new(&DisplayOpts::default(), os_window.clone());
        let gpu = Gpu::new(&mut runtime, os_window, &mut window, false).block_on()?;
        runtime.inject_resource(InputEvents::default())?;
        runtime.inject_resource(SystemEvents::default())?;
        runtime.inject_resource(window)?;
        runtime.inject_resource(gpu)?;
        runtime.load_extension::<TimeStep>()?;
        trace!("runtime up");

        // Run startup systems
        runtime.run_startup();

        Ok(runtime)
    }
}

#[cfg(test)]
mod tests {
    #[cfg(not(windows))]
    use super::*;

    #[cfg(not(windows))]
    #[test]
    fn test_core_boot() -> Result<()> {
        futures::executor::block_on(Core::boot(
            DisplayOpts::default(),
            (),
            "",
            |runtime: &mut Runtime, ()| -> Result<()> {
                runtime.resource_mut::<Window>().request_quit();
                Ok(())
            },
        ))?;
        Ok(())
    }
}
