// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::MetaEvent;
use anyhow::bail;
use nitrous::{inject_nitrous_resource, method, NitrousResource};
use std::{str::FromStr, sync::Arc};
use structopt::StructOpt;
use winit::{
    dpi::PhysicalSize,
    event_loop::EventLoopProxy,
    monitor::{MonitorHandle, VideoMode},
    window::{CursorIcon, Window as OsWindow},
};

#[derive(Clone, Debug, Default, StructOpt)]
pub struct DisplayOpts {
    /// Set the render width
    #[structopt(short, long)]
    width: Option<u32>,

    /// Set the render height
    #[structopt(short, long)]
    height: Option<u32>,

    /// Scale rendering resolution
    #[structopt(short, long)]
    scale: Option<f64>,

    /// Select how we output
    #[structopt(short, long)]
    display_mode: Option<DisplayMode>,

    #[cfg(not(target_arch = "wasm32"))]
    /// Opt in to web rendering mode on native
    #[structopt(long)]
    web_limits: bool,
}

impl DisplayOpts {
    #[cfg(not(target_arch = "wasm32"))]
    pub fn use_web_limits(&self) -> bool {
        self.web_limits
    }

    #[cfg(target_arch = "wasm32")]
    pub fn use_web_limits(&self) -> bool {
        true
    }
}

/// Fullscreen or windowed and how to do that.
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum DisplayMode {
    /// Render: whatever size the window is right now (scaled by render scaling)
    /// Window: don't change what the OS gives us
    /// Monitor: leave alone
    Windowed,

    /// Render: at the specified size (scaled by render scaling)
    /// Window: Attempt to make the window cover the full screen, but don't be
    ///         obnoxious about it. Only present configuration options for resolution
    ///         that match the aspect ratio of the monitor. If there is a mismatch at
    ///         runtime, letterbox as appropriate.
    /// Monitor: leave alone
    Fullscreen,

    /// Render: at the specified size (scaled by render scaling)
    /// Window: Attempt to cover the full screen; be obnoxious about it to be
    ///         successful more often on common platforms. Only show configuration
    ///         options for resolutions that the monitor supports.
    /// Monitor: Resize to the indicated size. If the provided dimensions are not
    ///          supported by the monitor, fall back to SoftFullscreen transparently.
    Exclusive,
}

impl DisplayMode {
    fn to_string(self) -> &'static str {
        match self {
            Self::Windowed => "windowed",
            Self::Fullscreen => "fullscreen",
            Self::Exclusive => "exclusive",
        }
    }
}

impl FromStr for DisplayMode {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "windowed" | "window" => Self::Windowed,
            "fullscreen" | "full" => Self::Fullscreen,
            "exclusive_fullscreen" | "exclusive" => Self::Exclusive,
            _ => bail!("unrecognized display mode"),
        })
    }
}

#[derive(Clone, Debug)]
pub struct DisplayConfig {
    // Determines how we reflect our config on the system and vice versa, at a very high level.
    display_mode: DisplayMode,

    // This is the actual, current window size (as best we are able to tell). Requests for scaling
    // are generally provided by the configured render_extent.
    window_size: PhysicalSize<u32>,

    // The requested "render" dimensions. This is scaled by render_scale to produce the actual
    // buffers that we render to, but the base values are important as those are the requested
    // apparent size on the monitor.
    base_render_extent: PhysicalSize<u32>,

    // Decouples the resolution from the window and monitor.
    render_scale: f64,

    // Relevant for font rendering.
    dpi_scale_factor: f64,
}

impl DisplayConfig {
    pub fn discover(opt: &DisplayOpts, os_window: Arc<OsWindow>) -> Self {
        // FIXME: use a better default display mode
        let display_mode = opt.display_mode.unwrap_or(DisplayMode::Windowed);

        // if the window mode is selected, use it's extent
        let base_render_extent = if display_mode == DisplayMode::Windowed {
            os_window.inner_size()
        } else if let Some(width) = opt.width {
            if let Some(height) = opt.height {
                PhysicalSize::new(width, height)
            } else {
                PhysicalSize::new(width, width * 9 / 16)
            }
        } else if let Some(height) = opt.height {
            PhysicalSize::new(height * 16 / 9, height)
        } else if let Some(monitor) = os_window.current_monitor() {
            if display_mode != DisplayMode::Windowed {
                monitor.size()
            } else {
                PhysicalSize::new(1280, 720)
            }
        } else {
            PhysicalSize::new(1280, 720)
        };

        Self {
            display_mode,
            window_size: os_window.inner_size(),
            base_render_extent,
            render_scale: opt.scale.unwrap_or(1.0),
            dpi_scale_factor: os_window.scale_factor(),
        }
    }

    pub fn for_web() -> Self {
        Self {
            display_mode: DisplayMode::Windowed,
            window_size: PhysicalSize::new(1024, 768),
            base_render_extent: PhysicalSize::new(1024, 768),
            render_scale: 1.0,
            dpi_scale_factor: 1.0,
        }
    }

    pub fn for_test() -> Self {
        Self {
            display_mode: DisplayMode::Windowed,
            window_size: PhysicalSize::new(1920, 1080),
            base_render_extent: PhysicalSize::new(1920, 1080),
            render_scale: 1.0,
            dpi_scale_factor: 1.2,
        }
    }

    pub fn for_xr_mirror(window_size: PhysicalSize<u32>) -> Self {
        Self {
            display_mode: DisplayMode::Windowed,
            window_size,
            base_render_extent: window_size,
            render_scale: 1.0,
            dpi_scale_factor: 1.2,
        }
    }

    pub fn window_size(&self) -> PhysicalSize<u32> {
        self.window_size
    }

    pub fn dpi_scale_factor(&self) -> f64 {
        self.dpi_scale_factor
    }

    pub fn base_render_extent(&self) -> PhysicalSize<u32> {
        self.base_render_extent
    }

    pub fn render_extent(&self) -> PhysicalSize<u32> {
        if matches!(self.display_mode, DisplayMode::Windowed) {
            PhysicalSize::new(
                (self.window_size.width as f64 * self.render_scale).floor() as u32,
                (self.window_size.height as f64 * self.render_scale).floor() as u32,
            )
        } else {
            unimplemented!()
        }
    }

    pub(crate) fn set_window_size(&mut self, new_size: PhysicalSize<u32>) {
        self.window_size = new_size;
        if matches!(self.display_mode, DisplayMode::Windowed) {
            self.base_render_extent = new_size;
        }
    }

    pub(crate) fn set_window_scale_factor(&mut self, new_scale_factor: f64) {
        self.dpi_scale_factor = new_scale_factor;
    }
}

#[derive(Clone, Copy, Debug)]
enum WindowRequest {
    Quit,
    SetCursorVisible(bool),
    SetCursorIcon(CursorIcon),
}

#[derive(Debug, NitrousResource)]
pub struct Window {
    // If the mode state changed, this will be Some for the duration of the next frame.
    updated_config: Option<DisplayConfig>,

    // Queue requests up for the main thread
    request_queue: Vec<WindowRequest>,

    // Cache of properties for off-main-thread access
    cache_config: DisplayConfig,
    cache_cursor_visible: bool,
    cache_cursor_icon: CursorIcon,

    // Store possible configurations for GUI
    _exclusive_modes: Vec<VideoMode>,
    _borderless_monitors: Vec<MonitorHandle>,
}

#[inject_nitrous_resource]
impl Window {
    pub(crate) fn new(opt: &DisplayOpts, os_window: Arc<OsWindow>) -> Self {
        Self {
            cache_config: DisplayConfig::discover(opt, os_window),
            updated_config: None,

            request_queue: Vec::new(),

            cache_cursor_visible: true,
            cache_cursor_icon: CursorIcon::Default,

            _exclusive_modes: Vec::new(),
            _borderless_monitors: Vec::new(),
        }
    }

    pub(crate) fn process_per_frame(
        &mut self,
        os_window: Arc<OsWindow>,
        proxy: &mut EventLoopProxy<MetaEvent>,
    ) {
        for req in self.request_queue.drain(..) {
            match req {
                WindowRequest::Quit => proxy.send_event(MetaEvent::Stop).expect("event loop died"),
                WindowRequest::SetCursorVisible(visible) => os_window.set_cursor_visible(visible),
                WindowRequest::SetCursorIcon(icon) => os_window.set_cursor_icon(icon),
            }
        }
        self.updated_config = None;
        os_window.request_redraw();
    }

    pub(crate) fn set_window_size(&mut self, new_size: PhysicalSize<u32>) {
        self.cache_config.set_window_size(new_size);
        self.updated_config = Some(self.cache_config.clone());
    }

    pub(crate) fn set_window_scale_factor(&mut self, new_scale_factor: f64) {
        self.cache_config.set_window_scale_factor(new_scale_factor);
        self.updated_config = Some(self.cache_config.clone());
    }

    pub fn updated_config(&self) -> &Option<DisplayConfig> {
        &self.updated_config
    }

    pub fn config(&self) -> &DisplayConfig {
        &self.cache_config
    }

    /* xr test path
    pub(crate) fn set_display_config(&mut self, config: DisplayConfig) {
        self.cache_config = config.clone();
        self.updated_config = Some(config);
    }
     */

    // Quit
    #[method]
    pub fn request_quit(&mut self) {
        self.request_queue.push(WindowRequest::Quit);
    }

    #[method]
    pub fn scale_factor(&self) -> f64 {
        self.cache_config.dpi_scale_factor
    }

    pub fn window_size(&self) -> PhysicalSize<u32> {
        self.cache_config.window_size
    }

    pub fn render_extent(&self) -> PhysicalSize<u32> {
        self.cache_config.render_extent()
    }

    #[method]
    pub fn width(&self) -> i64 {
        self.cache_config.window_size.width as i64
    }

    #[method]
    pub fn height(&self) -> i64 {
        self.cache_config.window_size.height as i64
    }

    #[method]
    pub fn display_mode(&self) -> String {
        self.cache_config.display_mode.to_string().to_owned()
    }

    // Cursor
    pub fn set_cursor_visible(&mut self, visible: bool) {
        self.request_queue
            .push(WindowRequest::SetCursorVisible(visible));
    }

    pub fn set_cursor_icon(&mut self, icon: CursorIcon) {
        self.request_queue.push(WindowRequest::SetCursorIcon(icon));
    }

    #[method]
    pub fn cursor_visible(&self) -> bool {
        self.cache_cursor_visible
    }

    pub fn cursor_icon(&self) -> CursorIcon {
        self.cache_cursor_icon
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Core;
    use anyhow::Result;

    #[test]
    fn test_window_exists() -> Result<()> {
        let runtime = Core::for_test()?;
        assert!(runtime.resource::<Window>().width() > 0);
        Ok(())
    }
}
