// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::{DisplayConfig, Window};
use anyhow::{anyhow, bail, Result};
use bevy_ecs::prelude::*;
use log::{info, trace};
use nitrous::{inject_nitrous_resource, method, NitrousResource};
use runtime::Runtime;
use shader_shared::binding;
use std::{mem, sync::Arc};
use wgpu::{util::DeviceExt, ShaderSource};
use winit::window::Window as OsWindow;
use zerocopy::{Immutable, IntoBytes};
// use ash::vk;
// use openxr as xr;

#[derive(Debug)]
pub struct RenderConfig {
    present_mode: wgpu::PresentMode,
}

impl Default for RenderConfig {
    fn default() -> Self {
        Self {
            present_mode: wgpu::PresentMode::AutoNoVsync,
        }
    }
}

#[derive(Default, NitrousResource)]
pub struct CurrentFrame {
    maybe_surface: Option<wgpu::SurfaceTexture>,
}

#[inject_nitrous_resource]
impl CurrentFrame {
    pub fn get_surface(&self) -> Option<&wgpu::SurfaceTexture> {
        self.maybe_surface.as_ref()
    }

    pub fn update_surface(&mut self, surface: Option<wgpu::SurfaceTexture>) {
        self.maybe_surface = surface;
    }

    pub fn present(&mut self) {
        let mut empty_surface = None as Option<wgpu::SurfaceTexture>;
        mem::swap(&mut empty_surface, &mut self.maybe_surface);
        if let Some(surface) = empty_surface {
            surface.present();
        }
    }
}

#[derive(Default, NitrousResource)]
pub struct CurrentEncoder {
    maybe_encoder: Option<wgpu::CommandEncoder>,
}

#[inject_nitrous_resource]
impl CurrentEncoder {
    pub fn get_encoder(&self) -> Option<&wgpu::CommandEncoder> {
        self.maybe_encoder.as_ref()
    }

    pub fn get_encoder_mut(&mut self) -> Option<&mut wgpu::CommandEncoder> {
        self.maybe_encoder.as_mut()
    }

    pub fn set_frame_encoder(&mut self, encoder: wgpu::CommandEncoder) {
        self.maybe_encoder = Some(encoder);
    }

    fn submit(&mut self, queue: &mut wgpu::Queue) {
        let mut empty_encoder = None as Option<wgpu::CommandEncoder>;
        mem::swap(&mut empty_encoder, &mut self.maybe_encoder);
        if let Some(encoder) = empty_encoder {
            queue.submit(vec![encoder.finish()]);
        }
    }
}

pub struct BindingCollection<'b, 'c> {
    bindings: &'b mut Vec<wgpu::BindGroupEntry<'c>>,
}

impl<'b, 'c> BindingCollection<'b, 'c> {
    #[inline]
    pub fn buffer<'a>(&mut self, buffer: &'a wgpu::Buffer)
    where
        'a: 'b,
        'a: 'c,
    {
        let entry = binding::buffer(self.bindings.len(), buffer);
        self.bindings.push(entry);
    }

    #[inline]
    pub fn texture<'a>(&mut self, texture_view: &'a wgpu::TextureView)
    where
        'a: 'b,
        'a: 'c,
    {
        let entry = binding::texture(self.bindings.len(), texture_view);
        self.bindings.push(entry);
    }

    #[inline]
    pub fn sampler<'a>(&mut self, sampler: &'a wgpu::Sampler)
    where
        'a: 'b,
        'a: 'c,
    {
        let entry = binding::sampler(self.bindings.len(), sampler);
        self.bindings.push(entry);
    }
}

/// A type that provides layouts and bindings for rendering.
pub trait LayoutProvider {
    fn push_layout(layouts: &mut Vec<wgpu::BindGroupLayoutEntry>);
    fn push_binding<'a, 'b, 'c>(&'a self, bindings: BindingCollection<'b, 'c>)
    where
        'a: 'b,
        'a: 'c;
}

#[derive(Clone, Debug, Default)]
pub struct LayoutBuilder<'a> {
    label: Option<&'a str>,
    layouts: Vec<wgpu::BindGroupLayoutEntry>,
}

impl<'a> LayoutBuilder<'a> {
    pub fn with_label(mut self, s: &'a str) -> Self {
        self.label = Some(s);
        self
    }

    pub fn with_layout<T: LayoutProvider>(mut self) -> Self {
        T::push_layout(&mut self.layouts);
        self
    }

    pub fn build(self, gpu: &Gpu) -> wgpu::BindGroupLayout {
        gpu.device
            .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                label: self.label,
                entries: &self.layouts,
            })
    }
}

#[derive(Clone, Debug)]
pub struct BindingsBuilder<'a> {
    layout: &'a wgpu::BindGroupLayout,
    label: Option<&'a str>,
    bindings: Vec<wgpu::BindGroupEntry<'a>>,
}

impl<'a> BindingsBuilder<'a> {
    pub fn new(layout: &'a wgpu::BindGroupLayout) -> Self {
        Self {
            layout,
            label: None,
            bindings: Vec::new(),
        }
    }

    pub fn with_label(mut self, s: &'a str) -> Self {
        self.label = Some(s);
        self
    }

    pub fn with_bindings<T: LayoutProvider>(mut self, provider: &'a T) -> Self {
        provider.push_binding(BindingCollection {
            bindings: &mut self.bindings,
        });
        self
    }

    pub fn build(self, gpu: &Gpu) -> wgpu::BindGroup {
        gpu.device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: self.label,
            layout: self.layout,
            entries: &self.bindings,
        })
    }
}

pub fn texture_format_sample_type(texture_format: wgpu::TextureFormat) -> wgpu::TextureSampleType {
    texture_format
        .sample_type(Some(wgpu::TextureAspect::All), None)
        .expect("not a depth texture format")
}

pub fn texture_format_size(texture_format: wgpu::TextureFormat) -> u32 {
    texture_format
        .block_copy_size(Some(wgpu::TextureAspect::All))
        .expect("not a depth texture format")
}

/*
#[allow(unused)]
struct XrSwapchain {
    handle: xr::Swapchain<xr::Vulkan>,
    resolution: vk::Extent2D,
    // buffers: Vec<Texture>,
}

impl std::fmt::Debug for XrSwapchain {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "XrSwapchain{{ {:?} }}", self.resolution)
    }
}

#[allow(unused)]
pub struct XrState {
    xr_instance: xr::Instance,
    environment_blend_mode: xr::EnvironmentBlendMode,
    session: xr::Session<xr::Vulkan>,
    session_running: bool,
    frame_wait: xr::FrameWaiter,
    frame_stream: xr::FrameStream<xr::Vulkan>,
    action_set: xr::ActionSet,
    right_action: xr::Action<xr::Posef>,
    left_action: xr::Action<xr::Posef>,
    right_space: xr::Space,
    left_space: xr::Space,
    stage: xr::Space,
    // event_storage: xr::EventDataBuffer,
    views: Vec<openxr::ViewConfigurationView>,
    swapchain: Option<XrSwapchain>,
}

impl std::fmt::Debug for XrState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "XrState")
    }
}
 */
type XrState = i32;

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum GpuStep {
    HandleDisplayChange,
    CreateTargetSurface,
    CreateCommandEncoder,
    SubmitCommands,
    PresentTargetSurface,
}

#[derive(Debug, NitrousResource)]
pub struct Gpu {
    config: RenderConfig,
    features: wgpu::Features,

    // Core GPU resources
    _instance: wgpu::Instance,
    adapter: wgpu::Adapter,
    device: wgpu::Device,
    queue: wgpu::Queue,

    #[allow(unused)]
    xr: Option<XrState>,

    // Surface for normal rendering.
    os_window: Arc<OsWindow>,
    surface: wgpu::Surface<'static>,
    render_extent: wgpu::Extent3d,
    depth_texture_view: wgpu::TextureView,

    // State
    frame_count: usize,
}

#[inject_nitrous_resource]
impl Gpu {
    pub const DEPTH_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Depth32Float;
    pub const SCREEN_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Bgra8Unorm;

    pub(crate) async fn new(
        runtime: &mut Runtime,
        os_window: Arc<OsWindow>,
        window: &mut Window,
        use_web_limits: bool,
    ) -> Result<Self> {
        let config = RenderConfig::default();

        #[allow(unused_mut)]
        let mut instance_flags = wgpu::InstanceFlags::empty();
        #[cfg(debug_assertions)]
        {
            instance_flags |= wgpu::InstanceFlags::DEBUG | wgpu::InstanceFlags::VALIDATION;
        }

        let physical_size = window.window_size();

        /*
        let (instance, adapter, device, queue, features, surface, xr) =
            if let Ok((instance, adapter, device, queue, features, xr)) =
                Self::xr_render_path_init(use_web_limits)
            {
                // Trigger resize on the window. This should percolate to a display config change.
                // (TODO: what to do for tiling WM that don't allow setting a size?)
                let view = xr.views[0];
                physical_size = winit::dpi::PhysicalSize::new(
                    view.recommended_image_rect_width,
                    view.recommended_image_rect_height,
                );
                os_window.set_resizable(false);
                let _ = os_window.request_inner_size(physical_size);

                window.set_display_config(DisplayConfig::for_xr_mirror(physical_size));

                let surface = instance.create_surface(os_window.clone())?;

                (
                    instance,
                    adapter,
                    device,
                    queue,
                    features,
                    surface,
                    Some(xr),
                )
            } else {
         */
        let xr = None;
        let (instance, adapter, device, queue, features, surface) =
            Self::desktop_render_path_init(os_window.clone(), instance_flags, use_web_limits)
                .await?;
        // (instance, adapter, device, queue, features, surface, None)
        // };

        // The surface must be configured to match the physical window size, in a format
        // that may be different from what we want to work in. Thus we allocate it once
        // here instead of trying to delegate.
        let sc_desc = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: Self::SCREEN_FORMAT,
            width: physical_size.width,
            height: physical_size.height,
            present_mode: config.present_mode,
            desired_maximum_frame_latency: 2,
            alpha_mode: wgpu::CompositeAlphaMode::Auto,
            view_formats: vec![Self::SCREEN_FORMAT],
        };
        surface.configure(&device, &sc_desc);

        let render_size = window.render_extent();
        let render_extent = wgpu::Extent3d {
            width: render_size.width,
            height: render_size.height,
            depth_or_array_layers: 1,
        };

        let depth_texture_view = Self::make_screen_depth(&device, render_extent);

        runtime.add_frame_system(
            Self::sys_handle_display_config_change.in_set(GpuStep::HandleDisplayChange),
        );

        runtime.inject_resource(CurrentFrame::default())?;
        runtime.add_frame_system(
            Self::sys_create_target_surface
                .in_set(GpuStep::CreateTargetSurface)
                .after(GpuStep::HandleDisplayChange),
        );
        runtime.inject_resource(CurrentEncoder::default())?;
        runtime.add_frame_system(
            Self::sys_create_command_encoder
                .in_set(GpuStep::CreateCommandEncoder)
                .after(GpuStep::CreateTargetSurface),
        );
        runtime.add_frame_system(
            Self::sys_submit_frame_commands
                .in_set(GpuStep::SubmitCommands)
                .after(GpuStep::CreateCommandEncoder),
        );
        runtime.add_frame_system(
            Self::sys_present_target_surface
                .in_set(GpuStep::PresentTargetSurface)
                .after(GpuStep::SubmitCommands),
        );

        Ok(Self {
            config,
            features,

            _instance: instance,
            adapter,
            device,
            queue,

            xr,

            os_window,
            surface,
            render_extent,
            depth_texture_view,

            frame_count: 0,
        })
    }

    // Simple initialization of rendering for a desktop
    async fn desktop_render_path_init(
        os_window: Arc<OsWindow>,
        instance_flags: wgpu::InstanceFlags,
        use_web_limits: bool,
    ) -> Result<(
        wgpu::Instance,
        wgpu::Adapter,
        wgpu::Device,
        wgpu::Queue,
        wgpu::Features,
        wgpu::Surface<'static>,
    )> {
        let instance = wgpu::Instance::new(wgpu::InstanceDescriptor {
            backends: wgpu::Backends::PRIMARY,
            flags: instance_flags,
            dx12_shader_compiler: wgpu::Dx12Compiler::Dxc {
                dxil_path: None,
                dxc_path: None,
            },
            gles_minor_version: wgpu::Gles3MinorVersion::Automatic,
        });
        let surface = instance.create_surface(os_window)?;
        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::HighPerformance,
                force_fallback_adapter: false,
                compatible_surface: Some(&surface),
            })
            .await
            .ok_or_else(|| anyhow!("no suitable graphics adapter"))?;

        #[cfg(not(target_arch = "wasm32"))]
        let buf = std::path::PathBuf::from("api_tracing.txt");
        #[cfg(not(target_arch = "wasm32"))]
        let trace_path = Some(buf.as_ref());
        #[cfg(target_arch = "wasm32")]
        let trace_path = None;

        let mut features = adapter.features();
        trace!("adapter features: {:?}", features);

        // Disable footguns.
        features &= !wgpu::Features::MAPPABLE_PRIMARY_BUFFERS;

        // pass a cli argument to opt-into web limits on desktop
        if use_web_limits {
            features = wgpu::Features::all_webgpu_mask() & adapter.features()
            // FIXME: implement workarounds for all the cool stuff we
            //        accidentally, or optimistically, used before
            //        the spec was fully baked.
            // | wgpu::Features::TEXTURE_ADAPTER_SPECIFIC_FORMAT_FEATURES
            ;
        }

        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    label: Some("nitrogen-device"),
                    required_features: features,
                    required_limits: adapter.limits(),
                    // memory_hints: wgpu::MemoryHints::Performance,
                },
                trace_path,
            )
            .await?;

        Ok((instance, adapter, device, queue, features, surface))
    }

    /*
    fn xr_render_path_init(
        _use_web_limits: bool,
    ) -> Result<(
        wgpu::Instance,
        wgpu::Adapter,
        wgpu::Device,
        wgpu::Queue,
        wgpu::Features,
        XrState,
    )> {
        use anyhow::Context;
        use ash::vk::{self, Handle};
        use std::ffi::{c_void, CString},
        use wgpu_hal::{api::Vulkan as V, Api};

        let mut wgpu_features =
            wgpu::Features::MULTIVIEW | wgpu::Features::TEXTURE_ADAPTER_SPECIFIC_FORMAT_FEATURES;
        if use_web_limits {
            wgpu_features = wgpu::Features::all_webgpu_mask() & wgpu_features;
            // FIXME: implement workarounds for all the cool stuff we
            //        accidentally, or optimistically, used before
            //        the spec was fully baked.
            // | wgpu::Features::TEXTURE_ADAPTER_SPECIFIC_FORMAT_FEATURES
        }
        let wgpu_limits = wgpu::Limits {
            max_compute_invocations_per_workgroup: 512,
            ..Default::default()
        };

        let entry = unsafe { xr::Entry::load() }.context("couldn't find the OpenXR loader")?;

        // OpenXR will fail to initialize if we ask for an extension that OpenXR can't provide! So we
        // need to check all our extensions before initializing OpenXR with them. Note that even if the
        // extension is present, it's still possible you may not be able to use it. For example: the
        // hand tracking extension may be present, but the hand sensor might not be plugged in or turned
        // on. There are often additional checks that should be made before using certain features!
        let available_extensions = entry.enumerate_extensions()?;

        // If a required extension isn't present, you want to ditch out here! It's possible something
        // like your rendering API might not be provided by the active runtime. APIs like OpenGL don't
        // have universal support.
        assert!(available_extensions.khr_vulkan_enable2);
        log::debug!("available xr exts: {:#?}", available_extensions);

        let available_layers = entry.enumerate_layers()?;
        log::debug!("available xr layers: {:#?}", available_layers);

        // Initialize OpenXR with the extensions we've found!
        let mut enabled_extensions = xr::ExtensionSet::default();
        enabled_extensions.khr_vulkan_enable2 = true;
        let xr_instance = entry.create_instance(
            &xr::ApplicationInfo {
                application_name: "xr-demon",
                application_version: 0,
                engine_name: "xr-demon",
                engine_version: 0,
            },
            &enabled_extensions,
            &[
                // TODO: seems useful
                // "XR_APILAYER_LUNARG_core_validation"
            ],
        )?;

        // Request a form factor from the device (HMD, Handheld, etc.)
        let xr_system_id = xr_instance.system(xr::FormFactor::HEAD_MOUNTED_DISPLAY)?;

        let instance_props = xr_instance.properties()?;
        let system_props = xr_instance.system_properties(xr_system_id)?;
        log::info!(
            "loaded OpenXR runtime: {} {} {}",
            instance_props.runtime_name,
            instance_props.runtime_version,
            if system_props.system_name.is_empty() {
                "<unnamed>"
            } else {
                &system_props.system_name
            }
        );

        // Check what blend mode is valid for this device (opaque vs transparent displays). We'll just
        // take the first one available!
        let environment_blend_mode = xr_instance.enumerate_environment_blend_modes(
            xr_system_id,
            xr::ViewConfigurationType::PRIMARY_STEREO,
        )?[0];

        // OpenXR wants to ensure apps are using the correct graphics card and Vulkan features and
        // extensions, so the instance and device MUST be set up before Instance::create_session.

        // Vulkan 1.1 guarantees multiview support
        let vk_target_version = vk::make_api_version(0, 1, 1, 0);
        let vk_target_version_xr = xr::Version::new(1, 1, 0);
        let reqs = xr_instance.graphics_requirements::<xr::Vulkan>(xr_system_id)?;
        if vk_target_version_xr < reqs.min_api_version_supported
            || vk_target_version_xr.major() > reqs.max_api_version_supported.major()
        {
            bail!(
                "OpenXR runtime requires Vulkan version > {}, < {}.0.0",
                reqs.min_api_version_supported,
                reqs.max_api_version_supported.major() + 1
            );
        }

        //// This section creates the vulkan instance by calling into OpenXR

        // Load the vulkan entry
        // Why here and not under XR?
        let vk_entry = unsafe { ash::Entry::load() }?;

        let app_name = CString::new("xr-demon")?;
        let vk_app_info = vk::ApplicationInfo::builder()
            .application_name(app_name.as_c_str())
            .application_version(1)
            .engine_name(app_name.as_c_str())
            .engine_version(2)
            .api_version(vk_target_version);

        let flags = wgpu_types::InstanceFlags::empty();
        // let flags = wgpu_types::InstanceFlags::VALIDATION;
        let extensions =
            <V as Api>::Instance::desired_extensions(&vk_entry, vk_target_version, flags)?;
        log::debug!(
            "creating vulkan instance with these extensions: {:#?}",
            extensions
        );
        let extensions_cchar: Vec<_> = extensions.iter().map(|s| s.as_ptr()).collect();

        let vk_instance = unsafe {
            let vk_instance = xr_instance
                .create_vulkan_instance(
                    xr_system_id,
                    std::mem::transmute(vk_entry.static_fn().get_instance_proc_addr),
                    &vk::InstanceCreateInfo::builder()
                        .application_info(&vk_app_info)
                        .enabled_extension_names(&extensions_cchar) as *const _
                        as *const _,
                )
                .context("XR error creating Vulkan instance")?
                .map_err(vk::Result::from_raw)
                .context("Vulkan error creating Vulkan instance")?;
            ash::Instance::load(
                vk_entry.static_fn(),
                vk::Instance::from_raw(vk_instance as _),
            )
        };
        log::debug!("created vulkan instance");

        // The XR system is going to have strong opinions of which VK device we render to, so it provides that for us.
        let vk_physical_device = vk::PhysicalDevice::from_raw(unsafe {
            xr_instance.vulkan_graphics_device(xr_system_id, vk_instance.handle().as_raw() as _)?
                as _
        });

        // Check that the device we got back from the XR system actuall supports XR. Not sure why the XR system
        // doesn't just tell us no?
        let vk_device_properties =
            unsafe { vk_instance.get_physical_device_properties(vk_physical_device) };
        if vk_device_properties.api_version < vk_target_version {
            unsafe { vk_instance.destroy_instance(None) }
            bail!("Vulkan physical device doesn't support version 1.1");
        }

        // Wrap the Vulkan instance into the WGPU Vulkan Instance. This is the low-level hal instance type.
        let wgpu_vk_instance = unsafe {
            <V as Api>::Instance::from_raw(
                vk_entry.clone(),
                vk_instance.clone(),
                vk_target_version,
                0,
                None,
                extensions,
                flags,
                false,
                Some(Box::new(())),
            )?
        };

        // Ask wgpu to get us the adapter for the PhysicalDevice that XR wants us to render with.
        let wgpu_exposed_adapter = wgpu_vk_instance
            .expose_adapter(vk_physical_device)
            .context("failed to expose adapter")?;

        let enabled_extensions = wgpu_exposed_adapter
            .adapter
            .required_device_extensions(wgpu_features);
        // enabled_extensions.push(ash::extensions::khr::Swapchain::name());
        println!("adapter required extensions: {enabled_extensions:#?}");

        let (wgpu_open_device, vk_device_ptr, queue_family_index) = {
            // We turn the "enabled extensions" here into "enabled features", but I'm not seeing swapchain in features?
            // Looks like swapchain is not one of the extensions we check for when enabling features to support those
            // extensions. Swapchain is always force pushed in "required_device_extensions", so is it supposed to be
            // picked up elsewhere?
            let mut enabled_phd_features = wgpu_exposed_adapter
                .adapter
                .physical_device_features(&enabled_extensions, wgpu_features);
            println!("Enabled PHD Features: {:#?}", enabled_phd_features);

            // Find the first graphics queue.
            let family_index = unsafe {
                vk_instance.get_physical_device_queue_family_properties(vk_physical_device)
            }
            .into_iter()
            .enumerate()
            .find_map(|(queue_family_index, info)| {
                if info.queue_flags.contains(vk::QueueFlags::GRAPHICS) {
                    Some(queue_family_index as u32)
                } else {
                    None
                }
            })
            .expect("Vulkan device has no graphics queue");
            let family_info = vk::DeviceQueueCreateInfo::builder()
                .queue_family_index(family_index)
                .queue_priorities(&[1.0])
                .build();
            let family_infos = [family_info];
            println!("Selected queue {}: {:#?}", family_index, family_info);

            let c_enabled_extensions = enabled_extensions
                .iter()
                .map(|s| s.as_ptr())
                .collect::<Vec<_>>();

            // Now we craft the infos blob, but this has lost the request for swapchain, so how are device extensions
            // supposed to get put into info? There's definately a place for it. Is it getting initialized at all?
            let info = enabled_phd_features
                .add_to_device_create_builder(
                    vk::DeviceCreateInfo::builder()
                        .queue_create_infos(&family_infos)
                        // This enables multiview, but do we need the swapchain?
                        .push_next(&mut vk::PhysicalDeviceMultiviewFeatures {
                            multiview: vk::TRUE,
                            ..Default::default()
                        })
                        // No, we also need to set enabled extensions here.
                        .enabled_extension_names(&c_enabled_extensions),
                )
                .build();
            // Yeah, extensions are null here, so where do we put those?
            println!("vk_device create info for xr: {:#?}", info);

            let vk_device = unsafe {
                // Ask XR to get us a device now, passing in the PhysicalDevice and "info".
                // Presumably we're passing useful stuff in in info about what we want to do in XR land on this device?
                let vk_device = xr_instance
                    .create_vulkan_device(
                        xr_system_id,
                        std::mem::transmute(vk_entry.static_fn().get_instance_proc_addr),
                        vk_physical_device.as_raw() as _,
                        &info as *const _ as *const _,
                    )
                    .context("XR error creating Vulkan device")?
                    .map_err(vk::Result::from_raw)
                    .context("Vulkan error creating Vulkan device")?;

                ash::Device::load(vk_instance.fp_v1_0(), vk::Device::from_raw(vk_device as _))
            };
            let vk_device_ptr = vk_device.handle().as_raw() as *const c_void;

            // This gets us the wgpu-hal::Device::<Vulkan>.
            // TODO: do we need to add swapchain here somehow?
            // extensions.push(ash::extensions::khr::Swapchain::name());
            let wgpu_open_device = unsafe {
                // This call with call Swapchain::new and get the wrong result...
                wgpu_exposed_adapter.adapter.device_from_raw(
                    vk_device,
                    true,
                    &enabled_extensions,
                    wgpu_features,
                    family_info.queue_family_index,
                    0,
                )
            }?;

            (
                wgpu_open_device,
                vk_device_ptr,
                family_info.queue_family_index,
            )
        };

        let wgpu_instance =
            unsafe { wgpu::Instance::from_hal::<wgpu_hal::api::Vulkan>(wgpu_vk_instance) };
        let wgpu_adapter = unsafe { wgpu_instance.create_adapter_from_hal(wgpu_exposed_adapter) };
        let (wgpu_device, wgpu_queue) = unsafe {
            wgpu_adapter.create_device_from_hal(
                wgpu_open_device,
                &wgpu::DeviceDescriptor {
                    label: Some("demon-device-desc"),
                    required_features: wgpu_features,
                    required_limits: wgpu_limits,
                },
                None,
            )
        }?;

        let vk_instance_ptr = vk_instance.handle().as_raw() as *const c_void;
        let vk_physical_device_ptr = vk_physical_device.as_raw() as *const c_void;
        let (session, frame_wait, frame_stream) = unsafe {
            xr_instance.create_session::<xr::Vulkan>(
                xr_system_id,
                &xr::vulkan::SessionCreateInfo {
                    instance: vk_instance_ptr,
                    physical_device: vk_physical_device_ptr,
                    device: vk_device_ptr,
                    queue_family_index,
                    queue_index: 0,
                },
            )
        }?;
        let action_set = xr_instance.create_action_set("input", "input pose information", 0)?;
        let right_action =
            action_set.create_action::<xr::Posef>("right_hand", "Right Hand Controller", &[])?;
        let left_action =
            action_set.create_action::<xr::Posef>("left_hand", "Left Hand Controller", &[])?;
        xr_instance.suggest_interaction_profile_bindings(
            xr_instance.string_to_path("/interaction_profiles/khr/simple_controller")?,
            &[
                xr::Binding::new(
                    &right_action,
                    xr_instance.string_to_path("/user/hand/right/input/grip/pose")?,
                ),
                xr::Binding::new(
                    &left_action,
                    xr_instance.string_to_path("/user/hand/left/input/grip/pose")?,
                ),
            ],
        )?;
        session.attach_action_sets(&[&action_set])?;
        let right_space =
            right_action.create_space(session.clone(), xr::Path::NULL, xr::Posef::IDENTITY)?;
        let left_space =
            left_action.create_space(session.clone(), xr::Path::NULL, xr::Posef::IDENTITY)?;
        let stage =
            session.create_reference_space(xr::ReferenceSpaceType::STAGE, xr::Posef::IDENTITY)?;

        let views = xr_instance.enumerate_view_configuration_views(
            xr_system_id,
            xr::ViewConfigurationType::PRIMARY_STEREO,
        )?;
        assert_eq!(views.len(), 2);
        assert_eq!(views[0], views[1]);

        let xr_state = XrState {
            xr_instance,
            environment_blend_mode,
            session,
            session_running: false,
            frame_wait,
            frame_stream,
            action_set,
            right_action,
            left_action,
            right_space,
            left_space,
            stage,
            //event_storage: xr::EventDataBuffer::new(),
            views,
            swapchain: None,
        };
        Ok((
            wgpu_instance,
            wgpu_adapter,
            wgpu_device,
            wgpu_queue,
            wgpu_features,
            xr_state,
        ))
    }
     */

    #[method]
    pub fn info(&self) -> String {
        let info = self.adapter.get_info();
        format!(
            "Name: {}\nVendor: {}\nDevice: {:?} {}\nBackend: {:?}",
            info.name, info.vendor, info.device_type, info.device, info.backend
        )
    }

    #[method]
    pub fn name(&self) -> String {
        self.adapter.get_info().name
    }

    #[method]
    pub fn vendor_id(&self) -> String {
        self.adapter.get_info().vendor.to_string()
    }

    #[method]
    pub fn device_id(&self) -> String {
        self.adapter.get_info().device.to_string()
    }

    #[method]
    pub fn device_type(&self) -> String {
        format!("{:?}", self.adapter.get_info().device_type)
    }

    #[method]
    pub fn backend(&self) -> String {
        format!("{:?}", self.adapter.get_info().backend)
    }

    #[method]
    pub fn limits(&self) -> String {
        format!("{:#?}", self.adapter.limits())
    }

    #[method]
    pub fn has_multi_draw_indirect(&self) -> bool {
        self.features.contains(wgpu::Features::MULTI_DRAW_INDIRECT)
    }

    #[method]
    pub fn has_polygon_mode_line(&self) -> bool {
        self.features.contains(wgpu::Features::MULTI_DRAW_INDIRECT)
    }

    #[method]
    pub fn features(&self) -> String {
        self.show_features(self.features)
    }

    #[method]
    pub fn adapter_features(&self) -> String {
        self.show_features(self.adapter.features())
    }

    fn show_features(&self, f: wgpu::Features) -> String {
        format!(
            "{:^6} - DEPTH_CLIP_CONTROL\n",
            f.contains(wgpu::Features::DEPTH_CLIP_CONTROL)
        ) + &format!(
            "{:^6} - TEXTURE_COMPRESSION_BC\n",
            f.contains(wgpu::Features::TEXTURE_COMPRESSION_BC)
        ) + &format!(
            "{:^6} - INDIRECT_FIRST_INSTANCE\n",
            f.contains(wgpu::Features::INDIRECT_FIRST_INSTANCE)
        ) + &format!(
            "{:^6} - TIMESTAMP_QUERY\n",
            f.contains(wgpu::Features::TIMESTAMP_QUERY)
        ) + &format!(
            "{:^6} - PIPELINE_STATISTICS_QUERY\n",
            f.contains(wgpu::Features::PIPELINE_STATISTICS_QUERY)
        ) + &format!(
            "{:^6} - MAPPABLE_PRIMARY_BUFFERS\n",
            f.contains(wgpu::Features::MAPPABLE_PRIMARY_BUFFERS)
        ) + &format!(
            "{:^6} - TEXTURE_BINDING_ARRAY\n",
            f.contains(wgpu::Features::TEXTURE_BINDING_ARRAY)
        ) + &format!(
            "{:^6} - BUFFER_BINDING_ARRAY\n",
            f.contains(wgpu::Features::BUFFER_BINDING_ARRAY)
        ) + &format!(
            "{:^6} - BUFFER_BINDING_ARRAY\n",
            f.contains(wgpu::Features::BUFFER_BINDING_ARRAY)
        ) + &format!(
            "{:^6} - STORAGE_RESOURCE_BINDING_ARRAY\n",
            f.contains(wgpu::Features::STORAGE_RESOURCE_BINDING_ARRAY)
        ) + &format!(
            "{:^6} - SAMPLED_TEXTURE_AND_STORAGE_BUFFER_ARRAY_NON_UNIFORM_INDEXING\n",
            f.contains(
                wgpu::Features::SAMPLED_TEXTURE_AND_STORAGE_BUFFER_ARRAY_NON_UNIFORM_INDEXING
            )
        ) + &format!(
            "{:^6} - UNIFORM_BUFFER_AND_STORAGE_TEXTURE_ARRAY_NON_UNIFORM_INDEXING\n",
            f.contains(
                wgpu::Features::UNIFORM_BUFFER_AND_STORAGE_TEXTURE_ARRAY_NON_UNIFORM_INDEXING
            )
        ) + &format!(
            "{:^6} - PARTIALLY_BOUND_BINDING_ARRAY\n",
            f.contains(wgpu::Features::PARTIALLY_BOUND_BINDING_ARRAY)
        ) + &format!(
            "{:^6} - MULTI_DRAW_INDIRECT\n",
            f.contains(wgpu::Features::MULTI_DRAW_INDIRECT)
        ) + &format!(
            "{:^6} - MULTI_DRAW_INDIRECT_COUNT\n",
            f.contains(wgpu::Features::MULTI_DRAW_INDIRECT_COUNT)
        ) + &format!(
            "{:^6} - PUSH_CONSTANTS\n",
            f.contains(wgpu::Features::PUSH_CONSTANTS)
        ) + &format!(
            "{:^6} - ADDRESS_MODE_CLAMP_TO_BORDER\n",
            f.contains(wgpu::Features::ADDRESS_MODE_CLAMP_TO_BORDER)
        ) + &format!(
            "{:^6} - POLYGON_MODE_LINE\n",
            f.contains(wgpu::Features::POLYGON_MODE_LINE)
        ) + &format!(
            "{:^6} - POLYGON_MODE_POINT\n",
            f.contains(wgpu::Features::POLYGON_MODE_POINT)
        ) + &format!(
            "{:^6} - TEXTURE_COMPRESSION_ETC2\n",
            f.contains(wgpu::Features::TEXTURE_COMPRESSION_ETC2)
        ) + &format!(
            "{:^6} - TEXTURE_COMPRESSION_ASTC\n",
            f.contains(wgpu::Features::TEXTURE_COMPRESSION_ASTC)
        ) + &format!(
            "{:^6} - TEXTURE_COMPRESSION_ASTC_HDR\n",
            f.contains(wgpu::Features::TEXTURE_COMPRESSION_ASTC_HDR)
        ) + &format!(
            "{:^6} - TEXTURE_ADAPTER_SPECIFIC_FORMAT_FEATURES\n",
            f.contains(wgpu::Features::TEXTURE_ADAPTER_SPECIFIC_FORMAT_FEATURES)
        ) + &format!(
            "{:^6} - SHADER_F16\n",
            f.contains(wgpu::Features::SHADER_F16)
        ) + &format!(
            "{:^6} - SHADER_F64\n",
            f.contains(wgpu::Features::SHADER_F64)
        ) + &format!(
            "{:^6} - SHADER_I16\n",
            f.contains(wgpu::Features::SHADER_I16)
        ) + &format!(
            "{:^6} - VERTEX_ATTRIBUTE_64BIT\n",
            f.contains(wgpu::Features::VERTEX_ATTRIBUTE_64BIT)
        ) + &format!(
            "{:^6} - CONSERVATIVE_RASTERIZATION\n",
            f.contains(wgpu::Features::CONSERVATIVE_RASTERIZATION)
        ) + &format!(
            "{:^6} - VERTEX_WRITABLE_STORAGE\n",
            f.contains(wgpu::Features::VERTEX_WRITABLE_STORAGE)
        ) + &format!(
            "{:^6} - SPIRV_SHADER_PASSTHROUGH\n",
            f.contains(wgpu::Features::SPIRV_SHADER_PASSTHROUGH)
        ) + &format!(
            "{:^6} - SHADER_PRIMITIVE_INDEX\n",
            f.contains(wgpu::Features::SHADER_PRIMITIVE_INDEX)
        ) + &format!("{:^6} - MULTIVIEW\n", f.contains(wgpu::Features::MULTIVIEW))
            + &format!(
                "{:^6} - TEXTURE_FORMAT_16BIT_NORM\n",
                f.contains(wgpu::Features::TEXTURE_FORMAT_16BIT_NORM)
            )
    }

    fn sys_create_target_surface(
        mut gpu: ResMut<Gpu>,
        mut current_frame: ResMut<CurrentFrame>,
        window: Res<Window>,
    ) {
        assert!(current_frame.get_surface().is_none());
        if let Ok(next_fb) = gpu.get_next_framebuffer() {
            current_frame.update_surface(next_fb);
        }
        if current_frame.get_surface().is_none() {
            gpu.on_display_config_changed(window.config());
        }
    }

    fn sys_create_command_encoder(
        gpu: Res<Gpu>,
        current_frame: Res<CurrentFrame>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) {
        // Only create the encoder if we have a surface to write to.
        assert!(maybe_encoder.get_encoder().is_none());
        if current_frame.get_surface().is_some() {
            maybe_encoder.set_frame_encoder(gpu.device().create_command_encoder(
                &wgpu::CommandEncoderDescriptor {
                    label: Some("frame-encoder"),
                },
            ));
        }
    }

    fn sys_submit_frame_commands(mut gpu: ResMut<Gpu>, mut maybe_encoder: ResMut<CurrentEncoder>) {
        maybe_encoder.submit(gpu.queue_mut());
    }

    fn sys_present_target_surface(gpu: Res<Gpu>, mut maybe_frame: ResMut<CurrentFrame>) {
        gpu.os_window.pre_present_notify();
        maybe_frame.present();
    }

    fn sys_handle_display_config_change(mut gpu: ResMut<Gpu>, window: Res<Window>) {
        if let Some(config) = window.updated_config() {
            gpu.on_display_config_changed(config);
        }
    }

    pub fn on_display_config_changed(&mut self, config: &DisplayConfig) {
        info!(
            "window config changed {}x{} @ {} -> {}x{}",
            config.window_size().width,
            config.window_size().height,
            config.dpi_scale_factor(),
            config.render_extent().width,
            config.render_extent().height,
        );

        // Recreate the screen resources for the new window size.
        let sc_desc = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: Self::SCREEN_FORMAT,
            width: config.window_size().width,
            height: config.window_size().height,
            present_mode: self.config.present_mode,
            desired_maximum_frame_latency: 2,
            alpha_mode: wgpu::CompositeAlphaMode::Auto,
            view_formats: vec![Self::SCREEN_FORMAT],
        };
        self.surface.configure(&self.device, &sc_desc);

        // Check if our render extent has changed and re-broadcast
        let extent = config.render_extent();
        if self.render_extent.width != extent.width || self.render_extent.height != extent.height {
            self.render_extent = wgpu::Extent3d {
                width: extent.width,
                height: extent.height,
                depth_or_array_layers: 1,
            };
        }

        self.depth_texture_view = Self::make_screen_depth(&self.device, self.render_extent);
    }

    pub fn attachment_extent(&self) -> wgpu::Extent3d {
        self.render_extent
    }

    pub fn render_extent(&self) -> wgpu::Extent3d {
        self.render_extent
    }

    #[method]
    pub fn frame_count(&self) -> i64 {
        self.frame_count as i64
    }

    pub fn get_next_framebuffer(&mut self) -> Result<Option<wgpu::SurfaceTexture>> {
        self.frame_count += 1;
        match self.surface.get_current_texture() {
            Ok(frame) => Ok(Some(frame)),
            Err(wgpu::SurfaceError::Timeout) => bail!("Timeout: gpu is locked up"),
            Err(wgpu::SurfaceError::OutOfMemory) => bail!("OOM: gpu is out of memory"),
            Err(wgpu::SurfaceError::Lost) => bail!("Lost: our context wondered off"),
            Err(wgpu::SurfaceError::Outdated) => {
                info!("GPU: swap chain outdated, must be recreated");
                Ok(None)
            }
        }
    }

    pub fn depth_attachment(&self) -> &wgpu::TextureView {
        &self.depth_texture_view
    }

    pub fn depth_stencil_attachment(&self) -> wgpu::RenderPassDepthStencilAttachment {
        wgpu::RenderPassDepthStencilAttachment {
            view: &self.depth_texture_view,
            depth_ops: Some(wgpu::Operations {
                // Note: clear to *behind* the plane so that our skybox raytrace pass can check
                // for pixels that have not yet been set.
                load: wgpu::LoadOp::Clear(0f32),
                store: wgpu::StoreOp::Store,
            }),
            stencil_ops: Some(wgpu::Operations {
                load: wgpu::LoadOp::Clear(0),
                store: wgpu::StoreOp::Store,
            }),
        }
    }

    pub fn color_attachment(view: &wgpu::TextureView) -> wgpu::RenderPassColorAttachment {
        wgpu::RenderPassColorAttachment {
            view,
            resolve_target: None,
            ops: wgpu::Operations {
                load: wgpu::LoadOp::Clear(wgpu::Color::RED),
                store: wgpu::StoreOp::Store,
            },
        }
    }

    /// Push `data` to the GPU and return a new buffer. Returns None if data is empty
    /// instead of crashing.
    pub fn maybe_push_buffer(
        &self,
        label: &'static str,
        data: &[u8],
        usage: wgpu::BufferUsages,
    ) -> Option<wgpu::Buffer> {
        if data.is_empty() {
            return None;
        }
        let size = data.len() as wgpu::BufferAddress;
        trace!("uploading {} with {} bytes", label, size);
        Some(
            self.device
                .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                    label: Some(label),
                    contents: data,
                    usage,
                }),
        )
    }

    /// Push `data` to the GPU and return a new buffer. Returns None if data is empty
    /// instead of crashing.
    pub fn maybe_push_slice<T: IntoBytes + Immutable>(
        &self,
        label: &'static str,
        data: &[T],
        usage: wgpu::BufferUsages,
    ) -> Option<wgpu::Buffer> {
        if data.is_empty() {
            return None;
        }
        let size = mem::size_of_val(data) as wgpu::BufferAddress;
        trace!("uploading {} with {} bytes", label, size);
        Some(
            self.device
                .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                    label: Some(label),
                    contents: data.as_bytes(),
                    usage,
                }),
        )
    }

    /// Push `data` to the GPU and return a new buffer. Panics if data is empty.
    pub fn push_buffer(
        &self,
        label: &'static str,
        data: &[u8],
        usage: wgpu::BufferUsages,
    ) -> wgpu::Buffer {
        self.maybe_push_buffer(label, data, usage)
            .expect("push non-empty buffer")
    }

    /// Push `data` to the GPU and return a new buffer. Panics if data is empty.
    pub fn push_slice<T: IntoBytes + Immutable>(
        &self,
        label: &'static str,
        data: &[T],
        usage: wgpu::BufferUsages,
    ) -> wgpu::Buffer {
        self.maybe_push_slice(label, data, usage)
            .expect("push non-empty slice")
    }

    /// Push `data` to the GPU and return a new buffer. Panics if data is empty.
    pub fn push_data<T: IntoBytes + Immutable>(
        &self,
        label: &'static str,
        data: &T,
        usage: wgpu::BufferUsages,
    ) -> wgpu::Buffer {
        let size = mem::size_of::<T>() as wgpu::BufferAddress;
        trace!("uploading {} with {} bytes", label, size);
        self.device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some(label),
                contents: data.as_bytes(),
                usage,
            })
    }

    pub fn upload_data_to<T: IntoBytes + Immutable>(
        &self,
        data: &T,
        target: &wgpu::Buffer,
        encoder: &mut wgpu::CommandEncoder,
    ) {
        let source = self.push_data("tmp-upload-data-buf", data, wgpu::BufferUsages::COPY_SRC);
        encoder.copy_buffer_to_buffer(
            &source,
            0,
            target,
            0,
            mem::size_of_val(data) as wgpu::BufferAddress,
        );
    }

    /// Push `data` to the GPU and copy it to `target`. Does nothing if data is empty.
    /// The copy appears to currently be fenced with respect to usages of the target,
    /// but this is not specified as of the time of writing. This is optimized under
    /// the hood and is supposed to be, I think, faster than creating a new bind group.
    pub fn upload_slice_to<T: IntoBytes + Immutable>(
        &self,
        data: &[T],
        target: &wgpu::Buffer,
        encoder: &mut wgpu::CommandEncoder,
    ) {
        if let Some(source) =
            self.maybe_push_slice("tmp-upload-slice-buf", data, wgpu::BufferUsages::COPY_SRC)
        {
            encoder.copy_buffer_to_buffer(
                &source,
                0,
                target,
                0,
                mem::size_of_val(data) as wgpu::BufferAddress,
            );
        }
    }

    pub fn compile_shader(&self, name: &str, wgsl: &str) -> wgpu::ShaderModule {
        self.device
            .create_shader_module(wgpu::ShaderModuleDescriptor {
                label: Some(name),
                source: ShaderSource::Wgsl(wgsl.into()),
            })
    }

    pub const fn stride_for_row_size(size_in_bytes: u32) -> u32 {
        size_in_bytes.div_ceil(wgpu::COPY_BYTES_PER_ROW_ALIGNMENT)
            * wgpu::COPY_BYTES_PER_ROW_ALIGNMENT
    }

    /// Create a texture and view with current screen extent and format, suitable for rendering
    /// to and then compositing into the final display.
    pub fn make_screen_target(&self, label: &str) -> (wgpu::Texture, wgpu::TextureView) {
        Self::make_target_texture(
            label,
            self.device(),
            self.render_extent(),
            Gpu::SCREEN_FORMAT,
            wgpu::TextureUsages::RENDER_ATTACHMENT
                | wgpu::TextureUsages::COPY_SRC
                | wgpu::TextureUsages::TEXTURE_BINDING,
        )
    }

    /// Create a depth buffer and view with current screen extent and target format, suitable
    /// for rendering and then compositing with the final scene.
    pub fn make_depth_target(&self, label: &str) -> (wgpu::Texture, wgpu::TextureView) {
        Self::make_target_texture(
            label,
            self.device(),
            self.render_extent(),
            Gpu::DEPTH_FORMAT,
            wgpu::TextureUsages::RENDER_ATTACHMENT
                | wgpu::TextureUsages::COPY_SRC
                | wgpu::TextureUsages::TEXTURE_BINDING,
        )
    }

    /// Create a texture with current screen extent with chosen format and usage, suitable for
    /// use internally.
    pub fn make_internal_target(
        &self,
        label: &str,
        format: wgpu::TextureFormat,
        usage: wgpu::TextureUsages,
    ) -> (wgpu::Texture, wgpu::TextureView) {
        Self::make_target_texture(label, self.device(), self.render_extent(), format, usage)
    }

    fn make_screen_depth(device: &wgpu::Device, size: wgpu::Extent3d) -> wgpu::TextureView {
        let (_depth_texture, depth_texture_view) = Self::make_target_texture(
            "screen-depth",
            device,
            size,
            Gpu::DEPTH_FORMAT,
            wgpu::TextureUsages::RENDER_ATTACHMENT,
        );
        depth_texture_view
    }

    fn make_target_texture(
        label: &str,
        device: &wgpu::Device,
        size: wgpu::Extent3d,
        format: wgpu::TextureFormat,
        usage: wgpu::TextureUsages,
    ) -> (wgpu::Texture, wgpu::TextureView) {
        let texture = device.create_texture(&wgpu::TextureDescriptor {
            label: Some(&format!("{label}-texture")),
            size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format,
            usage,
            view_formats: &[format],
        });
        let view = texture.create_view(&wgpu::TextureViewDescriptor {
            label: Some(&format!("{label}-texture-view")),
            format: None,
            dimension: None,
            aspect: wgpu::TextureAspect::All,
            base_mip_level: 0,
            mip_level_count: None,
            base_array_layer: 0,
            array_layer_count: None,
        });
        (texture, view)
    }

    #[cfg(not(target_arch = "wasm32"))]
    pub fn dump_texture(
        texture: &wgpu::Texture,
        extent: wgpu::Extent3d,
        format: wgpu::TextureFormat,
        gpu: &mut Gpu,
        callback: Box<
            dyn FnOnce(wgpu::Extent3d, wgpu::TextureFormat, Vec<u8>) + Send + Sync + 'static,
        >,
    ) -> Result<()> {
        use std::{
            fs,
            sync::{Arc, Mutex},
        };
        let _ = fs::create_dir("__dump__");
        let sample_size = texture_format_size(format);
        let bytes_per_row = Self::stride_for_row_size(extent.width * sample_size);
        let buf_size = u64::from(bytes_per_row * extent.height);
        let download_buffer = gpu.device().create_buffer(&wgpu::BufferDescriptor {
            label: Some("dump-download-buffer"),
            size: buf_size,
            usage: wgpu::BufferUsages::all(),
            mapped_at_creation: false,
        });
        let mut encoder = gpu
            .device()
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("dump-download-command-encoder"),
            });
        println!(
            "dumping texture: fmt-size: {}, byes-per-row: {}, stride: {}",
            sample_size,
            extent.width * sample_size,
            bytes_per_row
        );
        encoder.copy_texture_to_buffer(
            wgpu::ImageCopyTexture {
                texture,
                mip_level: 0,
                origin: wgpu::Origin3d::ZERO,
                aspect: wgpu::TextureAspect::All,
            },
            wgpu::ImageCopyBuffer {
                buffer: &download_buffer,
                layout: wgpu::ImageDataLayout {
                    offset: 0,
                    bytes_per_row: Some(bytes_per_row),
                    rows_per_image: Some(extent.height),
                },
            },
            extent,
        );
        gpu.queue_mut().submit(vec![encoder.finish()]);
        gpu.device().poll(wgpu::Maintain::Wait);
        let waiter = Arc::new(Mutex::new(false));
        let waiter_ref = waiter.clone();
        download_buffer
            .slice(..)
            .map_async(wgpu::MapMode::Read, move |err| {
                if let Err(e) = err {
                    println!("Failed to dump texture: {e}");
                }
                *waiter_ref.lock().unwrap() = true;
            });
        while !*waiter.lock().unwrap() {
            gpu.device().poll(wgpu::Maintain::Wait);
        }
        // block_on(reader)?;
        let raw = download_buffer.slice(..).get_mapped_range().to_owned();
        callback(extent, format, raw);
        Ok(())
    }

    pub fn device(&self) -> &wgpu::Device {
        &self.device
    }

    pub fn device_mut(&mut self) -> &mut wgpu::Device {
        &mut self.device
    }

    pub fn queue(&self) -> &wgpu::Queue {
        &self.queue
    }

    pub fn queue_mut(&mut self) -> &mut wgpu::Queue {
        &mut self.queue
    }

    pub fn device_and_queue_mut(&mut self) -> (&mut wgpu::Device, &mut wgpu::Queue) {
        (&mut self.device, &mut self.queue)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Core;

    #[test]
    fn test_create() -> Result<()> {
        let runtime = Core::for_test()?;
        assert!(runtime.resource::<Gpu>().render_extent().width > 0);
        Ok(())
    }
}
