// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::MetaEvent;
use bitbag::{BitBag, Flags};
use nitrous::{inject_nitrous_resource, method, NitrousResource};
use smallvec::SmallVec;
use std::collections::HashMap;
use winit::{
    dpi::{LogicalSize, PhysicalPosition},
    event::{
        DeviceEvent, DeviceId, ElementState, Event, KeyEvent, MouseButton, MouseScrollDelta,
        WindowEvent,
    },
    keyboard::{KeyLocation, ModifiersState, PhysicalKey},
};

pub type InputEventVec = SmallVec<[InputEvent; 8]>;

#[derive(Debug, Default, NitrousResource)]
pub struct InputEvents {
    events: InputEventVec,
}

#[inject_nitrous_resource]
impl InputEvents {
    pub(crate) fn append(&mut self, events: InputEventVec) {
        self.events.extend(events);
    }

    pub(crate) fn clear(&mut self) {
        self.events.clear();
    }

    #[method]
    fn count(&self) -> i64 {
        self.events.len() as i64
    }

    pub fn iter(&self) -> impl Iterator<Item = &InputEvent> {
        self.events.iter()
    }
}

pub fn test_make_input_events(mut events: Vec<InputEvent>) -> InputEvents {
    let mut out = InputEvents::default();
    for evt in events.drain(..) {
        out.events.push(evt);
    }
    out
}

pub type SystemEventVec = SmallVec<[SystemEvent; 8]>;

#[derive(Debug, Default, NitrousResource)]
pub struct SystemEvents {
    events: SystemEventVec,
}

#[inject_nitrous_resource]
impl SystemEvents {
    pub(crate) fn append(&mut self, events: SystemEventVec) {
        self.events.extend(events);
    }

    pub(crate) fn clear(&mut self) {
        self.events.clear();
    }

    #[method]
    fn count(&self) -> i64 {
        self.events.len() as i64
    }

    pub fn iter(&self) -> impl Iterator<Item = &SystemEvent> {
        self.events.iter()
    }
}

#[derive(Flags, Clone, Copy, Debug, Eq, PartialEq)]
#[repr(u8)]
pub enum InputMethodErrata {
    UseWindowScrollEvents,
    InvertVerticalScrollEvents,
}

#[derive(Debug)]
pub struct GlobalInputState {
    modifiers_state: ModifiersState,
    cursor_in_window: HashMap<DeviceId, bool>,
    window_focused: bool,
    errata: BitBag<InputMethodErrata>,
}

impl GlobalInputState {
    pub fn new() -> Self {
        Self {
            modifiers_state: Default::default(),
            cursor_in_window: Default::default(),
            window_focused: Default::default(),
            errata: BitBag::default(),
        }
    }

    pub fn use_window_scroll_events(&self) -> bool {
        self.errata.is_set(InputMethodErrata::UseWindowScrollEvents)
    }

    pub fn invert_vertical_scroll_events(&self) -> bool {
        self.errata
            .is_set(InputMethodErrata::InvertVerticalScrollEvents)
    }

    pub fn add_errata(&mut self, fact: InputMethodErrata) {
        self.errata.set(fact);
    }
}

#[derive(Debug, Copy, Clone)]
pub enum MouseAxis {
    X,
    Y,
    ScrollH,
    ScrollV,
    Tilt,
}

// Every platform winit supports provides a slightly different perspective on how events show up
// in this system. Our needs are less than completely general, so we can get away with projecting
// that onto a simpler surface. In particular:
//   * There is a single window that is generally fullscreen.
//   * There are limited interactions with the system outside of mouse/keyboard/joystick/gpu.
// As such, we try to make the following guarantees, papering over platform differences:
//   * Keyboard events may only fire when the window is focused.
//   * Keyboard events contain the current state of modifiers when the event happens.
//   * Keyboard events contain both a scancode and a "virtual" keycode.
//   * The virtual keycode is just a friendly name for the physical key that was interacted.
//     e.g. Events in alternate planes reflect the plane in the ModifiersState, not in the keycode.
//   * Mouse movement and button events fire always, but are marked with the window in-out state.
//     (we cannot track this accurately on all platforms, so generally we can ignore this entirely)
//   * There is only one "user-caused-window-to-close" signal.

// DeviceEvents flow into the game logic.
#[derive(Debug, Clone)]
pub enum InputEvent {
    KeyboardKey {
        physical_key: PhysicalKey,
        location: KeyLocation,
        press_state: ElementState,
        modifiers_state: ModifiersState,
        window_focused: bool,
    },

    MouseButton {
        button: MouseButton,
        press_state: ElementState,
        modifiers_state: ModifiersState,
        in_window: bool,
        window_focused: bool,
    },

    JoystickButton {
        dummy: u32,
        press_state: ElementState,
        modifiers_state: ModifiersState,
        window_focused: bool,
    },

    CursorMove {
        pixel_position: (f64, f64),
        modifiers_state: ModifiersState,
        in_window: bool,
        window_focused: bool,
    },

    MouseWheel {
        horizontal_delta: f64,
        vertical_delta: f64,
        modifiers_state: ModifiersState,
        in_window: bool,
        window_focused: bool,
    },

    MouseMotion {
        dx: f64,
        dy: f64,
        modifiers_state: ModifiersState,
        in_window: bool,
        window_focused: bool,
    },

    JoystickAxis {
        id: u32,
        value: f64,
        modifiers_state: ModifiersState,
        window_focused: bool,
    },

    // We do not generally care about individual mice or keyboards: the events will still come
    // through automatically. This will be very important for Joystick management, however, as we
    // expect those to cycle relatively frequently during gameplay.
    DeviceAdded {
        dummy: u32,
    },
    DeviceRemoved {
        dummy: u32,
    },
}

#[derive(Debug, Clone)]
pub enum SystemEvent {
    // Note that the sizes passed here may race with the ones returned by the surface/window,
    // so code should be careful to use these values instead of the ones returned by those apis.
    WindowResized { width: u32, height: u32 },

    // Note that the scale factor passed here may race with the one given back by the surface
    // so code that responds to this should be careful to use this value instead of the one there.
    ScaleFactorChanged { scale: f64 },

    // Aggregate of various "user wants the program to go away" interactions. Close button (the X)
    // pressed in the window's bar or task bar, Win+F4 pressed, File+Quit, etc.
    Quit,
}

impl InputEvent {
    pub fn press_state(&self) -> Option<ElementState> {
        match self {
            Self::KeyboardKey { press_state, .. } => Some(*press_state),
            Self::MouseButton { press_state, .. } => Some(*press_state),
            Self::JoystickButton { press_state, .. } => Some(*press_state),
            _ => None,
        }
    }

    pub fn modifiers_state(&self) -> Option<ModifiersState> {
        match self {
            Self::KeyboardKey {
                modifiers_state, ..
            } => Some(*modifiers_state),
            Self::MouseButton {
                modifiers_state, ..
            } => Some(*modifiers_state),
            Self::MouseMotion {
                modifiers_state, ..
            } => Some(*modifiers_state),
            Self::MouseWheel {
                modifiers_state, ..
            } => Some(*modifiers_state),
            Self::CursorMove {
                modifiers_state, ..
            } => Some(*modifiers_state),
            Self::JoystickAxis {
                modifiers_state, ..
            } => Some(*modifiers_state),
            Self::JoystickButton {
                modifiers_state, ..
            } => Some(*modifiers_state),
            _ => None,
        }
    }

    pub fn is_window_focused(&self) -> bool {
        matches!(
            self,
            Self::KeyboardKey {
                window_focused: true,
                ..
            } | Self::MouseButton {
                window_focused: true,
                ..
            } | Self::JoystickButton {
                window_focused: true,
                ..
            } | Self::MouseMotion {
                window_focused: true,
                ..
            } | Self::MouseWheel {
                window_focused: true,
                ..
            } | Self::CursorMove {
                window_focused: true,
                ..
            } | Self::JoystickAxis {
                window_focused: true,
                ..
            }
        )
    }

    pub fn pixel_position(&self) -> Option<(f64, f64)> {
        match self {
            Self::CursorMove { pixel_position, .. } => Some(*pixel_position),
            _ => None,
        }
    }

    pub fn gpu_position(&self, logical_size: LogicalSize<f64>) -> Option<(f32, f32)> {
        self.pixel_position().map(|(x, y)| {
            (
                (x / logical_size.width * 2.0) as f32,
                (y / logical_size.height * 2.0) as f32,
            )
        })
    }

    pub fn is_primary_mouse_down(&self) -> bool {
        match self {
            Self::MouseButton {
                button,
                press_state,
                ..
            } => *button == MouseButton::Left && *press_state == ElementState::Pressed,
            _ => false,
        }
    }
}

pub(crate) fn wrap_event(
    e: &Event<MetaEvent>,
    input_state: &mut GlobalInputState,
    input_events: &mut InputEventVec,
    system_events: &mut SystemEventVec,
) {
    match e {
        Event::NewEvents(_) => {}
        Event::WindowEvent { event, .. } => {
            wrap_window_event(event, input_state, input_events, system_events)
        }
        Event::DeviceEvent { device_id, event } => {
            wrap_device_event(device_id, event, input_state, input_events)
        }
        Event::UserEvent(_) => {}
        Event::Suspended => {}
        Event::Resumed => {}
        Event::AboutToWait => {}
        Event::LoopExiting => {}
        Event::MemoryWarning => {}
    }
}

// Uh, clippy? It's mutable: we need the vec-ness?
#[allow(clippy::ptr_arg)]
fn wrap_window_event(
    event: &WindowEvent,
    input_state: &mut GlobalInputState,
    out: &mut InputEventVec,
    system_events: &mut SystemEventVec,
) {
    match event {
        WindowEvent::Ime(_) => {}
        WindowEvent::Occluded(_) => {}
        WindowEvent::Resized(s) => {
            system_events.push(SystemEvent::WindowResized {
                width: s.width,
                height: s.height,
            });
        }
        WindowEvent::Moved(_) => {}
        WindowEvent::Destroyed => {
            system_events.push(SystemEvent::Quit);
        }
        WindowEvent::CloseRequested => {
            system_events.push(SystemEvent::Quit);
        }
        WindowEvent::Focused(b) => {
            input_state.window_focused = *b;
        }
        WindowEvent::DroppedFile(_) => {}
        WindowEvent::HoveredFile(_) => {}
        WindowEvent::HoveredFileCancelled => {}
        WindowEvent::ScaleFactorChanged { scale_factor, .. } => {
            system_events.push(SystemEvent::ScaleFactorChanged {
                scale: *scale_factor,
            });
        }
        WindowEvent::CursorEntered { device_id } => {
            input_state.cursor_in_window.insert(*device_id, true);
        }
        WindowEvent::CursorLeft { device_id } => {
            input_state.cursor_in_window.insert(*device_id, false);
        }

        // Track real cursor position in the window including window system accel
        // warping, and other such; mostly useful for software mice, but also for
        // picking with a hardware mouse.
        WindowEvent::CursorMoved {
            position,
            device_id,
            ..
        } => {
            let in_window = *input_state
                .cursor_in_window
                .get(device_id)
                .unwrap_or(&false);
            out.push(InputEvent::CursorMove {
                pixel_position: (position.x, position.y),
                modifiers_state: input_state.modifiers_state,
                in_window,
                window_focused: input_state.window_focused,
            });
        }

        // We need to capture keyboard input both here and below because of web.
        WindowEvent::KeyboardInput {
            event:
                KeyEvent {
                    physical_key,    // : PhysicalKey,
                    text: _text,     // : Option<SmolStr>,
                    location,        // : KeyLocation,
                    state,           // : ElementState,
                    repeat: _repeat, // : bool,
                    ..
                },
            ..
        } => out.push(InputEvent::KeyboardKey {
            physical_key: *physical_key,
            press_state: *state,
            location: *location,
            modifiers_state: input_state.modifiers_state,
            window_focused: true,
        }),

        // Ignore events duplicated by other capture methods.
        WindowEvent::MouseInput {
            device_id,
            state,
            button,
        } => {
            let in_window = *input_state
                .cursor_in_window
                .get(device_id)
                .unwrap_or(&false);
            out.push(InputEvent::MouseButton {
                button: *button,
                press_state: *state,
                modifiers_state: input_state.modifiers_state,
                in_window,
                window_focused: input_state.window_focused,
            })
        }
        WindowEvent::MouseWheel {
            device_id,
            delta,
            phase: _phase,
        } => {
            if input_state.use_window_scroll_events() {
                out.push(match delta {
                    MouseScrollDelta::LineDelta(dh, dv) => {
                        wrap_scroll_line_delta(*dh, *dv, input_state, device_id)
                    }
                    MouseScrollDelta::PixelDelta(s) => {
                        wrap_scroll_pixel_delta(s, input_state, device_id)
                    }
                });
            }
        }
        WindowEvent::AxisMotion { .. } => {}

        // Don't worry about touch just yet.
        WindowEvent::SmartMagnify { .. } => {}
        WindowEvent::Touch(_) => {}
        WindowEvent::TouchpadMagnify { .. } => {}
        WindowEvent::TouchpadPressure { .. } => {}
        WindowEvent::TouchpadRotate { .. } => {}

        WindowEvent::ModifiersChanged(modifiers_state) => {
            input_state.modifiers_state = modifiers_state.state();
        }

        WindowEvent::ThemeChanged(_) => {}
        WindowEvent::ActivationTokenDone { .. } => {}

        // Handled by main loop
        WindowEvent::RedrawRequested => {}
    }
}

fn wrap_device_event(
    device_id: &DeviceId,
    event: &DeviceEvent,
    input_state: &mut GlobalInputState,
    out: &mut InputEventVec,
) {
    match event {
        // Device change events
        DeviceEvent::Added => {
            out.push(InputEvent::DeviceAdded { dummy: 0 });
        }
        DeviceEvent::Removed => {
            out.push(InputEvent::DeviceRemoved { dummy: 0 });
        }

        // Mouse Motion: unfiltered, arbitrary units
        DeviceEvent::MouseMotion { delta: (dx, dy) } => {
            let in_window = *input_state
                .cursor_in_window
                .get(device_id)
                .unwrap_or(&false);
            out.push(InputEvent::MouseMotion {
                dx: *dx,
                dy: *dy,
                modifiers_state: input_state.modifiers_state,
                in_window,
                window_focused: input_state.window_focused,
            });
        }

        // Mouse Wheel
        DeviceEvent::MouseWheel {
            delta: MouseScrollDelta::LineDelta(dh, dv),
        } => {
            if !input_state.use_window_scroll_events() {
                out.push(wrap_scroll_line_delta(*dh, *dv, input_state, device_id));
            }
        }
        DeviceEvent::MouseWheel {
            delta: MouseScrollDelta::PixelDelta(s),
        } => {
            if !input_state.use_window_scroll_events() {
                out.push(wrap_scroll_pixel_delta(s, input_state, device_id));
            }
        }

        // Mouse Button, maybe also joystick button?
        DeviceEvent::Button { .. } => {
            // Note: the window event wrapper works now, at least on windows and x11
            // TODO: revisit for joysticks?
        }

        // Match virtual keycodes.
        DeviceEvent::Key(
            /*KeyboardInput {
                virtual_keycode: _virtual_keycode,
                scancode: _scancode,
                state: _state,
                ..
            }*/
            _,
        ) => {
            // If not focused, send from the device event. Note that this is important for
            // keeping key states, e.g. when the mouse is not locked with focus-on-hover.
            // if !input_state.window_focused {
            //     if let Some(vkey) = guess_key(
            //         *_scancode,
            //         *_virtual_keycode,
            //         input_state.modifiers_state.shift(),
            //     ) {
            //         out.push(InputEvent::KeyboardKey {
            //             scancode: *_scancode,
            //             virtual_keycode: vkey,
            //             press_state: *_state,
            //             modifiers_state: input_state.modifiers_state,
            //             window_focused: false,
            //         });
            //     }
            // }
        }

        // Includes both joystick and mouse axis motion.
        DeviceEvent::Motion { axis, value } => {
            out.push(InputEvent::JoystickAxis {
                id: *axis,
                value: *value,
                modifiers_state: input_state.modifiers_state,
                window_focused: input_state.window_focused,
            });
        }
    }
}

fn wrap_scroll_line_delta(
    dh: f32,
    dv: f32,
    input_state: &GlobalInputState,
    device_id: &DeviceId,
) -> InputEvent {
    let horizontal_delta = dh as f64;
    let mut vertical_delta = dv as f64;
    if input_state.invert_vertical_scroll_events() {
        vertical_delta = -vertical_delta;
    }
    let in_window = *input_state
        .cursor_in_window
        .get(device_id)
        .unwrap_or(&false);
    InputEvent::MouseWheel {
        horizontal_delta,
        vertical_delta,
        modifiers_state: input_state.modifiers_state,
        in_window,
        window_focused: input_state.window_focused,
    }
}

fn wrap_scroll_pixel_delta(
    s: &PhysicalPosition<f64>,
    input_state: &GlobalInputState,
    device_id: &DeviceId,
) -> InputEvent {
    let in_window = *input_state
        .cursor_in_window
        .get(device_id)
        .unwrap_or(&false);
    let mut vertical_delta = s.y;
    if input_state.invert_vertical_scroll_events() {
        vertical_delta = -vertical_delta;
    }
    InputEvent::MouseWheel {
        horizontal_delta: s.x,
        vertical_delta,
        modifiers_state: input_state.modifiers_state,
        in_window,
        window_focused: input_state.window_focused,
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::path::PathBuf;
    use winit::{dpi::PhysicalSize, event::TouchPhase, window::WindowId};

    fn physical_size() -> PhysicalSize<u32> {
        PhysicalSize {
            width: 8,
            height: 9,
        }
    }

    fn path() -> PathBuf {
        let mut buf = PathBuf::new();
        buf.push("a");
        buf.push("b");
        buf
    }

    fn win_evt(event: WindowEvent) -> Event<MetaEvent> {
        Event::WindowEvent {
            window_id: unsafe { WindowId::dummy() },
            event,
        }
    }

    fn dev_evt(event: DeviceEvent) -> Event<MetaEvent> {
        Event::DeviceEvent {
            device_id: unsafe { DeviceId::dummy() },
            event,
        }
    }

    #[test]
    fn test_handle_system_events() {
        let mut input_state = GlobalInputState::new();
        let psz = physical_size();

        let mut evts = InputEventVec::new();
        let mut syss = SystemEventVec::new();
        wrap_event(
            &win_evt(WindowEvent::Resized(psz)),
            &mut input_state,
            &mut evts,
            &mut syss,
        );
        assert!(matches!(syss[0], SystemEvent::WindowResized { .. }));

        let mut evts = InputEventVec::new();
        let mut syss = SystemEventVec::new();
        wrap_event(
            &win_evt(WindowEvent::Destroyed),
            &mut input_state,
            &mut evts,
            &mut syss,
        );
        assert!(matches!(syss[0], SystemEvent::Quit));

        let mut evts = InputEventVec::new();
        let mut syss = SystemEventVec::new();
        wrap_event(
            &win_evt(WindowEvent::CloseRequested),
            &mut input_state,
            &mut evts,
            &mut syss,
        );
        assert!(matches!(syss[0], SystemEvent::Quit));

        let mut evts = InputEventVec::new();
        let mut syss = SystemEventVec::new();
        wrap_event(
            &win_evt(WindowEvent::DroppedFile(path())),
            &mut input_state,
            &mut evts,
            &mut syss,
        );
        assert!(evts.is_empty());

        let mut evts = InputEventVec::new();
        let mut syss = SystemEventVec::new();
        wrap_event(
            &win_evt(WindowEvent::Focused(true)),
            &mut input_state,
            &mut evts,
            &mut syss,
        );
        assert!(evts.is_empty());

        let mut evts = InputEventVec::new();
        let mut syss = SystemEventVec::new();
        wrap_event(
            &dev_evt(DeviceEvent::Added),
            &mut input_state,
            &mut evts,
            &mut syss,
        );
        assert!(matches!(evts[0], InputEvent::DeviceAdded { .. }));

        let mut evts = InputEventVec::new();
        let mut syss = SystemEventVec::new();
        wrap_event(
            &dev_evt(DeviceEvent::Removed),
            &mut input_state,
            &mut evts,
            &mut syss,
        );
        assert!(matches!(evts[0], InputEvent::DeviceRemoved { .. }));

        let mut evts = InputEventVec::new();
        let mut syss = SystemEventVec::new();
        wrap_event(
            &dev_evt(DeviceEvent::MouseMotion { delta: (8., 9.) }),
            &mut input_state,
            &mut evts,
            &mut syss,
        );
        assert!(matches!(evts[0], InputEvent::MouseMotion { .. }));

        let mut evts = InputEventVec::new();
        let mut syss = SystemEventVec::new();
        if input_state.use_window_scroll_events() {
            wrap_event(
                &win_evt(WindowEvent::MouseWheel {
                    device_id: unsafe { DeviceId::dummy() },
                    delta: MouseScrollDelta::LineDelta(8., 9.),
                    phase: TouchPhase::Moved,
                }),
                &mut input_state,
                &mut evts,
                &mut syss,
            );
        } else {
            wrap_event(
                &dev_evt(DeviceEvent::MouseWheel {
                    delta: MouseScrollDelta::LineDelta(8., 9.),
                }),
                &mut input_state,
                &mut evts,
                &mut syss,
            );
        }
        assert!(matches!(evts[0], InputEvent::MouseWheel { .. }));
    }
}
