// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use approx::{AbsDiffEq, RelativeEq};
use glam::DVec3;
use std::{f64::consts::PI, fmt};

#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct EquatorialCoord {
    ra: Angle<Radians>,
    dec: Angle<Radians>,
}

impl EquatorialCoord {
    pub fn from_ra_dec<Unit: AngleUnit>(ra: Angle<Unit>, dec: Angle<Unit>) -> Self {
        let ra = Angle::<Radians>::from(&ra);
        let dec = Angle::<Radians>::from(&dec);
        assert!(ra <= radians!(2. * PI));
        assert!(ra >= radians!(0));
        assert!(dec <= radians!(PI));
        assert!(dec >= radians!(-PI));
        Self { ra, dec }
    }

    /// Note: Important the vector must be in NITROGEN world space
    pub fn from_nitrogen_dvec3(v: DVec3) -> Self {
        let [x, y, z] = v.to_array();
        let distance = (x * x + y * y + z * z).sqrt();
        let ra = radians!(-x.atan2(z)) + radians!(PI);
        let dec = (y / distance).asin();
        Self::from_ra_dec(radians!(ra), radians!(dec))
    }

    /// Note: Important! This vector is in NITROGEN world space, not ECI space.
    pub fn nitrogen_dvec3(&self) -> DVec3 {
        let dec = self.dec.f64();
        let ra = (self.ra - radians!(PI)).f64();
        DVec3::new(-(ra.sin() * dec.cos()), dec.sin(), ra.cos() * dec.cos())
    }

    pub fn right_ascension(&self) -> Angle<Radians> {
        self.ra
    }

    pub fn declination(&self) -> Angle<Radians> {
        self.dec
    }
}

impl fmt::Display for EquatorialCoord {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}ra{}d", degrees!(self.ra), degrees!(self.dec))
    }
}

impl AbsDiffEq for EquatorialCoord {
    type Epsilon = f64;

    fn default_epsilon() -> Self::Epsilon {
        f64::default_epsilon()
    }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        self.dec.f64().abs_diff_eq(&other.dec.f64(), epsilon)
            && self.ra.f64().abs_diff_eq(&other.ra.f64(), epsilon)
    }
}

impl RelativeEq for EquatorialCoord {
    fn default_max_relative() -> Self::Epsilon {
        f64::default_epsilon()
    }

    fn relative_eq(
        &self,
        other: &Self,
        epsilon: Self::Epsilon,
        max_relative: Self::Epsilon,
    ) -> bool {
        self.dec
            .f64()
            .relative_eq(&other.dec.f64(), epsilon, max_relative)
            && self
                .ra
                .f64()
                .relative_eq(&other.ra.f64(), epsilon, max_relative)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use anyhow::Result;
    use approx::assert_relative_eq;

    #[test]
    fn test_roundtrip_nitrogen_vec() -> Result<()> {
        for ra_i in 0..=360 {
            for dec_i in -89..=80 {
                let a = EquatorialCoord::from_ra_dec(degrees!(ra_i), degrees!(dec_i));
                let v = a.nitrogen_dvec3();
                let b = EquatorialCoord::from_nitrogen_dvec3(v);
                assert_relative_eq!(a, b, epsilon = 0.00001);
            }
        }

        Ok(())
    }
}
