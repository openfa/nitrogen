// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

/// Shorter spelling for Color::rgb
#[macro_export]
macro_rules! rgb {
    ($v:expr) => {
        $crate::Color::rgb($v)
    };
}

/// Shorter spelling for Color::rgb
#[macro_export]
macro_rules! rgba {
    ($v:expr) => {
        $crate::Color::rgba($v)
    };
}

/// Represent a u8x4 rgba color. Implement Color10 or something if we want more bits ever.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Color {
    rgba: [u8; 4],
}

impl Color {
    pub const RED: Self = Self {
        rgba: [255, 0, 0, 255],
    };
    pub const GREEN: Self = Self {
        rgba: [0, 255, 0, 255],
    };
    pub const BLUE: Self = Self {
        rgba: [0, 0, 255, 255],
    };
    pub const MAGENTA: Self = Self {
        rgba: [255, 0, 255, 255],
    };

    pub fn rgb(v: u32) -> Self {
        assert!(v <= 0xFFFFFF);
        Self {
            rgba: [
                ((v >> 16) & 0xFF) as u8,
                ((v >> 8) & 0xFF) as u8,
                (v & 0xFF) as u8,
                255,
            ],
        }
    }

    pub fn rgba(v: u32) -> Self {
        Self {
            rgba: [
                ((v >> 24) & 0xFF) as u8,
                ((v >> 16) & 0xFF) as u8,
                ((v >> 8) & 0xFF) as u8,
                (v & 0xFF) as u8,
            ],
        }
    }

    pub fn rgb_f32(r: f32, g: f32, b: f32) -> Self {
        Self {
            rgba: [(255. * r) as u8, (255. * g) as u8, (255. * b) as u8, 255],
        }
    }

    pub fn to_array(&self) -> [u8; 4] {
        self.rgba
    }

    pub fn to_array_f32(&self) -> [f32; 4] {
        self.rgba.map(|v| v as f32 / 255.)
    }

    pub fn inverse(&self) -> Self {
        Self {
            rgba: [
                255 - self.rgba[0],
                255 - self.rgba[1],
                255 - self.rgba[2],
                self.rgba[3],
            ],
        }
    }

    pub fn smooth_scale(&self, scale: f32) -> Self {
        let rgba = self
            .to_array_f32()
            .map(|v| v * scale)
            .map(|v| (v * 255.) as u8);
        Self { rgba }
    }

    pub fn to_u32(&self) -> u32 {
        u32::from_be_bytes(self.rgba)
    }
}
