// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use anyhow::{bail, ensure, Result};
use bevy_ecs::prelude::*;
use geodesy::{Bearing, Geodetic, PitchCline};
use glam::{DQuat, EulerRot};
use nitrous::{inject_nitrous_component, method, NitrousComponent};
use phase::{Frame, MapFrame};
use runtime::{Extension, Runtime};
use std::fmt::Write;

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum ArcBallStep {
    ApplyInput,
}

#[derive(Debug)]
struct InputState {
    in_rotate: bool,
    in_move: bool,
    target_height_delta: Length<Meters>,
}

/// The ArcBall system will, if the "player" entity has an ArcBallController
/// will translate device events into relevant arcball updates on that player
/// and apply those updates on each frame.
#[derive(Debug, NitrousComponent)]
#[component(name = "arcball")]
pub struct ArcBallController {
    input: InputState,

    map_frame: MapFrame,
    eye_distance: Length<Meters>,
}

impl Extension for ArcBallController {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.add_sim_system(Self::sys_apply_input.in_set(ArcBallStep::ApplyInput));
        Ok(())
    }
}

impl Default for ArcBallController {
    fn default() -> Self {
        Self {
            input: InputState {
                target_height_delta: meters!(0),
                in_rotate: false,
                in_move: false,
            },
            map_frame: MapFrame::new_level_north(Geodetic::new(
                radians!(0),
                radians!(0),
                meters!(10.),
            )),
            eye_distance: meters!(100.),
        }
    }
}

#[inject_nitrous_component]
impl ArcBallController {
    pub fn world_space_frame(&self) -> Frame {
        let eye = self.eye_pt3::<Meters>();
        Frame::from_geodetic_bearing_and_pitch(Geodetic::from(eye), self.bearing(), -self.pitch())
    }

    #[method]
    pub fn notable_location(&self, name: &str) -> Result<Geodetic> {
        Ok(match name {
            "ISS" => Geodetic::new(degrees!(27.988), degrees!(86.924), meters!(408_000.)),
            "Everest" => Geodetic::new(degrees!(27.9880704), degrees!(86.9245623), meters!(8000.)),
            "London" => Geodetic::new(degrees!(51.5), degrees!(-0.1), meters!(100.)),
            _ => bail!("unknown notable location: {}", name),
        })
    }

    #[method]
    pub fn bearing_for_notable_location(&self, name: &str) -> Bearing {
        Bearing::new(match name {
            "ISS" => degrees!(308.0),
            "Everest" => degrees!(130),
            _ => degrees!(149.5),
        })
    }

    #[method]
    pub fn pitch_for_notable_location(&self, name: &str) -> PitchCline {
        PitchCline::new(match name {
            "ISS" => degrees!(58),
            "Everest" => degrees!(9),
            _ => degrees!(11.5),
        })
    }

    #[method]
    pub fn distance_for_notable_location(&self, name: &str) -> Length<Meters> {
        match name {
            "ISS" => meters!(1_308.),
            "Everest" => meters!(12_000.),
            _ => meters!(67_668.),
        }
    }

    #[method]
    pub fn target(&self) -> Geodetic {
        self.map_frame.geodetic().to_owned()
    }

    pub fn set_target_imm_unsafe(&mut self, target: Geodetic) {
        self.map_frame.set_geodetic(target);
    }

    pub fn warp_to_notable(&mut self, location: &str) -> Result<()> {
        let target = self.notable_location(location)?;
        let bearing = self.bearing_for_notable_location(location);
        let pitch = self.pitch_for_notable_location(location);
        let distance = self.distance_for_notable_location(location);
        self.map_frame.set_geodetic(target);
        self.map_frame.set_bearing(bearing);
        self.map_frame.set_pitch(pitch);
        self.set_distance(distance)?;
        Ok(())
    }

    pub fn move_to(
        &mut self,
        target: Geodetic,
        bearing: Bearing,
        pitch: PitchCline,
        distance: Length<Meters>,
    ) -> Result<()> {
        self.map_frame.set_geodetic(target);
        self.map_frame.set_bearing(bearing);
        self.map_frame.set_pitch(pitch);
        self.set_distance(distance)?;
        Ok(())
    }

    pub fn target_mut(&mut self) -> &mut Geodetic {
        self.map_frame.geodetic_mut()
    }

    pub fn map_frame(&self) -> &MapFrame {
        &self.map_frame
    }

    pub fn set_map_frame(&mut self, map_frame: MapFrame) {
        self.map_frame = map_frame;
    }

    #[method]
    pub fn bearing(&self) -> Bearing {
        self.map_frame.bearing()
    }

    #[method]
    pub fn set_bearing(&mut self, bearing: Bearing) {
        self.map_frame.set_bearing(bearing);
    }

    pub fn bearing_mut(&mut self) -> &mut Bearing {
        self.map_frame.bearing_mut()
    }

    #[method]
    pub fn pitch(&self) -> PitchCline {
        self.map_frame.pitch()
    }

    #[method]
    pub fn set_pitch(&mut self, pitch: PitchCline) {
        self.map_frame.set_pitch(pitch);
    }

    pub fn pitch_mut(&mut self) -> &mut PitchCline {
        self.map_frame.pitch_mut()
    }

    #[method]
    pub fn distance(&self) -> Length<Meters> {
        self.eye_distance
    }

    #[method]
    pub fn set_distance(&mut self, distance: Length<Meters>) -> Result<()> {
        ensure!(distance > meters!(0), "eye distance negative");
        self.eye_distance = distance;
        Ok(())
    }

    #[method]
    pub fn show_parameters(&self) -> Result<String> {
        let mut out = String::new();
        writeln!(
            out,
            "@camera.arcball.set_target_latitude_degrees({});",
            self.target_latitude().f64()
        )?;
        writeln!(
            out,
            "@camera.arcball.set_target_longitude_degrees({});",
            self.target_longitude().f64()
        )?;
        writeln!(
            out,
            "@camera.arcball.set_target_height_meters({});",
            self.target_height_meters().f64()
        )?;
        writeln!(
            out,
            "@camera.arcball.set_bearing_degrees({});",
            self.bearing().angle::<Degrees>().f64()
        )?;
        writeln!(
            out,
            "@camera.arcball.set_pitch_degrees({});",
            self.pitch().angle::<Degrees>().f64()
        )?;
        writeln!(
            out,
            "@camera.arcball.set_eye_distance_meters({});",
            self.distance().f64()
        )?;
        println!("{out}");
        Ok(out)
    }

    #[method]
    pub fn target_latitude(&self) -> Angle<Degrees> {
        self.target().lat::<Degrees>()
    }

    #[method]
    pub fn target_longitude(&self) -> Angle<Degrees> {
        self.target().lon::<Degrees>()
    }

    #[method]
    pub fn target_height_meters(&self) -> Length<Meters> {
        self.target().asl::<Meters>()
    }

    #[method]
    pub fn set_target_latitude(&mut self, v: Angle<Degrees>) {
        self.target().set_lat(v);
    }

    #[method]
    pub fn set_target_longitude(&mut self, v: Angle<Degrees>) {
        self.target().set_lon(v);
    }

    #[method]
    pub fn set_target_height(&mut self, v: Length<Meters>) {
        self.target().set_asl(v);
    }

    // FIXME: remove these once we enhance the prelude with common units
    #[method]
    pub fn set_target_latitude_degrees(&mut self, v: f64) {
        self.target().set_lat(degrees!(v));
    }
    #[method]
    pub fn set_target_longitude_degrees(&mut self, v: f64) {
        self.target().set_lon(degrees!(v));
    }
    #[method]
    pub fn set_target_height_meters(&mut self, v: f64) {
        self.target().set_asl(meters!(v));
    }

    #[method]
    pub fn set_eye_latitude_degrees(&mut self, v: f64) {
        self.set_pitch(PitchCline::new(degrees!(v)));
    }

    #[method]
    pub fn set_pitch_degrees(&mut self, v: f64) {
        self.set_pitch(PitchCline::new(degrees!(v)));
    }

    #[method]
    pub fn set_eye_longitude_degrees(&mut self, v: f64) {
        self.set_bearing(Bearing::new(degrees!(v)));
    }

    #[method]
    pub fn set_bearing_degrees(&mut self, v: f64) {
        self.set_bearing(Bearing::new(degrees!(v)));
    }

    #[method]
    pub fn set_eye_distance_meters(&mut self, v: f64) {
        self.eye_distance = meters!(v);
    }

    fn map_space_forward(&self) -> Pt3<Meters> {
        // Bearing is a map coords; e.g. 2-d with y as north, x as right.

        // Note: pitch is inverted because we are eye rel not target rel
        let mut bearing = self.bearing().dvec3(-(self.pitch()));
        // Invert x because map coordinates are inverted from our left-handed setup.
        bearing.x = -bearing.x;
        bearing * self.eye_distance
    }

    fn world_space_forward(&self) -> Pt3<Meters> {
        let orient = DQuat::from_euler(
            EulerRot::YXZ,
            -self.target().lon::<Radians>().f64(),
            -self.target().lat::<Radians>().f64(),
            0.,
        );
        orient * self.map_space_forward()
    }

    fn eye_pt3<Unit: LengthUnit>(&self) -> Pt3<Unit> {
        self.target().pt3::<Unit>() - Pt3::<Unit>::from(&self.world_space_forward())
    }

    #[method]
    pub fn pan_view(&mut self, pressed: bool) {
        self.input.in_rotate = pressed;
    }

    #[method]
    pub fn move_view(&mut self, pressed: bool) {
        self.input.in_move = pressed;
    }

    #[method]
    pub fn handle_mousemotion(&mut self, x: f64, y: f64) {
        if self.input.in_rotate {
            *self.bearing_mut().angle_mut() += radians!(degrees!(x * 0.5));
            *self.bearing_mut().angle_mut() =
                radians!(degrees!(self.bearing().angle::<Degrees>().f64() % 360.));
            *self.pitch_mut().angle_mut() = radians!((self.pitch().angle::<Degrees>()
                + degrees!(y * 0.5))
            .clamp(degrees!(-80), degrees!(80)));
        }

        if self.input.in_move {
            let sensitivity: f64 = (meters!(self.eye_distance) / scalar!(60_000_000.0)).f64();

            let fract_y = self.bearing().dvec2();
            let fract_x = (self.bearing() + Bearing::west()).dvec2();
            let dy_lat = -fract_y.y * y * sensitivity;
            let dy_lon = fract_y.x * y * sensitivity;
            let dx_lat = fract_x.y * x * sensitivity;
            let dx_lon = -fract_x.x * x * sensitivity;
            *self.target_mut().lat_mut() -= degrees!(-dx_lat + dy_lat);
            *self.target_mut().lon_mut() -= degrees!(dx_lon - dy_lon);
        }
    }

    #[method]
    pub fn handle_mousewheel(&mut self, vertical: f64) {
        // up/down is y
        //   Up is negative
        //   Down is positive
        //   Works in steps of 15 for my mouse.
        self.eye_distance *= scalar!(if vertical > 0f64 { 1.1f64 } else { 0.9f64 });
        self.eye_distance = self.eye_distance.max(meters!(0.01));
    }

    #[method]
    pub fn target_up(&mut self, pressed: bool) {
        if pressed {
            self.input.target_height_delta = meters!(1);
        } else {
            self.input.target_height_delta = meters!(0);
        }
    }

    #[method]
    pub fn target_down(&mut self, pressed: bool) {
        if pressed {
            self.input.target_height_delta = meters!(-1);
        } else {
            self.input.target_height_delta = meters!(0);
        }
    }

    #[method]
    pub fn target_up_fast(&mut self, pressed: bool) {
        if pressed {
            self.input.target_height_delta = meters!(100);
        } else {
            self.input.target_height_delta = meters!(0);
        }
    }

    #[method]
    pub fn target_down_fast(&mut self, pressed: bool) {
        if pressed {
            self.input.target_height_delta = meters!(-100);
        } else {
            self.input.target_height_delta = meters!(0);
        }
    }

    // Take the inputs applied via interpreting key presses in the prior stage and apply it.
    fn sys_apply_input(mut query: Query<(&mut ArcBallController, &mut Frame)>) {
        for (mut arcball, mut frame) in query.iter_mut() {
            arcball.apply_input_state();
            *frame = arcball.world_space_frame();
        }
    }

    fn apply_input_state(&mut self) {
        let delta = self.input.target_height_delta;
        *self.target_mut().asl_mut() += delta;
        if self.target().asl() < meters!(0f64) {
            self.target_mut().set_asl(meters!(0f64));
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use absolute_unit::Kilometers;
    use anyhow::Result;
    use approx::{assert_abs_diff_eq, assert_relative_eq};
    use camera::ScreenCamera;
    use geodesy::{Bearing, Geodetic, PitchCline};
    use glam::{DVec4, Vec4Swizzles};
    use hifitime::Epoch;
    use mantle::{Core, Gpu, PhysicalSize};
    use once_cell::sync::Lazy;
    use planck::EARTH_RADIUS;

    static TESTS: Lazy<[((f64, f64), (f64, f64), (f64, f64, f64)); 16]> = Lazy::new(|| {
        let sc45 = degrees!(45).sin().f64();
        let er = meters!(*EARTH_RADIUS).f64();
        [
            ((0_f64, 0_f64), (0_f64, 0_f64), (0_f64, -1_f64, er)),
            ((0_f64, 90_f64), (0_f64, 0_f64), (1_f64, 0_f64, er)),
            ((0_f64, -90_f64), (0_f64, 0_f64), (-1_f64, 0_f64, er)),
            ((0_f64, -180_f64), (0_f64, 0_f64), (0_f64, 1_f64, er)),
            ((45_f64, 0_f64), (0_f64, 0_f64), (0_f64, -sc45, er + sc45)),
            ((45_f64, 90_f64), (0_f64, 0_f64), (sc45, 0_f64, er + sc45)),
            ((45_f64, -90_f64), (0_f64, 0_f64), (-sc45, 0_f64, er + sc45)),
            ((45_f64, -180_f64), (0_f64, 0_f64), (0_f64, sc45, er + sc45)),
            ((0_f64, 0_f64), (0_f64, 90_f64), (-er, -1_f64, 0_f64)),
            ((0_f64, 90_f64), (0_f64, 90_f64), (-er, 0_f64, 1_f64)),
            ((0_f64, -90_f64), (0_f64, 90_f64), (-er, 0_f64, -1_f64)),
            ((0_f64, -180_f64), (0_f64, 90_f64), (-er, 1_f64, 0_f64)),
            ((0_f64, 0_f64), (0_f64, -90_f64), (er, -1_f64, 0_f64)),
            ((0_f64, 90_f64), (0_f64, -90_f64), (er, 0_f64, -1_f64)),
            ((0_f64, -90_f64), (0_f64, -90_f64), (er, 0_f64, 1_f64)),
            ((0_f64, -180_f64), (0_f64, -90_f64), (er, 1_f64, 0_f64)),
        ]
    });

    #[test]
    fn test_eye_relative_mapping() -> Result<()> {
        let mut c = ArcBallController::default();
        c.set_target_imm_unsafe(Geodetic::new(radians!(0), radians!(0), meters!(0)));
        c.set_bearing(Bearing::north());
        c.set_distance(meters!(1))?;

        // Verify base target position.
        let t = c.target().pt3::<Kilometers>();
        assert_abs_diff_eq!(t.x_as::<Meters>(), meters!(0_f64));
        assert_abs_diff_eq!(t.y_as::<Meters>(), meters!(0_f64));
        assert_abs_diff_eq!(t.z_as::<Meters>(), meters!(*EARTH_RADIUS));

        // Target: 0/0; at latitude of 0:
        for ((elat, elon), (tlat, tlon), (x, y, z)) in &*TESTS {
            // Longitude 0 maps to south, latitude 90 to up,
            // when rotated into the surface frame.
            c.set_target_imm_unsafe(Geodetic::new(degrees!(*tlat), degrees!(*tlon), meters!(0)));
            c.set_bearing(Bearing::new(degrees!(*elon)));
            c.set_pitch(PitchCline::new(degrees!(*elat)));
            c.set_distance(meters!(1))?;
            let e = c.eye_pt3::<Meters>();
            assert_abs_diff_eq!(e.x(), meters!(*x));
            assert_abs_diff_eq!(e.y(), meters!(*y));
            assert_relative_eq!(e.z(), meters!(*z), epsilon = 1e-9);
        }

        Ok(())
    }

    #[test]
    fn test_world_to_eye() -> Result<()> {
        let runtime = Core::for_test()?;

        // At lat/lon 0/0, looking due north
        let mut arcball = ArcBallController::default();
        arcball.set_target_imm_unsafe(Geodetic::new(degrees!(0), degrees!(0), meters!(2)));
        arcball.set_bearing(Bearing::new(degrees!(0)));
        arcball.set_pitch(PitchCline::new(degrees!(0)));
        arcball.set_distance(meters!(1.))?;

        let mut camera = ScreenCamera::new(
            "test",
            degrees!(90),
            PhysicalSize::new(1920, 1080),
            meters!(0.3),
            runtime.resource::<Gpu>(),
        );
        camera.update_frame(&arcball.world_space_frame(), &Epoch::now()?);

        // In left handed world space, +1 x/z is going to be forward and left.
        let mut pos = arcball.target().pt3::<Meters>();
        pos.set_x(pos.x() + meters!(1));

        // Translated into eye space with z forward, left on x should be -1 and z should be one ahead of us
        let view = camera.world_to_eye::<Meters>();
        let v = view.transform_point3(pos.dvec3());
        println!("Pos: {pos:0.2} => {v:0.2}");
        assert_relative_eq!(v.x, -1., epsilon = 1e-6);
        assert_relative_eq!(v.y, 0., epsilon = 1e-6);
        assert_relative_eq!(v.z, 1., epsilon = 1e-6);

        // Rotating the view up 90 degrees should not affect the position of the point:
        // it's still in front and to the left of the camera.
        arcball.set_pitch(PitchCline::new(degrees!(90)));
        camera.update_frame(&arcball.world_space_frame(), &Epoch::now()?);
        let v = camera
            .world_to_eye::<Meters>()
            .transform_point3(pos.dvec3());
        println!("Pos: {pos:0.2} => {v:0.2}");
        assert_relative_eq!(v.x, -1., epsilon = 1e-6);
        assert_relative_eq!(v.y, 0., epsilon = 1e-6);
        assert_relative_eq!(v.z, 1., epsilon = 1e-6);

        // Ditto 45 degrees
        arcball.set_pitch(PitchCline::new(degrees!(45)));
        camera.update_frame(&arcball.world_space_frame(), &Epoch::now()?);
        let v = camera
            .world_to_eye::<Meters>()
            .transform_point3(pos.dvec3());
        println!("Pos: {pos:0.2} => {v:0.2}");
        assert_relative_eq!(v.x, -1., epsilon = 1e-6);
        assert_relative_eq!(v.y, 0., epsilon = 1e-6);
        assert_relative_eq!(v.z, 1., epsilon = 1e-6);

        // Ditto -45 degrees
        arcball.set_pitch(PitchCline::new(degrees!(-45)));
        camera.update_frame(&arcball.world_space_frame(), &Epoch::now()?);
        let v = camera
            .world_to_eye::<Meters>()
            .transform_point3(pos.dvec3());
        println!("Pos: {pos:0.2} => {v:0.2}");
        assert_relative_eq!(v.x, -1., epsilon = 1e-6);
        assert_relative_eq!(v.y, 0., epsilon = 1e-6);
        assert_relative_eq!(v.z, 1., epsilon = 1e-6);

        // Doubling the camera distance shouldn't change anything but the distance to point
        arcball.set_distance(meters!(2.))?;
        camera.update_frame(&arcball.world_space_frame(), &Epoch::now()?);
        let v = camera
            .world_to_eye::<Meters>()
            .transform_point3(pos.dvec3());
        println!("Pos: {pos:0.2} => {v:0.2}");
        assert_relative_eq!(v.x, -1., epsilon = 1e-6);
        assert_relative_eq!(v.y, 0., epsilon = 1e-6);
        assert_relative_eq!(v.z, 2., epsilon = 1e-6);

        Ok(())
    }

    #[test]
    fn test_depth_restore() -> Result<()> {
        let runtime = Core::for_test()?;
        let mut camera = ScreenCamera::new(
            "test",
            degrees!(90),
            PhysicalSize::new(1920, 1080),
            meters!(0.5),
            runtime.resource::<Gpu>(),
        );
        let mut arcball = ArcBallController::default();
        arcball.set_target_imm_unsafe(Geodetic::new(degrees!(0), degrees!(0), meters!(2)));
        arcball.set_bearing(Bearing::new(degrees!(0)));
        arcball.set_pitch(PitchCline::new(degrees!(89)));
        arcball.set_distance(
            meters!(4_000_000),
            // meters!(1_400_000),
        )?;
        let frame = arcball.world_space_frame();
        camera.update_frame(&frame, &Epoch::now()?);

        let camera_position_km = camera.position::<Kilometers>();
        let camera_inverse_perspective_km = camera.perspective::<Kilometers>().inverse();
        let camera_inverse_view_km = camera.world_to_eye::<Kilometers>().inverse();

        // Given corner positions in ndc of -1,-1 and 1,1... what does a mostly forward vector
        // in ndc map to, given the above camera?
        let corner = DVec4::new(0.1, 0.1, 0.0, 1.0);

        let eye = (camera_inverse_perspective_km * corner).normalize();
        let wrld = (camera_inverse_view_km * eye).normalize();
        println!("pos: {camera_position_km}");
        println!("eye : {eye}");
        println!("wrld: {wrld}");
        println!("pos: {}", camera_position_km.dvec3() + wrld.xyz() * 8000.0);

        Ok(())
    }
}
