// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use anyhow::{bail, ensure, Result};
use bevy_ecs::prelude::*;
use futures::future::{ready, FutureExt};
use hifitime::{Duration, Epoch};
use log::error;
use lyon_geom::{cubic_bezier::CubicBezierSegment, Point};
use nitrous::{inject_nitrous_resource, method, Dict, HeapMut, NitrousResource, Value};
use parking_lot::RwLock;
use runtime::{Extension, Runtime, ScriptStep, TimeStep};
use std::{collections::HashMap, sync::Arc};
use triggered::{trigger, Trigger};

#[derive(Debug)]
pub struct CubicBezierCurve {
    bezier: CubicBezierSegment<f64>,
}

impl CubicBezierCurve {
    pub const LINEAR: CubicBezierCurve = CubicBezierCurve::new((0., 0.), (1., 1.));
    pub const EASE: CubicBezierCurve = CubicBezierCurve::new((0.25, 0.1), (0.25, 1.));
    pub const EASE_IN: CubicBezierCurve = CubicBezierCurve::new((0.42, 0.), (1., 1.));
    pub const EASE_OUT: CubicBezierCurve = CubicBezierCurve::new((0., 0.), (0.58, 1.));
    pub const EASE_IN_OUT: CubicBezierCurve = CubicBezierCurve::new((0.42, 0.), (0.58, 1.));

    pub const fn new((x1, y1): (f64, f64), (x2, y2): (f64, f64)) -> Self {
        Self {
            bezier: CubicBezierSegment {
                from: Point::new(0., 0.),
                ctrl1: Point::new(x1, y1),
                ctrl2: Point::new(x2, y2),
                to: Point::new(1., 1.),
            },
        }
    }

    pub fn interpolate(&self, x: f64) -> f64 {
        let ts = self.bezier.solve_t_for_x(x);
        if let Some(&t) = ts.first() {
            self.bezier.y(t)
        } else {
            x
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum AnimationState {
    Starting,
    Running,
    Finished,
}

#[derive(Debug)]
struct ScriptableAnimation {
    trigger: Trigger,
    callable: Option<Value>,
    start: Value,
    end: Value,
    bezier: CubicBezierCurve,
    duration: Duration,
    duration_f64: f64,
    start_time: Option<Epoch>,
    state: AnimationState,
}

impl ScriptableAnimation {
    pub fn new(
        trigger: Trigger,
        callable: Value,
        start: Value,
        end: Value,
        bezier: CubicBezierCurve,
        duration: Duration,
    ) -> Self {
        Self {
            trigger,
            callable: Some(callable),
            start,
            end,
            bezier,
            duration,
            duration_f64: duration.to_seconds(),
            start_time: None,
            state: AnimationState::Starting,
        }
    }

    pub fn empty(trigger: Trigger, duration: Duration) -> Self {
        Self {
            trigger,
            callable: None,
            start: 0.0.into(),
            end: 0.0.into(),
            bezier: CubicBezierCurve::LINEAR,
            duration,
            duration_f64: duration.to_seconds(),
            start_time: None,
            state: AnimationState::Starting,
        }
    }

    pub fn apply_fract(&self, f: f64) -> Result<Value> {
        Ok(if self.start.is_numeric() {
            let t0 = self.start.to_numeric()?;
            let t1 = self.end.to_numeric()?;
            (t0 + (t1 - t0) * f).into()
        } else {
            bail!("don't know how to animate non-float values")
        })
    }

    pub fn step_time(&mut self, now: &Epoch, world: &mut World) -> Result<()> {
        assert_ne!(self.state, AnimationState::Finished);
        let (current, mut ended) = if let Some(start_time) = self.start_time {
            let f0 = (*now - start_time).to_seconds() / self.duration_f64;
            let f = self.bezier.interpolate(f0);
            let current = self.apply_fract(f)?;
            if (*now - start_time) >= self.duration {
                (self.end.clone(), true)
            } else {
                (current, false)
            }
        } else {
            self.start_time = Some(*now);
            self.state = AnimationState::Running;
            (self.start.clone(), false)
        };
        if let Some(callable) = &mut self.callable {
            if callable
                .call_method(vec![current], HeapMut::wrap(world))
                .is_err()
            {
                ended = true;
            }
        }
        if ended {
            self.state = AnimationState::Finished;
            self.trigger.trigger();
        }
        Ok(())
    }

    pub fn is_finished(&self) -> bool {
        self.state == AnimationState::Finished
    }
}

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum TimelineStep {
    Animate,
}

/// Drive scriptable animations.
#[derive(Default, Debug, NitrousResource)]
pub struct Timeline {
    animations: HashMap<i64, ScriptableAnimation>,
    last_handle: i64,
}

impl Extension for Timeline {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.inject_resource(Timeline::default())?;
        runtime.add_script_system(
            Self::sys_animate
                .in_set(TimelineStep::Animate)
                .after(ScriptStep::Run),
        );
        Ok(())
    }
}

#[inject_nitrous_resource]
impl Timeline {
    fn sys_animate(world: &mut World) {
        let now = *world.get_resource::<TimeStep>().unwrap().sim_now_epoch();
        world.resource_scope(|world, mut timeline: Mut<Timeline>| {
            timeline.step_time(&now, world);
        });
    }

    pub fn step_time(&mut self, now: &Epoch, world: &mut World) {
        for animation in self.animations.values_mut() {
            // One animation failing should not propagate to others.
            if let Err(e) = animation.step_time(now, world) {
                animation.state = AnimationState::Finished;
                error!("step_time failed with: {}", e);
            }
        }
        self.animations
            .retain(|_handle, animation| !animation.is_finished());
    }

    pub fn none_handle() -> Value {
        let mut out = Dict::default();
        out.insert("handle", Value::Integer(0));
        out.into()
    }

    #[method]
    pub fn stop(&mut self, handle: Value) -> Result<Value> {
        ensure!(handle.is_numeric(), "not a handle type");
        self.animations.remove(&handle.to_int()?);
        Ok(Value::True())
    }

    pub fn with_curve(
        &mut self,
        callable: Value,
        start: Value,
        end: Value,
        duration: Time<Seconds>,
        bezier: CubicBezierCurve,
    ) -> Result<Value> {
        ensure!(start.is_numeric() && end.is_numeric());
        self.last_handle += 1;
        let handle = self.last_handle;
        let (trigger, listener) = trigger();
        self.animations.insert(
            handle,
            ScriptableAnimation::new(
                trigger,
                callable,
                start,
                end,
                bezier,
                Duration::from_seconds(duration.f64()),
            ),
        );
        let mut out = Dict::default();
        out.insert("handle", Value::Integer(handle));
        out.insert(
            "listener",
            Value::Future(Arc::new(RwLock::new(Box::pin(
                listener.then(|_| ready(Value::True())),
            )))),
        );
        Ok(Value::Dict(out))
    }

    #[method]
    pub fn lerp(
        &mut self,
        callable: Value,
        start: f64,
        offset: f64,
        duration: Time<Seconds>,
    ) -> Result<Value> {
        self.with_curve(
            callable,
            start.into(),
            (start + offset).into(),
            duration,
            CubicBezierCurve::LINEAR,
        )
    }

    #[method]
    pub fn ease(
        &mut self,
        callable: Value,
        start: f64,
        offset: f64,
        duration: Time<Seconds>,
    ) -> Result<Value> {
        self.with_curve(
            callable,
            start.into(),
            (start + offset).into(),
            duration,
            CubicBezierCurve::EASE,
        )
    }

    #[method]
    pub fn ease_in(
        &mut self,
        callable: Value,
        start: f64,
        offset: f64,
        duration: Time<Seconds>,
    ) -> Result<Value> {
        self.with_curve(
            callable,
            start.into(),
            (start + offset).into(),
            duration,
            CubicBezierCurve::EASE_IN,
        )
    }

    #[method]
    pub fn ease_out(
        &mut self,
        callable: Value,
        start: f64,
        offset: f64,
        duration: Time<Seconds>,
    ) -> Result<Value> {
        self.with_curve(
            callable,
            start.into(),
            (start + offset).into(),
            duration,
            CubicBezierCurve::EASE_OUT,
        )
    }

    #[method]
    pub fn ease_in_out(
        &mut self,
        callable: Value,
        start: f64,
        offset: f64,
        duration: Time<Seconds>,
    ) -> Result<Value> {
        self.with_curve(
            callable,
            start.into(),
            (start + offset).into(),
            duration,
            CubicBezierCurve::EASE_IN_OUT,
        )
    }

    #[method]
    pub fn ease_to(
        &mut self,
        callable: Value,
        start: Value,
        end: Value,
        duration: Time<Seconds>,
    ) -> Result<Value> {
        self.with_curve(callable, start, end, duration, CubicBezierCurve::EASE)
    }

    #[method]
    pub fn ease_in_to(
        &mut self,
        callable: Value,
        start: Value,
        end: Value,
        duration: Time<Seconds>,
    ) -> Result<Value> {
        self.with_curve(callable, start, end, duration, CubicBezierCurve::EASE_IN)
    }

    #[method]
    pub fn ease_out_to(
        &mut self,
        callable: Value,
        start: Value,
        end: Value,
        duration: Time<Seconds>,
    ) -> Result<Value> {
        self.with_curve(callable, start, end, duration, CubicBezierCurve::EASE_OUT)
    }

    #[method]
    pub fn ease_in_out_to(
        &mut self,
        callable: Value,
        start: Value,
        end: Value,
        duration: Time<Seconds>,
    ) -> Result<Value> {
        self.with_curve(
            callable,
            start,
            end,
            duration,
            CubicBezierCurve::EASE_IN_OUT,
        )
    }

    #[allow(clippy::too_many_arguments)]
    #[method]
    pub fn ease_bezier_to(
        &mut self,
        callable: Value,
        start: Value,
        end: Value,
        duration: Time<Seconds>,
        x1: f64,
        y1: f64,
        x2: f64,
        y2: f64,
    ) -> Result<Value> {
        self.with_curve(
            callable,
            start,
            end,
            duration,
            CubicBezierCurve::new((x1, y1), (x2, y2)),
        )
    }

    #[method]
    pub fn sleep(&mut self, duration: Time<Seconds>) -> Result<Value> {
        let (trigger, listener) = trigger();
        self.last_handle += 1;
        let handle = self.last_handle;
        self.animations.insert(
            handle,
            ScriptableAnimation::empty(trigger, Duration::from_seconds(duration.f64())),
        );
        let mut out = Dict::default();
        out.insert("handle", Value::Integer(handle));
        out.insert(
            "listener",
            Value::Future(Arc::new(RwLock::new(Box::pin(
                listener.then(|_| ready(Value::True())),
            )))),
        );
        Ok(Value::Dict(out))
    }
}
