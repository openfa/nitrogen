// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::Result;
use bevy_ecs::prelude::*;
use camera::{CameraStep, ScreenCamera};
use gui::{Gui, GuiStep};
use log::trace;
use mantle::{CurrentEncoder, CurrentFrame, Gpu, GpuStep};
use nitrous::{inject_nitrous_resource, NitrousResource};
use runtime::{Extension, Runtime};

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum CompositeStep {
    Render,
}

#[derive(Debug, NitrousResource)]
pub struct Composite {
    pipeline: wgpu::RenderPipeline,
}

impl Extension for Composite {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        let composite = Composite::new(runtime.resource::<Gui>(), runtime.resource::<Gpu>())?;
        runtime.inject_resource(composite)?;
        runtime.add_frame_system(
            Self::sys_composite_scene
                .in_set(CompositeStep::Render)
                .after(CameraStep::UploadToGpu)
                .after(GpuStep::CreateCommandEncoder)
                .after(GuiStep::EndFrame)
                .before(GpuStep::SubmitCommands),
        );
        Ok(())
    }
}

#[inject_nitrous_resource]
impl Composite {
    pub fn new(gui: &Gui, gpu: &Gpu) -> Result<Self> {
        trace!("CompositeRenderPass::new");

        let shader = gpu.compile_shader("composite.wgsl", include_str!("../target/composite.wgsl"));

        // Layout shared by all three render passes.
        let pipeline_layout =
            gpu.device()
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: Some("composite-pipeline-layout"),
                    push_constant_ranges: &[],
                    bind_group_layouts: &[
                        &ScreenCamera::info_bind_group_layout(gpu),
                        gui.bind_group_layout(),
                        &ScreenCamera::make_bind_group_layout(
                            &[ScreenCamera::COLOR_FORMAT],
                            true,
                            gpu,
                        ),
                    ],
                });

        let pipeline = gpu
            .device()
            .create_render_pipeline(&wgpu::RenderPipelineDescriptor {
                label: Some("composite-pipeline"),
                layout: Some(&pipeline_layout),
                vertex: wgpu::VertexState {
                    module: &shader,
                    entry_point: "main_vert",
                    buffers: &[],
                    compilation_options: wgpu::PipelineCompilationOptions {
                        constants: &Default::default(),
                        zero_initialize_workgroup_memory: false,
                    },
                },
                fragment: Some(wgpu::FragmentState {
                    module: &shader,
                    entry_point: "main_frag",
                    targets: &[Some(wgpu::ColorTargetState {
                        format: Gpu::SCREEN_FORMAT,
                        blend: None,
                        write_mask: wgpu::ColorWrites::ALL,
                    })],
                    compilation_options: wgpu::PipelineCompilationOptions {
                        constants: &Default::default(),
                        zero_initialize_workgroup_memory: false,
                    },
                }),
                primitive: wgpu::PrimitiveState {
                    topology: wgpu::PrimitiveTopology::TriangleList,
                    strip_index_format: None,
                    front_face: wgpu::FrontFace::Ccw,
                    cull_mode: Some(wgpu::Face::Back),
                    unclipped_depth: false,
                    polygon_mode: wgpu::PolygonMode::Fill,
                    conservative: false,
                },
                depth_stencil: Some(wgpu::DepthStencilState {
                    format: Gpu::DEPTH_FORMAT,
                    depth_write_enabled: false,
                    depth_compare: wgpu::CompareFunction::Always,
                    stencil: wgpu::StencilState {
                        front: wgpu::StencilFaceState::IGNORE,
                        back: wgpu::StencilFaceState::IGNORE,
                        read_mask: 0,
                        write_mask: 0,
                    },
                    bias: wgpu::DepthBiasState {
                        constant: 0,
                        slope_scale: 0.0,
                        clamp: 0.0,
                    },
                }),
                multisample: wgpu::MultisampleState {
                    count: 1,
                    mask: !0,
                    alpha_to_coverage_enabled: false,
                },
                multiview: None,
            });

        Ok(Self { pipeline })
    }

    // composite: Accumulate offscreen buffers into a final image.
    #[allow(clippy::too_many_arguments)]
    fn sys_composite_scene(
        composite: Res<Composite>,
        gui: Res<Gui>,
        gpu: Res<Gpu>,
        maybe_frame: Res<CurrentFrame>,
        mut camera: ResMut<ScreenCamera>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) {
        if let Some(surface_texture) = maybe_frame.get_surface() {
            if let Some(encoder) = maybe_encoder.get_encoder_mut() {
                camera.ensure_bind_group("composite", &["color"], true, &gpu);
                let view = surface_texture
                    .texture
                    .create_view(&::wgpu::TextureViewDescriptor::default());
                let render_pass_desc_ref = wgpu::RenderPassDescriptor {
                    label: Some("screen-composite-render-pass"),
                    color_attachments: &[Some(Gpu::color_attachment(&view))],
                    depth_stencil_attachment: Some(gpu.depth_stencil_attachment()),
                    timestamp_writes: None,
                    occlusion_query_set: None,
                };
                let rpass = encoder.begin_render_pass(&render_pass_desc_ref);
                let _rpass = composite.composite_scene(rpass, &camera, &gui);
            }
        }
    }

    pub fn composite_scene<'a>(
        &'a self,
        mut rpass: wgpu::RenderPass<'a>,
        camera: &'a ScreenCamera,
        gui: &'a Gui,
    ) -> wgpu::RenderPass<'a> {
        rpass.set_pipeline(&self.pipeline);
        rpass.set_bind_group(0, camera.info_bind_group(), &[]);
        rpass.set_bind_group(1, gui.screen_bind_group(), &[]);
        rpass.set_bind_group(2, camera.bind_group("composite"), &[]);
        rpass.draw(0..3, 0..1);
        rpass
    }
}
