// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <shader_shared/include/fullscreen.wgsl>
//!include <camera/include/camera.wgsl>

//!bindings [0] <camera/bindings/camera.wgsl>
//!include <gui/include/gui-offscreen_1.wgsl>
//!bindings [2] <camera/bindings/target.wgsl>

struct VertexOutput {
    @location(0) texcoord: vec2<f32>,
    @builtin(position) position: vec4<f32>,
}

@vertex
fn main_vert(@builtin(vertex_index) i: u32) -> VertexOutput {
    let input = fullscreen_input(i);
    return VertexOutput(input.frame, input.clip);
}

fn compute_glass_background(tc: vec2<f32>) -> vec3<f32> {
    let x_step = (1. / f32(camera.render_width)) * 4.;
    let y_step = (1. / f32(camera.render_height)) * 4.;
    var weights: array<f32, 49> = array<f32, 49>(
        0.000, 0.000, 0.001, 0.001, 0.001, 0.000, 0.000,
        0.000, 0.002, 0.012, 0.020, 0.012, 0.002, 0.000,
        0.001, 0.012, 0.068, 0.109, 0.068, 0.012, 0.001,
        0.001, 0.020, 0.109, 0.172, 0.109, 0.020, 0.001,
        0.001, 0.012, 0.068, 0.109, 0.068, 0.012, 0.001,
        0.000, 0.002, 0.012, 0.020, 0.012, 0.002, 0.000,
        0.000, 0.000, 0.001, 0.001, 0.001, 0.000, 0.000
    );
    var acc = vec3<f32>(0.);
    for (var i: i32 = 0; i < 49; i++) {
        let weight = weights[i];
        let x = i % 7;
        let y = i / 7;
        let dx = (f32(x) - 3.0) * x_step;
        let dy = (f32(y) - 3.0) * y_step;
        let world = textureSample(camera_texture, camera_sampler, tc + vec2<f32>(dx, dy));
        acc = acc + (world.rgb * weight);
    }
    return acc;
}

struct FragOut {
    @location(0) color: vec4<f32>,
}

@fragment
fn main_frag(@location(0) uv: vec2<f32>) -> FragOut {
    let base_color = textureSample(camera_texture, camera_sampler, uv);
    let gui = textureSample(gui_offscreen_texture, gui_offscreen_sampler, uv);
    let world_glass = vec4(compute_glass_background(uv), 1.);
    let world_underlay = base_color.rgb * (1. - gui.a) + world_glass.rgb * gui.a;
    let new_color = world_underlay * (1. - gui.a) + gui.rgb * gui.a;
    return FragOut(vec4<f32>(new_color, 1.));
}
