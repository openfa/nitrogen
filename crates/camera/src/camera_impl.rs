// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use animate::CubicBezierCurve;
use anyhow::Result;
use bevy_ecs::prelude::*;
use geodb::GeoDbStep;
use geometry::Plane;
use glam::{DMat4, DVec2, DVec3, DVec4, Vec4, Vec4Swizzles};
use hifitime::Epoch;
use mantle::{CurrentEncoder, DisplayConfig, Gpu, GpuStep, PhysicalSize, Window};
use nitrous::{
    inject_nitrous_component, inject_nitrous_resource, method, HeapMut, NitrousComponent,
    NitrousResource,
};
use orrery::OrreryStep;
use phase::Frame;
use runtime::{Extension, Runtime, TimeStep};
use shader_shared::{binding, layout};
use std::{collections::HashMap, f64::consts::PI};
use zerocopy::{Immutable, IntoBytes};

#[repr(C)]
#[derive(IntoBytes, Immutable, Clone, Copy, Default)]
pub(crate) struct CameraInfo {
    pad0: f32,
    pad1: f32,

    // Screen info
    render_width: u32,
    render_height: u32,

    pad2: [f32; 4],

    // Camera properties
    fov_y: f32,
    aspect_ratio: f32,
    z_near_m: f32,
    z_near_km: f32,
    forward: [f32; 4],
    up: [f32; 4],
    right: [f32; 4],
    position_m: [f32; 4],
    position_km: [f32; 4],
    perspective_m: [[f32; 4]; 4],
    perspective_km: [[f32; 4]; 4],
    inverse_perspective_m: [[f32; 4]; 4],
    inverse_perspective_km: [[f32; 4]; 4],
    view_m: [[f32; 4]; 4],
    view_km: [[f32; 4]; 4],
    inverse_view_m: [[f32; 4]; 4],
    inverse_view_km: [[f32; 4]; 4],
    look_at_lhs_m: [[f32; 4]; 4],

    // Tone mapping
    exposure: f32,
    tone_gamma: f32,

    pad3: f32,
    pad4: f32,
}

#[derive(Debug)]
struct TargetLayer {
    // Inputs to the layer
    format: wgpu::TextureFormat,

    // Keys the size we created with, so that we know when to re-create
    size: wgpu::Extent3d,

    // The GPU resources we're tracking
    layer: (wgpu::Texture, wgpu::TextureView),
}

#[derive(Debug)]
struct BoundLayers {
    // Keys the size we created with, so that we know when to re-create
    size: wgpu::Extent3d,

    // The source targets
    _targets: Vec<String>,

    // The GPU resources we've created
    bind_group: wgpu::BindGroup,
}

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum CameraStep {
    // ApplyInput,
    HandleDisplayChange,
    MoveCameraToFrame,
    UploadToGpu,
}

#[derive(Clone, Debug, Default)]
struct InputState {
    fov_delta: Angle<Degrees>,
}

#[derive(NitrousComponent, Clone, Debug, Default)]
pub struct ScreenCameraController;

#[inject_nitrous_component]
impl ScreenCameraController {}

#[derive(Debug, NitrousResource)]
#[resource(name = "camera")]
pub struct ScreenCamera {
    // Camera parameters
    label: String,
    fov_y: Angle<Radians>,
    aspect_ratio: f64,
    size: wgpu::Extent3d,
    z_near: Length<Meters>,

    #[property]
    exposure: f64,

    #[property]
    gamma: f64,

    input: InputState,

    // Gpu state
    info_buffer: wgpu::Buffer,
    info_bind_group: wgpu::BindGroup,
    sampler: wgpu::Sampler,
    layers: HashMap<String, TargetLayer>,
    bindings: HashMap<String, BoundLayers>,

    // Camera view state.
    position: Pt3<Meters>,
    forward: DVec3,
    up: DVec3,
    right: DVec3,

    // If a change to the view would move the camera abruptly -- e.g. switching views in the player
    // camera, moving to a different background in the menus, etc -- the caller should initiate a
    // "warp", on the camera. Centralizing the interpolation here lets us have many different
    // mechanisms for controlling the camera entity's location (e.g. pinning to a satellite,
    // control with an arcball, or the many different player controllers) all without losing the
    // immersion for a continuous camera system or forcing those mechanisms to talk to each other.
    warp: Option<(Frame, Time<Seconds>, Epoch, CubicBezierCurve)>,
}

impl Extension for ScreenCamera {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.inject_resource(Self::new(
            "resource",
            radians!(PI / 2.0),
            runtime.resource::<Window>().render_extent(),
            meters!(0.5),
            runtime.resource::<Gpu>(),
        ))?;

        runtime.add_frame_system(
            ScreenCamera::sys_apply_display_changes
                .in_set(CameraStep::HandleDisplayChange)
                .before(GpuStep::CreateCommandEncoder),
        );

        // Slurp the Frame data out of the frame and into the camera, for use in the camera buffer,
        // if a Frame entity is installed.
        runtime.add_frame_system(
            ScreenCamera::sys_move_camera_to_frame
                .in_set(CameraStep::MoveCameraToFrame)
                .after(CameraStep::HandleDisplayChange)
                .after(GeoDbStep::CheckDownloads),
        );

        runtime.add_frame_system(
            ScreenCamera::sys_upload_camera_params
                .in_set(CameraStep::UploadToGpu)
                .after(CameraStep::MoveCameraToFrame)
                .after(GpuStep::CreateCommandEncoder)
                .before(GpuStep::SubmitCommands)
                // Note: pin ordering for consistency
                .after(OrreryStep::UploadToGpu),
        );
        Ok(())
    }
}

#[inject_nitrous_resource]
impl ScreenCamera {
    const INITIAL_EXPOSURE: f64 = 10e-5;
    const INITIAL_GAMMA: f64 = 2.2;
    pub const COLOR_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Rgba8Unorm;
    pub const DEPTH_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Depth32Float;

    pub fn new<AngUnit: AngleUnit>(
        label: &str,
        fov_y: Angle<AngUnit>,
        render_size: PhysicalSize<u32>,
        z_near: Length<Meters>,
        gpu: &Gpu,
    ) -> Self {
        let info_buffer = gpu.push_data(
            "camera-buffer",
            &CameraInfo::default(),
            wgpu::BufferUsages::UNIFORM
                | wgpu::BufferUsages::STORAGE
                | wgpu::BufferUsages::COPY_DST,
        );
        let info_bind_group = gpu.device().create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some(&format!("{label}-camera-info-bind-group")),
            layout: &Self::info_bind_group_layout(gpu),
            entries: &[binding::buffer(0, &info_buffer)],
        });

        let size = wgpu::Extent3d {
            width: render_size.width,
            height: render_size.height,
            depth_or_array_layers: 1,
        };

        Self {
            label: label.to_string(),
            fov_y: radians!(fov_y),
            aspect_ratio: size.width as f64 / size.height as f64,
            size,
            z_near,
            exposure: Self::INITIAL_EXPOSURE,
            gamma: Self::INITIAL_GAMMA,

            input: InputState {
                fov_delta: degrees!(0),
            },

            info_buffer,
            info_bind_group,
            sampler: gpu.device().create_sampler(&wgpu::SamplerDescriptor {
                label: Some(&format!("{}-camera-target-sampler", label)),
                address_mode_u: wgpu::AddressMode::ClampToEdge,
                address_mode_v: wgpu::AddressMode::ClampToEdge,
                address_mode_w: wgpu::AddressMode::ClampToEdge,
                mag_filter: wgpu::FilterMode::Linear,
                min_filter: wgpu::FilterMode::Linear,
                mipmap_filter: wgpu::FilterMode::Linear,
                lod_min_clamp: 0f32,
                lod_max_clamp: 9_999_999f32,
                compare: None,
                anisotropy_clamp: 1,
                border_color: None,
            }),
            layers: HashMap::new(),
            bindings: HashMap::new(),

            position: Pt3::zero(),
            forward: DVec3::Z,
            up: DVec3::Y,
            right: DVec3::X,

            warp: None,
        }
    }

    pub fn info_bind_group_layout(gpu: &Gpu) -> wgpu::BindGroupLayout {
        gpu.device()
            .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                label: Some("camera-params-bind-group-layout"),
                entries: &[layout::buffer::<CameraInfo>(0, wgpu::ShaderStages::all())],
            })
    }

    pub fn info_bind_group(&self) -> &wgpu::BindGroup {
        &self.info_bind_group
    }

    // Panics if either layer is not present. Callers should use ensure_layer before starting
    // the render pass. It is not combined because mutability.
    pub fn begin_render_pass<'a>(
        &'a self,
        color_layer: &'a str,
        depth_layer: Option<&'a str>,
        clear_before_render: bool,
        encoder: &'a mut wgpu::CommandEncoder,
    ) -> wgpu::RenderPass<'a> {
        let color = self.color_target(color_layer, clear_before_render);
        let depth = depth_layer.and_then(|depth| self.depth_target(depth, clear_before_render));
        encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: Some("camera-render-pass"),
            color_attachments: &color,
            depth_stencil_attachment: depth,
            timestamp_writes: None,
            occlusion_query_set: None,
        })
    }

    // Panics if no such target exists.
    pub fn target_texture(&self, name: &str) -> &wgpu::Texture {
        &self.layers.get(name).expect("layer-texture").layer.0
    }

    // Panics if no such binding exists.
    pub fn bind_group(&self, name: &str) -> &wgpu::BindGroup {
        &self.bindings.get(name).expect("bind-group").bind_group
    }

    pub fn ensure_layer(&mut self, name: &str, format: wgpu::TextureFormat, gpu: &Gpu) {
        self.ensure_layer_with_usage(name, format, wgpu::TextureUsages::empty(), gpu)
    }

    pub fn ensure_layer_with_usage(
        &mut self,
        name: &str,
        format: wgpu::TextureFormat,
        usages: wgpu::TextureUsages,
        gpu: &Gpu,
    ) {
        if let Some(layer) = self.layers.get(name) {
            if layer.size == self.size {
                return;
            }
        }
        let texture = gpu.device().create_texture(&wgpu::TextureDescriptor {
            label: Some(&format!("{}-{name}-camera-target-texture", self.label)),
            size: self.size,
            mip_level_count: 1,
            // TODO: antialiasing
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format,
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT
                | wgpu::TextureUsages::COPY_SRC
                | wgpu::TextureUsages::TEXTURE_BINDING
                | usages,
            view_formats: &[format],
        });
        let view = texture.create_view(&wgpu::TextureViewDescriptor {
            label: Some(&format!("{}-{name}-camera-target-texture-view", self.label)),
            format: None,
            dimension: None,
            aspect: wgpu::TextureAspect::All,
            base_mip_level: 0,
            mip_level_count: None,
            base_array_layer: 0,
            array_layer_count: None,
        });
        self.layers.insert(
            name.to_owned(),
            TargetLayer {
                format,
                size: self.size,
                layer: (texture, view),
            },
        );
    }

    pub fn render_extent(&self) -> &wgpu::Extent3d {
        &self.size
    }

    // Panics if the layer does not exist
    pub fn color_target(
        &self,
        name: &str,
        cleared: bool,
    ) -> [Option<wgpu::RenderPassColorAttachment>; 1] {
        let load = if cleared {
            wgpu::LoadOp::Clear(wgpu::Color::RED)
        } else {
            wgpu::LoadOp::Load
        };
        [Some(wgpu::RenderPassColorAttachment {
            view: &self.layers.get(name).expect("layer").layer.1,
            resolve_target: None,
            ops: wgpu::Operations {
                load,
                store: wgpu::StoreOp::Store,
            },
        })]
    }

    pub fn depth_target(
        &self,
        name: &str,
        cleared: bool,
    ) -> Option<wgpu::RenderPassDepthStencilAttachment> {
        let depth_load = if cleared {
            wgpu::LoadOp::Clear(0f32)
        } else {
            wgpu::LoadOp::Load
        };
        Some(wgpu::RenderPassDepthStencilAttachment {
            view: &self.layers.get(name).expect("layer").layer.1,
            depth_ops: Some(wgpu::Operations {
                load: depth_load,
                store: wgpu::StoreOp::Store,
            }),
            stencil_ops: None,
        })
    }

    pub fn make_bind_group_layout(
        sources: &[wgpu::TextureFormat],
        sampled: bool,
        gpu: &Gpu,
    ) -> wgpu::BindGroupLayout {
        let mut layouts = Vec::new();
        for format in sources {
            let sample_type = if format.is_depth_stencil_format() {
                wgpu::TextureSampleType::Depth
            } else {
                wgpu::TextureSampleType::Float { filterable: true }
            };
            layouts.push(wgpu::BindGroupLayoutEntry {
                binding: layouts.len() as u32,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Texture {
                    sample_type,
                    view_dimension: wgpu::TextureViewDimension::D2,
                    // TODO: support antialiasing?
                    multisampled: false,
                },
                count: None,
            });
        }
        if sampled {
            layouts.push(wgpu::BindGroupLayoutEntry {
                binding: layouts.len() as u32,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                count: None,
            });
        }
        gpu.device()
            .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                label: Some("camera-bind-group-layout"),
                entries: &layouts,
            })
    }

    pub fn ensure_bind_group(&mut self, name: &str, sources: &[&str], sampled: bool, gpu: &Gpu) {
        if let Some(bound_layers) = self.bindings.get(name) {
            if bound_layers.size == self.size {
                return;
            }
        }

        let mut formats = Vec::with_capacity(sources.len());
        let mut entries = Vec::new();
        for source in sources {
            let layer = self
                .layers
                .get(*source)
                .unwrap_or_else(|| panic!("layout source: {source}"));
            formats.push(layer.format);
            entries.push(wgpu::BindGroupEntry {
                binding: entries.len() as u32,
                resource: wgpu::BindingResource::TextureView(&layer.layer.1),
            });
        }
        if sampled {
            entries.push(wgpu::BindGroupEntry {
                binding: entries.len() as u32,
                resource: wgpu::BindingResource::Sampler(&self.sampler),
            })
        }

        let bind_group_layout = Self::make_bind_group_layout(&formats, sampled, gpu);
        let bind_group = gpu.device().create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some(&format!("{}-{name}-bind-group", self.label)),
            layout: &bind_group_layout,
            entries: &entries,
        });
        self.bindings.insert(
            name.to_owned(),
            BoundLayers {
                size: self.size,
                _targets: sources
                    .iter()
                    .map(|v| v.to_string())
                    .collect::<Vec<String>>(),
                bind_group,
            },
        );
    }

    // Apply updated system config, e.g. aspect
    fn sys_apply_display_changes(window: Res<Window>, mut camera: ResMut<ScreenCamera>) {
        if let Some(config) = window.updated_config() {
            camera.on_display_config_updated(config);
        }
    }

    pub fn on_display_config_updated(&mut self, config: &DisplayConfig) {
        self.size.width = config.render_extent().width;
        self.size.height = config.render_extent().height;
        self.aspect_ratio = self.size.width as f64 / self.size.height as f64;
    }

    #[method]
    pub fn increase_fov(&mut self, pressed: bool) {
        self.input.fov_delta = degrees!(i32::from(pressed));
    }

    #[method]
    pub fn decrease_fov(&mut self, pressed: bool) {
        self.input.fov_delta = degrees!(-i32::from(pressed));
    }

    #[method]
    pub fn increase_exposure(&mut self) {
        self.exposure *= 1.1;
    }

    #[method]
    pub fn decrease_exposure(&mut self) {
        self.exposure /= 1.1;
    }

    pub fn exposure(&self) -> f32 {
        self.exposure as f32
    }

    pub fn gamma(&self) -> f32 {
        self.gamma as f32
    }

    pub fn fov_y<T: AngleUnit>(&self) -> Angle<T> {
        Angle::<T>::from(&self.fov_y)
    }

    pub fn set_fov_y<T: AngleUnit>(&mut self, fov: Angle<T>) {
        self.fov_y = radians!(fov);
    }

    pub fn z_near<Unit: LengthUnit>(&self) -> Length<Unit> {
        Length::<Unit>::from(&self.z_near)
    }

    pub fn aspect_ratio(&self) -> f64 {
        self.aspect_ratio
    }

    pub fn position<T: LengthUnit>(&self) -> Pt3<T> {
        Pt3::<T>::from(&self.position)
    }

    pub fn forward(&self) -> &DVec3 {
        &self.forward
    }

    pub fn up(&self) -> &DVec3 {
        &self.up
    }

    pub fn right(&self) -> &DVec3 {
        &self.right
    }

    // Source: https://nlguillemot.wordpress.com/2016/12/07/reversed-z-in-opengl/
    // See also: https://outerra.blogspot.com/2012/11/maximizing-depth-buffer-range-and.html
    // Infinite depth perspective with flipped w so that we can use inverted depths.
    // float f = 1.0f / tan(fovY_radians / 2.0f);
    // return glm::mat4(
    //     f / WbyH, 0.0f,  0.0f,  0.0f,
    //     0.0f,        f,  0.0f,  0.0f,
    //     0.0f,     0.0f,  0.0f,  1.0f, // or -1 for right handed
    //     0.0f,     0.0f, zNear,  0.0f);
    //
    // The assumption for right handed is that z coordinates are negative, whereas
    // for left handed systems, like WebGPU, is that z coordinate will be positive.
    //
    // TL;DR is that we set the Z in clip space to zNear instead of -1 (and write z
    // into the w coordinate, like always). When we do the perspective divide by w, this
    // inverts the z _and_ changes the scaling.
    //
    // Note for inverting the transform on the GPU:
    // z = -1
    // w = z*zNear
    // z' = -1 / (z / zNear)
    // z = -1 / (z' / zNear)
    pub fn perspective<T: LengthUnit>(&self) -> DMat4 {
        DMat4::perspective_infinite_reverse_lh(
            radians!(self.fov_y).f64(),
            self.aspect_ratio,
            Length::<T>::from(&self.z_near).f64(),
        )
    }

    pub fn world_to_eye<T: LengthUnit>(&self) -> DMat4 {
        DMat4::look_to_lh(self.position.dvec3(), self.forward, self.up)
    }

    pub fn world_to_eye_rotation(&self) -> DMat4 {
        DMat4::look_to_lh(DVec3::ZERO, self.forward, self.up)
    }

    // Map from a 2d point in winit window space to the world space position
    // of the cursor on the frustum.
    pub fn mouse_to_world(&self, cursor: (f64, f64)) -> Pt3<Meters> {
        let (cursor_x, cursor_y) = cursor;
        let extent = self.render_extent();
        let ndc = DVec2::new(
            cursor_x / extent.width as f64,
            1. - (cursor_y / extent.height as f64),
        ) * 2.
            - 1.;
        let z_near = self.z_near::<Meters>();
        let w = 1.; // z_near / z_near;
        let clip = DVec4::new(ndc.x * w, ndc.y * w, z_near.f64(), w);
        let eye_m = self.perspective::<Meters>().inverse() * clip;
        let wrld_m = self.world_to_eye::<Meters>().inverse() * eye_m;
        Pt3::<Meters>::new_dvec3(wrld_m.xyz())
    }

    pub fn world_space_frustum<T: LengthUnit>(&self) -> [Plane<T>; 5] {
        // Taken from this paper:
        //   https://www.gamedevs.org/uploads/fast-extraction-viewing-frustum-planes-from-world-view-projection-matrix.pdf
        let m = self.perspective::<T>() * self.world_to_eye::<T>();

        let left = Plane::from_dvec4(m.row(3) + m.row(0));
        let right = Plane::from_dvec4(m.row(3) - m.row(0));
        let bottom = Plane::from_dvec4(m.row(3) + m.row(1));
        let top = Plane::from_dvec4(m.row(3) - m.row(1));
        let near = Plane::from_dvec4(m.row(3) + m.row(2));

        [near, left, right, bottom, top]
    }

    fn _make_info(&self) -> CameraInfo {
        CameraInfo {
            pad0: 0f32,
            pad1: 0f32,

            render_width: self.size.width,
            render_height: self.size.height,

            pad2: [0f32; 4],

            fov_y: self.fov_y::<Radians>().f32(),
            aspect_ratio: self.aspect_ratio() as f32,
            z_near_m: self.z_near::<Meters>().f32(),
            z_near_km: self.z_near::<Kilometers>().f32(),
            forward: Vec4::from((self.forward().as_vec3(), 0.)).to_array(),
            up: Vec4::from((self.up().as_vec3(), 0.)).to_array(),
            right: Vec4::from((self.right().as_vec3(), 0.)).to_array(),
            position_m: self.position::<Meters>().dvec4(1.).as_vec4().to_array(),
            position_km: self.position::<Kilometers>().dvec4(1.).as_vec4().to_array(),

            perspective_m: self.perspective::<Meters>().as_mat4().to_cols_array_2d(),
            perspective_km: self
                .perspective::<Kilometers>()
                .as_mat4()
                .to_cols_array_2d(),

            inverse_perspective_m: self
                .perspective::<Meters>()
                .as_mat4()
                .inverse()
                .to_cols_array_2d(),
            inverse_perspective_km: self
                .perspective::<Kilometers>()
                .as_mat4()
                .inverse()
                .to_cols_array_2d(),

            view_m: self.world_to_eye::<Meters>().as_mat4().to_cols_array_2d(),
            view_km: self
                .world_to_eye::<Kilometers>()
                .as_mat4()
                .to_cols_array_2d(),

            inverse_view_m: self
                .world_to_eye::<Meters>()
                .as_mat4()
                .inverse()
                .to_cols_array_2d(),
            inverse_view_km: self
                .world_to_eye::<Kilometers>()
                .as_mat4()
                .inverse()
                .to_cols_array_2d(),

            look_at_lhs_m: self.world_to_eye_rotation().as_mat4().to_cols_array_2d(),

            exposure: self.exposure(),
            tone_gamma: self.gamma(),

            pad3: 0f32,
            pad4: 0f32,
        }
    }

    fn apply_input_state(&mut self) {
        let mut fov = degrees!(self.fov_y);
        fov += self.input.fov_delta;
        fov = fov.clamp(degrees!(1), degrees!(90));
        self.fov_y = radians!(fov);
    }

    pub fn update_frame(&mut self, frame: &Frame, now: &Epoch) {
        let mut warp_done = None;
        if let Some((initial, duration, start, bezier)) = &self.warp {
            let delta = (*now - *start).to_seconds();
            let f0 = delta / duration.f64();
            let f = bezier.interpolate(f0);
            warp_done = Some(f >= 1.0 - f64::EPSILON);
            let position = initial.geodetic().interpolate(&frame.geodetic(), f);
            let facing = initial.facing().rotate_towards(
                frame.facing(),
                f * initial.facing().angle_between(frame.facing()),
            );
            let frame = Frame::new(position.pt3(), facing);
            self.position = frame.position();
            let basis = frame.basis();
            self.forward = basis.forward;
            self.right = basis.right;
            self.up = basis.up;
        }
        if let Some(warp_done) = warp_done {
            if warp_done {
                self.warp = None;
            }
            return;
        }
        self.position = frame.position();
        let basis = frame.basis();
        self.forward = basis.forward;
        self.right = basis.right;
        self.up = basis.up;
        debug_assert!(self.forward.is_normalized());
        debug_assert!(self.right.is_normalized());
        debug_assert!(self.up.is_normalized());
    }

    // Apply interpreted inputs from prior stage; apply new world position.
    fn sys_move_camera_to_frame(
        mut camera: ResMut<ScreenCamera>,
        step: Res<TimeStep>,
        query: Query<&Frame, With<ScreenCameraController>>,
    ) {
        if let Ok(frame) = query.get_single() {
            camera.apply_input_state();
            camera.update_frame(frame, step.sim_now_epoch());
        }
    }

    // pub fn initiate_warp(&mut self, frame: Frame, duration: Time<Seconds>, now: Epoch) {
    pub fn begin_warp(duration: Time<Seconds>, mut heap: HeapMut) {
        // Warp is stateless and applied by update_frame, so we can clobber and reset the move
        // without further ceremony.
        let frame = heap.get::<Frame>("camera").to_owned();
        let now = *heap.resource::<TimeStep>().sim_now_epoch();
        heap.resource_mut::<ScreenCamera>().warp =
            Some((frame, duration, now, CubicBezierCurve::EASE_IN_OUT));
    }

    fn sys_upload_camera_params(
        gpu: Res<Gpu>,
        camera: Res<ScreenCamera>,
        // all_cameras: Query<&Camera>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) {
        if let Some(encoder) = maybe_encoder.get_encoder_mut() {
            //     for camera in all_cameras.iter() {
            gpu.upload_data_to(&camera._make_info(), &camera.info_buffer, encoder);
            //     }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use absolute_unit::{degrees, meters};
    use approx::assert_relative_eq;
    use glam::{DVec4, Vec4Swizzles};
    use mantle::Core;

    #[test]
    fn test_perspective() -> Result<()> {
        let runtime = Core::for_test()?;
        let camera = ScreenCamera::new(
            "main",
            degrees!(90),
            PhysicalSize::new(1920, 1080),
            meters!(0.3),
            runtime.resource::<Gpu>(),
        );
        let perspective = camera.perspective::<Meters>();
        let wrld = DVec4::new(0000.0, 0.0, -10000.0, 1.0);
        let eye = camera.world_to_eye::<Meters>() * wrld;
        let clip = perspective * eye;
        let ndc = (clip / clip[3]).xyz();
        let w = camera.z_near::<Meters>().f64() / ndc.z;
        let eyep = DVec3::new(ndc.x * w / camera.aspect_ratio(), ndc.y * w, w);
        let wrldp = camera
            .world_to_eye::<Meters>()
            .inverse()
            .transform_point3(eyep);

        println!(
            "wrld: {wrld}\neye: {eye}\nclip: {clip}\nndc: {ndc}, w: {w}\neyep: {eyep}, wrldp: {wrldp}",
        );

        assert_relative_eq!(wrld.x, wrldp.x, epsilon = 0.000000001);
        assert_relative_eq!(wrld.y, wrldp.y, epsilon = 0.000000001);
        assert_relative_eq!(wrld.z, wrldp.z, epsilon = 0.000000001);

        Ok(())
    }
}
