// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

struct CameraInfo {
    pad0: f32,
    pad1: f32,

    // Screen info
    render_width: u32,
    render_height: u32,

    pad2: vec4<f32>,

    // Camera properties
    fov_y: f32,
    aspect_ratio: f32,
    z_near_m: f32,
    z_near_km: f32,
    forward: vec4<f32>,
    up: vec4<f32>,
    right: vec4<f32>,
    position_m: vec4<f32>,
    position_km: vec4<f32>,
    perspective_m: mat4x4<f32>,
    perspective_km: mat4x4<f32>,
    inverse_perspective_m: mat4x4<f32>,
    inverse_perspective_km: mat4x4<f32>,
    view_m: mat4x4<f32>,
    view_km: mat4x4<f32>,
    inverse_view_m: mat4x4<f32>,
    inverse_view_km: mat4x4<f32>,
    look_at_lhs_m: mat4x4<f32>,

    // Tone mapping
    exposure: f32,
    tone_gamma: f32,

    pad3: f32,
    pad4: f32,
}