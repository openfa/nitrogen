// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::{degrees, meters};
use anyhow::Result;
use arcball::ArcBallController;
use bevy_ecs::prelude::*;
use camera::{ScreenCamera, ScreenCameraController};
use event_mapper::EventMapper;
use geodesy::{Bearing, Geodetic, PitchCline};
use mantle::{
    BindingsBuilder, Core, CurrentEncoder, CurrentFrame, DisplayOpts, Gpu, GpuStep, LayoutBuilder,
};
use nitrous::{inject_nitrous_resource, NitrousResource};
use orrery::Orrery;
use phase::Frame;
use pollster::FutureExt as _;
use runtime::{Extension, Runtime};
use stars::Stars;

#[derive(NitrousResource)]
struct App {
    stars_bind_group: wgpu::BindGroup,
    pipeline: wgpu::RenderPipeline,
}

impl Extension for App {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        let app = App::new(runtime.resource::<Stars>(), runtime.resource::<Gpu>())?;
        runtime.inject_resource(app)?;
        runtime.add_frame_system(
            Self::sys_render
                .after(GpuStep::CreateCommandEncoder)
                .before(GpuStep::SubmitCommands),
        );
        runtime.add_sim_system(Self::sys_rotate);
        Ok(())
    }
}

#[inject_nitrous_resource]
impl App {
    fn new(stars: &Stars, gpu: &Gpu) -> Result<Self> {
        let shader = gpu.compile_shader("example.wgsl", include_str!("../target/example.wgsl"));

        let stars_bind_group_layout = LayoutBuilder::default()
            .with_label("world-composite-bind-group-layout")
            .with_layout::<Stars>()
            .build(gpu);
        let stars_bind_group = BindingsBuilder::new(&stars_bind_group_layout)
            .with_label("world-composite-bind-group")
            .with_bindings(stars)
            .build(gpu);

        let pipeline_layout =
            gpu.device()
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: Some("demo-stars-pipeline-layout"),
                    push_constant_ranges: &[],
                    bind_group_layouts: &[
                        &stars_bind_group_layout,
                        &ScreenCamera::info_bind_group_layout(gpu),
                    ],
                });
        let pipeline = gpu
            .device()
            .create_render_pipeline(&wgpu::RenderPipelineDescriptor {
                label: Some("demo-stars-pipeline"),
                layout: Some(&pipeline_layout),
                vertex: wgpu::VertexState {
                    module: &shader,
                    entry_point: "main_vert",
                    buffers: &[],
                    compilation_options: wgpu::PipelineCompilationOptions {
                        constants: &Default::default(),
                        zero_initialize_workgroup_memory: false,
                    },
                },
                fragment: Some(wgpu::FragmentState {
                    module: &shader,
                    entry_point: "main_frag",
                    targets: &[Some(wgpu::ColorTargetState {
                        format: Gpu::SCREEN_FORMAT,
                        blend: None,
                        write_mask: wgpu::ColorWrites::COLOR,
                    })],
                    compilation_options: wgpu::PipelineCompilationOptions {
                        constants: &Default::default(),
                        zero_initialize_workgroup_memory: false,
                    },
                }),
                primitive: wgpu::PrimitiveState {
                    topology: wgpu::PrimitiveTopology::TriangleList,
                    strip_index_format: None,
                    front_face: wgpu::FrontFace::Ccw,
                    cull_mode: Some(wgpu::Face::Back),
                    unclipped_depth: false,
                    polygon_mode: wgpu::PolygonMode::Fill,
                    conservative: false,
                },
                depth_stencil: Some(wgpu::DepthStencilState {
                    format: Gpu::DEPTH_FORMAT,
                    depth_write_enabled: false,
                    depth_compare: wgpu::CompareFunction::Always,
                    stencil: wgpu::StencilState {
                        front: wgpu::StencilFaceState::IGNORE,
                        back: wgpu::StencilFaceState::IGNORE,
                        read_mask: 0,
                        write_mask: 0,
                    },
                    bias: wgpu::DepthBiasState {
                        constant: 0,
                        slope_scale: 0.0,
                        clamp: 0.0,
                    },
                }),
                multisample: wgpu::MultisampleState {
                    count: 1,
                    mask: !0,
                    alpha_to_coverage_enabled: false,
                },
                multiview: None,
            });

        Ok(Self {
            stars_bind_group,
            pipeline,
        })
    }

    fn sys_rotate(mut player: Query<(&mut ArcBallController, &ScreenCameraController)>) {
        player.single_mut().0.handle_mousemotion(-1f64, 0f64);
    }

    fn sys_render(
        app: Res<App>,
        camera: Res<ScreenCamera>,
        gpu: Res<Gpu>,
        maybe_surface: Res<CurrentFrame>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) {
        if let Some(surface_texture) = maybe_surface.get_surface() {
            if let Some(encoder) = maybe_encoder.get_encoder_mut() {
                let view = surface_texture
                    .texture
                    .create_view(&::wgpu::TextureViewDescriptor::default());
                let render_pass_desc_ref = wgpu::RenderPassDescriptor {
                    label: Some("screen-composite-render-pass"),
                    color_attachments: &[Some(Gpu::color_attachment(&view))],
                    depth_stencil_attachment: Some(gpu.depth_stencil_attachment()),
                    timestamp_writes: None,
                    occlusion_query_set: None,
                };
                let mut rpass = encoder.begin_render_pass(&render_pass_desc_ref);
                rpass.set_pipeline(&app.pipeline);
                rpass.set_bind_group(0, &app.stars_bind_group, &[]);
                rpass.set_bind_group(1, camera.info_bind_group(), &[]);
                rpass.draw(0..3, 0..1);
            }
        }
    }
}

fn main() -> Result<()> {
    Core::boot(
        DisplayOpts::default(),
        (),
        "Nitrogen Stars Demo",
        setup_runtime,
    )
    .block_on()
}

fn setup_runtime(runtime: &mut Runtime, _: ()) -> Result<()> {
    runtime
        .load_extension::<EventMapper>()?
        .load_extension::<Stars>()?
        .load_extension::<App>()?
        .load_extension::<Orrery>()?
        .load_extension::<ScreenCamera>()?
        .load_extension::<ArcBallController>()?
        .run_string(r#"bindings.bind("Escape", "window.request_quit()");"#)?;

    let mut arcball = ArcBallController::default();
    arcball.pan_view(true);
    arcball.set_bearing(Bearing::new(degrees!(0)));
    arcball.set_pitch(PitchCline::new(degrees!(0)));
    arcball.set_distance(meters!(40.0))?;
    arcball.set_target_imm_unsafe(Geodetic::new(degrees!(0), degrees!(0), meters!(10)));
    let _player_ent = runtime
        .spawn("player")?
        .inject(Frame::default())?
        .inject(arcball)?
        .inject(ScreenCameraController)?
        .id();

    Ok(())
}
