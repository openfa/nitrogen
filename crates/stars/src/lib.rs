// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use anyhow::Result;
use bevy_ecs::prelude::*;
use glam::{Mat4, Quat, Vec3A};
use log::trace;
use mantle::{BindingCollection, CurrentEncoder, Gpu, GpuStep, LayoutProvider};
use nitrous::{inject_nitrous_resource, NitrousResource};
use orrery::Orrery;
use planck::EquatorialCoord;
use runtime::{Extension, Runtime};
use shader_shared::layout::frag;
use star_catalog::Stars as StarsCatalog;
use static_assertions::{assert_eq_align, assert_eq_size};
use std::{collections::HashSet, f32::consts::PI, f32::consts::TAU};
use zerocopy::{FromBytes, Immutable, IntoBytes};

const PI_2: f32 = PI / 2f32;
const RADIUS: f32 = 0.0015f32;

#[repr(C)]
#[derive(IntoBytes, FromBytes, Immutable, Copy, Clone)]
struct BandMetadata {
    index: u32,
    bins_per_row: u32,
    base_index: u32,
    _pad: u32,
}
assert_eq_size!(BandMetadata, [f32; 4]);
assert_eq_align!(BandMetadata, [f32; 4]);

#[repr(C)]
#[derive(IntoBytes, FromBytes, Immutable, Copy, Clone)]
struct BinPosition {
    _index_base: u32,
    _num_indexes: u32,
}
assert_eq_size!(BinPosition, [f32; 2]);
assert_eq_align!(BinPosition, [f32; 4]);

#[repr(C)]
#[derive(IntoBytes, FromBytes, Immutable, Copy, Clone)]
struct StarInst {
    ra: f32,
    dec: f32,
    _color: [u8; 4],
    _radius: f32,
}
assert_eq_size!(StarInst, [f32; 4]);
assert_eq_align!(StarInst, [f32; 4]);

macro_rules! mkband {
    ($index:expr, $bins_per_row:expr, $base_index:expr) => {
        BandMetadata {
            index: $index,
            bins_per_row: $bins_per_row,
            base_index: $base_index,
            _pad: 0,
        }
    };
}

const DEC_BINS: usize = 64;
const DEC_BANDS: [BandMetadata; DEC_BINS] = [
    mkband!(0, 1, 0),
    mkband!(1, 8, 1),
    mkband!(2, 12, 9),
    mkband!(3, 16, 21),
    mkband!(4, 32, 37),
    mkband!(5, 32, 69),
    mkband!(6, 32, 101),
    mkband!(7, 32, 133),
    mkband!(8, 64, 165),
    mkband!(9, 64, 229),
    mkband!(10, 64, 293),
    mkband!(11, 64, 357),
    mkband!(12, 84, 421),
    mkband!(13, 84, 505),
    mkband!(14, 84, 589),
    mkband!(15, 84, 673),
    mkband!(16, 84, 757),
    mkband!(17, 84, 841),
    mkband!(18, 128, 925),
    mkband!(19, 128, 1053),
    mkband!(20, 128, 1181),
    mkband!(21, 128, 1309),
    mkband!(22, 128, 1437),
    mkband!(23, 128, 1565),
    mkband!(24, 128, 1693),
    mkband!(25, 128, 1821),
    mkband!(26, 128, 1949),
    mkband!(27, 128, 2077),
    mkband!(28, 128, 2205),
    mkband!(29, 128, 2333),
    mkband!(30, 128, 2461),
    mkband!(31, 128, 2589),
    mkband!(32, 128, 2717),
    mkband!(33, 128, 2845),
    mkband!(34, 128, 2973),
    mkband!(35, 128, 3101),
    mkband!(36, 128, 3229),
    mkband!(37, 128, 3357),
    mkband!(38, 128, 3485),
    mkband!(39, 128, 3613),
    mkband!(40, 128, 3741),
    mkband!(41, 128, 3869),
    mkband!(42, 128, 3997),
    mkband!(43, 128, 4125),
    mkband!(44, 128, 4253),
    mkband!(45, 128, 4381),
    mkband!(46, 84, 4509),
    mkband!(47, 84, 4593),
    mkband!(48, 84, 4677),
    mkband!(49, 84, 4761),
    mkband!(50, 84, 4845),
    mkband!(51, 84, 4929),
    mkband!(52, 64, 5013),
    mkband!(53, 64, 5077),
    mkband!(54, 64, 5141),
    mkband!(55, 64, 5205),
    mkband!(56, 32, 5269),
    mkband!(57, 32, 5301),
    mkband!(58, 32, 5333),
    mkband!(59, 32, 5365),
    mkband!(60, 16, 5397),
    mkband!(61, 12, 5413),
    mkband!(62, 8, 5425),
    mkband!(63, 1, 5433),
];

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum StarsStep {
    UpdateHourAngle,
}

#[derive(Debug, NitrousResource)]
pub struct Stars {
    band_buffer: wgpu::Buffer,
    bin_positions_buffer: wgpu::Buffer,
    star_indices_buffer: wgpu::Buffer,
    star_buffer: wgpu::Buffer,
    hour_angle_matrix: wgpu::Buffer,
}

impl Extension for Stars {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        let stars = Stars::new(runtime.resource::<Gpu>())?;
        runtime.inject_resource(stars)?;
        runtime.add_frame_system(
            Self::sys_update_hour_angle
                .in_set(StarsStep::UpdateHourAngle)
                .after(GpuStep::CreateCommandEncoder)
                .before(GpuStep::SubmitCommands),
        );
        Ok(())
    }
}

impl LayoutProvider for Stars {
    fn push_layout(layouts: &mut Vec<wgpu::BindGroupLayoutEntry>) {
        // Band metadata
        layouts.push(frag::buffer::<[BandMetadata; DEC_BINS]>(layouts.len()));
        // Bin positions
        layouts.push(frag::buffer_rt(layouts.len()));
        // Star indices
        layouts.push(frag::buffer_rt(layouts.len()));
        // Star buffer
        layouts.push(frag::buffer_rt(layouts.len()));
        // hour angle matrix
        layouts.push(frag::buffer::<Mat4>(layouts.len()));
    }

    fn push_binding<'a, 'b, 'c>(&'a self, mut bindings: BindingCollection<'b, 'c>)
    where
        'a: 'b,
        'a: 'c,
    {
        bindings.buffer(&self.band_buffer);
        bindings.buffer(&self.bin_positions_buffer);
        bindings.buffer(&self.star_indices_buffer);
        bindings.buffer(&self.star_buffer);
        bindings.buffer(&self.hour_angle_matrix);
    }
}

#[inject_nitrous_resource]
impl Stars {
    fn ra_d_to_vec(ra: f32, dec: f32) -> Vec3A {
        let check = EquatorialCoord::from_ra_dec(radians!(ra), radians!(dec));
        check.nitrogen_dvec3().as_vec3a()
    }

    fn vec_to_ra_d(v: &Vec3A) -> (f32, f32) {
        let check = EquatorialCoord::from_nitrogen_dvec3(v.as_dvec3());
        (check.right_ascension().f32(), check.declination().f32())
    }

    fn num_bins() -> usize {
        let last_band = &DEC_BANDS[DEC_BANDS.len() - 1];
        (last_band.base_index + last_band.bins_per_row) as usize
    }

    fn band_for_dec(dec: f32) -> &'static BandMetadata {
        assert!(dec < PI_2);
        assert!(dec >= -PI_2);
        // See implementation in shader for comments.
        let decz = ((dec + PI_2) * 2f32) / TAU;
        let deci = (decz * DEC_BINS as f32) as usize;
        &DEC_BANDS[deci]
    }

    fn bin_for_ra_d(ra: f32, dec: f32) -> usize {
        let band = Self::band_for_dec(dec);
        let raz = ra / TAU;
        let rai = (band.bins_per_row as f32 * raz) as usize;
        band.base_index as usize + rai
    }

    pub fn new(gpu: &Gpu) -> Result<Self> {
        trace!("StarsBuffer::new");

        let mut offset = 0;
        for (i, band) in DEC_BANDS.iter().enumerate() {
            assert_eq!(band.index as usize, i);
            assert_eq!(band.base_index, offset);
            offset += band.bins_per_row;
        }

        let mut star_buf = Vec::new();
        let stars = StarsCatalog::new()?;
        for i in 0..stars.catalog_size() {
            let entry = stars.entry(i)?;
            let coord = entry.coord();
            // let ra = entry.right_ascension();
            // let dec = entry.declination();
            let color = entry.color();
            let radius = RADIUS * entry.radius_scale();
            let star = StarInst {
                ra: coord.right_ascension().f32(),
                dec: coord.declination().f32(),
                _color: color.to_array(),
                _radius: radius,
            };
            star_buf.push(star);
        }

        // Bin all stars.
        let mut bins = Vec::with_capacity(Self::num_bins());
        bins.resize_with(Self::num_bins(), HashSet::new);
        for (star_off, star) in star_buf.iter().enumerate() {
            // Sample bins in a circle at the star's approximate radius.
            let star_ray = Self::ra_d_to_vec(star.ra, star.dec);
            let perp = star_ray.any_orthogonal_vector() * RADIUS * 2f32;
            let norm_star_ray = star_ray.normalize();
            const N_TAPS: usize = 4;
            for i in 0..N_TAPS {
                let ang = i as f32 * TAU / N_TAPS as f32;
                let rot = Quat::from_axis_angle(norm_star_ray.into(), ang);
                let sample = (star_ray + (rot * perp)).normalize();
                let (sample_ra, sample_dec) = Self::vec_to_ra_d(&sample);
                bins[Self::bin_for_ra_d(sample_ra, sample_dec)].insert(star_off as u32);
            }
            bins[Self::bin_for_ra_d(star.ra, star.dec)].insert(star_off as u32);
        }

        // Now that we have sorted all stars into all bins they might affect,
        // build the index and bin position buffers.
        let mut bin_positions = Vec::new();
        let mut indices = Vec::new();
        for bin_indices in &bins {
            let bin_base = indices.len();
            let bin_len = bin_indices.len();

            let pos = BinPosition {
                _index_base: bin_base as u32,
                _num_indexes: bin_len as u32,
            };
            bin_positions.push(pos);
            for index in bin_indices {
                indices.push(*index);
            }
        }

        // Upload to GPU
        let band_buffer =
            gpu.push_slice("stars-band-buffer", &DEC_BANDS, wgpu::BufferUsages::STORAGE);
        let bin_positions_buffer = gpu.push_slice(
            "stars-bin-positions-buffer",
            &bin_positions,
            wgpu::BufferUsages::STORAGE,
        );
        let star_indices_buffer = gpu.push_slice(
            "stars-indices-buffer",
            &indices,
            wgpu::BufferUsages::STORAGE,
        );
        let star_buffer = gpu.push_slice("stars-buffer", &star_buf, wgpu::BufferUsages::STORAGE);
        let hour_angle_matrix = gpu.push_data(
            "hour-angle-matrix",
            &Mat4::IDENTITY.to_cols_array(),
            wgpu::BufferUsages::STORAGE | wgpu::BufferUsages::COPY_DST,
        );

        Ok(Self {
            band_buffer,
            bin_positions_buffer,
            star_indices_buffer,
            star_buffer,
            hour_angle_matrix,
        })
    }

    fn sys_update_hour_angle(
        stars: Res<Stars>,
        orrery: Res<Orrery>,
        gpu: Res<Gpu>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) {
        if let Some(encoder) = maybe_encoder.get_encoder_mut() {
            let angle = orrery.hour_angle_matrix();
            gpu.upload_data_to(&angle.to_cols_array(), &stars.hour_angle_matrix, encoder)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Stars as SB;
    use super::*;
    use approx::assert_relative_eq;

    #[test]
    fn ra_d_to_bin_transform() {
        for i in 0..32 {
            assert_eq!(SB::bin_for_ra_d(i as f32 / 32f32, -PI_2), 0);
            assert_eq!(SB::bin_for_ra_d(i as f32 / 32f32, PI_2 - 0.0001), 5433);
        }
        for i in 0..DEC_BINS {
            let f = i as f32 / DEC_BINS as f32;
            let dec = -PI_2 + f * PI + 0.001;
            let band = SB::band_for_dec(dec);
            assert_eq!(band.index, i as u32);

            for j in 0..band.bins_per_row {
                let g = j as f32 / band.bins_per_row as f32;
                let ra = g * TAU + 0.001;
                let bin_idx = SB::bin_for_ra_d(ra, dec);
                assert_eq!(bin_idx as u32, band.base_index + j);
            }
        }
    }

    #[test]
    fn vec_to_rad_to_vec() -> Result<()> {
        let stars = StarsCatalog::new()?;
        for i in 0..stars.catalog_size() {
            let entry = stars.entry(i)?;
            let band = SB::band_for_dec(entry.declination());
            if entry.declination() < 0f32 {
                assert!(band.index < 32);
            } else {
                assert!(band.index >= 32);
            }
            let v = SB::ra_d_to_vec(entry.right_ascension(), entry.declination());
            let (ra, dec) = SB::vec_to_ra_d(&v);

            assert_relative_eq!(ra, entry.right_ascension(), epsilon = 0.00001);
            assert_relative_eq!(dec, entry.declination(), epsilon = 0.00001);
        }

        Ok(())
    }

    #[test]
    fn fast_taps() {
        let v = Vec3A::new(0f32, 0f32, 1f32);
        let perp = v.any_orthogonal_vector() * 0.001;
        let vn = v.normalize();

        for i in 0..8 {
            let ang = i as f32 * TAU / 8f32;
            let rot = Quat::from_axis_angle(vn.into(), ang);
            let sample = v + rot * perp;
            println!("PERP: {sample}");
        }
    }
}
