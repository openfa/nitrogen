// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <camera/include/camera.wgsl>

//!bindings [0] <stars/bindings/stars.wgsl>
//!bindings [1] <camera/bindings/camera.wgsl>

//!include <shader_shared/include/consts.wgsl>
//!include <shader_shared/include/fullscreen.wgsl>
//!include <stars/include/stars.wgsl>

var<private> SHOW_BINS: bool = false;

struct VertexOutput {
    @location(0) v_ray: vec3<f32>,
    @builtin(position) position: vec4<f32>,
}

@vertex
fn main_vert(@builtin(vertex_index) i: u32) -> VertexOutput {
    let input = fullscreen_input(i);
    let v_ray = raymarching_view_ray(input.clip, camera.inverse_perspective_m, camera.inverse_view_m);
    return VertexOutput(v_ray, input.clip);
}

struct FragmentOutput {
    @location(0) color: vec4<f32>,
}

@fragment
fn main_frag(@location(0) v_ray: vec3<f32>) -> FragmentOutput {
    if (SHOW_BINS) {
        return FragmentOutput(vec4<f32>(show_bins(v_ray), 1.));
    }
    let star = show_stars(v_ray, hour_angle);
    return FragmentOutput(vec4<f32>(star.radiance, star.alpha));
}
