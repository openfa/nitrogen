// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

struct BandMetadata {
    index: u32,
    bins_per_row: u32,
    base_index: u32,
    pad: u32,
}

struct BinPosition {
    index_base: u32,
    num_indexes: u32,
}

struct StarInst {
    ra: f32,
    dec: f32,
    color: u32,
    radius: f32,
}

var<private> DEC_BINS: u32 = 64u;

struct CelestialCoord {
    ra: f32,
    dec: f32
}

fn v_to_ra_d(v: vec3<f32>) -> CelestialCoord {
    let dist = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
    let ra = -atan2(v.x, v.z) + PI;
    let dec = asin(v.y / dist);
    return CelestialCoord(ra, dec);
}

fn ra_d_to_v(ra_i: f32, dec: f32) -> vec3<f32> {
    let ra = ra_i - PI;
    return vec3(
        -(cos(dec) * sin(ra)),
        sin(dec),
        cos(dec) * cos(ra)
    );
}

fn band_for_dec(dec: f32) -> BandMetadata {
    // Project dec into 0..1
    let decz: f32 = ((dec + PI_2) * 2.0) / TAU;

    // Quantize dec into bands.
    let deci: u32 = u32(decz * f32(DEC_BINS));
    return stars_bands[deci];
}

fn bin_for_ra_d(ra: f32, dec: f32) -> u32 {
    let band = band_for_dec(dec);
    let raz = ra / TAU;
    let rai = u32(f32(band.bins_per_row) * raz);
    return band.base_index + rai;
}

struct StarSample {
    radiance: vec3<f32>,
    alpha: f32
}

fn show_stars(view_world: vec3<f32>, hour_angle: mat4x4<f32>) -> StarSample {
    let view =  (hour_angle * vec4(view_world, 1.)).xyz;

    let coord = v_to_ra_d(view);
    let bin = bin_for_ra_d(coord.ra, coord.dec);
    let pos = stars_bins[bin];

    var radiance = vec3<f32>(0.);
    var alpha = f32(0.);
    for (var i = pos.index_base; i < pos.index_base + pos.num_indexes; i++) {
        let star_index = stars_indices[i];
        let star = stars_stars[star_index];
        let star_ray = ra_d_to_v(star.ra, star.dec);
        let dist = acos(dot(star_ray, normalize(view)));
        if (dist < star.radius) {
            let clr8 = vec3<u32>(
                (star.color >> 24u) & 0xFF,
                (star.color >> 16u) & 0xFF,
                (star.color >> 8u) & 0xFF
            );
            radiance = vec3<f32>(clr8) / 255.;
            alpha = 1.;
        }
    }
    return StarSample(radiance, alpha);
}

fn show_bins(view: vec3<f32>) -> vec3<f32> {
    let coord = v_to_ra_d(view);

    let raz = coord.ra / TAU;
    let decz = ((coord.dec + PI_2) * 2.) / TAU;

    let _meta = band_for_dec(coord.dec);
    let rai = u32(raz * f32(_meta.bins_per_row));
    let deci = u32(decz * f32(DEC_BINS));

    var clr = vec3<f32>(0.);
    if ((rai & 1u) == 1u) {
        if ((deci & 1u) == 1u) {
            clr = vec3<f32>(0., 1., 1.);
        } else {
            clr = vec3<f32>(1., 0., 1.);
        }
    } else {
        if ((deci & 1u) == 1u) {
            clr = vec3<f32>(1., 0., 1.);
        } else {
            clr = vec3<f32>(0., 1., 1.);
        }
    }
    return clr;
}
