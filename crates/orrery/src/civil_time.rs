use anyhow::{ensure, Error};
// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::Result;
use hifitime::Epoch;
use jiff::{
    civil::{Date, DateTime, Time},
    tz::TimeZone,
    Timestamp, Zoned,
};
use nitrous::{Dict, Value};

// A zoned wrapper to enable Value conversions
#[derive(Clone, Debug)]
pub struct CivilTime {
    zoned: Zoned,
}

impl Default for CivilTime {
    fn default() -> Self {
        Self {
            zoned: Zoned::new(Timestamp::from_second(0).unwrap(), TimeZone::UTC),
        }
    }
}

impl CivilTime {
    pub fn new(zoned: Zoned) -> Self {
        Self { zoned }
    }

    pub fn zoned(&self) -> &Zoned {
        &self.zoned
    }

    pub fn time_zone(&self) -> &TimeZone {
        self.zoned.time_zone()
    }

    pub fn date(&self) -> Date {
        self.zoned.date()
    }

    pub fn time(&self) -> Time {
        self.zoned.time()
    }

    pub fn to_epoch(&self) -> Epoch {
        let utc = self.zoned.intz("UTC").unwrap();
        Epoch::from_gregorian_utc(
            utc.year() as i32,
            utc.month() as u8,
            utc.day() as u8,
            utc.hour() as u8,
            utc.minute() as u8,
            utc.second() as u8,
            utc.millisecond() as u32,
        )
    }

    pub fn with_year(mut self, year: i16) -> Result<Self> {
        self.zoned = self.zoned.with().year(year).build()?;
        Ok(self)
    }

    pub fn with_month(mut self, month: i8) -> Result<Self> {
        self.zoned = self.zoned.with().month(month).build()?;
        Ok(self)
    }

    pub fn with_day(mut self, day: i8) -> Result<Self> {
        self.zoned = self.zoned.with().day(day).build()?;
        Ok(self)
    }

    pub fn with_hour(mut self, hour: i8) -> Result<Self> {
        self.zoned = self.zoned.with().hour(hour).build()?;
        Ok(self)
    }

    pub fn with_minute(mut self, minute: i8) -> Result<Self> {
        self.zoned = self.zoned.with().minute(minute).build()?;
        Ok(self)
    }

    pub fn with_second(mut self, second: i8) -> Result<Self> {
        self.zoned = self.zoned.with().second(second).build()?;
        Ok(self)
    }

    pub fn with_zone(mut self, zone_name: &str) -> Result<Self> {
        self.zoned = self.zoned.with_time_zone(TimeZone::get(zone_name)?);
        Ok(self)
    }

    pub fn with_date(mut self, date: Date) -> Result<Self> {
        self.zoned = self.zoned.with().date(date).build()?;
        Ok(self)
    }

    pub fn with_time(mut self, time: Time) -> Result<Self> {
        self.zoned = self.zoned.with().time(time).build()?;
        Ok(self)
    }
}

impl From<Zoned> for CivilTime {
    fn from(zoned: Zoned) -> Self {
        Self { zoned }
    }
}

impl From<CivilTime> for Zoned {
    fn from(civil: CivilTime) -> Self {
        civil.zoned
    }
}

impl From<CivilTime> for Epoch {
    fn from(value: CivilTime) -> Self {
        value.to_epoch()
    }
}

impl From<CivilTime> for Dict {
    fn from(value: CivilTime) -> Self {
        let mut out = Dict::default();
        out.insert("type", Value::from_str("CivilTime"));
        out.insert("year", Value::from_int(value.zoned.year() as i64));
        out.insert("month", Value::from_int(value.zoned.month() as i64));
        out.insert("day", Value::from_int(value.zoned.day() as i64));
        out.insert("hour", Value::from_int(value.zoned.hour() as i64));
        out.insert("minute", Value::from_int(value.zoned.minute() as i64));
        out.insert("second", Value::from_int(value.zoned.second() as i64));
        out.insert(
            "nanos",
            Value::from_int(value.zoned.subsec_nanosecond() as i64),
        );
        out.insert(
            "zone",
            Value::from_str(value.zoned.time_zone().iana_name().unwrap_or("UTC")),
        );
        out
    }
}

impl TryFrom<&Dict> for CivilTime {
    type Error = Error;

    fn try_from(value: &Dict) -> anyhow::Result<Self> {
        let ty = value.expect_str("type", "CivilTime")?;
        ensure!(ty == "CivilTime");
        let year = value.expect_int("year", "CivilTime")? as i16;
        let month = value.expect_int("month", "CivilTime")? as i8;
        let day = value.expect_int("day", "CivilTime")? as i8;
        let hour = value.expect_int("hour", "CivilTime")? as i8;
        let minute = value.expect_int("minute", "CivilTime")? as i8;
        let second = value.expect_int("second", "CivilTime")? as i8;
        let nanos = value.expect_int("nanos", "CivilTime")? as i32;
        let zone_name = value.expect_str("zone", "CivilTime")?;
        let tz = TimeZone::get(zone_name)?;
        let dt = DateTime::new(year, month, day, hour, minute, second, nanos)?;
        Ok(Self {
            zoned: dt.to_zoned(tz)?,
        })
    }
}
