// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use anyhow::Result;
use geodesy::Geode;
use rstar::{primitives::GeomWithData, RTree};
use std::collections::HashMap;
use xz_wrapper::decompress;

const ZONE_TAGS: &[u8] = include_bytes!("../assets/geonames-filtered.csv.xz");

type ZoneTaggedLocation = GeomWithData<[f32; 2], usize>;

#[derive(Debug)]
pub struct TimeZoneMap {
    zone_tags: RTree<GeomWithData<[f32; 2], usize>>,
    zone_id_to_name: HashMap<usize, String>,
}

impl TimeZoneMap {
    pub(crate) fn load() -> Result<Self> {
        let mut last_zone_id = 1usize;
        let mut zone_id_to_name: HashMap<usize, String> = HashMap::new();
        let mut zone_name_to_id: HashMap<String, usize> = HashMap::new();
        let mut zone_tags = RTree::new();

        let uncompressed = decompress(ZONE_TAGS)?;
        let mut reader = csv::ReaderBuilder::new()
            .delimiter(b';')
            .from_reader(uncompressed.as_slice());
        for record in reader.records() {
            let record = record?;
            let coords = &record[3];
            let timezone = &record[2];

            let id = if zone_name_to_id.contains_key(timezone) {
                *zone_name_to_id.get(timezone).unwrap()
            } else {
                zone_name_to_id.insert(timezone.to_owned(), last_zone_id);
                zone_id_to_name.insert(last_zone_id, timezone.to_owned());
                last_zone_id += 1;
                last_zone_id - 1
            };

            let mut latlon_iter = coords
                .split(',')
                .flat_map(|v| v.trim_start().parse::<f32>());
            let latlon = [latlon_iter.next().unwrap(), latlon_iter.next().unwrap()];

            zone_tags.insert(ZoneTaggedLocation::new(latlon, id));
        }
        Ok(Self {
            zone_tags,
            zone_id_to_name,
        })
    }

    pub(crate) fn get_time_zone(&self, geode: &Geode) -> &str {
        // FIXME: upload in radians to avoid the division here.
        let coord = [geode.lat::<Degrees>().f32(), geode.lon::<Degrees>().f32()];
        let Some(data) = self.zone_tags.nearest_neighbor(&coord) else {
            return "UTC";
        };

        self.zone_id_to_name
            .get(&data.data)
            .map(|v| v.as_str())
            .unwrap_or("UTC")
    }
}
