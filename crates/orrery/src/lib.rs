// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
mod civil_time;
mod timezone;

pub use civil_time::CivilTime;

use crate::timezone::TimeZoneMap;
use absolute_unit::prelude::*;
use animate::Timeline;
use anyhow::Result;
use bevy_ecs::prelude::*;
use geodesy::Geode;
use glam::{DVec3, Mat4, Vec4};
use hifitime::{Duration, Epoch};
use jiff::{tz::TimeZone, Span, Timestamp, Zoned};
use mantle::{BindingCollection, CurrentEncoder, Gpu, GpuStep, LayoutProvider};
use nitrous::{inject_nitrous_resource, method, HeapMut, NitrousResource, Value};
use runtime::{Extension, Runtime, TimeStep, TimeStepStep};
use shader_shared::{
    binding,
    layout::{self, frag},
};
use std::{f64::consts::PI, sync::Arc};
use zerocopy::{FromBytes, Immutable, IntoBytes};

#[repr(C)]
#[derive(IntoBytes, FromBytes, Immutable, Copy, Clone, Debug, Default)]
struct OrreryData {
    // Vector towards the sun, normalized, in world coordinates.
    sun_direction: [f32; 4],
}

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum OrreryStep {
    StepTime,
    UploadToGpu,
}

/**
 * Orbital mechanics works great. Time, however, does not. The time reference for ephimeris is a
 * position on a spinning thing, whose period drifts by human observable amounts over human relevant
 * timespans. To complicate matters further, that spinning thing is itself tidally locked to a mass
 * called the moon, which means that the celestially relevant orbital parameters have to be
 * specified around the "barycenter", rather than about the center of spin. Minor celestial
 * fluctuations are amplified in this system, resulting in a spin rate on Earth that is not
 * a constant. Thus, the reference time and direction are not periodic with respect to each other.
 * To throw a further wrench in the works, we offset the meaning of time occasionally so that
 * things appear to more or less line up locally, confounding the larger picture. So if one wants
 * to use J2000 to find the relative position of planets, one needs to subtract leap seconds, but
 * if one wants the locally relevant spin position of a planet, one must not subtract leap seconds.
 *
 * The name orrery was chosen for this module to put people in mind of the tiny and obviously
 * inaccurate physical solar system models built with gears. That is ultimately how this module
 * is intended: a hack that gives a flavor of the the real thing without trying too hard. Proper
 * scientists should look elsewhere or hold their nose while reading below.
 */
#[derive(Debug, NitrousResource)]
pub struct Orrery {
    orrery_buffer: wgpu::Buffer,
    bind_group_layout: wgpu::BindGroupLayout,
    bind_group: wgpu::BindGroup,

    // Where our TimeStep's sim time is a fixed and unchanging advance, our orrery maps that
    // sim time into a human-relevant time and lets us change the human relevant time without
    // affecting more basic simulation parameters, like animation, fuel consumption, etc.
    local_now_epoch: Epoch,

    // We may be manually moving the sun position.
    in_debug_override: bool,

    // We may be animating the sun position.
    sun_anim_handle: Value,

    // Map from geode to timezone offset/name
    time_zone_map: TimeZoneMap,
}

impl Extension for Orrery {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        let orrery = Orrery::new_current_time(runtime.resource::<Gpu>())?;
        runtime.inject_resource(orrery)?;
        runtime.add_input_system(
            Self::sys_step_time
                .in_set(OrreryStep::StepTime)
                .after(TimeStepStep::Tick),
        );
        runtime.add_frame_system(
            Self::sys_upload_to_gpu
                .in_set(OrreryStep::UploadToGpu)
                .after(GpuStep::CreateCommandEncoder)
                .before(GpuStep::PresentTargetSurface),
        );
        Ok(())
    }
}

impl LayoutProvider for Orrery {
    fn push_layout(layouts: &mut Vec<wgpu::BindGroupLayoutEntry>) {
        layouts.push(frag::uniform::<OrreryData>(layouts.len()));
    }

    fn push_binding<'a, 'b, 'c>(&'a self, mut bindings: BindingCollection<'b, 'c>)
    where
        'a: 'b,
        'a: 'c,
    {
        bindings.buffer(&self.orrery_buffer);
    }
}

#[inject_nitrous_resource]
impl Orrery {
    pub fn new_current_time(gpu: &Gpu) -> Result<Self> {
        Self::new(Epoch::now()?, gpu)
    }

    pub fn new(initial_utc: Epoch, gpu: &Gpu) -> Result<Self> {
        let orrery_buffer = gpu.push_data(
            "orrery-buf",
            &OrreryData {
                sun_direction: Vec4::X.to_array(),
            },
            wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::UNIFORM,
        );
        let bind_group_layout =
            gpu.device()
                .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                    label: Some("orrery-bind-group-layout"),
                    entries: &[layout::uniform::<OrreryData>(0, wgpu::ShaderStages::all())],
                });
        let bind_group = gpu.device().create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("orrery-bind-group"),
            layout: &bind_group_layout,
            entries: &[binding::buffer(0, &orrery_buffer)],
        });
        let obj = Self {
            orrery_buffer,
            bind_group_layout,
            bind_group,
            local_now_epoch: initial_utc,
            in_debug_override: false,
            sun_anim_handle: Timeline::none_handle(),
            time_zone_map: TimeZoneMap::load()?,
        };
        Ok(obj)
    }

    // From:
    //   "Using UTC to Determine the Earth's Rotation Angle"
    //   Dennis D. McCarthy
    //   Fetched 13 Sept 24
    //   http://hanksville.org/futureofutc/2011/preprints/13_AAS_11-666_McCarthy.pdf
    //
    // ERA(Tu)=
    //    θ(Tu) = 2 * π * (0.779_057_273_264_0 + 1.002_737_811_911_354_48 * Tu)
    //
    // Tu = (Julian UT1 date - 2451545.0)
    // UT1 = UTC + (UT1–UTC)
    //
    // Per the discussion in the paper, the (UT1-UTC) error factor is observational because
    // the earth's spin is not constant or fully predictable. This could result in errors
    // up to half a kilometer. We set it to 0 for our purposes here.
    pub fn hour_angle_matrix(&self) -> Mat4 {
        let jd_days = self.local_now_epoch.to_jde_tai_days();
        let t_u = jd_days - 2451545.0;
        let era = 2. * PI * (0.779_057_273_264_0f64 + 1.002_737_811_911_354_5f64 * t_u);
        let era = era % (2. * PI);
        Mat4::from_rotation_y(era as f32).inverse()
    }

    pub fn bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        &self.bind_group_layout
    }

    pub fn bind_group(&self) -> &wgpu::BindGroup {
        &self.bind_group
    }

    #[method]
    pub fn lookup_time_zone(&self, geode: Geode) -> &str {
        self.time_zone_map.get_time_zone(&geode)
    }

    #[method]
    pub fn get_local_civil(&self, geode: Geode) -> Result<CivilTime> {
        let unix_ms = self.local_now_epoch.to_unix_milliseconds();
        let jiff_ts = Timestamp::from_millisecond(unix_ms as i64)?;
        let jiff_tz = TimeZone::get(self.lookup_time_zone(geode))?;
        let zoned = Zoned::new(jiff_ts, jiff_tz);
        Ok(zoned.into())
    }

    pub fn get_local_civil_for_zone(&self, zone: TimeZone) -> Result<CivilTime> {
        let unix_ms = self.local_now_epoch.to_unix_milliseconds();
        let jiff_ts = Timestamp::from_millisecond(unix_ms as i64)?;
        let zoned = Zoned::new(jiff_ts, zone);
        Ok(zoned.into())
    }

    #[method]
    pub fn set_local_civil_immediate(&mut self, civil: CivilTime) {
        self.local_now_epoch = civil.into();
    }

    // Smoothly animate to the civil time provided by the CivilTime.
    //
    // Note: that we can only set a _time_ here. Setting a day with an animation leads
    //       to rapid flashing if the dates are too far apart, which we want to avoid at
    //       all costs. Thus, this ignores the Date and only uses the TZ and Time.
    //
    //  FIXME: Using a separate set_local_civil_immediate to change the date will also
    //         cause a discontinuity. In theory we should also be able to blend within a
    //         year to both move the sun within a day and also precess it to the right
    //         position in the sky at the given season, without discontinuities.
    #[method]
    pub fn set_local_civil(
        &mut self,
        time: CivilTime,
        duration: Time<Seconds>,
        mut heap: HeapMut,
    ) -> Result<()> {
        // Get our current time at the current location.
        let civil_now: CivilTime =
            self.get_local_civil_for_zone(time.zoned().time_zone().to_owned())?;

        // Convert to a pure civil time.
        let time = time.time();

        // Stop any currently running animation
        heap.resource_mut::<Timeline>()
            .stop(self.sun_anim_handle.to_dict()?.try_get("handle")?.clone())?;

        // Over-ride the time component with `time`, but advance the day,
        // if the time is earlier than the time of now.
        let mut target_zoned: Zoned = civil_now.zoned().with().time(time).build()?;
        if time < civil_now.time() {
            target_zoned = (&target_zoned) + Span::new().days(1);
        }

        let initial = self.local_now_epoch.to_unix_milliseconds();
        let target = CivilTime::new(target_zoned)
            .to_epoch()
            .to_unix_milliseconds();
        self.sun_anim_handle = heap.resource_mut::<Timeline>().ease_in(
            Value::RustMethod(Arc::new(move |args, mut heap| {
                let dist = target - initial;
                let f = args[0].to_float()?;
                let unix_ms = initial + f * dist;
                heap.resource_mut::<Orrery>().local_now_epoch =
                    Epoch::from_unix_milliseconds(unix_ms);
                Ok(Value::Boolean(false))
            })),
            0.,
            1.,
            duration,
        )?;
        Ok(())
    }

    // FIXME: DELETE
    #[method]
    pub fn get_unix_ms(&self) -> f64 {
        self.local_now_epoch.to_unix_milliseconds()
    }

    // FIXME: DELETE
    #[method]
    pub fn set_date_time(
        &mut self,
        year: i64,
        month: i64,
        day: i64,
        hour: i64,
        min: i64,
        sec: i64,
    ) {
        self.local_now_epoch = Epoch::from_gregorian_utc(
            year as i32,
            month as u8,
            day as u8,
            hour as u8,
            min as u8,
            sec as u8,
            0,
        );
    }

    pub fn sys_step_time(step: Res<TimeStep>, mut orrery: ResMut<Orrery>) {
        orrery.step_time(*step.fixed_sim_step());
    }

    fn step_time(&mut self, dt: Duration) {
        self.local_now_epoch += dt;

        // FIXME: find a way to make this available from the console easily
        // self.local_now_epoch += Duration::from_days(1.);
    }

    pub fn local_now(&self) -> &Epoch {
        &self.local_now_epoch
    }

    pub fn sys_upload_to_gpu(
        orrery: Res<Orrery>,
        gpu: Res<Gpu>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) {
        orrery.upload_to_gpu(&gpu, &mut maybe_encoder);
    }

    fn upload_to_gpu(&self, gpu: &Gpu, maybe_encoder: &mut CurrentEncoder) {
        if let Some(encoder) = maybe_encoder.get_encoder_mut() {
            gpu.upload_data_to(
                &OrreryData {
                    sun_direction: Vec4::from((self.sun_direction().as_vec3(), 0.)).to_array(),
                },
                &self.orrery_buffer,
                encoder,
            );
        }
    }

    pub fn sun_direction(&self) -> DVec3 {
        // The *modified* julian date has had the normal number of days subtrated off already.
        // We want to use TAI when computing the ecliptic position of the sun in the inertial frame.
        // We want to use the (relatively) spin corrected utc when adjusting to the local rotation.
        let tai_days = self.local_now_epoch.to_mjd_tai_days();
        let (sun, _radius) = astro::sun::geocent_ecl_pos(tai_days);
        let mn_obliq = astro::ecliptic::mn_oblq_IAU(tai_days);

        // Now convert from ecliptic lat/lon to equatorial ra/dec.
        let sun_ra = astro::coords::asc_frm_ecl(sun.long, sun.lat, mn_obliq);
        let sun_dec = astro::coords::dec_frm_ecl(sun.long, sun.lat, mn_obliq);

        // And convert from the equatorial ra/dec to local non-inertial coordinates by computing
        // the sidereal time. Use the UTC (UT1 would be preferable, but that would require more data)
        // here instead of TAI, to get arcsecond errors instead of arcminute errors in sun position.
        let utc_days = self.local_now_epoch.to_mjd_utc_days();
        let sidereal = radians!(astro::time::mn_sidr(utc_days));
        let hr_angle = astro::coords::hr_angl_frm_loc_sidr(sidereal.f64(), sun_ra);

        // Convert to a direction in nitrous space
        // London is on +Z, so when the hr is 0, we should be at full on Z.
        // America is on +X, with Y up.
        // As the hour gets later, the sun moves towards the +X axis
        // I'm not sure why we're 180 off (but only on hr-angle, not dec).
        let hr = hr_angle + PI;
        DVec3::new(
            hr.sin() * sun_dec.cos(),
            sun_dec.sin(),
            hr.cos() * sun_dec.cos(),
        )
    }

    #[method]
    pub fn move_sun(&mut self, pressed: bool) {
        self.in_debug_override = pressed;
    }

    #[method]
    pub fn handle_mousemove(&mut self, dx: f64) {
        if self.in_debug_override {
            self.local_now_epoch += Duration::from_seconds(60. * dx);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use event_mapper::EventMapper;
    use mantle::Core;

    #[test]
    fn it_works() -> Result<()> {
        let runtime = Core::for_test()?
            .with_extension::<EventMapper>()?
            .with_extension::<Orrery>()?;
        runtime.resource::<Orrery>().sun_direction();
        Ok(())
    }
}
