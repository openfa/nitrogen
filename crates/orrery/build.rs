// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::Result;
use std::{
    fs::{self, File},
    io::{Read, Write},
};
use xz_wrapper::{compress, decompress};

const MIN_POPULATION: usize = 50_000;

const SOURCE: &str = "./assets/geonames.csv.xz";
const TARGET: &str = "./assets/geonames-filtered.csv.xz";

fn main() -> Result<()> {
    println!("cargo:rerun-if-changed={SOURCE}");
    println!("cargo:rerun-if-changed={TARGET}");

    let mut initial = File::open(SOURCE)?;

    // Check if we can skip the rest of our work.
    if let Ok(target) = File::open(TARGET) {
        if initial.metadata()?.created()? < target.metadata()?.created()? {
            // We've already created the target, and it's older than the source.
            return Ok(());
        }
        fs::remove_file(TARGET)?;
    }

    // xzdec the source
    let mut src_raw = vec![];
    initial.read_to_end(&mut src_raw)?;
    let src_data = decompress(&src_raw)?;
    let src_str = String::from_utf8(src_data)?;

    // Parse
    //  0 Geoname ID;
    //  1 Name;
    //  2 ASCII Name;
    //  3 Alternate Names;
    //  4 Feature Class;
    //  5 Feature Code;
    //  6 Country Code;
    //  7 Country name EN;
    //  8 Country Code 2;
    //  9 Admin1 Code;
    // 10 Admin2 Code;
    // 11 Admin3 Code;
    // 12 Admin4 Code;
    // 13 Population;
    // 14 Elevation;
    // 15 DIgital Elevation Model;
    // 16 Timezone;
    // 17 Modification date;
    // 18 LABEL EN;
    // 19 Coordinates
    let mut reader = csv::ReaderBuilder::new()
        .delimiter(b';')
        .from_reader(src_str.as_bytes());

    // Keep only the data we need and only from cities with population greater than 2500.
    // The base data set is >1000 people. The data has an exponential distribution so this
    // cuts our size down to something reasonable while still giving reasonable accuracy.
    // 0 Name;
    // 1 Country Code;
    // 2 Timezone;
    // 3 Coordinates
    let mut tgt = vec![];
    {
        let mut writer = csv::WriterBuilder::new()
            .delimiter(b';')
            .from_writer(&mut tgt);
        writer.write_record(["Name", "Country Code", "Timezone", "Coordinates"])?;

        for record in reader.records() {
            let record = record?;
            let population = record[13].parse::<usize>()?;
            if population > MIN_POPULATION {
                writer.write_record([&record[1], &record[6], &record[16], &record[19]])?;
            }
        }
        writer.flush()?;
    }

    // Compress
    let tgt_data = compress(&tgt, 9)?;
    let mut tgt_xz = File::create(TARGET)?;
    tgt_xz.write_all(&tgt_data)?;

    Ok(())
}
