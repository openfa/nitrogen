// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
pub mod layout {
    use std::mem;

    #[inline]
    pub fn generic_buffer(
        binding: usize,
        visibility: wgpu::ShaderStages,
        sized: Option<wgpu::BufferSize>,
        read_only: bool,
    ) -> wgpu::BindGroupLayoutEntry {
        wgpu::BindGroupLayoutEntry {
            binding: binding as u32,
            visibility,
            ty: wgpu::BindingType::Buffer {
                ty: wgpu::BufferBindingType::Storage { read_only },
                has_dynamic_offset: false,
                // If not set, then the size is checked at each draw call
                min_binding_size: sized,
            },
            // Note: count is only relevant for texture-arrays and those need native support
            count: None,
        }
    }

    #[inline]
    pub fn buffer_for(
        binding: usize,
        buffer: &wgpu::Buffer,
        visibility: wgpu::ShaderStages,
    ) -> wgpu::BindGroupLayoutEntry {
        let size = wgpu::BufferSize::new(buffer.size());
        generic_buffer(binding, visibility, size, true)
    }

    #[inline]
    pub fn buffer_rw_for(
        binding: usize,
        buffer: &wgpu::Buffer,
        visibility: wgpu::ShaderStages,
    ) -> wgpu::BindGroupLayoutEntry {
        let size = wgpu::BufferSize::new(buffer.size());
        generic_buffer(binding, visibility, size, false)
    }

    #[inline]
    pub fn buffer<T: Sized>(
        binding: usize,
        visibility: wgpu::ShaderStages,
    ) -> wgpu::BindGroupLayoutEntry {
        let size = wgpu::BufferSize::new(mem::size_of::<T>() as u64);
        generic_buffer(binding, visibility, size, true)
    }

    #[inline]
    pub fn buffer_rt(binding: usize, visibility: wgpu::ShaderStages) -> wgpu::BindGroupLayoutEntry {
        generic_buffer(binding, visibility, None, true)
    }

    #[inline]
    pub fn buffer_rw(binding: usize, visibility: wgpu::ShaderStages) -> wgpu::BindGroupLayoutEntry {
        generic_buffer(binding, visibility, None, false)
    }

    #[inline]
    pub fn uniform<T: Sized>(
        binding: usize,
        visibility: wgpu::ShaderStages,
    ) -> wgpu::BindGroupLayoutEntry {
        let size = wgpu::BufferSize::new(mem::size_of::<T>() as u64);
        wgpu::BindGroupLayoutEntry {
            binding: binding as u32,
            visibility,
            ty: wgpu::BindingType::Buffer {
                ty: wgpu::BufferBindingType::Uniform,
                has_dynamic_offset: false,
                min_binding_size: size,
            },
            count: None,
        }
    }

    #[inline]
    pub fn texture_for(
        binding: usize,
        texture: &wgpu::Texture,
        visibility: wgpu::ShaderStages,
    ) -> wgpu::BindGroupLayoutEntry {
        wgpu::BindGroupLayoutEntry {
            binding: binding as u32,
            visibility,
            ty: wgpu::BindingType::Texture {
                sample_type: texture
                    .format()
                    .sample_type(
                        Some(wgpu::TextureAspect::All),
                        Some(wgpu::Features::all_webgpu_mask()),
                    )
                    .expect("a valid texture format"),
                view_dimension: match texture.dimension() {
                    wgpu::TextureDimension::D1 => wgpu::TextureViewDimension::D1,
                    wgpu::TextureDimension::D2 => wgpu::TextureViewDimension::D2,
                    wgpu::TextureDimension::D3 => wgpu::TextureViewDimension::D3,
                },
                multisampled: texture.sample_count() > 1,
            },
            count: None,
        }
    }

    #[inline]
    pub fn texture(binding: usize, visibility: wgpu::ShaderStages) -> wgpu::BindGroupLayoutEntry {
        texture_any(
            binding,
            visibility,
            wgpu::TextureSampleType::Float { filterable: true },
        )
    }

    #[inline]
    pub fn texture_uint(
        binding: usize,
        visibility: wgpu::ShaderStages,
    ) -> wgpu::BindGroupLayoutEntry {
        texture_any(binding, visibility, wgpu::TextureSampleType::Uint)
    }

    #[inline]
    pub fn texture_any(
        binding: usize,
        visibility: wgpu::ShaderStages,
        sample_type: wgpu::TextureSampleType,
    ) -> wgpu::BindGroupLayoutEntry {
        wgpu::BindGroupLayoutEntry {
            binding: binding as u32,
            visibility,
            ty: wgpu::BindingType::Texture {
                sample_type,
                view_dimension: wgpu::TextureViewDimension::D2,
                multisampled: false,
            },
            count: None,
        }
    }

    #[inline]
    pub fn texture3d(binding: usize, visibility: wgpu::ShaderStages) -> wgpu::BindGroupLayoutEntry {
        wgpu::BindGroupLayoutEntry {
            binding: binding as u32,
            visibility,
            ty: wgpu::BindingType::Texture {
                sample_type: wgpu::TextureSampleType::Float { filterable: true },
                view_dimension: wgpu::TextureViewDimension::D3,
                multisampled: false,
            },
            count: None,
        }
    }

    #[inline]
    pub fn sampler(binding: usize, visibility: wgpu::ShaderStages) -> wgpu::BindGroupLayoutEntry {
        wgpu::BindGroupLayoutEntry {
            binding: binding as u32,
            visibility,
            ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
            count: None,
        }
    }

    #[inline]
    pub fn sampler_nearest(
        binding: usize,
        visibility: wgpu::ShaderStages,
    ) -> wgpu::BindGroupLayoutEntry {
        wgpu::BindGroupLayoutEntry {
            binding: binding as u32,
            visibility,
            ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::NonFiltering),
            count: None,
        }
    }

    #[inline]
    pub fn sampler_for_mode(
        binding: usize,
        mode: wgpu::FilterMode,
        visibility: wgpu::ShaderStages,
    ) -> wgpu::BindGroupLayoutEntry {
        match mode {
            wgpu::FilterMode::Linear => sampler(binding, visibility),
            wgpu::FilterMode::Nearest => sampler_nearest(binding, visibility),
        }
    }

    #[inline]
    pub fn storage_texture_wo(
        binding: usize,
        texture: &wgpu::Texture,
        visibility: wgpu::ShaderStages,
    ) -> wgpu::BindGroupLayoutEntry {
        wgpu::BindGroupLayoutEntry {
            binding: binding as u32,
            visibility,
            ty: wgpu::BindingType::StorageTexture {
                view_dimension: match texture.dimension() {
                    wgpu::TextureDimension::D1 => wgpu::TextureViewDimension::D1,
                    wgpu::TextureDimension::D2 => wgpu::TextureViewDimension::D2,
                    wgpu::TextureDimension::D3 => wgpu::TextureViewDimension::D3,
                },
                format: texture.format(),
                access: wgpu::StorageTextureAccess::WriteOnly,
            },
            count: None,
        }
    }

    #[inline]
    pub fn storage_texture_rw(
        binding: usize,
        texture: &wgpu::Texture,
        visibility: wgpu::ShaderStages,
    ) -> wgpu::BindGroupLayoutEntry {
        wgpu::BindGroupLayoutEntry {
            binding: binding as u32,
            visibility,
            ty: wgpu::BindingType::StorageTexture {
                view_dimension: match texture.dimension() {
                    wgpu::TextureDimension::D1 => wgpu::TextureViewDimension::D1,
                    wgpu::TextureDimension::D2 => wgpu::TextureViewDimension::D2,
                    wgpu::TextureDimension::D3 => wgpu::TextureViewDimension::D3,
                },
                format: texture.format(),
                access: wgpu::StorageTextureAccess::ReadWrite,
            },
            count: None,
        }
    }

    pub mod comp {
        use crate::layout;

        #[inline]
        pub fn buffer_for(binding: usize, buffer: &wgpu::Buffer) -> wgpu::BindGroupLayoutEntry {
            layout::buffer_for(binding, buffer, wgpu::ShaderStages::COMPUTE)
        }

        #[inline]
        pub fn buffer_rw_for(binding: usize, buffer: &wgpu::Buffer) -> wgpu::BindGroupLayoutEntry {
            layout::buffer_rw_for(binding, buffer, wgpu::ShaderStages::COMPUTE)
        }

        #[inline]
        pub fn buffer<T: Sized>(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::buffer::<T>(binding, wgpu::ShaderStages::COMPUTE)
        }

        #[inline]
        pub fn buffer_rt(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::buffer_rt(binding, wgpu::ShaderStages::COMPUTE)
        }

        #[inline]
        pub fn buffer_rw(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::buffer_rw(binding, wgpu::ShaderStages::COMPUTE)
        }

        #[inline]
        pub fn uniform<T: Sized>(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::uniform::<T>(binding, wgpu::ShaderStages::COMPUTE)
        }

        #[inline]
        pub fn texture_for(binding: usize, texture: &wgpu::Texture) -> wgpu::BindGroupLayoutEntry {
            layout::texture_for(binding, texture, wgpu::ShaderStages::COMPUTE)
        }

        #[inline]
        pub fn texture(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::texture(binding, wgpu::ShaderStages::COMPUTE)
        }

        #[inline]
        pub fn sampler(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::sampler(binding, wgpu::ShaderStages::COMPUTE)
        }

        #[inline]
        pub fn storage_texture_wo(
            binding: usize,
            texture: &wgpu::Texture,
        ) -> wgpu::BindGroupLayoutEntry {
            layout::storage_texture_wo(binding, texture, wgpu::ShaderStages::COMPUTE)
        }

        #[inline]
        pub fn storage_texture_rw(
            binding: usize,
            texture: &wgpu::Texture,
        ) -> wgpu::BindGroupLayoutEntry {
            layout::storage_texture_rw(binding, texture, wgpu::ShaderStages::COMPUTE)
        }
    }

    pub mod frag {
        use crate::layout;

        #[inline]
        pub fn buffer<T: Sized>(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::buffer::<T>(binding, wgpu::ShaderStages::FRAGMENT)
        }

        #[inline]
        pub fn buffer_rt(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::buffer_rt(binding, wgpu::ShaderStages::FRAGMENT)
        }

        #[inline]
        pub fn buffer_rw(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::buffer_rw(binding, wgpu::ShaderStages::FRAGMENT)
        }

        #[inline]
        pub fn uniform<T: Sized>(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::uniform::<T>(binding, wgpu::ShaderStages::FRAGMENT)
        }

        #[inline]
        pub fn texture(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::texture(binding, wgpu::ShaderStages::FRAGMENT)
        }

        #[inline]
        pub fn texture3d(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::texture3d(binding, wgpu::ShaderStages::FRAGMENT)
        }

        #[inline]
        pub fn sampler(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::sampler(binding, wgpu::ShaderStages::FRAGMENT)
        }

        #[inline]
        pub fn sampler_for_mode(
            binding: usize,
            mode: wgpu::FilterMode,
        ) -> wgpu::BindGroupLayoutEntry {
            layout::sampler_for_mode(binding, mode, wgpu::ShaderStages::FRAGMENT)
        }
    }

    pub mod vert {
        use crate::layout;

        #[inline]
        pub fn buffer<T: Sized>(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::buffer::<T>(binding, wgpu::ShaderStages::VERTEX)
        }

        #[inline]
        pub fn buffer_rt(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::buffer_rt(binding, wgpu::ShaderStages::VERTEX)
        }

        #[inline]
        pub fn buffer_rw(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::buffer_rw(binding, wgpu::ShaderStages::VERTEX)
        }

        #[inline]
        pub fn uniform<T: Sized>(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::uniform::<T>(binding, wgpu::ShaderStages::VERTEX)
        }

        #[inline]
        pub fn texture(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::texture(binding, wgpu::ShaderStages::VERTEX)
        }

        #[inline]
        pub fn sampler(binding: usize) -> wgpu::BindGroupLayoutEntry {
            layout::sampler(binding, wgpu::ShaderStages::VERTEX)
        }
    }
}

pub mod binding {
    #[inline]
    pub fn buffer(binding: usize, buffer: &wgpu::Buffer) -> wgpu::BindGroupEntry {
        wgpu::BindGroupEntry {
            binding: binding as u32,
            resource: wgpu::BindingResource::Buffer(wgpu::BufferBinding {
                buffer,
                // These are only relevant if we are using a sub-slice of the buffer.
                offset: 0,
                size: None,
            }),
        }
    }

    #[inline]
    pub fn texture(binding: usize, texture_view: &wgpu::TextureView) -> wgpu::BindGroupEntry {
        wgpu::BindGroupEntry {
            binding: binding as u32,
            resource: wgpu::BindingResource::TextureView(texture_view),
        }
    }

    #[inline]
    pub fn sampler(binding: usize, sampler: &wgpu::Sampler) -> wgpu::BindGroupEntry {
        wgpu::BindGroupEntry {
            binding: binding as u32,
            resource: wgpu::BindingResource::Sampler(sampler),
        }
    }
}
