// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

#[derive(Clone, Debug)]
enum LayoutBuilderLayer<'a> {
    None,
    Existing(&'a wgpu::BindGroupLayout),
    Build(Vec<wgpu::BindGroupLayoutEntry>),
}

/// Help create a compute pipeline
#[derive(Debug)]
pub struct ComputePipelineBuilder<'a> {
    label: &'a str,
    device: &'a wgpu::Device,
    layouts: Vec<LayoutBuilderLayer<'a>>,
    module: Option<wgpu::ShaderModule>,
    entry_point: Option<&'a str>,
}

impl<'a> ComputePipelineBuilder<'a> {
    pub fn new(label: &'a str, num_layouts: usize, device: &'a wgpu::Device) -> Self {
        Self {
            label,
            device,
            layouts: vec![LayoutBuilderLayer::None; num_layouts],
            module: None,
            entry_point: None,
        }
    }

    pub fn with_shader_wgsl(mut self, label: &str, source: &str) -> Self {
        self.module = Some(
            self.device
                .create_shader_module(wgpu::ShaderModuleDescriptor {
                    label: Some(label),
                    source: wgpu::ShaderSource::Wgsl(source.into()),
                }),
        );
        self
    }

    pub fn with_entry_point(mut self, entry: &'a str) -> Self {
        self.entry_point = Some(entry);
        self
    }

    pub fn with_existing_layout(
        mut self,
        offset: usize,
        layout: &'a wgpu::BindGroupLayout,
    ) -> Self {
        assert!(matches!(self.layouts[offset], LayoutBuilderLayer::None));
        self.layouts[offset] = LayoutBuilderLayer::Existing(layout);
        self
    }

    pub fn with_binding_entries(
        mut self,
        offset: usize,
        entries: Vec<wgpu::BindGroupLayoutEntry>,
    ) -> Self {
        assert!(matches!(self.layouts[offset], LayoutBuilderLayer::None));
        self.layouts[offset] = LayoutBuilderLayer::Build(entries);
        self
    }

    pub fn with_binding_entry(mut self, offset: usize, entry: wgpu::BindGroupLayoutEntry) -> Self {
        assert!(offset < self.layouts.len());
        match &mut self.layouts[offset] {
            LayoutBuilderLayer::Build(v) => {
                v.push(entry);
            }
            LayoutBuilderLayer::Existing(_) => {
                panic!("attempting to add an entry to an 'existing' type layer");
            }
            layer => {
                debug_assert!(matches!(layer, LayoutBuilderLayer::None));
                *layer = LayoutBuilderLayer::Build(vec![entry]);
            }
        }
        self
    }

    pub fn build(self) -> (wgpu::ComputePipeline, Vec<wgpu::BindGroupLayout>) {
        // Note: we have to do this in two passes so that reallocs in the new-layouts vec will not
        //       destroy the refs we need to take to build the layouts vec.
        let mut new_layouts = vec![];
        for (i, layer) in self.layouts.iter().enumerate() {
            match layer {
                LayoutBuilderLayer::None => {
                    panic!("do not know how to handle empty layers");
                }
                LayoutBuilderLayer::Build(entries) => {
                    new_layouts.push(self.device.create_bind_group_layout(
                        &wgpu::BindGroupLayoutDescriptor {
                            label: Some(&format!("{}-bind-group-{}", self.label, i)),
                            entries,
                        },
                    ));
                }
                LayoutBuilderLayer::Existing(_) => {}
            }
        }
        // Pull refs from our new_layouts and existing layouts.
        let mut layouts = vec![];
        let mut cursor = 0usize;
        for layer in &self.layouts {
            match layer {
                LayoutBuilderLayer::None => {
                    panic!("do not know how to handle empty layers");
                }
                LayoutBuilderLayer::Build(_) => {
                    layouts.push(&new_layouts[cursor]);
                    cursor += 1;
                }
                LayoutBuilderLayer::Existing(layout) => {
                    layouts.push(*layout);
                }
            }
        }
        let compute_pipeline =
            self.device
                .create_compute_pipeline(&wgpu::ComputePipelineDescriptor {
                    label: Some(&format!("{}-pipeline", self.label)),
                    layout: Some(&self.device.create_pipeline_layout(
                        &wgpu::PipelineLayoutDescriptor {
                            label: Some(&format!("{}-layout", self.label)),
                            push_constant_ranges: &[],
                            bind_group_layouts: &layouts,
                        },
                    )),
                    module: self.module.as_ref().unwrap(),
                    entry_point: self.entry_point.unwrap_or("main"),
                    compilation_options: wgpu::PipelineCompilationOptions {
                        constants: &Default::default(),
                        zero_initialize_workgroup_memory: false,
                    },
                    // cache: None,
                });
        (compute_pipeline, new_layouts)
    }
}
