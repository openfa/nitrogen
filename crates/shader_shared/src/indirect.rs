// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use zerocopy::{FromBytes, Immutable, IntoBytes};

/// As per the documentation on draw_indexed_indirect
#[repr(C)]
#[derive(IntoBytes, FromBytes, Immutable, Copy, Clone, Debug, Default)]
pub struct DrawIndexedIndirect {
    pub index_count: u32,    // The number of vertices to draw.
    pub instance_count: u32, // The number of instances to draw.
    pub base_index: u32,     // The base index within the index buffer.
    pub vertex_offset: i32, // The value added to the vertex index before indexing into the vertex buffer.
    pub base_instance: u32, // The instance ID of the first instance to draw.
}

#[repr(C)]
#[derive(IntoBytes, FromBytes, Immutable, Copy, Clone, Debug, Default)]
pub struct DrawIndirectCommand {
    pub vertex_count: u32,
    pub instance_count: u32,
    pub first_vertex: u32,
    pub first_instance: u32,
}
