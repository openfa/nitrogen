// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

fn arr_to_vec2_(arr: array<f32,2u>) -> vec2<f32> {
    return vec2<f32>(arr[0], arr[1]);
}

fn arr_to_vec3_(arr: array<f32,3u>) -> vec3<f32> {
    return vec3<f32>(arr[0], arr[1], arr[2]);
}

fn vec3_to_arr(v: vec3<f32>) -> array<f32,3u> {
    return array<f32,3u>(v.x, v.y, v.z);
}
