// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

struct Quat {
    v: vec4<f32>
}

fn quat_from_axis_angle(axis: vec3<f32>, angle: f32) -> Quat
{
    var qr = vec4(0.);
    let half_angle = (angle * 0.5); // * 3.14159 / 180.0;
    let sin_ha = sin(half_angle);
    qr.x = axis.x * sin_ha;
    qr.y = axis.y * sin_ha;
    qr.z = axis.z * sin_ha;
    qr.w = cos(half_angle);
    return Quat(qr);
}

fn quat_conj(q: Quat) -> Quat
{
    return Quat(vec4(-q.v.x, -q.v.y, -q.v.z, q.v.w));
}

fn quat_mult(quat1: Quat, quat2: Quat) -> Quat
{
    let q1 = quat1.v;
    let q2 = quat2.v;
    var qr = vec4(0.);
    qr.x = (q1.w * q2.x) + (q1.x * q2.w) + (q1.y * q2.z) - (q1.z * q2.y);
    qr.y = (q1.w * q2.y) - (q1.x * q2.z) + (q1.y * q2.w) + (q1.z * q2.x);
    qr.z = (q1.w * q2.z) + (q1.x * q2.y) - (q1.y * q2.x) + (q1.z * q2.w);
    qr.w = (q1.w * q2.w) - (q1.x * q2.x) - (q1.y * q2.y) - (q1.z * q2.z);
    return Quat(qr);
}

fn quat_rotate(qr: Quat, position: vec3<f32>) -> Quat
{
    let qr_conj = quat_conj(qr);
    let q_pos = Quat(vec4(position, 0.));
    let q_tmp = quat_mult(qr, q_pos);
    let rv = quat_mult(q_tmp, qr_conj).v.xyz;
    return Quat(vec4(rv, 1.));
}

//mat4 quat_to_rotation_rh(vec4 q)
//{
//    float x = q.x;
//    float y = q.y;
//    float z = q.z;
//    float w = q.w;
//
//    float r0c0 = 1 - 2*y*y - 2*z*z;
//    float r0c1 = 2*x*y + 2*w*z;
//    float r0c2 = 2*x*z - 2*w*y;
//    float r1c0 = 2*x*y - 2*w*z;
//    float r1c1 = 1 - 2*x*x - 2*z*z;
//    float r1c2 = 2*y*z + 2*w*x;
//    float r2c0 = 2*x*z + 2*w*y;
//    float r2c1 = 2*y*z - 2*w*x;
//    float r2c2 = 1 - 2*x*x - 2*y*y;
//
//    return mat4(mat3(
//        r0c0, r0c1, r0c2,
//        r1c0, r1c1, r1c2,
//        r2c0, r2c1, r2c2
//    ));
//}
//
//mat4 quat_to_rotation_lh(vec4 q)
//{
//    float x = q.x;
//    float y = q.y;
//    float z = q.z;
//    float w = q.w;
//
//    float r0c0 = 1 - 2*y*y - 2*z*z;
//    float r0c1 = 2*x*y - 2*w*z;
//    float r0c2 = 2*x*z + 2*w*y;
//    float r1c0 = 2*x*y + 2*w*z;
//    float r1c1 = 1 - 2*x*x - 2*z*z;
//    float r1c2 = 2*y*z - 2*w*x;
//    float r2c0 = 2*x*z - 2*w*y;
//    float r2c1 = 2*y*z + 2*w*x;
//    float r2c2 = 1 - 2*x*x - 2*y*y;
//
//    return mat4(mat3(
//        r0c0, r0c1, r0c2,
//        r1c0, r1c1, r1c2,
//        r2c0, r2c1, r2c2
//    ));
//}
//
