// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

var<private> PI: f32 = 3.14159265358979323846;
var<private> PI_2: f32 = 1.5707963705062866;
var<private> TAU: f32 =  6.2831854820251465;
var<private> EARTH_RADIUS_KM: f32 = 6360.0;
var<private> EARTH_RADIUS_M: f32 = 6360000.0;
var<private> EVEREST_HEIGHT_KM: f32 = 8.8480392;
var<private> EVEREST_HEIGHT_M: f32 = 8848.0392;
