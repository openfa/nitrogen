// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

struct FullscreenInput {
    // See the WebGPU spec for details on each of these coordinate spaces.

    // NDC has range x:[-1,1], y:[-1,1], z:[0,1]
    ndc: vec3<f32>,

    // Clip space equivalent of NDC
    clip: vec4<f32>,

    // UV has range x:[0,1], y:[0,1] with 0,0 at bottom left corner.
    uv: vec2<f32>,

    // Framebuffer coords have the same range as UV, but with 0,0 at the *top* left corner.
    frame: vec2<f32>,
}

fn fullscreen_input(i: u32) -> FullscreenInput {
    let uv = vec2(f32((i << 1u) & 2u), f32(i & 2u));
    let p = uv * 2. - 1.;
    return FullscreenInput(
        vec3(p, 0.),
        vec4(p, 0., 1.),
        uv,
        vec2(uv.x, 1. - uv.y),
    );
}

fn raymarching_view_ray(
    clip_position: vec4<f32>,
    inverse_perspective: mat4x4<f32>,
    inverse_view: mat4x4<f32>
) -> vec3<f32> {
    // https://www.derschmale.com/2014/01/26/reconstructing-positions-from-the-depth-buffer/

    // Reverse clip into eye space vector.
    var eye = inverse_perspective * clip_position;

    // Convert from homogeneous to directional coordinates: we want a non-transformed
    // rotation out of the view inversal.
    eye.w = 0.;

    // Reverse eye space into world space direction.
    let wrld = inverse_view * eye;

    return wrld.xyz;
}
