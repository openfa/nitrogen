// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use bevy_ecs::prelude::*;
use glam::{DQuat, EulerRot};
use hifitime::Duration;
use nitrous::{inject_nitrous_component, NitrousComponent};
use rapier3d_f64::na::Vector3;

/// BodyMotion is body-relative motion, not frame relative. That said
/// it is easy to decompose the body-relative motion into frame-relative
/// motion, given a Frame.
///
/// Note: this structure uses the same left handed WebGPU styled
/// coordinates as the world space frame:
///     +Z Forward
///     +X Right
///     +Y Down // <- I think this is wrong?
/// With left handed rotations.
///
/// This is a bit problematic: the standard aircraft coordinate system
/// (e.g. that used in Allerton's "Principles of Flight Simulation") is
/// entirely right handed.
///     +X Forward
///     +Z Down (-Z up)
///     +Y Right
/// With attendant right handed rotations for all the axes.
///
/// This class provides accessors ([set_]vehicle_) that provide the various
/// axes in this system, rather than in the native nitrogen system.
#[derive(NitrousComponent, Copy, Clone, Debug, Default)]
#[component(name = "motion")]
pub struct BodyMotion {
    // Linear motion
    acceleration: V3<Acceleration<Meters, Seconds>>,
    linear_velocity: V3<Velocity<Meters, Seconds>>,

    // Rotational motion
    angular_velocity: V3<AngularVelocity<Radians, Seconds>>,

    // Current motion vector, which may differ from the facing vector
    stability: DQuat,
}

#[inject_nitrous_component]
impl BodyMotion {
    pub fn new_forward<UnitLength: LengthUnit, UnitTime: TimeUnit>(
        vehicle_forward_velocity: Velocity<UnitLength, UnitTime>,
        facing: DQuat,
    ) -> Self {
        Self {
            acceleration: V3::zero(),
            linear_velocity: V3::new(
                0f64.into(),
                0f64.into(),
                meters_per_second!(vehicle_forward_velocity),
            ),
            angular_velocity: V3::zero(),
            stability: facing,
        }
    }

    #[inline]
    pub fn stability_axis(&self) -> &DQuat {
        &self.stability
    }

    pub fn rotate_vehicle_about_stability_axis(&mut self, dt: &Duration) {
        let dt = seconds!(dt.to_seconds());
        let rot = DQuat::from_euler(
            EulerRot::XYZ,
            radians!(self.angular_velocity.x() * dt).f64(),
            radians!(self.angular_velocity.y() * dt).f64(),
            radians!(self.angular_velocity.z() * dt).f64(),
        );
        self.stability *= rot;
    }

    #[inline]
    pub fn world_space_velocity(&self) -> V3<Velocity<Meters, Seconds>> {
        self.stability * self.linear_velocity
    }

    #[inline]
    pub fn set_world_space_velocity(&mut self, velocity: V3<Velocity<Meters, Seconds>>) {
        self.linear_velocity = self.stability.inverse() * velocity;
    }

    #[inline]
    pub fn apply_world_space_acceleration(
        &mut self,
        accel: V3<Acceleration<Meters, Seconds>>,
        time_interval: Time<Seconds>,
    ) {
        let mut velocity = self.world_space_velocity();
        velocity += accel * time_interval;
        self.set_world_space_velocity(velocity);
    }

    #[inline]
    pub fn cg_velocity(&self) -> Velocity<Meters, Seconds> {
        self.linear_velocity.magnitude()
    }

    // In body frame.
    #[inline]
    pub fn linear_velocity_na(&self) -> Vector3<f64> {
        (*self.linear_velocity.dvec3()).into()
    }

    #[inline]
    pub fn set_linear_velocity_na(&mut self, linvel: &Vector3<f64>) {
        self.linear_velocity = V3::new_dvec3((*linvel).into());
    }

    #[inline]
    pub fn angular_velocity_na(&self) -> Vector3<f64> {
        // TODO: what is our angle order here?
        (*self.angular_velocity.dvec3()).into()
    }

    #[inline]
    pub fn set_angular_velocity_na(&mut self, angvel: &Vector3<f64>) {
        self.angular_velocity = V3::new_dvec3((*angvel).into());
    }

    pub fn freeze(&mut self) {
        self.acceleration = V3::zero();
        self.linear_velocity = V3::zero();
        self.angular_velocity = V3::zero();
    }

    // vehicle fwd axis: X -> u, L -> p
    // This maps to +z
    pub fn vehicle_forward_acceleration(&self) -> Acceleration<Meters, Seconds> {
        self.acceleration.z()
    }

    pub fn set_vehicle_forward_acceleration(&mut self, u_dot: Acceleration<Meters, Seconds>) {
        self.acceleration.set_z(u_dot);
    }

    pub fn vehicle_forward_velocity(&self) -> Velocity<Meters, Seconds> {
        self.linear_velocity.z()
    }

    pub fn set_vehicle_forward_velocity(&mut self, u: Velocity<Meters, Seconds>) {
        self.linear_velocity.set_z(u);
    }

    pub fn vehicle_roll_velocity(&self) -> AngularVelocity<Radians, Seconds> {
        -self.angular_velocity.z()
    }

    pub fn set_vehicle_roll_velocity(&mut self, p: AngularVelocity<Radians, Seconds>) {
        self.angular_velocity.set_z(-p);
    }

    // vehicle right axis: Y -> v, M -> q
    // this maps to +x
    pub fn vehicle_sideways_acceleration(&self) -> Acceleration<Meters, Seconds> {
        self.acceleration.x()
    }

    pub fn set_vehicle_sideways_acceleration(&mut self, v_dot: Acceleration<Meters, Seconds>) {
        self.acceleration.set_x(v_dot);
    }

    pub fn vehicle_sideways_velocity(&self) -> Velocity<Meters, Seconds> {
        self.linear_velocity.x()
    }

    pub fn set_vehicle_sideways_velocity(&mut self, v: Velocity<Meters, Seconds>) {
        self.linear_velocity.set_x(v);
    }

    pub fn vehicle_pitch_velocity(&self) -> AngularVelocity<Radians, Seconds> {
        -self.angular_velocity.x()
    }

    pub fn set_vehicle_pitch_velocity(&mut self, q: AngularVelocity<Radians, Seconds>) {
        self.angular_velocity.set_x(-q);
    }

    // down  axis: Z, w, N, r
    // this maps to -y
    pub fn vehicle_vertical_acceleration(&self) -> Acceleration<Meters, Seconds> {
        -self.acceleration.y()
    }

    pub fn set_vehicle_vertical_acceleration(&mut self, w_dot: Acceleration<Meters, Seconds>) {
        self.acceleration.set_y(-w_dot);
    }

    pub fn vehicle_vertical_velocity(&self) -> Velocity<Meters, Seconds> {
        -self.linear_velocity.y()
    }

    pub fn set_vehicle_vertical_velocity(&mut self, w: Velocity<Meters, Seconds>) {
        self.linear_velocity.set_y(-w);
    }

    pub fn vehicle_yaw_velocity(&self) -> AngularVelocity<Radians, Seconds> {
        // Note: handedness for rotation is flipped, but so is the axis direction
        self.angular_velocity.y()
    }

    pub fn set_vehicle_yaw_velocity(&mut self, r: AngularVelocity<Radians, Seconds>) {
        self.angular_velocity.set_y(r);
    }
}
