// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use bevy_ecs::prelude::*;
use geometry::Aabb3;
use nitrous::{inject_nitrous_component, NitrousComponent};
use rapier3d_f64::{
    geometry::ColliderHandle,
    na::{Isometry3, Point3},
    parry::shape::{Compound, SharedShape},
};

/// Attaches collision to an entity.
/// Requires:
///     Frame - for positioning and orientation
///     Impostor - for geometry for the collision
#[derive(NitrousComponent, Debug)]
#[component(name = "collider")]
pub struct CollisionImpostor {
    // Inserted when the physics engine picks this shape up
    handle: Option<ColliderHandle>,
    shape: Compound,
}

#[inject_nitrous_component]
impl CollisionImpostor {
    pub fn new(shape: Compound) -> Self {
        Self {
            handle: None,
            shape,
        }
    }

    pub fn handle(&self) -> Option<ColliderHandle> {
        self.handle
    }

    pub fn set_handle(&mut self, handle: ColliderHandle) {
        self.handle = Some(handle);
    }

    pub fn shape(&self) -> &Compound {
        &self.shape
    }

    // pub fn intersect_ray(&self, _frame: &Frame, _ray: &Ray<Meters>) -> Option<Pt3<Meters>> {
    //     None
    // }

    // Intersect with a Ray in world space.
    // pub fn intersect_ray(&self, frame: &Frame, ray: &Ray<Meters>) -> Option<Pt3<Meters>> {
    //     // Pull the ray into model space so that we don't need to pull all
    //     // of the primitives out to world space.
    //     // TODO: this should maybe let us take advantage of f32?
    //     // ray.remove_frame(frame);
    //     let ray = Ray::new(
    //         frame.untransform_point(ray.origin()),
    //         frame.facing().inverse().mul_vec3(*ray.direction()),
    //     );
    //
    //     // Broad phase
    //     if sphere_vs_ray(&self.sphere, &ray).is_some() {
    //         // Narrow phase
    //         for part in &self.parts {
    //             //let mut pts: SmallVec<[Pt3<Meters>; 3]> = smallvec![];
    //             if let Some(pos) = match part {
    //                 CollisionPart::Sphere(sphere) => sphere_vs_ray(sphere, &ray),
    //                 CollisionPart::Box(aabb) => {
    //                     intersect_ray_aabb(&ray, aabb).maybe_entrance().cloned()
    //                 }
    //                 // CollisionPart::Capsule(cap) => ray_vs_capsule(&cap, &ray),
    //                 // CollisionPart::Runway(a) => ray_vs_plane()
    //                 _ => todo!(),
    //             } {
    //                 return Some(pos);
    //             }
    //         }
    //     }
    //
    //     None
    // }

    // pub fn intersect_trimesh(
    //     &self,
    //     _frame: &Frame,
    //     _trimesh: &TriMesh<Meters>,
    // ) -> Option<Pt3<Meters>> {
    //     for part in &self.parts {
    //         match part {
    //             CollisionPart::Sphere(_sphere) => {}
    //             _ => todo!(),
    //         }
    //     }
    //     None
    // }
}

/// Computes bounding sphere and aabb for any collision parts inserted into the collision.
///
/// Use Default to start with an empty collision volume and attach specific parts,
/// or start with one of the for_* methods to initialize with something default for
/// that kind of shape.
///
/// The collision parts are in the default frame and will get transform by Frame::facing
/// for collision detection purposes.
#[derive(Clone, Debug)]
pub struct CollisionBuilder {
    shape: Compound,
}

impl CollisionBuilder {
    /// Take the entire cubic volume of the given body Aabb
    pub fn for_building(body: &Aabb3<Meters>) -> Self {
        let (iso, cube) = body.cuboid();
        Self {
            shape: Compound::new(vec![(iso, SharedShape::new(cube))]),
        }
    }

    /// Take the bottom plane for intersection with tires and ignore the volume.
    pub fn for_runway(body: &Aabb3<Meters>) -> Self {
        Self {
            shape: Compound::new(vec![(
                Isometry3::default(),
                SharedShape::cuboid(
                    body.span(0).f64() / 2.,
                    body.span(1).f64() / 2.,
                    body.span(2).f64() / 2.,
                ),
            )]),
        }
    }

    pub fn for_airplane(body: &Aabb3<Meters>, gear_length: Length<Meters>) -> Self {
        let fuselage_radius = (-body.low(1) - gear_length).max(meters!(1));
        let wingspan = body.span(0);
        let center_to_nose = body.high(2);
        let center_to_tail = -body.low(2);
        let center_to_wing = meters!(0);
        let wing = Aabb3::from_bounds(
            Pt3::new(
                -wingspan / scalar!(2),
                center_to_wing - meters!(0.1),
                meters!(-center_to_tail / scalar!(3)),
            ),
            Pt3::new(
                wingspan / scalar!(2),
                center_to_wing + meters!(0.1),
                meters!(center_to_nose / scalar!(4)),
            ),
        );
        let fuselage_front = Point3::new(0., 0., (center_to_nose - fuselage_radius).f64());
        let fuselage_rear = Point3::new(0., 0., (-center_to_tail + fuselage_radius).f64());
        let (wing_iso, wing_cube) = wing.cuboid();
        let shape = Compound::new(vec![
            // (
            //     Isometry3::identity(),
            //     SharedShape::ball(fuselage_radius.f64()),
            // ),
            (wing_iso, SharedShape::new(wing_cube)),
            (
                Isometry3::identity(),
                SharedShape::capsule(fuselage_front, fuselage_rear, fuselage_radius.f64()),
            ),
        ]);
        Self { shape }
    }

    pub fn build(self) -> CollisionImpostor {
        // let mut radius = meters!(0);
        // let mut aabb = Aabb3::empty();
        // for part in &self.parts {
        //     match part {
        //         CollisionPart::Sphere(sphere) => {
        //             let r = sphere.center().v3().magnitude() + sphere.radius();
        //             let bb = Aabb3::from_sphere(sphere);
        //             aabb.union(&bb);
        //             radius = radius.max(r);
        //         }
        //         CollisionPart::Box(bb) => {
        //             aabb.union(bb);
        //             radius = radius.max(bb.radius());
        //         }
        //         CollisionPart::Runway(bb) => {
        //             aabb.union(bb);
        //             radius = radius.max(bb.radius());
        //         }
        //         _ => todo!(),
        //     }
        // }
        CollisionImpostor::new(self.shape)
    }
}
