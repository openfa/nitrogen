// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use bevy_ecs::prelude::*;
use geodesy::{Bearing, Geodetic, PitchCline};
use glam::{DQuat, DVec3, EulerRot};
use nitrous::{inject_nitrous_component, method, NitrousComponent};
use planck::EARTH_RADIUS;
use rapier3d_f64::parry::na::Isometry3;
use std::f64::consts::PI;

pub struct BasisVectors {
    pub forward: DVec3,
    pub right: DVec3,
    pub up: DVec3,
}

/// World space is WebGPU-style, left-handed coordinates with the
/// prime meridian (London) on the positive z-axis, the pacific on
/// the positive x-axis and the North Pole on the positive y-axis.
/// For convenience, `facing` and `motion` translate into and out
/// of the same style of local coordinate system.
///
/// The coordinates are equatorial, rather than ecliptic, but have
/// the equatorial plane as the Z-X rather than X-Y and are left-
/// handed rather than right.
#[derive(NitrousComponent, Debug, Clone)]
#[component(name = "frame")]
pub struct Frame {
    position: Pt3<Meters>,
    // Transform between our canonical coordinates with +Z as forward and the forward direction.
    facing: DQuat,
}

impl Default for Frame {
    fn default() -> Self {
        Self {
            position: Geodetic::new(radians!(0_f64), radians!(0_f64), meters!(0_f64)).pt3(),
            facing: DQuat::default(),
        }
    }
}

#[inject_nitrous_component]
impl Frame {
    pub fn identity() -> Self {
        Self {
            position: Pt3::zero(),
            facing: DQuat::default(),
        }
    }

    #[method]
    fn x(&self) -> f64 {
        self.position.x().f64()
    }

    #[method]
    fn y(&self) -> f64 {
        self.position.y().f64()
    }

    #[method]
    fn z(&self) -> f64 {
        self.position.z().f64()
    }

    pub fn new(position: Pt3<Meters>, facing: DQuat) -> Self {
        Self { position, facing }
    }

    pub fn from_geodetic_and_bearing(geodetic: Geodetic, bearing: Bearing) -> Self {
        Self::from_geodetic_bearing_and_pitch(geodetic, bearing, PitchCline::level())
    }

    pub fn from_geodetic_bearing_and_pitch(
        geodetic: Geodetic,
        bearing: Bearing,
        pitch: PitchCline,
    ) -> Self {
        let position = geodetic.pt3::<Meters>();

        // It appears we need a series of 3 rotations to get to our final alignment, annoyingly.
        // There may be a faster way to make this alignment, but it's not on a hot path and this
        // code is actually comprehensible.
        let to_geo = DQuat::from_euler(
            EulerRot::YXZ,
            -geodetic.lon::<Radians>().f64(),
            -geodetic.lat::<Radians>().f64(),
            0.,
        );
        let to_fwd = DQuat::from_euler(EulerRot::XZY, -PI / 2., PI, 0.);
        let to_bearing = DQuat::from_euler(
            EulerRot::YXZ,
            bearing.angle::<Radians>().f64(),
            // Positive is "up" in logical pitch, opposite of left-handed positive
            -pitch.angle::<Radians>().f64(),
            0.,
        );

        Self {
            position,
            facing: to_geo * to_fwd * to_bearing,
        }
    }

    pub fn position(&self) -> Pt3<Meters> {
        self.position
    }

    pub fn geodetic(&self) -> Geodetic {
        // FIXME: cache the geodetic?
        self.position.into()
    }

    pub fn set_asl<Unit: LengthUnit>(&mut self, asl: Length<Unit>) {
        let mut geo = self.geodetic();
        *geo.asl_mut() = meters!(asl);
        self.set_position(geo.pt3());
    }

    // Decompose for places that don't have Phase
    pub fn parts(&self) -> (&Pt3<Meters>, &DQuat) {
        (&self.position, &self.facing)
    }

    /// Relocate a point from "model" space to world space by applying this frame.
    pub fn transform_point(&self, pt: &Pt3<Meters>) -> Pt3<Meters> {
        self.position + self.facing * *pt
    }

    /// Relocate a point from world space to model space by unapplying this frame.
    pub fn untransform_point(&self, pt: &Pt3<Meters>) -> Pt3<Meters> {
        // Derivation from transform_point
        // world = self.position + self.facing * model
        // world - self.position = self.facing * model
        // self.facing.inverse() * (world - self.position) = model
        self.facing.inverse() * (*pt - self.position)
    }

    // FIXME: delete this whole method once we're caching the geodetic?
    pub fn altitude_asl(&self) -> Length<Meters> {
        self.position.length() - *EARTH_RADIUS
    }

    pub fn set_position(&mut self, point: Pt3<Meters>) {
        self.position = point;
    }

    pub fn basis(&self) -> BasisVectors {
        // Left-handed coordinate system
        BasisVectors {
            forward: self.facing * DVec3::Z,
            right: self.facing * DVec3::X,
            up: self.facing * DVec3::Y,
        }
    }

    pub fn set_facing(&mut self, facing: DQuat) {
        self.facing = facing;
    }

    pub fn facing(&self) -> DQuat {
        self.facing
    }

    pub fn local_to_world(&self, p: Pt3<Meters>) -> Pt3<Meters> {
        self.position + self.facing * p
    }

    // Combine this frame with another frame.
    pub fn combine(&self, other: &Frame) -> Frame {
        Frame::new(self.position + other.position, self.facing * other.facing)
    }

    pub fn from_isometry3(iso: Isometry3<f64>) -> Self {
        let (pos, facing): (DVec3, DQuat) = iso.into();
        Self {
            position: Pt3::<Meters>::new_dvec3(pos),
            facing,
        }
    }

    pub fn isometry3(&self) -> Isometry3<f64> {
        (self.position.dvec3(), self.facing).into()
    }
}

impl From<Isometry3<f64>> for Frame {
    fn from(value: Isometry3<f64>) -> Self {
        Frame::from_isometry3(value)
    }
}

impl From<&Isometry3<f64>> for Frame {
    fn from(value: &Isometry3<f64>) -> Self {
        Frame::from_isometry3(*value)
    }
}

impl From<Frame> for Isometry3<f64> {
    fn from(value: Frame) -> Self {
        value.isometry3()
    }
}

impl From<&Frame> for Isometry3<f64> {
    fn from(value: &Frame) -> Self {
        value.isometry3()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use absolute_unit::meters;

    #[test]
    fn can_create_a_frame() {
        Frame::from_geodetic_bearing_and_pitch(
            Geodetic::new(degrees!(0), degrees!(0), meters!(0)),
            Bearing::north(),
            PitchCline::level(),
        );
    }
}
