// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use anyhow::Result;
use bevy_ecs::prelude::*;
use geometry::{Aabb3, Cylinder, Sphere};
use glam::DVec3;
use nitrous::{inject_nitrous_component, NitrousComponent};
use smallvec::{smallvec, SmallVec};

pub enum InertiaPart {
    Sphere {
        geometry: Sphere<Meters>,
        // mass: Mass<Kilograms>,
    },
    Cylinder {
        geometry: Cylinder<Meters>,
        // mass: Mass<Kilograms>,
    },
    Box {
        geometry: Aabb3<Meters>,
        // mass: Mass<Kilograms>,
    },
}

/// Attaches inertia to an entity.
#[derive(NitrousComponent)]
#[component(name = "impostor")]
pub struct InertiaImpostor {
    _parts: SmallVec<[InertiaPart; 6]>,
    // aabb: Aabb3<Meters>,
    // sphere: Sphere<Meters>,
}

#[inject_nitrous_component]
impl InertiaImpostor {
    // pub fn for_collision<K: MassUnit>(collision: &CollisionImpostor, _mass: Mass<K>) -> Self {
    //     let mut parts = smallvec![];
    //
    //     // FIXME: derive from volume ratios
    //     // FIXME: also account for overlapping parts
    //     // let part_mass = mass / scalar!(collision.parts().count());
    //     for part in collision.parts() {
    //         match part {
    //             CollisionPart::Sphere(sphere) => {
    //                 parts.push(InertiaPart::Sphere {
    //                     geometry: *sphere,
    //                     // mass: Mass::<Kilograms>::from(&part_mass),
    //                 });
    //             }
    //             _ => todo!(),
    //         }
    //     }
    //
    //     Self { _parts: parts }
    // }

    pub fn for_airplane(
        _aabb: &Aabb3<Meters>,
        fuselage_radius: Length<Meters>,
        cm_to_nose: Length<Meters>,
        cm_to_tail: Length<Meters>,
        wingspan: Length<Meters>,
        cm_to_wing: Length<Meters>,
    ) -> Result<Self> {
        Ok(Self {
            // sphere: aabb.bounding_sphere(),
            // aabb: aabb.to_owned(),
            _parts: smallvec![
                InertiaPart::Cylinder {
                    geometry: Cylinder::new(
                        Pt3::zero(),
                        DVec3::Z,
                        cm_to_nose - fuselage_radius,
                        fuselage_radius
                    )
                },
                InertiaPart::Sphere {
                    geometry: Sphere::from_center_and_radius(
                        Pt3::new_dvec3(DVec3::Z * (cm_to_nose - fuselage_radius).f64()),
                        fuselage_radius
                    )
                },
                InertiaPart::Cylinder {
                    geometry: Cylinder::new(
                        Pt3::zero(),
                        -DVec3::Z,
                        cm_to_tail - fuselage_radius,
                        fuselage_radius
                    )
                },
                InertiaPart::Sphere {
                    geometry: Sphere::from_center_and_radius(
                        Pt3::new_dvec3(-DVec3::Z * (cm_to_tail - fuselage_radius).f64()),
                        fuselage_radius
                    )
                },
                InertiaPart::Box {
                    geometry: Aabb3::from_bounds(
                        Pt3::new(
                            -wingspan / scalar!(2),
                            cm_to_wing - meters!(0.1),
                            meters!(-cm_to_tail / scalar!(2)),
                        ),
                        Pt3::new(
                            wingspan / scalar!(2),
                            cm_to_wing + meters!(0.1),
                            meters!(cm_to_nose / scalar!(2))
                        )
                    )
                }
            ],
        })
    }

    // pub fn aabb(&self) -> &Aabb3<Meters> {
    //     &self.aabb
    // }
    //
    // pub fn sphere(&self) -> &Sphere<Meters> {
    //     &self.sphere
    // }
}
