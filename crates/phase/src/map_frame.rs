// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use bevy_ecs::prelude::*;
use geodesy::{Bearing, Geodetic, PitchCline};
use nitrous::{inject_nitrous_component, NitrousComponent};

/// A higher level version of Frame, oriented towards orienteering.
#[derive(NitrousComponent, Debug, Clone)]
#[component(name = "frame")]
pub struct MapFrame {
    geodetic: Geodetic,
    bearing: Bearing,
    pitch: PitchCline,
}

impl Default for MapFrame {
    fn default() -> Self {
        Self::new_level_north(Geodetic::new(
            radians!(0_f64),
            radians!(0_f64),
            meters!(0_f64),
        ))
    }
}

#[inject_nitrous_component]
impl MapFrame {
    pub fn new_level_north(geodetic: Geodetic) -> Self {
        Self::new(geodetic, Bearing::north(), PitchCline::level())
    }

    pub fn new_level(geodetic: Geodetic, bearing: Bearing) -> Self {
        Self::new(geodetic, bearing, PitchCline::level())
    }

    pub fn new(geodetic: Geodetic, bearing: Bearing, pitch: PitchCline) -> Self {
        Self {
            geodetic,
            bearing,
            pitch,
        }
    }

    pub fn geodetic(&self) -> &Geodetic {
        &self.geodetic
    }

    pub fn set_geodetic(&mut self, geodetic: Geodetic) {
        assert!(geodetic.lat::<Degrees>().is_finite());
        self.geodetic = geodetic;
    }

    pub fn geodetic_mut(&mut self) -> &mut Geodetic {
        &mut self.geodetic
    }

    pub fn bearing(&self) -> Bearing {
        self.bearing
    }

    pub fn set_bearing(&mut self, bearing: Bearing) {
        self.bearing = bearing;
    }

    pub fn bearing_mut(&mut self) -> &mut Bearing {
        &mut self.bearing
    }

    pub fn pitch(&self) -> PitchCline {
        self.pitch
    }

    pub fn set_pitch(&mut self, pitch: PitchCline) {
        self.pitch = pitch;
    }

    pub fn pitch_mut(&mut self) -> &mut PitchCline {
        &mut self.pitch
    }

    pub fn interpolate(&self, other: &Self, f: f64) -> Self {
        Self {
            geodetic: self.geodetic.interpolate(&other.geodetic, f),
            bearing: self.bearing.interpolate(&other.bearing, f),
            pitch: self.pitch.interpolate(&other.pitch, f),
        }
    }
}
