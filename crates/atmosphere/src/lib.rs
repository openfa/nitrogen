// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

// All code in this module is heavily inspired by -- and all too
// frequently directly copied from -- the most excellent:
//     https://ebruneton.github.io/precomputed_atmospheric_scattering/
// Which is:
//     Copyright (c) 2017 Eric Bruneton
// All errors and omissions below were introduced in transcription
// to Rust/Vulkan/wgpu and are not reflective of the high quality of the
// original work in any way.
mod colorspace;
mod earth_consts;
mod precompute;
mod table_helpers;

pub use crate::{
    earth_consts::AtmosphereParameters, precompute::Precompute, table_helpers::TableHelpers,
};

use anyhow::Result;
use log::trace;
use mantle::{BindingCollection, Gpu, LayoutProvider};
use nitrous::{inject_nitrous_resource, NitrousResource};
use runtime::{Extension, Runtime};
use shader_shared::layout::frag;
use std::env;

#[derive(Debug, NitrousResource)]
pub struct Atmosphere {
    atmosphere_params_buffer: wgpu::Buffer,
    transmittance_texture: (wgpu::Texture, wgpu::TextureView),
    irradiance_texture: (wgpu::Texture, wgpu::TextureView),
    scattering_texture: (wgpu::Texture, wgpu::TextureView),
    single_mie_scattering_texture: (wgpu::Texture, wgpu::TextureView),
    sampler: wgpu::Sampler,
}

impl Extension for Atmosphere {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        let atmo = Atmosphere::new(&mut runtime.resource_mut::<Gpu>())?;
        runtime.inject_resource(atmo)?;
        Ok(())
    }
}

impl LayoutProvider for Atmosphere {
    fn push_layout(layouts: &mut Vec<wgpu::BindGroupLayoutEntry>) {
        // atmosphere params
        layouts.push(frag::uniform::<AtmosphereParameters>(layouts.len()));
        // transmittance texture
        layouts.push(frag::texture(layouts.len()));
        layouts.push(frag::sampler(layouts.len()));
        // irradiance texture
        layouts.push(frag::texture(layouts.len()));
        layouts.push(frag::sampler(layouts.len()));
        // scattering texture
        layouts.push(frag::texture3d(layouts.len()));
        layouts.push(frag::sampler(layouts.len()));
        // single mie scattering texture
        layouts.push(frag::texture3d(layouts.len()));
        layouts.push(frag::sampler(layouts.len()));
    }

    fn push_binding<'a, 'b, 'c>(&'a self, mut bindings: BindingCollection<'b, 'c>)
    where
        'a: 'b,
        'a: 'c,
    {
        // atmosphere
        bindings.buffer(&self.atmosphere_params_buffer);
        bindings.texture(&self.transmittance_texture.1);
        bindings.sampler(&self.sampler);
        bindings.texture(&self.irradiance_texture.1);
        bindings.sampler(&self.sampler);
        bindings.texture(&self.scattering_texture.1);
        bindings.sampler(&self.sampler);
        bindings.texture(&self.single_mie_scattering_texture.1);
        bindings.sampler(&self.sampler);
    }
}

#[inject_nitrous_resource]
impl Atmosphere {
    pub fn new(gpu: &mut Gpu) -> Result<Self> {
        trace!("AtmosphereBuffer::new");

        let (
            atmosphere_params_buffer,
            transmittance_texture,
            irradiance_texture,
            scattering_texture,
            single_mie_scattering_texture,
        ) = if env::var("SKIP_PREBUILT_ATMOSPHERE").is_err() {
            let atmosphere_params_buffer = TableHelpers::initial_atmosphere_parameters(gpu);
            let (
                transmittance_texture,
                irradiance_texture,
                scattering_texture,
                single_mie_scattering_texture,
            ) = TableHelpers::initial_textures(gpu)?;
            (
                atmosphere_params_buffer,
                transmittance_texture,
                irradiance_texture,
                scattering_texture,
                single_mie_scattering_texture,
            )
        } else {
            let precompute = Precompute::new(gpu)?;
            precompute.precompute(gpu)?
        };

        let sampler = gpu.device().create_sampler(&wgpu::SamplerDescriptor {
            label: Some("atmosphere-sampler"),
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Linear,
            mipmap_filter: wgpu::FilterMode::Linear,
            lod_min_clamp: 0f32,
            lod_max_clamp: 9_999_999f32,
            compare: None,
            anisotropy_clamp: 1,
            border_color: None,
        });

        let view_descriptor = wgpu::TextureViewDescriptor {
            label: Some("atmosphere-texture-view"),
            format: None,
            dimension: None,
            aspect: wgpu::TextureAspect::All,
            base_mip_level: 0,
            mip_level_count: None,
            base_array_layer: 0,
            array_layer_count: None,
        };

        let transmittance_texture_view = transmittance_texture.create_view(&view_descriptor);
        let irradiance_texture_view = irradiance_texture.create_view(&view_descriptor);
        let scattering_texture_view = scattering_texture.create_view(&view_descriptor);
        let single_mie_scattering_texture_view =
            single_mie_scattering_texture.create_view(&view_descriptor);

        Ok(Self {
            atmosphere_params_buffer,
            transmittance_texture: (transmittance_texture, transmittance_texture_view),
            irradiance_texture: (irradiance_texture, irradiance_texture_view),
            scattering_texture: (scattering_texture, scattering_texture_view),
            single_mie_scattering_texture: (
                single_mie_scattering_texture,
                single_mie_scattering_texture_view,
            ),
            sampler,
        })
    }
}
