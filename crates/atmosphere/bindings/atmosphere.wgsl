// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

@group(GROUP) @binding(B0) var<uniform> atmosphere: AtmosphereParameters;
@group(GROUP) @binding(B1) var transmittance_texture: texture_2d<f32>;
@group(GROUP) @binding(B2) var transmittance_sampler: sampler;
@group(GROUP) @binding(B3) var irradiance_texture: texture_2d<f32>;
@group(GROUP) @binding(B4) var irradiance_sampler: sampler;
@group(GROUP) @binding(B5) var scattering_texture: texture_3d<f32>;
@group(GROUP) @binding(B6) var scattering_sampler: sampler;
@group(GROUP) @binding(B7) var single_mie_scattering_texture: texture_3d<f32>;
@group(GROUP) @binding(B8) var single_mie_scattering_sampler: sampler;
