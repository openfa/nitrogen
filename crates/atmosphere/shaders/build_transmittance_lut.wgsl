// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <atmosphere/include/lut_builder_global.wgsl>

fn compute_optical_length_to_top_atmosphere_boundary(
    rmu: vec2<f32>,
    profile: DensityProfile,
    bottom_radius: f32,
    top_radius: f32
) -> f32 {
    let r = rmu.x;
    let mu = rmu.y;

    // assert(r >= bottom_radius && r <= top_radius);
    // assert(mu >= -1.0 && mu <= 1.0);
    // Number of intervals for the numerical integration.
    let SAMPLE_COUNT = 500;
    // The integration step, i.e. the length of each integration interval.
    let dx = distance_to_top_atmosphere_boundary(rmu, top_radius) / f32(SAMPLE_COUNT);
    // Integration loop.
    var result = 0.0;
    for (var i = 0; i <= SAMPLE_COUNT; i++) {
        let d_i = f32(i) * dx;
        // Distance between the current sample point and the planet center.
        let r_i = sqrt(d_i * d_i + 2.0 * r * mu * d_i + r * r);
        // Number density at the current sample point (divided by the number density
        // at the bottom of the atmosphere, yielding a dimensionless number).
        let y_i = get_profile_density(profile, r_i - bottom_radius);
        // Sample weight (from the trapezoidal rule).
        var weight_i = 1.;
        if (i == 0 || i == SAMPLE_COUNT) {
            weight_i = 0.5;
        }
        result += y_i * weight_i * dx;
    }
    return result;
}

fn compute_transmittance_to_top_atmosphere_boundary(
    rmu: vec2<f32>,
    atmosphere: AtmosphereParameters,
) -> vec4<f32> {
    // assert(r >= atmosphere.bottom_radius && r <= atmosphere.top_radius);
    // assert(mu >= -1.0 && mu <= 1.0);
    let rayleigh_depth = atmosphere.rayleigh_scattering_coefficient *
        compute_optical_length_to_top_atmosphere_boundary(
            rmu,
            atmosphere.rayleigh_density,
            atmosphere.bottom_radius,
            atmosphere.top_radius
        );

    let mie_depth = atmosphere.mie_extinction_coefficient *
        compute_optical_length_to_top_atmosphere_boundary(
            rmu,
            atmosphere.mie_density,
            atmosphere.bottom_radius,
            atmosphere.top_radius
        );

    let ozone_depth = atmosphere.absorption_extinction_coefficient *
        compute_optical_length_to_top_atmosphere_boundary(
            rmu,
            atmosphere.absorption_density,
            atmosphere.bottom_radius,
            atmosphere.top_radius
        );

    return exp(-(rayleigh_depth + mie_depth + ozone_depth));
}

@group(0) @binding(0) var<uniform> atmosphere: AtmosphereParameters;
@group(0) @binding(1) var transmittance_texture: texture_storage_2d<rgba32float,write>;

@compute @workgroup_size(8, 8, 1)
fn compute_transmittance_main(@builtin(global_invocation_id) global_idx: vec3<u32>) {
    let coord = vec2<f32>(global_idx.xy) + vec2(0.5, 0.5);
    let TEXTURE_SIZE = vec2(f32(TRANSMITTANCE_TEXTURE_WIDTH), f32(TRANSMITTANCE_TEXTURE_HEIGHT));
    let uv = coord / TEXTURE_SIZE;
    let rmu = transmittance_uv_to_rmu(
        uv,
        atmosphere.bottom_radius,
        atmosphere.top_radius
    );
    let transmittance = compute_transmittance_to_top_atmosphere_boundary(rmu, atmosphere);
    textureStore(transmittance_texture, vec2<i32>(coord), transmittance);
}
