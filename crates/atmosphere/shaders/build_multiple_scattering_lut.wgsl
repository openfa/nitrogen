// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <atmosphere/include/lut_builder_global.wgsl>

fn compute_multiple_scattering(
    sc: ScatterCoord,
    atmosphere: AtmosphereParameters,
    scattering_order: u32,
    transmittance_texture: texture_2d<f32>,
    delta_scattering_density_texture: texture_3d<f32>,
) -> vec4<f32> {
    // Number of intervals for the numerical integration.
    let SAMPLE_COUNT = 50u;
    // The integration step, i.e. the length of each integration interval.
    let dx = distance_to_nearest_atmosphere_boundary(
        vec2(sc.r, sc.mu),
        atmosphere.bottom_radius,
        atmosphere.top_radius,
        sc.intersects_ground
    ) / f32(SAMPLE_COUNT);
    // Integration loop.
    var rayleigh_mie_sum = vec4(0.0);
    for (var i = 0u; i <= SAMPLE_COUNT; i++) {
        let d_i = f32(i) * dx;

        // The r, mu and mu_s parameters at the current integration point (see the
        // single scattering section for a detailed explanation).
        let r_i = clamp_radius(
            sqrt(d_i * d_i + 2.0 * sc.r * sc.mu * d_i + sc.r * sc.r),
            atmosphere.bottom_radius, atmosphere.top_radius
        );
        let mu_i = clamp_cosine((sc.r * sc.mu + d_i) / r_i);
        let mu_s_i = clamp_cosine((sc.r * sc.mu_s + d_i * sc.nu) / r_i);

        // The Rayleigh and Mie multiple scattering at the current sample point.
        let rayleigh_mie_i = get_scattering(
                delta_scattering_density_texture,
                ScatterCoord(r_i, mu_i, mu_s_i, sc.nu, sc.intersects_ground),
                atmosphere.bottom_radius,
                atmosphere.top_radius,
                atmosphere.mu_s_min,
            ) * get_transmittance(
                transmittance_texture,
                sc.r,
                sc.mu,
                d_i,
                sc.intersects_ground,
                atmosphere.bottom_radius,
                atmosphere.top_radius
            ) * dx;

        // Sample weight (from the trapezoidal rule).
        var weight_i = 1.;
        if (i == 0u || i == SAMPLE_COUNT) {
            weight_i = 0.5;
        }
        rayleigh_mie_sum += rayleigh_mie_i * weight_i;
    }
    return rayleigh_mie_sum;
}

@group(0) @binding(0) var<uniform> atmosphere: AtmosphereParameters;
@group(0) @binding(1) var<uniform> rad_to_lum: mat4x4<f32>; // FIXME: remove
@group(0) @binding(2) var<uniform> scattering_order: u32;
@group(0) @binding(3) var transmittance_texture: texture_2d<f32>;
@group(0) @binding(4) var delta_scattering_density_texture: texture_3d<f32>;
@group(0) @binding(5) var delta_multiple_scattering_texture: texture_storage_3d<rgba32float,write>;
@group(0) @binding(6) var scattering_texture: texture_storage_3d<rgba32float,read_write>;

@compute @workgroup_size(8, 8, 8)
fn compute_multiple_scattering_main(@builtin(global_invocation_id) global_idx: vec3<u32>) {
    let frag_coord = vec3<f32>(global_idx.xyz) + vec3(0.5, 0.5, 0.5);
    let sc = scattering_frag_coord_to_rmumusnu(frag_coord, atmosphere);

    let delta_multiple_scattering = compute_multiple_scattering(
        sc,
        atmosphere,
        scattering_order,
        transmittance_texture,
        delta_scattering_density_texture,
    );
    textureStore(
        delta_multiple_scattering_texture,
        vec3<i32>(frag_coord), // FIXME: should these all be global_idx?
        delta_multiple_scattering
    );

    let scattering = vec4(
          (rad_to_lum * delta_multiple_scattering).xyz / rayleigh_phase_function(sc.nu),
          0.0);
    let prior_scattering = textureLoad(scattering_texture, vec3<i32>(global_idx.xyz));
    textureStore(scattering_texture, vec3<i32>(global_idx.xyz), prior_scattering + scattering);
}
