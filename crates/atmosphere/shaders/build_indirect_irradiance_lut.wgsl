// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <atmosphere/include/lut_builder_global.wgsl>

// For the indirect ground irradiance the integral over the hemisphere must be
// computed numerically. More precisely we need to compute the integral over all
// the directions $\bw$ of the hemisphere, of the product of:
//   * the radiance arriving from direction $\bw$ after $n$ bounces,
//   * the cosine factor, i.e. $\omega_z$
//     This leads to the following implementation (where
//     `multiple_scattering_texture` is supposed to contain the $n$-th
//     order of scattering, if $n>1$, and `scattering_order` is equal to
//     $n$):
fn compute_indirect_irradiance(
    rmus: vec2<f32>,
    scattering_order: u32,
    atmosphere: AtmosphereParameters,
    delta_rayleigh_scattering_texture: texture_3d<f32>,
    delta_mie_scattering_texture: texture_3d<f32>,
    delta_multiple_scattering_texture: texture_3d<f32>
) -> vec4<f32> {
    let SAMPLE_COUNT = 32u;
    let dphi = PI / f32(SAMPLE_COUNT);
    let dtheta = PI / f32(SAMPLE_COUNT);

    var result = vec4(0.0);
    let omega_s = vec3(sqrt(1.0 - rmus.y * rmus.y), 0.0, rmus.y);

    for (var j = 0u; j < SAMPLE_COUNT / 2u; j++) {
        let theta = (f32(j) + 0.5) * dtheta;
        for (var i = 0u; i < 2u * SAMPLE_COUNT; i++) {
            let phi = (f32(i) + 0.5) * dphi;
            let omega = vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta));
            let domega = dtheta * dphi * sin(theta);

            let nu = dot(omega, omega_s);
            result += get_best_scattering(
                delta_rayleigh_scattering_texture,
                delta_mie_scattering_texture,
                delta_multiple_scattering_texture,
                ScatterCoord(rmus.x, omega.z, rmus.y, nu, false),
                atmosphere.bottom_radius,
                atmosphere.top_radius,
                atmosphere.mu_s_min,
                atmosphere.mie_phase_function_g,
                scattering_order
            ) * omega.z * domega;
        }
    }
    return result;
}

@group(0) @binding(0) var<uniform> atmosphere: AtmosphereParameters;
@group(0) @binding(1) var<uniform> rad_to_lum: mat4x4<f32>; // FIXME: remove
@group(0) @binding(2) var<uniform> scattering_order: u32;
@group(0) @binding(3) var delta_rayleigh_scattering_texture: texture_3d<f32>;
@group(0) @binding(4) var delta_mie_scattering_texture: texture_3d<f32>;
@group(0) @binding(5) var delta_multiple_scattering_texture: texture_3d<f32>;
@group(0) @binding(6) var delta_irradiance_texture: texture_storage_2d<rgba32float,write>;
@group(0) @binding(7) var irradiance_texture: texture_storage_2d<rgba32float,read_write>;

@compute @workgroup_size(8, 8, 1)
fn compute_indirect_irradiance_main(@builtin(global_invocation_id) global_idx: vec3<u32>) {
    let frag_coord = vec2<f32>(global_idx.xy) + vec2(0.5, 0.5);
    let TEXTURE_SIZE = vec2(f32(IRRADIANCE_TEXTURE_WIDTH), f32(IRRADIANCE_TEXTURE_HEIGHT));
    let uv = frag_coord / TEXTURE_SIZE;
    let rmus = irradiance_uv_to_rmus(
        uv,
        atmosphere.bottom_radius,
        atmosphere.top_radius
    );
    let indirect_irradiance = compute_indirect_irradiance(
        rmus,
        scattering_order,
        atmosphere,
        delta_rayleigh_scattering_texture,
        delta_mie_scattering_texture,
        delta_multiple_scattering_texture
    );
    textureStore(
        delta_irradiance_texture,
        vec2<i32>(frag_coord),
        indirect_irradiance
    );

    // FIXME: this should all be vec4... why are we subbing in a 1 here?
    let prior_irradiance = textureLoad(irradiance_texture, vec2<i32>(global_idx.xy)).rgb;
    textureStore(
        irradiance_texture,
        vec2<i32>(global_idx.xy),
        vec4(prior_irradiance + (rad_to_lum * indirect_irradiance).xyz, 1.0)
    );
}
