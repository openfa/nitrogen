// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <atmosphere/include/lut_builder_global.wgsl>

fn compute_scattering_density(
    sc: ScatterCoord,
    atmosphere: AtmosphereParameters,
    scattering_order: u32,
    transmittance_texture: texture_2d<f32>,
    delta_multiple_scattering_texture: texture_3d<f32>,
    delta_irradiance_texture: texture_2d<f32>,
) -> vec4<f32> {
    // Compute unit direction vectors for the zenith, the view direction omega and
    // and the sun direction omega_s, such that the cosine of the view-zenith
    // angle is mu, the cosine of the sun-zenith angle is mu_s, and the cosine of
    // the view-sun angle is nu. The goal is to simplify computations below.
    let zenith_direction = vec3(0.0, 0.0, 1.0);
    let omega = vec3(sqrt(1.0 - sc.mu * sc.mu), 0.0, sc.mu);
    var sun_dir_x = 0.;
    if (omega.x != 0.0) {
        sun_dir_x = (sc.nu - sc.mu * sc.mu_s) / omega.x;
    }
    let sun_dir_y = sqrt(max(1.0 - sun_dir_x * sun_dir_x - sc.mu_s * sc.mu_s, 0.0));
    let omega_s = vec3(sun_dir_x, sun_dir_y, sc.mu_s);

    let SAMPLE_COUNT = 16u;
    let dphi = PI / f32(SAMPLE_COUNT);
    let dtheta = PI / f32(SAMPLE_COUNT);
    var rayleigh_mie = vec4(0.0);

    // Nested loops for the integral over all the incident directions omega_i.
    for (var l = 0u; l < SAMPLE_COUNT; l++) {
        let theta = (f32(l) + 0.5) * dtheta;
        let cos_theta = cos(theta);
        let sin_theta = sin(theta);
        let ray_r_theta_intersects_ground = ray_intersects_ground(vec2(sc.r, cos_theta), atmosphere.bottom_radius);

        // The distance and transmittance to the ground only depend on theta, so we
        // can compute them in the outer loop for efficiency.
        var distance_to_ground = 0.0;
        var transmittance_to_ground = vec4(0.0);
        var ground_albedo = vec4(0.0);
        if (ray_r_theta_intersects_ground) {
            distance_to_ground = distance_to_bottom_atmosphere_boundary(vec2(sc.r, cos_theta), atmosphere.bottom_radius);
            transmittance_to_ground = get_transmittance(
                transmittance_texture,
                sc.r,
                cos_theta,
                distance_to_ground,
                true, // ray_intersects_ground
                atmosphere.bottom_radius,
                atmosphere.top_radius
            );
            ground_albedo = atmosphere.ground_albedo;
        }

        for (var m = 0u; m < 2u * SAMPLE_COUNT; m++) {
            let phi = (f32(m) + 0.5) * dphi;
            let omega_i = vec3(cos(phi) * sin_theta, sin(phi) * sin_theta, cos_theta);
            let domega_i = dtheta * dphi * sin(theta);

            // The radiance L_i arriving from direction omega_i after n-1 bounces is
            // the sum of a term given by the precomputed scattering texture for the
            // (n-1)-th order:
            let nu1 = dot(omega_s, omega_i);
            var incident_radiance = get_scattering(
                delta_multiple_scattering_texture,
                ScatterCoord(sc.r, omega_i.z, sc.mu_s, nu1, ray_r_theta_intersects_ground),
                atmosphere.bottom_radius,
                atmosphere.top_radius,
                atmosphere.mu_s_min
            );

            // and of the contribution from the light paths with n-1 bounces and whose
            // last bounce is on the ground. This contribution is the product of the
            // transmittance to the ground, the ground albedo, the ground BRDF, and
            // the irradiance received on the ground after n-2 bounces.
            let ground_normal = normalize(zenith_direction * sc.r + omega_i * distance_to_ground);
            let ground_irradiance = get_irradiance(
                delta_irradiance_texture,
                atmosphere.bottom_radius,
                dot(ground_normal, omega_s),
                atmosphere.bottom_radius,
                atmosphere.top_radius
            );
            incident_radiance += transmittance_to_ground * ground_albedo *
                (1.0 / PI) * ground_irradiance;

            // The radiance finally scattered from direction omega_i towards direction
            // -omega is the product of the incident radiance, the scattering
            // coefficient, and the phase function for directions omega and omega_i
            // (all this summed over all particle types, i.e. Rayleigh and Mie).
            let nu2 = dot(omega, omega_i);
            let rayleigh_density = get_profile_density(
                atmosphere.rayleigh_density,
                sc.r - atmosphere.bottom_radius
            );
            let mie_density = get_profile_density(
                atmosphere.mie_density,
                sc.r - atmosphere.bottom_radius
            );
            rayleigh_mie += incident_radiance * (
                atmosphere.rayleigh_scattering_coefficient * rayleigh_density * rayleigh_phase_function(nu2) +
                atmosphere.mie_scattering_coefficient * mie_density * mie_phase_function(atmosphere.mie_phase_function_g, nu2)
            ) * domega_i;
        }
    }

    return rayleigh_mie;
}

@group(0) @binding(0) var<uniform> atmosphere: AtmosphereParameters;
@group(0) @binding(1) var<uniform> scattering_order: u32;
@group(0) @binding(2) var transmittance_texture: texture_2d<f32>;
@group(0) @binding(3) var delta_rayleigh_scattering_texture: texture_3d<f32>; // FIXME: why is this unused?!?
@group(0) @binding(4) var delta_mie_scattering_texture: texture_3d<f32>; // FIXME: why is this unused?!?
@group(0) @binding(5) var delta_multiple_scattering_texture: texture_3d<f32>;
@group(0) @binding(6) var delta_irradiance_texture: texture_2d<f32>;
@group(0) @binding(7) var delta_scattering_density_texture: texture_storage_3d<rgba32float,write>;

@compute @workgroup_size(8, 8, 8)
fn compute_scattering_density_main(@builtin(global_invocation_id) global_idx: vec3<u32>) {
    let frag_coord = vec3<f32>(global_idx.xyz) + vec3(0.5, 0.5, 0.5);

    let sc = scattering_frag_coord_to_rmumusnu(frag_coord, atmosphere);
    let rayleigh_mie = compute_scattering_density(
        sc,
        atmosphere,
        scattering_order,
        transmittance_texture,
        delta_multiple_scattering_texture,
        delta_irradiance_texture,
    );

    textureStore(
        delta_scattering_density_texture,
        vec3<i32>(frag_coord),
        rayleigh_mie
    );
}
