// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <atmosphere/include/lut_builder_global.wgsl>

fn compute_direct_irradiance(
    atmosphere: AtmosphereParameters,
    rmus: vec2<f32>,
    transmittance_texture: texture_2d<f32>,
) -> vec4<f32> {
    let r = rmus.x;
    let mu_s = rmus.y;

    let alpha_s = atmosphere.sun_angular_radius;
    // Approximate average of the cosine factor mu_s over the visible fraction of
    // the Sun disc.
    var average_cosine_factor = 0.;
    if (mu_s < -alpha_s) {
        average_cosine_factor = 0.;
    } else if (mu_s > alpha_s) {
        average_cosine_factor = mu_s;
    } else {
        average_cosine_factor = (mu_s + alpha_s) * (mu_s + alpha_s) / (4.0 * alpha_s);
    }

    let transmittance = get_transmittance_to_top_atmosphere_boundary(
        rmus,
        transmittance_texture,
        atmosphere.bottom_radius,
        atmosphere.top_radius
    );
    return atmosphere.sun_irradiance * transmittance * average_cosine_factor;
}

@group(0) @binding(0) var<uniform> atmosphere: AtmosphereParameters;
@group(0) @binding(1) var transmittance_texture: texture_2d<f32>;
@group(0) @binding(2) var delta_irradiance_texture: texture_storage_2d<rgba32float,write>;

@compute @workgroup_size(8, 8, 1)
fn compute_direct_irradiance_main(@builtin(global_invocation_id) global_idx: vec3<u32>) {
    let coord = vec2<f32>(global_idx.xy) + vec2(0.5, 0.5);
    let TEXTURE_SIZE = vec2(f32(IRRADIANCE_TEXTURE_WIDTH), f32(IRRADIANCE_TEXTURE_HEIGHT));
    let uv = coord / TEXTURE_SIZE;
    let rmus = irradiance_uv_to_rmus(uv, atmosphere.bottom_radius, atmosphere.top_radius);
    let direct_irradiance = compute_direct_irradiance(atmosphere, rmus, transmittance_texture);
    textureStore(delta_irradiance_texture, vec2<i32>(coord), direct_irradiance);
}
