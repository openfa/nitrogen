// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <atmosphere/include/lut_builder_global.wgsl>

struct SingleScatteringResult {
    rayleigh: vec4<f32>,
    mie: vec4<f32>,
}

fn compute_single_scattering_integrand(
    atmosphere: AtmosphereParameters,
    transmittance_texture: texture_2d<f32>,
    coord: ScatterCoord,
    d: f32,
) -> SingleScatteringResult {
    let altitude = sqrt(d * d + 2.0 * coord.r * coord.mu * d + coord.r * coord.r);
    let r_d = clamp_radius(altitude, atmosphere.bottom_radius, atmosphere.top_radius);
    let mu_s_d = clamp_cosine((coord.r * coord.mu_s + d * coord.nu) / r_d);
    let base_transmittance = get_transmittance(
        transmittance_texture,
        coord.r, coord.mu,
        d,
        coord.intersects_ground,
        atmosphere.bottom_radius,
        atmosphere.top_radius
    );
    let transmittance_to_sun = get_transmittance_to_sun(
        transmittance_texture,
        r_d,
        mu_s_d,
        atmosphere.bottom_radius,
        atmosphere.top_radius,
        atmosphere.sun_angular_radius
    );
    let transmittance = base_transmittance * transmittance_to_sun;
    let rayleigh = transmittance * get_profile_density(atmosphere.rayleigh_density, r_d - atmosphere.bottom_radius);
    let mie = transmittance * get_profile_density(atmosphere.mie_density, r_d - atmosphere.bottom_radius);
    return SingleScatteringResult(rayleigh, mie);
}

fn compute_single_scattering(
    atmosphere: AtmosphereParameters,
    transmittance_texture: texture_2d<f32>,
    coord: ScatterCoord,
) -> SingleScatteringResult {
    // assert(coord.r >= atmosphere.bottom_radius && coord.r <= atmosphere.top_radius);
    // assert(coord.mu >= -1.0 && coord.mu <= 1.0);
    // assert(coord.mu_s >= -1.0 && coord.mu_s <= 1.0);
    // assert(coord.nu >= -1.0 && coord.nu <= 1.0);

    // Number of intervals for the numerical integration.
    let SAMPLE_COUNT = 50u;
    // The integration step, i.e. the length of each integration interval.
    let path_length = distance_to_nearest_atmosphere_boundary(
        vec2(coord.r, coord.mu),
        atmosphere.bottom_radius,
        atmosphere.top_radius,
        coord.intersects_ground
    );
    let dx =  path_length / f32(SAMPLE_COUNT);
    // Integration loop.
    var rayleigh_sum = vec4(0.0);
    var mie_sum = vec4(0.0);
    for (var i = 0u; i <= SAMPLE_COUNT; i++) {
        let d_i = f32(i) * dx;
        // The Rayleigh and Mie single scattering at the current sample point.
        let delta_i = compute_single_scattering_integrand(
            atmosphere,
            transmittance_texture,
            coord,
            d_i,
        );
        // Sample weight (from the trapezoidal rule).
        var weight_i = 1.;
        if (i == 0u || i == SAMPLE_COUNT) {
            weight_i = 0.5;
        }
        rayleigh_sum += delta_i.rayleigh * weight_i;
        mie_sum += delta_i.mie * weight_i;
    }
    let rayleigh = rayleigh_sum * dx * atmosphere.sun_irradiance * atmosphere.rayleigh_scattering_coefficient;
    let mie = mie_sum * dx * atmosphere.sun_irradiance * atmosphere.mie_scattering_coefficient;
    return SingleScatteringResult(rayleigh, mie);
}

@group(0) @binding(0) var<uniform> atmosphere: AtmosphereParameters;
@group(0) @binding(1) var transmittance_texture: texture_2d<f32>;
@group(0) @binding(2) var<uniform> rad_to_lum: mat4x4<f32>; // FIXME: remove
@group(0) @binding(3) var delta_rayleigh_scattering_texture: texture_storage_3d<rgba32float,write>;
@group(0) @binding(4) var delta_mie_scattering_texture: texture_storage_3d<rgba32float,write>;
@group(0) @binding(5) var scattering_texture: texture_storage_3d<rgba32float,read_write>;
@group(0) @binding(6) var single_mie_scattering_texture: texture_storage_3d<rgba32float,read_write>;

@compute @workgroup_size(8, 8, 8)
fn compute_single_scattering_main(@builtin(global_invocation_id) global_idx: vec3<u32>) {
    let sample_coord = vec3<f32>(global_idx) + vec3(0.5);
    let scatter_coord = scattering_frag_coord_to_rmumusnu(sample_coord, atmosphere);

    let delta = compute_single_scattering(atmosphere, transmittance_texture, scatter_coord);

    let frag_coord = vec3<i32>(sample_coord);
    textureStore(
        delta_rayleigh_scattering_texture,
        frag_coord,
        delta.rayleigh
    );
    textureStore(
        delta_mie_scattering_texture,
        frag_coord,
        delta.mie
    );

    let scattering = (rad_to_lum * delta.rayleigh).xyz;
    let single_mie_scattering = (rad_to_lum * delta.mie).xyz;

    let img_coord = vec3<i32>(global_idx.xyz);

    let prior_scattering = textureLoad(scattering_texture, img_coord).rgb;
    let next_scattering = vec4(prior_scattering + scattering, 1.0);
    textureStore(scattering_texture, img_coord, next_scattering);

    let prior_single_mie_scattering = textureLoad(single_mie_scattering_texture, img_coord).rgb;
    let next_single_mie_scattering = vec4(prior_single_mie_scattering + single_mie_scattering, 1.0);
    textureStore(single_mie_scattering_texture, img_coord, next_single_mie_scattering);
}
