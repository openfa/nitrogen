// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <shader_shared/include/consts.wgsl>
//!include <atmosphere/include/global.wgsl>

fn get_layer_density(layer: DensityProfileLayer, altitude: f32) -> f32 {
    let density = layer.exp_term * exp(layer.exp_scale * altitude) +
        layer.linear_term * altitude + layer.constant_term;
    return clamp(density, 0.0, 1.0);
}

fn get_profile_density(profile: DensityProfile, altitude: f32) -> f32 {
    if (altitude < profile.layer0.width) {
        return get_layer_density(profile.layer0, altitude);
    } else {
        return get_layer_density(profile.layer1, altitude);
    }
}

fn scattering_uvwz_to_rmumusnu(
    uvwz: vec4<f32>,
    mu_s_min: f32,
    bottom_radius: f32,
    top_radius: f32,
) -> ScatterCoord {
    //assert(uvwz.x >= 0.0 && uvwz.x <= 1.0);
    //assert(uvwz.y >= 0.0 && uvwz.y <= 1.0);
    //assert(uvwz.z >= 0.0 && uvwz.z <= 1.0);
    //assert(uvwz.w >= 0.0 && uvwz.w <= 1.0);

    // Distance to top atmosphere boundary for a horizontal ray at ground level.
    let H = sqrt(top_radius * top_radius - bottom_radius * bottom_radius);
    // Distance to the horizon.
    let rho = H * get_unit_range_from_texture_coord(uvwz.w, SCATTERING_TEXTURE_R_SIZE);
    let r = sqrt(rho * rho + bottom_radius * bottom_radius);

    var mu = 0.;
    var ray_r_mu_intersects_ground = false;
    if (uvwz.z < 0.5) {
        // Distance to the ground for the ray (r,mu), and its minimum and maximum
        // values over all mu - obtained for (r,-1) and (r,mu_horizon) - from which
        // we can recover mu:
        let d_min = r - bottom_radius;
        let d_max = rho;
        let d = d_min + (d_max - d_min) * get_unit_range_from_texture_coord(
            1.0 - 2.0 * uvwz.z, SCATTERING_TEXTURE_MU_SIZE / 2);
        if (d == 0.) {
            mu = -1.;
        } else {
            mu = clamp_cosine(-(rho * rho + d * d) / (2.0 * r * d));
        }
        ray_r_mu_intersects_ground = true;
    } else {
        // Distance to the top atmosphere boundary for the ray (r,mu), and its
        // minimum and maximum values over all mu - obtained for (r,1) and
        // (r,mu_horizon) - from which we can recover mu:
        let d_min = top_radius - r;
        let d_max = rho + H;
        let d = d_min + (d_max - d_min) * get_unit_range_from_texture_coord(
            2.0 * uvwz.z - 1.0, SCATTERING_TEXTURE_MU_SIZE / 2);
        if (d == 0.) {
            mu = 1.;
        } else {
            mu = clamp_cosine((H * H - rho * rho - d * d) / (2.0 * r * d));
        }
        ray_r_mu_intersects_ground = false;
    }

    let x_mu_s = get_unit_range_from_texture_coord(uvwz.y, SCATTERING_TEXTURE_MU_S_SIZE);
    let d_min = top_radius - bottom_radius;
    let d_max = H;
    let A = -2.0 * mu_s_min * bottom_radius / (d_max - d_min);
    let a = (A - x_mu_s * A) / (1.0 + x_mu_s * A);
    let d = d_min + min(a, A) * (d_max - d_min);
    var mu_s = 1.;
    if (d == 0.0) {
        mu_s = 1.;
    } else {
        mu_s = clamp_cosine((H * H - d * d) / (2.0 * bottom_radius * d));
    }

    let nu = clamp_cosine(uvwz.x * 2.0 - 1.0);

    return ScatterCoord(r, mu, mu_s, nu, ray_r_mu_intersects_ground);
}

fn scattering_frag_coord_to_uvwz(frag_coord: vec3<f32>) -> vec4<f32> {
    let SCATTERING_TEXTURE_SIZE = vec4<f32>(
        f32(SCATTERING_TEXTURE_NU_SIZE) - 1.,
        f32(SCATTERING_TEXTURE_MU_S_SIZE),
        f32(SCATTERING_TEXTURE_MU_SIZE),
        f32(SCATTERING_TEXTURE_R_SIZE)
    );
    let frag_coord_nu = floor(frag_coord.x / f32(SCATTERING_TEXTURE_MU_S_SIZE));
    let frag_coord_mu_s = frag_coord.x % f32(SCATTERING_TEXTURE_MU_S_SIZE);
    let frag4 = vec4(frag_coord_nu, frag_coord_mu_s, frag_coord.y, frag_coord.z);
    return frag4 / SCATTERING_TEXTURE_SIZE;
}

fn scattering_frag_coord_to_rmumusnu(
    frag_coord: vec3<f32>,
    atmosphere: AtmosphereParameters,
) -> ScatterCoord {
    let uvwz = scattering_frag_coord_to_uvwz(frag_coord);
    var coord = scattering_uvwz_to_rmumusnu(
        uvwz,
        atmosphere.mu_s_min,
        atmosphere.bottom_radius,
        atmosphere.top_radius,
    );

    // Clamp nu to its valid range of values, given mu and mu_s.
    let mu = coord.mu;
    let mu_s = coord.mu_s;
    let min_nu = mu * mu_s - sqrt((1.0 - mu * mu) * (1.0 - mu_s * mu_s));
    let max_nu = mu * mu_s + sqrt((1.0 - mu * mu) * (1.0 - mu_s * mu_s));
    coord.nu = clamp(coord.nu, min_nu, max_nu);

    return coord;
}

fn get_best_scattering(
    delta_rayleigh_scattering_texture: texture_3d<f32>,
    delta_mie_scattering_texture: texture_3d<f32>,
    delta_multiple_scattering_texture: texture_3d<f32>,
    sc: ScatterCoord,
    atmosphere_bottom_radius: f32,
    atmosphere_top_radius: f32,
    atmosphere_mu_s_min: f32,
    atmosphere_mie_phase_function_g: f32,
    scattering_order: u32
) -> vec4<f32> {
    if (scattering_order == 1u) {
        let rayleigh = get_scattering(
            delta_rayleigh_scattering_texture,
            sc,
            atmosphere_bottom_radius,
            atmosphere_top_radius,
            atmosphere_mu_s_min,
        );
        let mie = get_scattering(
            delta_mie_scattering_texture,
            sc,
            atmosphere_bottom_radius,
            atmosphere_top_radius,
            atmosphere_mu_s_min,
        );
        return rayleigh * rayleigh_phase_function(sc.nu) +
            mie * mie_phase_function(atmosphere_mie_phase_function_g, sc.nu);
    } else {
        return get_scattering(
            delta_multiple_scattering_texture,
            sc,
            atmosphere_bottom_radius,
            atmosphere_top_radius,
            atmosphere_mu_s_min,
        );
    }
}
