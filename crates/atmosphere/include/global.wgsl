// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

// The conversion factor between watts and lumens.
var<private> MAX_LUMINOUS_EFFICACY: f32 = 683.0;

//var<private> SKY_SPECTRAL_RADIANCE_TO_LUMINANCE: vec3<f32> = vec3(
//    MAX_LUMINOUS_EFFICACY,
//    MAX_LUMINOUS_EFFICACY,
//    MAX_LUMINOUS_EFFICACY
//);

//var<private> SUN_SPECTRAL_RADIANCE_TO_LUMINANCE: vec3<f32> = vec3(
//    MAX_LUMINOUS_EFFICACY,
//    MAX_LUMINOUS_EFFICACY,
//    MAX_LUMINOUS_EFFICACY
//);

var<private> TRANSMITTANCE_TEXTURE_WIDTH: i32 = 256;
var<private> TRANSMITTANCE_TEXTURE_HEIGHT: i32 = 64;

var<private> IRRADIANCE_TEXTURE_WIDTH: i32 = 64;
var<private> IRRADIANCE_TEXTURE_HEIGHT: i32 = 16;

var<private> SCATTERING_TEXTURE_R_SIZE: i32 = 32;
var<private> SCATTERING_TEXTURE_MU_SIZE: i32 = 128;
var<private> SCATTERING_TEXTURE_MU_S_SIZE: i32 = 32;
var<private> SCATTERING_TEXTURE_NU_SIZE: i32 = 8;

var<private> SCATTERING_TEXTURE_WIDTH: i32 = 256; // SCATTERING_TEXTURE_NU_SIZE * SCATTERING_TEXTURE_MU_S_SIZE;
var<private> SCATTERING_TEXTURE_HEIGHT: i32 = 128; // SCATTERING_TEXTURE_MU_SIZE;
var<private> SCATTERING_TEXTURE_DEPTH: i32 = 32; // SCATTERING_TEXTURE_R_SIZE;

struct DensityProfileLayer {
    // Height of this layer, except for the last layer which always
    // extends to the top of the atmosphere region.
    width: f32, // meters

    // Density in this layer in [0,1) as defined by the following function:
    //   'exp_term' * exp('exp_scale' * h) + 'linear_term' * h + 'constant_term',
    exp_term: f32,
    exp_scale: f32, // 1 / meters
    linear_term: f32, // 1 / meters
    constant_term: f32,

    pad0: f32,
    pad1: f32,
    pad2: f32,
};

// From low to high.
struct DensityProfile {
    // Note: Arrays are busted in shaderc right now.
    layer0: DensityProfileLayer,
    layer1: DensityProfileLayer,
};

struct AtmosphereParameters {
    // The density profile of tiny air molecules.
    rayleigh_density: DensityProfile,

    // The density profile of aerosols.
    mie_density: DensityProfile,

    // The density profile of O3.
    absorption_density: DensityProfile,

    // Per component, at max density.
    rayleigh_scattering_coefficient: vec4<f32>,

    // Per component, at max density.
    mie_scattering_coefficient: vec4<f32>,

    // Per component, at max density.
    mie_extinction_coefficient: vec4<f32>,

    // Per component, at max density.
    absorption_extinction_coefficient: vec4<f32>,

    // Energy received into the system from the nearby star.
    sun_irradiance: vec4<f32>,

    // The average albedo of the ground, per component.
    ground_albedo: vec4<f32>,

    // The whitepoint, given the relative contributions of all possible wavelengths.
    whitepoint: vec4<f32>,

    // Conversion between the solar irradiance above and our desired sRGB luminance output.
    sun_spectral_radiance_to_luminance: vec3<f32>,

    // Conversion between the irradiance stored in our LUT and sRGB luminance outputs.
    // Note that this is where we re-add the luminous efficacy constant that we factored
    // out of the precomputations to keep the numbers closer to 0 for precision.
    sky_spectral_radiance_to_luminance: vec3<f32>,

    // From center to subocean.
    bottom_radius: f32, // meters

    // from center to top of simulated atmosphere.
    top_radius: f32, // meters

    // The size of the nearby star in radians.
    sun_angular_radius: f32, // radians

    // The asymmetry parameter for the Cornette-Shanks phase function for the
    // aerosols.
    mie_phase_function_g: f32,

    // The cosine of the maximum Sun zenith angle for which atmospheric scattering
    // must be precomputed (for maximum precision, use the smallest Sun zenith
    // angle yielding negligible sky light radiance values. For instance, for the
    // Earth case, 102 degrees is a good choice - yielding mu_s_min = -0.2).
    mu_s_min: f32,
};

fn texSample2(texture: texture_2d<f32>, uv: vec2<f32>, size: vec2<i32>) -> vec4<f32> {
    let px_f = uv * vec2<f32>(size);
    let f = fract(px_f - vec2(0.5));
    let px = vec2<i32>(floor(px_f - vec2(0.5)));
    var i0 = px.x; // pixel grid
    var i1 = px.x + 1;
    var j0 = px.y;
    var j1 = px.y + 1;
    if (i0 < 0 || j0 < 0 || i1 >= size.x || j1 >= size.y) {
        return textureLoad(texture, vec2<i32>(uv * vec2<f32>(size)), 0);
    } else {
        let s00 = textureLoad(texture, vec2(i0, j0), 0);
        let s01 = textureLoad(texture, vec2(i0, j1), 0);
        let s10 = textureLoad(texture, vec2(i1, j0), 0);
        let s11 = textureLoad(texture, vec2(i1, j1), 0);
        let fp = 1. - f;
        let t00 = fp.x * fp.y * s00;
        let t10 =  f.x * fp.y * s10;
        let t01 = fp.x *  f.y * s01;
        let t11 =  f.x *  f.y * s11;
        return t00 + t10 + t01 + t11;
    }
}

fn clamp_radius(
    r: f32,
    bottom_radius: f32,
    top_radius: f32
) -> f32 {
    return clamp(r, bottom_radius, top_radius);
}

fn clamp_cosine(mu: f32) -> f32 {
    return clamp(mu, -1.0, 1.0);
}

fn clamp_distance(d: f32) -> f32 {
    return max(d, 0.0);
}

fn safe_sqrt(a: f32) -> f32 {
    return sqrt(max(a, 0.0));
}

fn get_texture_coord_from_unit_range(x: f32, texture_size: i32) -> f32 {
    return 0.5 / f32(texture_size) + x * (1.0 - 1.0 / f32(texture_size));
}

fn get_unit_range_from_texture_coord(u: f32, texture_size: i32) -> f32 {
    return (u - 0.5 / f32(texture_size)) / (1.0 - (1.0 / f32(texture_size)));
}

fn ray_intersects_ground(
    rmu: vec2<f32>,
    bottom_radius: f32
) -> bool {
    let r = rmu.x;
    let mu = rmu.y;

    let f = r * r * (mu * mu - 1.0) + bottom_radius * bottom_radius;
    return mu < 0.0 && f >= 0.0;
}

fn distance_to_bottom_atmosphere_boundary(
    rmu: vec2<f32>,
    bottom_radius: f32
) -> f32 {
    let r = rmu.x;
    let mu = rmu.y;

    // assert(r >= atmosphere.bottom_radius);
    // assert(mu >= -1.0 && mu <= 1.0);
    let discriminant = r * r * (mu * mu - 1.0) + bottom_radius * bottom_radius;
    return clamp_distance(-r * mu - safe_sqrt(discriminant));
}

fn distance_to_top_atmosphere_boundary(
    rmu: vec2<f32>,
    top_radius: f32
) -> f32 {
    let r = rmu.x;
    let mu = rmu.y;

    // assert(r <= top_radius);
    // assert(mu >= -1.0 && mu <= 1.0);
    let discriminant = r * r * (mu * mu - 1.0) + top_radius * top_radius;
    return clamp_distance(-r * mu + safe_sqrt(discriminant));
}

// FIXME: can we just pass ScatterCoord here with ground intersection in the mix?
fn distance_to_nearest_atmosphere_boundary(
    rmu: vec2<f32>,
    bottom_radius: f32,
    top_radius: f32,
    ray_r_mu_intersects_ground: bool
) -> f32 {
    if (ray_r_mu_intersects_ground) {
        return distance_to_bottom_atmosphere_boundary(rmu, bottom_radius);
    } else {
        return distance_to_top_atmosphere_boundary(rmu, top_radius);
    }
}

fn transmittance_rmu_to_uv(
    rmu: vec2<f32>,
    bottom_radius: f32,
    top_radius: f32
) -> vec2<f32> {
    let r = rmu.x;
    let mu = rmu.y;

    // assert(r >= bottom_radius && r <= top_radius);
    // assert(mu >= -1.0 && mu <= 1.0);
    // Distance to top atmosphere boundary for a horizontal ray at ground level.
    let H = sqrt(top_radius * top_radius - bottom_radius * bottom_radius);
    // Distance to the horizon.
    let rho = safe_sqrt(r * r - bottom_radius * bottom_radius);
    // Distance to the top atmosphere boundary for the ray (r,mu), and its minimum
    // and maximum values over all mu - obtained for (r,1) and (r,mu_horizon).
    let d = distance_to_top_atmosphere_boundary(rmu, top_radius);
    let d_min = top_radius - r;
    let d_max = rho + H;
    let x_mu = (d - d_min) / (d_max - d_min);
    let x_r = rho / H;
    return vec2(
        get_texture_coord_from_unit_range(x_mu, TRANSMITTANCE_TEXTURE_WIDTH),
        get_texture_coord_from_unit_range(x_r, TRANSMITTANCE_TEXTURE_HEIGHT)
    );
}

fn transmittance_uv_to_rmu(
    uv: vec2<f32>,
    bottom_radius: f32,
    top_radius: f32
) -> vec2<f32> {
    // assert(uv.x >= 0.0 && uv.x <= 1.0);
    // assert(uv.y >= 0.0 && uv.y <= 1.0);
    let x_mu = get_unit_range_from_texture_coord(uv.x, TRANSMITTANCE_TEXTURE_WIDTH);
    let x_r = get_unit_range_from_texture_coord(uv.y, TRANSMITTANCE_TEXTURE_HEIGHT);
    // Distance to top atmosphere boundary for a horizontal ray at ground level.
    let H = sqrt(top_radius * top_radius - bottom_radius * bottom_radius);
    // Distance to the horizon, from which we can compute r:
    let rho = H * x_r;
    let r = sqrt(rho * rho + bottom_radius * bottom_radius);
    // Distance to the top atmosphere boundary for the ray (r,mu), and its minimum
    // and maximum values over all mu - obtained for (r,1) and (r,mu_horizon) -
    // from which we can recover mu:
    let d_min = top_radius - r;
    let d_max = rho + H;
    let d = d_min + x_mu * (d_max - d_min);
    var mu = 1.0;
    if (d != 0.0) {
        mu = (H * H - rho * rho - d * d) / (2.0 * r * d);
    }
    mu = clamp_cosine(mu);
    return vec2(r, mu);
}

fn get_transmittance_to_top_atmosphere_boundary(
    rmu: vec2<f32>,
    transmittance_texture: texture_2d<f32>,
    bottom_radius: f32,
    top_radius: f32
) -> vec4<f32> {
    // assert(r >= bottom_radius && r <= top_radius);
    let uv = transmittance_rmu_to_uv(rmu, bottom_radius, top_radius);
    return texSample2(transmittance_texture, uv, vec2(TRANSMITTANCE_TEXTURE_WIDTH, TRANSMITTANCE_TEXTURE_HEIGHT));
}

fn get_transmittance_to_sun(
    transmittance_texture: texture_2d<f32>,
    r: f32,
    mu_s: f32,
    bottom_radius: f32,
    top_radius: f32,
    sun_angular_radius: f32
) -> vec4<f32> {
    let sin_theta_h = bottom_radius / r;
    let cos_theta_h = -sqrt(max(1.0 - sin_theta_h * sin_theta_h, 0.0));
    let base = get_transmittance_to_top_atmosphere_boundary(
        vec2(r, mu_s),
        transmittance_texture,
        bottom_radius,
        top_radius
    );
    return base * smoothstep(
        -sin_theta_h * sun_angular_radius,
        sin_theta_h * sun_angular_radius,
        mu_s - cos_theta_h);
}

fn get_transmittance(
    transmittance_texture: texture_2d<f32>,
    r: f32,
    mu: f32,
    d: f32,
    ray_r_mu_intersects_ground: bool,
    bottom_radius: f32,
    top_radius: f32
) -> vec4<f32> {
    // assert(r >= bottom_radius && r <= top_radius);
    // assert(mu >= -1.0 && mu <= 1.0);
    // assert(d >= 0.0 * m);

    let r_d = clamp_radius(
        sqrt(d * d + 2.0 * r * mu * d + r * r),
        bottom_radius,
        top_radius
    );
    let mu_d = clamp_cosine((r * mu + d) / r_d);

    var a_rmu = vec2(0.);
    var b_rmu = vec2(0.);
    if (ray_r_mu_intersects_ground) {
        a_rmu = vec2(r_d, -mu_d);
        b_rmu = vec2(r, -mu);
    } else {
        a_rmu = vec2(r, mu);
        b_rmu = vec2(r_d, mu_d);
    }
    let a = get_transmittance_to_top_atmosphere_boundary(
        a_rmu, transmittance_texture, bottom_radius, top_radius);
    let b = get_transmittance_to_top_atmosphere_boundary(
        b_rmu, transmittance_texture, bottom_radius, top_radius);
    return min(a / b, vec4(1.0));
}

// In order to precompute the ground irradiance in a texture we need a mapping
// from the ground irradiance parameters to texture coordinates. Since we
// precompute the ground irradiance only for horizontal surfaces, this irradiance
// depends only on $r$ and $\mu_s$, so we need a mapping from $(r,\mu_s)$ to
// $(u,v)$ texture coordinates. The simplest, affine mapping is sufficient here,
// because the ground irradiance function is very smooth:
fn irradiance_rmus_to_uv(
    r: f32,
    mu_s: f32,
    bottom_radius: f32,
    top_radius: f32
) -> vec2<f32> {
    let x_r = (r - bottom_radius) / (top_radius - bottom_radius);
    let x_mu_s = mu_s * 0.5 + 0.5;
    return vec2(
        get_texture_coord_from_unit_range(x_mu_s, IRRADIANCE_TEXTURE_WIDTH),
        get_texture_coord_from_unit_range(x_r, IRRADIANCE_TEXTURE_HEIGHT)
    );
}

fn irradiance_uv_to_rmus(
    uv: vec2<f32>,
    bottom_radius: f32,
    top_radius: f32
) -> vec2<f32> {
    // assert(uv.x >= 0.0 && uv.x <= 1.0);
    // assert(uv.y >= 0.0 && uv.y <= 1.0);
    let x_mu_s = get_unit_range_from_texture_coord(uv.x, IRRADIANCE_TEXTURE_WIDTH);
    let x_r = get_unit_range_from_texture_coord(uv.y, IRRADIANCE_TEXTURE_HEIGHT);
    let r = bottom_radius + x_r * (top_radius - bottom_radius);
    let mu_s = clamp_cosine(2.0 * x_mu_s - 1.0);
    return vec2(r, mu_s);
}

fn get_irradiance(
    irradiance_texture: texture_2d<f32>,
    r: f32,
    mu_s: f32,
    bottom_radius: f32,
    top_radius: f32
) -> vec4<f32> {
    let uv = irradiance_rmus_to_uv(r, mu_s, bottom_radius, top_radius);
    let size = vec2(IRRADIANCE_TEXTURE_WIDTH, IRRADIANCE_TEXTURE_HEIGHT);
    return texSample2(irradiance_texture, uv, size);
}

struct ScatterCoord {
    r: f32,
    mu: f32,
    mu_s: f32,
    nu: f32,
    intersects_ground: bool,
};

fn scattering_rmumusnu_to_uvwz(
    sc: ScatterCoord,
    bottom_radius: f32,
    top_radius: f32,
    mu_s_min: f32,
) -> vec4<f32> {
    // Distance to top atmosphere boundary for a horizontal ray at ground level.
    let H = sqrt(top_radius * top_radius - bottom_radius * bottom_radius);

    // Distance to the horizon.
    let rho = safe_sqrt(sc.r * sc.r - bottom_radius * bottom_radius);
    let u_r = get_texture_coord_from_unit_range(rho / H, SCATTERING_TEXTURE_R_SIZE);

    // Discriminant of the quadratic equation for the intersections of the ray
    // (r,mu) with the ground (see RayIntersectsGround).
    let r_mu = sc.r * sc.mu;
    let discriminant = r_mu * r_mu - sc.r * sc.r + bottom_radius * bottom_radius;
    var u_mu = 0.;
    if (sc.intersects_ground) {
        // Distance to the ground for the ray (r,mu), and its minimum and maximum
        // values over all mu - obtained for (r,-1) and (r,mu_horizon).
        let d = -r_mu - safe_sqrt(discriminant);
        let d_min = sc.r - bottom_radius;
        let d_max = rho;
        var unit_range = 0.;
        if (d_max != d_min) {
            unit_range = (d - d_min) / (d_max - d_min);
        }
        u_mu = 0.5 - 0.5 * get_texture_coord_from_unit_range(unit_range, SCATTERING_TEXTURE_MU_SIZE / 2);
    } else {
        // Distance to the top atmosphere boundary for the ray (r,mu), and its
        // minimum and maximum values over all mu - obtained for (r,1) and
        // (r,mu_horizon).
        let d = -r_mu + safe_sqrt(discriminant + H * H);
        let d_min = top_radius - sc.r;
        let d_max = rho + H;
        u_mu = 0.5 + 0.5 * get_texture_coord_from_unit_range(
            (d - d_min) / (d_max - d_min), SCATTERING_TEXTURE_MU_SIZE / 2);
    }

    let d = distance_to_top_atmosphere_boundary(vec2(bottom_radius, sc.mu_s), top_radius);
    let d_min = top_radius - bottom_radius;
    let d_max = H;
    let a = (d - d_min) / (d_max - d_min);
    let A = -2.0 * mu_s_min * bottom_radius / (d_max - d_min);
    let u_mu_s = get_texture_coord_from_unit_range(
        max(1.0 - a / A, 0.0) / (1.0 + a), SCATTERING_TEXTURE_MU_S_SIZE);

    let u_nu = (sc.nu + 1.0) / 2.0;
    return vec4(u_nu, u_mu_s, u_mu, u_r);
}

// Note that we added the solar irradiance and the scattering coefficient terms
// that we omitted in <code>ComputeSingleScatteringIntegrand</code>, but not the
// phase function terms - they are added at <a href="#rendering">render time</a>
// for better angular precision. We provide them here for completeness:
fn rayleigh_phase_function(nu: f32) -> f32 {
    let k = 3.0 / (16.0 * PI);
    return k * (1.0 + nu * nu);
}

fn mie_phase_function(g: f32, nu: f32) -> f32 {
    let k = 3.0 / (8.0 * PI) * (1.0 - g * g) / (2.0 + g * g);
    return k * (1.0 + nu * nu) / pow(1.0 + g * g - 2.0 * g * nu, 1.5);
}

struct ScatterGatherCoords {
    tc0: vec3<f32>,
    tc1: vec3<f32>,
    uvw0: vec3<i32>,
    uvw1: vec3<i32>,
    lerp: f32
}

fn get_scattering_gather_coords(
    sc: ScatterCoord,
    atmosphere_bottom_radius: f32,
    atmosphere_top_radius: f32,
    atmosphere_mu_s_min: f32,
) -> ScatterGatherCoords {
    let uvwz = scattering_rmumusnu_to_uvwz(
        sc,
        atmosphere_bottom_radius,
        atmosphere_top_radius,
        atmosphere_mu_s_min
    );
    let tex_coord_x = uvwz.x * f32(SCATTERING_TEXTURE_NU_SIZE - 1);
    let tex_x = floor(tex_coord_x);
    let lerp = tex_coord_x - tex_x;
    let uvw0 = vec3((tex_x + uvwz.y) / f32(SCATTERING_TEXTURE_NU_SIZE), uvwz.z, uvwz.w);
    let uvw1 = vec3((tex_x + 1.0 + uvwz.y) / f32(SCATTERING_TEXTURE_NU_SIZE), uvwz.z, uvwz.w);
    let size = vec3(f32(SCATTERING_TEXTURE_WIDTH), f32(SCATTERING_TEXTURE_HEIGHT), f32(SCATTERING_TEXTURE_DEPTH));
    return ScatterGatherCoords(
        uvw0,
        uvw1,
        vec3<i32>(uvw0 * size),
        vec3<i32>(uvw1 * size),
        lerp
    );
}

fn get_scattering(
    scattering_texture: texture_3d<f32>,
    sc: ScatterCoord,
    atmosphere_bottom_radius: f32,
    atmosphere_top_radius: f32,
    atmosphere_mu_s_min: f32,
) -> vec4<f32> {
    let coords = get_scattering_gather_coords(
        sc,
        atmosphere_bottom_radius,
        atmosphere_top_radius,
        atmosphere_mu_s_min,
    );
    let samp0 = textureLoad(scattering_texture, coords.uvw0, 0);
    let samp1 = textureLoad(scattering_texture, coords.uvw1, 0);
    return samp0 * (1. - coords.lerp) + samp1 * coords.lerp;
}

struct CombinedScatteringOut {
    scattering: vec3<f32>,
    single_mie_scattering: vec3<f32>
}

fn get_scattering_combined(
    scattering_texture: texture_3d<f32>,
    single_mie_scattering_texture: texture_3d<f32>,
    sc: ScatterCoord,
    atmosphere_bottom_radius: f32,
    atmosphere_top_radius: f32,
    atmosphere_mu_s_min: f32,
) -> CombinedScatteringOut {
    let coords = get_scattering_gather_coords(
        sc,
        atmosphere_bottom_radius,
        atmosphere_top_radius,
        atmosphere_mu_s_min,
    );
    let scat0 = textureLoad(scattering_texture, coords.uvw0, 0);
    let scat1 = textureLoad(scattering_texture, coords.uvw1, 0);
    let scattering = scat0 * (1. - coords.lerp) + scat1 * coords.lerp;
    let mie0 = textureLoad(single_mie_scattering_texture, coords.uvw0, 0);
    let mie1 = textureLoad(single_mie_scattering_texture, coords.uvw1, 0);
    let mie = mie0 * (1. - coords.lerp) + mie1 * coords.lerp;
    return CombinedScatteringOut(scattering.xyz, mie.xyz);
}

fn get_scattering_combined_hw(
    scattering_texture: texture_3d<f32>,
    scattering_sampler: sampler,
    single_mie_scattering_texture: texture_3d<f32>,
    single_mie_scattering_sampler: sampler,
    sc: ScatterCoord,
    atmosphere_bottom_radius: f32,
    atmosphere_top_radius: f32,
    atmosphere_mu_s_min: f32,
) -> CombinedScatteringOut {
    let coords = get_scattering_gather_coords(
        sc,
        atmosphere_bottom_radius,
        atmosphere_top_radius,
        atmosphere_mu_s_min,
    );
    let scat0 = textureSample(scattering_texture, scattering_sampler, coords.tc0);
    let scat1 = textureSample(scattering_texture, scattering_sampler, coords.tc1);
    let scattering = scat0 * (1. - coords.lerp) + scat1 * coords.lerp;
    let mie0 = textureSample(single_mie_scattering_texture, single_mie_scattering_sampler, coords.tc0);
    let mie1 = textureSample(single_mie_scattering_texture, single_mie_scattering_sampler, coords.tc1);
    let mie = mie0 * (1. - coords.lerp) + mie1 * coords.lerp;
    return CombinedScatteringOut(scattering.xyz, mie.xyz);
}

//void get_combined_scattering(
//    AtmosphereParameters atmosphere,
//    texture3D scattering_texture,
//    sampler scattering_sampler,
//    texture3D single_mie_scattering_texture,
//    sampler single_mie_scattering_sampler,
//    ScatterCoord sc,
//    bool ray_r_mu_intersects_ground,
//    out vec3 scattering,
//    out vec3 single_mie_scattering
//) {
//    vec4 uvwz = scattering_rmumusnu_to_uvwz(
//        sc,
//        atmosphere.bottom_radius,
//        atmosphere.top_radius,
//        atmosphere.mu_s_min,
//        ray_r_mu_intersects_ground
//    );
//    float tex_coord_x = uvwz.x * float(SCATTERING_TEXTURE_NU_SIZE - 1);
//    float tex_x = floor(tex_coord_x);
//    float lerp = tex_coord_x - tex_x;
//    vec3 uvw0 = vec3((tex_x + uvwz.y) / float(SCATTERING_TEXTURE_NU_SIZE), uvwz.z, uvwz.w);
//    vec3 uvw1 = vec3((tex_x + 1.0 + uvwz.y) / float(SCATTERING_TEXTURE_NU_SIZE), uvwz.z, uvwz.w);
//    scattering = vec3(
//        texture(sampler3D(scattering_texture, scattering_sampler), uvw0) * (1.0 - lerp) +
//        texture(sampler3D(scattering_texture, scattering_sampler), uvw1) * lerp);
//    single_mie_scattering = vec3(
//        texture(sampler3D(single_mie_scattering_texture, single_mie_scattering_sampler), uvw0) * (1.0 - lerp) +
//        texture(sampler3D(single_mie_scattering_texture, single_mie_scattering_sampler), uvw1) * lerp);
//}
