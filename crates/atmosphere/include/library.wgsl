// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

fn get_solar_luminance(
    sun_irradiance: vec3<f32>,
    sun_angular_radius: f32,
    sun_spectral_radiance_to_luminance: vec3<f32>
) -> vec3<f32> {
  return (sun_irradiance / (PI * sun_angular_radius * sun_angular_radius))
    * sun_spectral_radiance_to_luminance;
}

struct SunAndSkyIrradianceOut {
    sun_irradiance: vec3<f32>,
    sky_irradiance: vec3<f32>
}

fn get_sun_and_sky_irradiance(
    atmosphere: AtmosphereParameters,
    transmittance_texture: texture_2d<f32>,
    irradiance_texture: texture_2d<f32>,
    at_point: vec3<f32>,
    normal: vec3<f32>,
    sun_direction: vec3<f32>,
) -> SunAndSkyIrradianceOut {
    let r = length(at_point);
    let mu_s = dot(at_point, sun_direction) / r;

    // FIXME: Sample this?
    // Indirect irradiance (approximated if the surface is not horizontal).
    let irradiance_at_point = get_irradiance(
        irradiance_texture,
        r,
        mu_s,
        atmosphere.bottom_radius,
        atmosphere.top_radius
    );
    let sky_irradiance = (irradiance_at_point * (1.0 + dot(normal, at_point) / r) * 0.5).xyz;

    // FIXME: sample this?
    // Direct irradiance.
    let transmittance_to_point = get_transmittance_to_sun(
        transmittance_texture,
        r,
        mu_s,
        atmosphere.bottom_radius,
        atmosphere.top_radius,
        atmosphere.sun_angular_radius
    );
    let sun_irradiance = (
        atmosphere.sun_irradiance *
        transmittance_to_point *
        max(dot(normal, sun_direction), 0.0)
    ).xyz;

    return SunAndSkyIrradianceOut(sun_irradiance, sky_irradiance);
}

// FIXME: use shadow maps for this?
fn get_sun_visibility(_pt: vec3<f32>, _sun_direction: vec3<f32>) -> f32 {

    /*
    vec3 p = point - kSphereCenter;
    float p_dot_v = dot(p, sun_direction);
    float p_dot_p = dot(p, p);
    float ray_sphere_center_squared_distance = p_dot_p - p_dot_v * p_dot_v;
    float distance_to_intersection = -p_dot_v - sqrt(
      kSphereRadius * kSphereRadius - ray_sphere_center_squared_distance);
    if (distance_to_intersection > 0.0) {
        // Compute the distance between the view ray and the sphere, and the
        // corresponding (tangent of the) subtended angle. Finally, use this to
        // compute an approximate sun visibility.
        float ray_sphere_distance =
            kSphereRadius - sqrt(ray_sphere_center_squared_distance);
        float ray_sphere_angular_distance = -ray_sphere_distance / p_dot_v;
        return smoothstep(1.0, 0.0, ray_sphere_angular_distance / sun_size.x);
    }
    return 1.0;
    */
    return 1.0;
}

// FIXME: use shadow maps for this?
fn get_sky_visibility(_pt: vec3<f32>) -> f32 {
    //vec3 p = point - kSphereCenter;
    //float p_dot_p = dot(p, p);
    //return 1.0 + p.z / sqrt(p_dot_p) * kSphereRadius * kSphereRadius / p_dot_p;
    return 1.0;
}

struct SkyInScatteringOut {
    transmittance: vec3<f32>,
    in_scattering: vec3<f32>
}

fn get_sky_radiance_to_point(
    atmosphere: AtmosphereParameters,
    transmittance_texture: texture_2d<f32>,
    transmittance_sampler: sampler,
    scattering_texture: texture_3d<f32>,
    scattering_sampler: sampler,
    single_mie_scattering_texture: texture_3d<f32>,
    single_mie_scattering_sampler: sampler,
    camera_pos_w_km_in: vec3<f32>,
    point_w_km: vec3<f32>,
    view_ray: vec3<f32>,
    sun_direction: vec3<f32>,
) -> SkyInScatteringOut {
    // Compute the distance to the top atmosphere boundary along the view ray,
    // assuming the viewer is in space (or NaN if the view ray does not intersect
    // the atmosphere).
    var camera_pos_w_km = camera_pos_w_km_in;
    var r = length(camera_pos_w_km);
    var rmu = dot(camera_pos_w_km, view_ray);
    let distance_to_top_atmosphere_boundary = -rmu -
        sqrt(rmu * rmu - r * r + atmosphere.top_radius * atmosphere.top_radius);

    // If the viewer is in space and the view ray intersects the atmosphere, move
    // the viewer to the top atmosphere boundary (along the view ray):
    if (distance_to_top_atmosphere_boundary > 0.0) {
        camera_pos_w_km = camera_pos_w_km + view_ray * distance_to_top_atmosphere_boundary;
        r = atmosphere.top_radius;
        rmu += distance_to_top_atmosphere_boundary;
    }

    // Compute the r, mu, mu_s and nu parameters for the first texture lookup.
    let mu = rmu / r;
    let mu_s = dot(camera_pos_w_km, sun_direction) / r;
    let nu = dot(view_ray, sun_direction);
    var d = length(point_w_km - camera_pos_w_km);
    // FIXME: should this... use the ground intersection for real or do we just care about horizon?
    let ray_r_mu_intersects_ground = ray_intersects_ground(vec2(r, mu), atmosphere.bottom_radius);
    // FIXME: sample this?
    let transmittance = get_transmittance(
        transmittance_texture,
        // transmittance_sampler,
        r, mu, d,
        ray_r_mu_intersects_ground,
        atmosphere.bottom_radius,
        atmosphere.top_radius
    ).xyz;

    let combo = get_scattering_combined_hw(
        scattering_texture,
        scattering_sampler,
        single_mie_scattering_texture,
        single_mie_scattering_sampler,
        ScatterCoord(r, mu, mu_s, nu, ray_r_mu_intersects_ground),
        atmosphere.bottom_radius,
        atmosphere.top_radius,
        atmosphere.mu_s_min,
    );
    let scattering_0 = combo.scattering;
    let single_mie_scattering_0 = combo.single_mie_scattering;

    // TODO: adjust scattering down by amount of atmosphere occluded by shadowing objects.

    // Compute the r, mu, mu_s and nu parameters for the second texture lookup.
    // If shadow_length is not 0 (case of light shafts), we want to ignore the
    // scattering along the last shadow_length meters of the view ray, which we
    // do by subtracting shadow_length from d (this way scattering_p is equal to
    // the S|x_s=x_0-lv term in Eq. (17) of our paper).
    let shadow_length = 0.0;
    d = max(d - shadow_length, 0.0);
    let r_p = clamp_radius(sqrt(d * d + 2.0 * r * mu * d + r * r), atmosphere.bottom_radius, atmosphere.top_radius);
    let mu_p = (r * mu + d) / r_p;
    let mu_s_p = (r * mu_s + d * nu) / r_p;

    let combo_p = get_scattering_combined_hw(
        scattering_texture,
        scattering_sampler,
        single_mie_scattering_texture,
        single_mie_scattering_sampler,
        ScatterCoord(r_p, mu_p, mu_s_p, nu, ray_r_mu_intersects_ground),
        atmosphere.bottom_radius,
        atmosphere.top_radius,
        atmosphere.mu_s_min,
    );
    let scattering_p = combo_p.scattering;
    let single_mie_scattering_p = combo_p.single_mie_scattering;

    // Combine the lookup results to get the scattering between camera and point.
    let scattering = scattering_0 - transmittance * scattering_p;
    var single_mie_scattering = single_mie_scattering_0 - transmittance * single_mie_scattering_p;

    // Hack to avoid rendering artifacts when the sun is below the horizon.
    single_mie_scattering = single_mie_scattering * smoothstep(0.0, 0.01, mu_s);

    let in_scattering = scattering * rayleigh_phase_function(nu) +
        single_mie_scattering * mie_phase_function(atmosphere.mie_phase_function_g, nu);

    return SkyInScatteringOut(transmittance, in_scattering);
}

struct SkyRadianceOut {
    transmittance: vec3<f32>,
    radiance: vec3<f32>,
}

fn get_sky_radiance(
    atmosphere: AtmosphereParameters,
    transmittance_texture: texture_2d<f32>,
    transmittance_sampler: sampler,
    scattering_texture: texture_3d<f32>,
    scattering_sampler: sampler,
    single_mie_scattering_texture: texture_3d<f32>,
    single_mie_scattering_sampler: sampler,
    camera_in: vec3<f32>,
    view: vec3<f32>,
    sun_direction: vec3<f32>,
) -> SkyRadianceOut {
    // Compute the distance to the top atmosphere boundary along the view ray,
    // assuming the viewer is in space (or NaN if the view ray does not intersect
    // the atmosphere).
    var camera = camera_in;
    var r = length(camera);
    var rmu = dot(camera, view);
    let t0 = -rmu - sqrt(rmu * rmu - r * r + atmosphere.top_radius * atmosphere.top_radius);
    if (t0 > 0.0) {
        // If the viewer is in space and the view ray intersects the atmosphere, move
        // the viewer to the top atmosphere boundary (along the view ray):
        camera = camera + view * t0;
        r = atmosphere.top_radius;
        rmu += t0;
    } else if (r > atmosphere.top_radius) {
        // Spaaaaace! I'm in space.
        // If the view ray does not intersect the atmosphere, simply return 0.
        return SkyRadianceOut(vec3(1.), vec3(0.));
    }

    // Compute the r, mu, mu_s and nu parameters needed for the texture lookups.
    let mu = rmu / r;
    let mu_s = dot(camera, sun_direction) / r;
    let nu = dot(view, sun_direction);
    let ray_r_mu_intersects_ground = ray_intersects_ground(vec2(r, mu), atmosphere.bottom_radius);

    var transmittance = vec3(0.);
    if (!ray_r_mu_intersects_ground) {
        // FIXME: sample this
        transmittance = get_transmittance_to_top_atmosphere_boundary(
            vec2(r, mu),
            transmittance_texture,
            // transmittance_sampler,
            atmosphere.bottom_radius,
            atmosphere.top_radius
        ).xyz;
    }

    let combo = get_scattering_combined_hw(
        scattering_texture,
        scattering_sampler,
        single_mie_scattering_texture,
        single_mie_scattering_sampler,
        ScatterCoord(r, mu, mu_s, nu, ray_r_mu_intersects_ground),
        atmosphere.bottom_radius,
        atmosphere.top_radius,
        atmosphere.mu_s_min,
    );

    let radiance = combo.scattering * rayleigh_phase_function(nu) +
            combo.single_mie_scattering * mie_phase_function(atmosphere.mie_phase_function_g, nu);

    return SkyRadianceOut(transmittance, radiance);
}
