// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use bitbag::{BitBag, BitOr, Flags};
use std::fmt;
use strum::EnumIter;

#[cfg(target_family = "unix")]
use std::mem;

#[cfg(target_family = "unix")]
pub fn terminal_size() -> (u16, u16) {
    unsafe {
        if libc::isatty(libc::STDOUT_FILENO) != 1 {
            return (24, 80);
        }

        let mut winsize: libc::winsize = mem::zeroed();

        // FIXME: ".into()" used as a temporary fix for a libc bug
        // https://github.com/rust-lang/libc/pull/704
        #[allow(clippy::useless_conversion)]
        libc::ioctl(libc::STDOUT_FILENO, libc::TIOCGWINSZ.into(), &mut winsize);
        if winsize.ws_row > 0 && winsize.ws_col > 0 {
            (winsize.ws_row, winsize.ws_col)
        } else {
            (24, 80)
        }
    }
}

#[cfg(target_family = "windows")]
pub fn terminal_size() -> (u16, u16) {
    (80, 24)
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Color {
    Black = 30,
    Red = 31,
    Green = 32,
    Yellow = 33,
    Blue = 34,
    Magenta = 35,
    Cyan = 36,
    White = 37,
    BrightBlack = 90,
    BrightRed = 91,
    BrightGreen = 92,
    BrightYellow = 93,
    BrightBlue = 94,
    BrightMagenta = 95,
    BrightCyan = 96,
    BrightWhite = 97,
}

impl Color {
    pub fn put(self, v: &mut Vec<char>) {
        for c in format!("{}", self as u8).chars() {
            v.push(c);
        }
    }
    pub fn put_bg(self, v: &mut Vec<char>) {
        for c in format!("{}", (self as u8) + 10).chars() {
            v.push(c);
        }
    }
    pub fn fmt(self) -> String {
        format!("{}", self as u8)
    }
    pub fn fmt_bg(self) -> String {
        format!("{}", (self as u8) + 10)
    }
}

#[rustfmt::skip]
#[derive(Flags, BitOr, Clone, Copy, Debug, Eq, PartialEq, EnumIter)]
#[repr(u8)]
pub enum AnsiStyle {
    NONE          = 0b0000_0000,
    BOLD          = 0b0000_0001,
    DIMMED        = 0b0000_0010,
    ITALIC        = 0b0000_0100,
    UNDERLINE     = 0b0000_1000,
    BLINK         = 0b0001_0000,
    REVERSE       = 0b0010_0000,
    HIDDEN        = 0b0100_0000,
    STRIKETHROUGH = 0b1000_0000,
}

fn put_style(style: BitBag<AnsiStyle>, v: &mut Vec<char>) -> bool {
    let mut acc = Vec::new();
    if style.is_set(AnsiStyle::BOLD) {
        acc.push('1');
    }
    if style.is_set(AnsiStyle::DIMMED) {
        acc.push('2');
    }
    if style.is_set(AnsiStyle::ITALIC) {
        acc.push('3');
    }
    if style.is_set(AnsiStyle::UNDERLINE) {
        acc.push('4');
    }
    if style.is_set(AnsiStyle::BLINK) {
        acc.push('5');
    }
    if style.is_set(AnsiStyle::REVERSE) {
        acc.push('7');
    }
    if style.is_set(AnsiStyle::HIDDEN) {
        acc.push('8');
    }
    if style.is_set(AnsiStyle::STRIKETHROUGH) {
        acc.push('9');
    }
    if !acc.is_empty() {
        for (i, &c) in acc.iter().enumerate() {
            v.push(c);
            if i + 1 < acc.len() {
                v.push(';');
            }
        }
    }
    !acc.is_empty()
}

#[derive(Debug, Eq, PartialEq)]
pub struct Escape {
    foreground: Option<Color>,
    background: Option<Color>,
    styles: BitBag<AnsiStyle>,
}

impl Escape {
    pub fn new() -> Self {
        Escape {
            foreground: None,
            background: None,
            styles: Default::default(),
        }
    }

    pub fn fg(mut self, clr: Color) -> Self {
        self.foreground = Some(clr);
        self
    }

    pub fn bg(mut self, clr: Color) -> Self {
        self.background = Some(clr);
        self
    }

    // Shortcuts for foreground colors
    pub fn black(self) -> Self {
        self.fg(Color::Black)
    }

    pub fn red(self) -> Self {
        self.fg(Color::Red)
    }

    pub fn green(self) -> Self {
        self.fg(Color::Green)
    }

    pub fn yellow(self) -> Self {
        self.fg(Color::Yellow)
    }

    pub fn blue(self) -> Self {
        self.fg(Color::Blue)
    }

    pub fn magenta(self) -> Self {
        self.fg(Color::Magenta)
    }

    pub fn cyan(self) -> Self {
        self.fg(Color::Cyan)
    }

    pub fn white(self) -> Self {
        self.fg(Color::White)
    }

    // Shortcut to upgrade a color to it's "bright" alternate.
    pub fn bright(mut self) -> Self {
        self.foreground = match self.foreground {
            None => None,
            Some(Color::Black) => Some(Color::BrightBlack),
            Some(Color::Red) => Some(Color::BrightRed),
            Some(Color::Green) => Some(Color::BrightGreen),
            Some(Color::Yellow) => Some(Color::BrightYellow),
            Some(Color::Blue) => Some(Color::BrightBlue),
            Some(Color::Magenta) => Some(Color::BrightMagenta),
            Some(Color::Cyan) => Some(Color::BrightCyan),
            Some(Color::White) => Some(Color::BrightWhite),
            Some(bright) => Some(bright),
        };
        self
    }

    pub fn style(mut self, style: BitBag<AnsiStyle>) -> Self {
        self.styles = style;
        self
    }

    #[allow(dead_code)]
    pub fn bold(mut self) -> Self {
        self.styles.set(AnsiStyle::BOLD);
        self
    }

    #[allow(dead_code)]
    pub fn dimmed(mut self) -> Self {
        self.styles.set(AnsiStyle::DIMMED);
        self
    }

    #[allow(dead_code)]
    pub fn italic(mut self) -> Self {
        self.styles.set(AnsiStyle::ITALIC);
        self
    }

    #[allow(dead_code)]
    pub fn underline(mut self) -> Self {
        self.styles.set(AnsiStyle::UNDERLINE);
        self
    }

    #[allow(dead_code)]
    pub fn blink(mut self) -> Self {
        self.styles.set(AnsiStyle::BLINK);
        self
    }

    #[allow(dead_code)]
    pub fn reverse(mut self) -> Self {
        self.styles.set(AnsiStyle::REVERSE);
        self
    }

    #[allow(dead_code)]
    pub fn hidden(mut self) -> Self {
        self.styles.set(AnsiStyle::HIDDEN);
        self
    }

    #[allow(dead_code)]
    pub fn strike_through(mut self) -> Self {
        self.styles.set(AnsiStyle::STRIKETHROUGH);
        self
    }

    #[allow(dead_code)]
    pub fn put_reset(v: &mut Vec<char>) {
        for c in "\x1B[0m".chars() {
            v.push(c);
        }
    }

    pub fn put(&self, v: &mut Vec<char>) {
        if self.foreground.is_none()
            && self.background.is_none()
            && self.styles == Default::default()
        {
            return Self::put_reset(v);
        }
        v.push('\x1B');
        v.push('[');
        let have_chars = put_style(self.styles, v);
        if let Some(c) = self.foreground {
            if have_chars {
                v.push(';');
            }
            c.put(v);
        }
        if let Some(c) = self.background {
            if have_chars {
                v.push(';');
            }
            c.put_bg(v);
        }
        v.push('m');
    }

    pub fn apply(&self, msg: &str) -> String {
        format!("{}{}{}", self, msg, Self::new())
    }
}

impl Default for Escape {
    fn default() -> Self {
        Self::new()
    }
}

impl fmt::Display for Escape {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut s = Vec::new();
        self.put(&mut s);
        write!(f, "{}", s.iter().collect::<String>())
    }
}

pub fn ansi() -> Escape {
    Escape::new()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn raw_formatting() {
        println!(
            "{}Hello{}, {}World{}!",
            ansi().fg(Color::Green).bold(),
            ansi(),
            ansi().fg(Color::Blue).bold(),
            ansi()
        );
    }

    #[test]
    fn apply_formatting() {
        println!(
            "{}, {}!",
            ansi().green().apply("Hello"),
            ansi().blue().apply("World")
        );
    }

    #[test]
    fn apply_bright_formatting() {
        println!(
            "{}, {}!",
            ansi().green().bright().apply("Hello"),
            ansi().blue().bright().apply("World")
        );
    }

    #[test]
    fn apply_terminal_size() {
        let (h, w) = terminal_size();
        assert!(h > 0);
        assert!(w > 0);
    }

    #[test]
    fn style_flags() {
        let style = AnsiStyle::BOLD | AnsiStyle::ITALIC;
        let mut acc = Vec::new();
        put_style(style, &mut acc);
        assert_eq!(acc, vec!['1', ';', '3']);
    }
}
