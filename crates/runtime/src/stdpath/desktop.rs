// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::{Extension, Runtime};
use anyhow::anyhow;
use nitrous::{inject_nitrous_resource, NitrousResource};
use platform_dirs::AppDirs;
use std::{fs::create_dir_all, path::Path};

#[derive(Debug)]
pub struct StdPathsOpts {
    name: String,
}

impl StdPathsOpts {
    pub fn new(name: &str) -> Self {
        Self {
            name: name.to_owned(),
        }
    }
}

#[derive(NitrousResource)]
pub struct StdPaths {
    app_dirs: AppDirs,
}

impl Extension for StdPaths {
    type Opts = StdPathsOpts;

    fn init(runtime: &mut Runtime, opts: Self::Opts) -> anyhow::Result<()> {
        let stdpaths = Self {
            app_dirs: AppDirs::new(Some(&opts.name), false)
                .ok_or_else(|| anyhow!("unable to find app directories"))?,
        };

        create_dir_all(&stdpaths.app_dirs.config_dir)?;
        create_dir_all(&stdpaths.app_dirs.state_dir)?;

        runtime.inject_resource(stdpaths)?;
        Ok(())
    }
}

#[inject_nitrous_resource]
impl StdPaths {
    pub fn state_dir(&self) -> Option<&Path> {
        Some(&self.app_dirs.state_dir)
    }
}
