// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::State;
use anyhow::{bail, ensure, Result};
use log::warn;
use mantle::InputEvent;
use once_cell::sync::Lazy;
use smallvec::SmallVec;
use std::{
    borrow::Cow,
    collections::{HashMap, HashSet},
    fmt,
};
use unicase::Ascii;
use winit::{
    event::{ButtonId, ElementState, MouseButton},
    keyboard::{KeyCode, ModifiersState, PhysicalKey},
};

/// This enum is the meeting place between "bindings" on one side -- canonically strings specified
/// by the user -- and "events" on the other side -- which are produced by the input system in
/// response to key presses and such. This enum can be created from either of those sources, thus
/// bridging the gap in a nice, strongly typed way.
#[derive(Debug, Clone, Copy, Hash, Eq, PartialEq)]
pub enum Input {
    KeyboardKey(PhysicalKey),
    MouseButton(MouseButton),
    JoystickButton(ButtonId),
    Axis(AxisInput),
    Cursor,
    DeviceAdded,
    DeviceRemoved,
}

impl Input {
    pub fn from_event(event: &InputEvent) -> Option<Self> {
        Some(match event {
            InputEvent::KeyboardKey { physical_key, .. } => Input::KeyboardKey(*physical_key),
            InputEvent::MouseButton { button, .. } => Input::MouseButton(*button),
            InputEvent::MouseMotion { .. } => Input::Axis(AxisInput::MouseMotion),
            InputEvent::MouseWheel { .. } => Input::Axis(AxisInput::MouseWheel),
            InputEvent::JoystickButton { dummy, .. } => Input::JoystickButton(*dummy),
            InputEvent::JoystickAxis { id, .. } => Input::Axis(AxisInput::JoystickAxis(*id)),
            InputEvent::CursorMove { .. } => Input::Cursor,
            InputEvent::DeviceAdded { .. } => Self::DeviceAdded,
            InputEvent::DeviceRemoved { .. } => Self::DeviceRemoved,
        })
    }
}

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub enum AxisInput {
    MouseMotion,
    MouseWheel,
    JoystickAxis(u32),
}

static MIRROR_MODIFIERS: Lazy<HashSet<Ascii<&'static str>>> = Lazy::new(|| {
    let mut s = HashSet::new();
    s.insert(Ascii::new("Control"));
    s.insert(Ascii::new("Alt"));
    s.insert(Ascii::new("Win"));
    s.insert(Ascii::new("Shift"));
    s.insert(Ascii::new("Any"));
    s
});

fn ascii(s: &'static str) -> Ascii<Cow<'static, str>> {
    Ascii::new(Cow::from(s))
}

#[rustfmt::skip]
static BIND_MAP: Lazy<HashMap<Ascii<Cow<'static, str>>, Input>> = Lazy::new(|| {
    let mut m = HashMap::new();
    // Device management
    m.insert(ascii("deviceAdded"), Input::DeviceAdded);
    m.insert(ascii("deviceRemoved"), Input::DeviceRemoved);
    // Mouse
    m.insert(ascii("mouseMotion"), Input::Axis(AxisInput::MouseMotion));
    m.insert(ascii("mouseCursor"), Input::Cursor);
    m.insert(ascii("mouseWheel"), Input::Axis(AxisInput::MouseWheel));
    m.insert(ascii("mouseLeft"), Input::MouseButton(MouseButton::Left));
    m.insert(ascii("mouseMiddle"), Input::MouseButton(MouseButton::Middle));
    m.insert(ascii("mouseRight"), Input::MouseButton(MouseButton::Right));
    m.insert(ascii("mouseBack"), Input::MouseButton(MouseButton::Back));
    m.insert(ascii("mouseForward"), Input::MouseButton(MouseButton::Forward));
    for i in 0..99 {
        m.insert(Ascii::new(Cow::from(format!("mouse{i}"))), Input::MouseButton(MouseButton::Other(i)));
    }
    // Joystick
    for i in 0..99 {
        m.insert(Ascii::new(Cow::from(format!("axis{i}", ))), Input::Axis(AxisInput::JoystickAxis(i)));
    }
    for i in 0..99 {
        m.insert(Ascii::new(Cow::from(format!("joy{i}", ))), Input::JoystickButton(i));
    }
    // Key
    m.insert(ascii("A"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyA)));
    m.insert(ascii("B"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyB)));
    m.insert(ascii("C"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyC)));
    m.insert(ascii("D"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyD)));
    m.insert(ascii("E"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyE)));
    m.insert(ascii("F"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyF)));
    m.insert(ascii("G"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyG)));
    m.insert(ascii("H"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyH)));
    m.insert(ascii("I"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyI)));
    m.insert(ascii("J"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyJ)));
    m.insert(ascii("K"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyK)));
    m.insert(ascii("L"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyL)));
    m.insert(ascii("M"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyM)));
    m.insert(ascii("N"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyN)));
    m.insert(ascii("O"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyO)));
    m.insert(ascii("P"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyP)));
    m.insert(ascii("Q"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyQ)));
    m.insert(ascii("R"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyR)));
    m.insert(ascii("S"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyS)));
    m.insert(ascii("T"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyT)));
    m.insert(ascii("U"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyU)));
    m.insert(ascii("V"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyV)));
    m.insert(ascii("W"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyW)));
    m.insert(ascii("X"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyX)));
    m.insert(ascii("Y"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyY)));
    m.insert(ascii("Z"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyZ)));
    m.insert(ascii("Key1"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Digit1)));
    m.insert(ascii("Key2"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Digit2)));
    m.insert(ascii("Key3"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Digit3)));
    m.insert(ascii("Key4"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Digit4)));
    m.insert(ascii("Key5"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Digit5)));
    m.insert(ascii("Key6"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Digit6)));
    m.insert(ascii("Key7"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Digit7)));
    m.insert(ascii("Key8"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Digit8)));
    m.insert(ascii("Key9"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Digit9)));
    m.insert(ascii("Key0"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Digit0)));
    m.insert(ascii("Escape"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Escape)));
    m.insert(ascii("F1"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F1)));
    m.insert(ascii("F2"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F2)));
    m.insert(ascii("F3"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F3)));
    m.insert(ascii("F4"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F4)));
    m.insert(ascii("F5"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F5)));
    m.insert(ascii("F6"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F6)));
    m.insert(ascii("F7"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F7)));
    m.insert(ascii("F8"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F8)));
    m.insert(ascii("F9"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F9)));
    m.insert(ascii("F10"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F10)));
    m.insert(ascii("F11"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F11)));
    m.insert(ascii("F12"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F12)));
    m.insert(ascii("F13"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F13)));
    m.insert(ascii("F14"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F14)));
    m.insert(ascii("F15"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F15)));
    m.insert(ascii("F16"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F16)));
    m.insert(ascii("F17"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F17)));
    m.insert(ascii("F18"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F18)));
    m.insert(ascii("F19"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F19)));
    m.insert(ascii("F20"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F20)));
    m.insert(ascii("F21"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F21)));
    m.insert(ascii("F22"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F22)));
    m.insert(ascii("F23"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F23)));
    m.insert(ascii("F24"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::F24)));
    m.insert(ascii("Pause"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Pause)));
    m.insert(ascii("Insert"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Insert)));
    m.insert(ascii("Home"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Home)));
    m.insert(ascii("Delete"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Delete)));
    m.insert(ascii("End"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::End)));
    m.insert(ascii("PageDown"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::PageDown)));
    m.insert(ascii("PageUp"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::PageUp)));
    m.insert(ascii("Left"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::ArrowLeft)));
    m.insert(ascii("Up"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::ArrowUp)));
    m.insert(ascii("Right"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::ArrowRight)));
    m.insert(ascii("Down"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::ArrowDown)));
    m.insert(ascii("Backspace"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Backspace)));
    m.insert(ascii("Enter"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Enter)));
    m.insert(ascii("Space"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Space)));
    m.insert(ascii("NumLock"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::NumLock)));
    m.insert(ascii("Numpad0"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Numpad0)));
    m.insert(ascii("Numpad1"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Numpad1)));
    m.insert(ascii("Numpad2"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Numpad2)));
    m.insert(ascii("Numpad3"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Numpad3)));
    m.insert(ascii("Numpad4"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Numpad4)));
    m.insert(ascii("Numpad5"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Numpad5)));
    m.insert(ascii("Numpad6"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Numpad6)));
    m.insert(ascii("Numpad7"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Numpad7)));
    m.insert(ascii("Numpad8"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Numpad8)));
    m.insert(ascii("Numpad9"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Numpad9)));
    m.insert(ascii("Abort"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Abort)));
    m.insert(ascii("Suspend"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Suspend)));
    m.insert(ascii("Resume"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Resume)));
    m.insert(ascii("Turbo"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Turbo)));
    m.insert(ascii("Hyper"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Hyper)));
    m.insert(ascii("NumpadAdd"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::NumpadAdd)));
    m.insert(ascii("NumpadComma"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::NumpadComma)));
    m.insert(ascii("NumpadDecimal"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::NumpadDecimal)));
    m.insert(ascii("NumpadDivide"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::NumpadDivide)));
    m.insert(ascii("NumpadEnter"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::NumpadEnter)));
    m.insert(ascii("NumpadEqual"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::NumpadEqual)));
    m.insert(ascii("NumpadMultiply"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::NumpadMultiply)));
    m.insert(ascii("NumpadSubtract"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::NumpadSubtract)));
    m.insert(ascii("Quote"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Quote)));
    m.insert(ascii("Backslash"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Backslash)));
    m.insert(ascii("Calculator"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::LaunchApp2)));
    m.insert(ascii("CapsLock"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::CapsLock)));
    m.insert(ascii("Semicolon"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Semicolon)));
    m.insert(ascii("Comma"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Comma)));
    m.insert(ascii("Convert"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Convert)));
    m.insert(ascii("Equal"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Equal)));
    m.insert(ascii("Backquote"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Backquote)));
    m.insert(ascii("KanaMode"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::KanaMode)));
    m.insert(ascii("LAlt"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::AltLeft)));
    m.insert(ascii("LBracket"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::BracketLeft)));
    m.insert(ascii("LControl"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::ControlLeft)));
    m.insert(ascii("LShift"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::ShiftLeft)));
    m.insert(ascii("LWin"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::SuperLeft)));
    m.insert(ascii("LaunchMail"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::LaunchMail)));
    m.insert(ascii("MediaSelect"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::MediaSelect)));
    m.insert(ascii("MediaStop"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::MediaStop)));
    m.insert(ascii("Minus"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Minus)));
    m.insert(ascii("Mute"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::AudioVolumeMute)));
    m.insert(ascii("MyComputer"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::LaunchApp1)));
    m.insert(ascii("BrowserForward"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::BrowserForward)));
    m.insert(ascii("BrowserBack"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::BrowserBack)));
    m.insert(ascii("MediaTrackNext"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::MediaTrackNext)));
    m.insert(ascii("NonConvert"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::NonConvert)));
    m.insert(ascii("Period"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Period)));
    m.insert(ascii("MediaPlayPause"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::MediaPlayPause)));
    m.insert(ascii("Power"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Power)));
    m.insert(ascii("MediaTrackPrevious"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::MediaTrackPrevious)));
    m.insert(ascii("RAlt"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::AltRight)));
    m.insert(ascii("RBracket"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::BracketRight)));
    m.insert(ascii("RControl"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::ControlRight)));
    m.insert(ascii("RShift"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::ShiftRight)));
    m.insert(ascii("RWin"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::SuperRight)));
    m.insert(ascii("Semicolon"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Semicolon)));
    m.insert(ascii("Slash"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Slash)));
    m.insert(ascii("Sleep"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Sleep)));
    m.insert(ascii("MediaStop"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::MediaStop)));
    m.insert(ascii("PrintScreen"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::PrintScreen)));
    m.insert(ascii("Tab"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Tab)));
    m.insert(ascii("AudioVolumeDown"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::AudioVolumeDown)));
    m.insert(ascii("AudioVolumeUp"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::AudioVolumeUp)));
    m.insert(ascii("WakeUp"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::WakeUp)));
    m.insert(ascii("BrowserBack"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::BrowserBack)));
    m.insert(ascii("BrowserFavorites"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::BrowserFavorites)));
    m.insert(ascii("BrowserForward"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::BrowserForward)));
    m.insert(ascii("BrowserHome"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::BrowserHome)));
    m.insert(ascii("BrowserRefresh"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::BrowserRefresh)));
    m.insert(ascii("BrowserSearch"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::BrowserSearch)));
    m.insert(ascii("BrowserStop"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::BrowserStop)));
    m.insert(ascii("IntlYen"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::IntlYen)));
    m.insert(ascii("Copy"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Copy)));
    m.insert(ascii("Paste"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Paste)));
    m.insert(ascii("Cut"), Input::KeyboardKey(PhysicalKey::Code(KeyCode::Cut)));
    m
});

impl Input {
    pub fn from_binding(s: &str) -> Result<Self> {
        Ok(if let Some(key) = BIND_MAP.get(&Ascii::new(Cow::from(s))) {
            *key
        } else {
            let mut similar = BIND_MAP
                .keys()
                .map(|key| (strsim::levenshtein(key, s), key.to_owned()))
                .collect::<Vec<_>>();
            similar.sort();
            bail!(
                "unrecognized event identifier: {}\ndid you mean: {}",
                s,
                similar.first().unwrap().1
            )
        })
    }

    pub fn modifier(&self) -> ModifiersState {
        match self {
            Input::KeyboardKey(vkey) => match vkey {
                PhysicalKey::Code(KeyCode::ControlLeft) => ModifiersState::CONTROL,
                PhysicalKey::Code(KeyCode::ControlRight) => ModifiersState::CONTROL,
                PhysicalKey::Code(KeyCode::ShiftLeft) => ModifiersState::SHIFT,
                PhysicalKey::Code(KeyCode::ShiftRight) => ModifiersState::SHIFT,
                PhysicalKey::Code(KeyCode::AltLeft) => ModifiersState::ALT,
                PhysicalKey::Code(KeyCode::AltRight) => ModifiersState::ALT,
                PhysicalKey::Code(KeyCode::SuperLeft) => ModifiersState::SUPER,
                PhysicalKey::Code(KeyCode::SuperRight) => ModifiersState::SUPER,
                _ => ModifiersState::default(),
            },
            _ => ModifiersState::default(),
        }
    }
}

#[derive(Debug, Clone, Hash, Eq, PartialEq)]
pub struct InputSet {
    pub(crate) keys: SmallVec<[Input; 2]>,
    pub(crate) modifiers: ModifiersState,
    pub(crate) trigger_on_down: bool,
    pub(crate) trigger_on_up: bool,
}

impl InputSet {
    // Parse keysets of the form a+b+c; e.g. LControl+RControl+Space into
    // a discreet keyset.
    //
    // Prefixing with a - will cause the binding to be called only on keyup.
    //
    // Prefixing with a + will cause the binding to fire on both keyup and keydown events.
    //
    // Note that there is a special case for the 4 modifiers in which we
    // expect to be able to refer to "Control" and not care what key it is.
    // In this case we emit all possible keysets, combinatorially.
    pub fn from_binding(keyset: &str) -> Result<Vec<Self>> {
        let mut out = vec![SmallVec::<[Input; 2]>::new()];
        let mut trigger_on_down = true;
        let mut trigger_on_up = false;
        ensure!(!keyset.is_empty(), "must specify a key to bind");
        let keyset = keyset.trim();
        let first = keyset.chars().next().unwrap();
        let keyset = if first == '+' {
            trigger_on_up = true;
            &keyset[1..]
        } else if first == '-' {
            trigger_on_down = false;
            trigger_on_up = true;
            &keyset[1..]
        } else {
            keyset
        };
        for keyname in keyset.split('+') {
            if let Ok(key) = Input::from_binding(keyname) {
                for tmp in &mut out {
                    tmp.push(key);
                }
            } else if MIRROR_MODIFIERS.contains(&Ascii::new(keyname)) {
                let mut next_out = Vec::new();
                for mut tmp in out.drain(..) {
                    let mut cpy = tmp.clone();
                    tmp.push(Input::from_binding(&format!("L{keyname}",))?);
                    cpy.push(Input::from_binding(&format!("R{keyname}",))?);
                    next_out.push(tmp);
                    next_out.push(cpy);
                }
                out = next_out;
            } else {
                println!("attempting to lookup unknown key name: {keyname}",);
                warn!("unknown key name: {}", keyname);
            }
        }
        ensure!(!out.is_empty(), "no key matching {}", keyset);
        Ok(out
            .drain(..)
            .map(|v| Self {
                modifiers: v.iter().fold(ModifiersState::default(), |modifiers, k| {
                    modifiers | k.modifier()
                }),
                keys: v,
                trigger_on_down,
                trigger_on_up,
            })
            .collect::<Vec<_>>())
    }

    pub fn contains_key(&self, key: &Input) -> bool {
        for own_key in &self.keys {
            if key == own_key {
                return true;
            }
        }
        false
    }

    pub fn is_subset_of(&self, other: &InputSet) -> bool {
        if self.keys.len() >= other.keys.len() {
            return false;
        }
        for key in &other.keys {
            if !other.keys.contains(key) {
                return false;
            }
        }
        true
    }

    pub fn is_pressed(&self, edge_input: Option<Input>, state: &State) -> bool {
        // We want to account for:
        //   * `Ctrl+e` && `Ctrl+o` being activated with a single hold of the Ctrl key.
        //   * Multiple actions can run at once: e.g. if someone is holding `w` to walk
        //     forward, they should still be able to tap `e` to use.
        //   * On the other hand, `Shift+e` is a superset of `e`, but should not trigger
        //     `e`'s action.
        //   * Potential weird case: what if `Ctrl+w` is walk forward and someone taps `e` to use?
        //     Should it depend on whether `Ctrl+e` is bound? I think we should disallow for now.
        //     Most apps treat modifiers as different planes of keys and I think that makes sense
        //     here as well.
        //
        // Simple solution: superset check, with special handling of modifier keys.
        for key in &self.keys {
            if Some(*key) == edge_input {
                continue;
            }
            if let Some(current_state) = state.input_states.get(key) {
                if *current_state == ElementState::Released {
                    return false;
                }
            } else {
                return false;
            }
        }

        // All matching keys (including modifiers) are pressed.
        // Also make sure we are in the same modifier plan.
        if state.modifiers_state != self.modifiers {
            return false;
        }

        true
    }
}

impl fmt::Display for InputSet {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{{")?;
        for (i, key) in self.keys.iter().enumerate() {
            if i != 0 {
                write!(f, ",")?;
            }
            write!(f, "{key:?}")?;
        }
        write!(f, "}}")
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_can_create_keys() -> Result<()> {
        assert_eq!(
            Input::from_binding("A")?,
            Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyA))
        );
        assert_eq!(
            Input::from_binding("a")?,
            Input::KeyboardKey(PhysicalKey::Code(KeyCode::KeyA))
        );
        assert_eq!(
            Input::from_binding("PageUp")?,
            Input::KeyboardKey(PhysicalKey::Code(KeyCode::PageUp))
        );
        assert_eq!(
            Input::from_binding("pageup")?,
            Input::KeyboardKey(PhysicalKey::Code(KeyCode::PageUp))
        );
        assert_eq!(
            Input::from_binding("pAgEuP")?,
            Input::KeyboardKey(PhysicalKey::Code(KeyCode::PageUp))
        );
        Ok(())
    }

    #[test]
    fn test_can_create_mouse() -> Result<()> {
        assert_eq!(
            Input::from_binding("MoUsE50")?,
            Input::MouseButton(MouseButton::Other(50))
        );
        Ok(())
    }

    #[test]
    fn test_can_create_keysets() -> Result<()> {
        assert_eq!(InputSet::from_binding("a+b")?.len(), 1);
        assert_eq!(InputSet::from_binding("Control+Win+a")?.len(), 4);
        assert_eq!(InputSet::from_binding("Control+b+Shift")?.len(), 4);
        Ok(())
    }
}
