// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::{bindings::Bindings, input::Input, input_focus::InputFocus};
use anyhow::{bail, Result};
use bevy_ecs::prelude::*;
use mantle::{InputEvent, InputEvents};
use nitrous::{inject_nitrous_resource, method, Dict, NitrousResource, Value};
use ordered_float::OrderedFloat;
use runtime::{Extension, Runtime, ScriptHerder};
use std::{collections::HashMap, fmt::Debug, str::FromStr};
use winit::{
    event::ElementState,
    keyboard::{KeyCode, ModifiersState, PhysicalKey},
};

#[derive(Debug, Default)]
pub struct State {
    pub(crate) modifiers_state: ModifiersState,
    pub(crate) input_states: HashMap<Input, ElementState>,
}

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum EventMapperStep {
    HandleEvents,
}

#[derive(Default, Debug, NitrousResource)]
#[resource(name = "bindings")]
pub struct EventMapper {
    bindings: HashMap<InputFocus, Bindings>,
    state: State,
    terminal_active: bool,
    input_focus: InputFocus,
}

impl Extension for EventMapper {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.inject_resource(EventMapper::new())?;
        runtime
            .add_input_system(Self::sys_handle_input_events.in_set(EventMapperStep::HandleEvents));
        Ok(())
    }
}

#[inject_nitrous_resource]
impl EventMapper {
    pub fn new() -> Self {
        Self {
            bindings: HashMap::new(),
            state: State::default(),
            terminal_active: false,
            input_focus: InputFocus::default(),
        }
    }

    pub fn bind_in_focus(
        &mut self,
        focus: InputFocus,
        event_name: &str,
        script_raw: &str,
    ) -> Result<()> {
        let bindings = self.bindings.entry(focus.clone()).or_default();
        bindings.bind(event_name, script_raw)?;
        Ok(())
    }

    #[method]
    pub fn bind_in(&mut self, focus_name: &str, event_name: &str, script_raw: &str) -> Result<()> {
        let focus = match InputFocus::from_str(focus_name) {
            Ok(focus) => focus,
            Err(e) => bail!("{:?}", e),
        };
        self.bind_in_focus(focus, event_name, script_raw)
    }

    #[method]
    pub fn bind(&mut self, event_name: &str, script_raw: &str) -> Result<()> {
        self.bind_in_focus(InputFocus::default(), event_name, script_raw)
    }

    #[method]
    pub fn list_in(&mut self, focus_name: &str) -> Result<Dict> {
        let mut out = Dict::default();
        let focus = match InputFocus::from_str(focus_name) {
            Ok(focus) => focus,
            Err(e) => bail!("{:?}", e),
        };
        if let Some(bindings) = self.bindings.get(&focus) {
            for (key, scripts) in bindings.iter() {
                out.insert(
                    key.to_string(),
                    Value::String(
                        scripts
                            .first()
                            .map(|v| v.to_string())
                            .or_else(|| Some(String::new()))
                            .unwrap(),
                    ),
                );
            }
        }
        Ok(out)
    }

    // #[method]
    // pub fn show_active(&self) -> String {
    //     format!("{:#?}", self.state.active_chords)
    // }

    #[method]
    pub fn terminal_active(&self) -> bool {
        self.terminal_active
    }

    #[method]
    pub fn set_terminal_active(&mut self, active: bool) {
        self.terminal_active = active;
    }

    #[method]
    pub fn toggle_terminal(&mut self) {
        self.terminal_active = !self.terminal_active;
    }

    pub fn input_focus(&self) -> &InputFocus {
        &self.input_focus
    }

    pub fn set_input_focus(&mut self, focus: InputFocus) {
        self.input_focus = focus;
    }

    #[method]
    pub fn current_focus(&self) -> String {
        self.input_focus.to_string()
    }

    #[method]
    pub fn set_focus(&mut self, focus: &str) -> Result<()> {
        self.set_input_focus(InputFocus::from_str(focus)?);
        Ok(())
    }

    pub fn sys_handle_input_events(
        events: Res<InputEvents>,
        mut herder: ResMut<ScriptHerder>,
        mut mapper: ResMut<EventMapper>,
    ) {
        mapper
            .handle_events(&events, &mut herder)
            .expect("EventMapper::handle_events");
    }

    pub fn handle_events(&mut self, events: &InputEvents, herder: &mut ScriptHerder) -> Result<()> {
        for event in events.iter() {
            self.handle_event(event, herder)?;
        }
        Ok(())
    }

    fn handle_event(&mut self, event: &InputEvent, herder: &mut ScriptHerder) -> Result<()> {
        let input = Input::from_event(event);
        if input.is_none() {
            return Ok(());
        }
        let input = input.unwrap();

        if self.is_toggle_terminal_event(event) {
            self.toggle_terminal();
        }

        let mut variables = HashMap::with_capacity(8);
        variables.insert("window_focused", Value::Boolean(event.is_window_focused()));

        if let Some(press_state) = event.press_state() {
            self.state.input_states.insert(input, press_state);
            // Note: pressed variable is set later, since we need to disable masked input sets.
        }

        if let Some(modifiers_state) = event.modifiers_state() {
            self.state.modifiers_state = modifiers_state;
            variables.insert("shift_pressed", Value::Boolean(modifiers_state.shift_key()));
            variables.insert("alt_pressed", Value::Boolean(modifiers_state.alt_key()));
            variables.insert(
                "ctrl_pressed",
                Value::Boolean(modifiers_state.control_key()),
            );
            variables.insert("logo_pressed", Value::Boolean(modifiers_state.super_key()));
        }

        // Break *after* maintaining state.
        if self.terminal_active() {
            return Ok(());
        }

        // Collect variables to inject.
        match event {
            InputEvent::MouseMotion {
                dx, dy, in_window, ..
            } => {
                variables.insert("dx", Value::Float(OrderedFloat(*dx)));
                variables.insert("dy", Value::Float(OrderedFloat(*dy)));
                variables.insert("in_window", Value::Boolean(*in_window));
            }
            InputEvent::CursorMove {
                pixel_position,
                in_window,
                ..
            } => {
                variables.insert("cursor_x", Value::Float(OrderedFloat(pixel_position.0)));
                variables.insert("cursor_y", Value::Float(OrderedFloat(pixel_position.1)));
                variables.insert("in_window", Value::Boolean(*in_window));
            }
            InputEvent::MouseWheel {
                horizontal_delta,
                vertical_delta,
                in_window,
                ..
            } => {
                variables.insert(
                    "horizontal_delta",
                    Value::Float(OrderedFloat(*horizontal_delta)),
                );
                variables.insert(
                    "vertical_delta",
                    Value::Float(OrderedFloat(*vertical_delta)),
                );
                variables.insert("in_window", Value::Boolean(*in_window));
            }
            InputEvent::DeviceAdded { dummy } => {
                variables.insert("device_id", Value::Integer(*dummy as i64));
            }
            InputEvent::DeviceRemoved { dummy } => {
                variables.insert("device_id", Value::Integer(*dummy as i64));
            }
            // FIXME: set variables for button state, key state, joy state, etc
            _ => {}
        }

        let locals = variables.into();
        for (focus, bindings) in self.bindings.iter_mut() {
            if *focus != self.input_focus {
                bindings.match_input(
                    input,
                    event.press_state(),
                    &self.state,
                    &locals,
                    &mut None,
                )?;
            } else {
                bindings.match_input(
                    input,
                    event.press_state(),
                    &self.state,
                    &locals,
                    &mut Some(herder),
                )?;
            }
        }

        Ok(())
    }

    fn is_toggle_terminal_event(&self, event: &InputEvent) -> bool {
        if let InputEvent::KeyboardKey {
            physical_key: key,
            press_state,
            modifiers_state,
            ..
        } = event
        {
            if self.terminal_active && *key == PhysicalKey::Code(KeyCode::Escape)
                || *key == PhysicalKey::Code(KeyCode::Backquote)
                    && *modifiers_state == ModifiersState::CONTROL
                    && *press_state == ElementState::Pressed
            {
                return true;
            }
        }
        false
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use mantle::{test_make_input_events as mkinp, InputEvent};
    use nitrous::{inject_nitrous_resource, method, NitrousResource};
    use runtime::Runtime;
    use winit::keyboard::KeyLocation;

    #[derive(Debug, Default, NitrousResource)]
    struct Player {
        walking: bool,
        running: bool,
    }

    #[inject_nitrous_resource]
    impl Player {
        #[method]
        fn walk(&mut self, pressed: bool) {
            self.walking = pressed;
        }

        #[method]
        fn run(&mut self, pressed: bool) {
            self.running = pressed;
        }
    }

    fn press(key: KeyCode, ms: &mut ModifiersState) -> InputEvent {
        match key {
            KeyCode::ShiftLeft | KeyCode::ShiftRight => *ms |= ModifiersState::SHIFT,
            KeyCode::AltLeft | KeyCode::AltRight => *ms |= ModifiersState::ALT,
            KeyCode::ControlLeft | KeyCode::ControlRight => *ms |= ModifiersState::CONTROL,
            KeyCode::SuperLeft | KeyCode::SuperRight => *ms |= ModifiersState::SUPER,
            _ => {}
        }
        InputEvent::KeyboardKey {
            physical_key: PhysicalKey::Code(key),
            location: KeyLocation::Standard,
            press_state: ElementState::Pressed,
            modifiers_state: *ms,
            window_focused: true,
        }
    }

    fn release(key: KeyCode, ms: &mut ModifiersState) -> InputEvent {
        match key {
            KeyCode::ShiftLeft | KeyCode::ShiftRight => ms.remove(ModifiersState::SHIFT),
            KeyCode::AltLeft | KeyCode::AltRight => ms.remove(ModifiersState::ALT),
            KeyCode::ControlLeft | KeyCode::ControlRight => ms.remove(ModifiersState::CONTROL),
            KeyCode::SuperLeft | KeyCode::SuperRight => ms.remove(ModifiersState::SUPER),
            _ => {}
        }
        InputEvent::KeyboardKey {
            physical_key: PhysicalKey::Code(key),
            location: KeyLocation::Standard,
            press_state: ElementState::Released,
            modifiers_state: *ms,
            window_focused: true,
        }
    }

    fn prepare() -> Result<Runtime> {
        let mut runtime = Runtime::new()?;
        runtime
            .inject_resource(Player::default())?
            .load_extension::<EventMapper>()?;
        runtime.resource_mut::<ScriptHerder>().run_string(
            r#"
                bindings.bind("+w", "player.walk($pressed)");
                bindings.bind("+shift+w", "player.run($pressed)");
            "#,
        )?;
        runtime.run_startup();
        Ok(runtime)
    }

    #[test]
    #[allow(clippy::bool_assert_comparison)]
    fn test_basic() -> Result<()> {
        let mut runtime = prepare()?;
        let mut state = ModifiersState::empty();
        let ms = &mut state;

        runtime.inject_resource(mkinp(vec![press(KeyCode::KeyW, ms)]))?;
        runtime.run_sim_once();
        assert_eq!(runtime.resource::<Player>().walking, true);
        runtime.remove_resource::<InputEvents>();
        runtime.inject_resource(mkinp(vec![release(KeyCode::KeyW, ms)]))?;
        runtime.run_sim_once();
        assert_eq!(runtime.resource::<Player>().walking, false);
        runtime.remove_resource::<InputEvents>();
        runtime.inject_resource(mkinp(vec![
            press(KeyCode::ShiftLeft, ms),
            press(KeyCode::KeyW, ms),
        ]))?;
        runtime.run_sim_once();
        assert_eq!(runtime.resource::<Player>().walking, false);
        assert_eq!(runtime.resource::<Player>().running, true);
        runtime.remove_resource::<InputEvents>();
        runtime.inject_resource(mkinp(vec![
            release(KeyCode::ShiftLeft, ms),
            release(KeyCode::KeyW, ms),
        ]))?;
        runtime.run_sim_once();
        assert_eq!(runtime.resource::<Player>().walking, false);
        assert_eq!(runtime.resource::<Player>().running, false);
        runtime.remove_resource::<InputEvents>();

        Ok(())
    }

    #[test]
    #[allow(clippy::bool_assert_comparison)]
    fn test_modifier_planes() -> Result<()> {
        let mut runtime = prepare()?;
        let mut state = ModifiersState::empty();
        let ms = &mut state;

        // modifier planes should mask pressed keys
        runtime.inject_resource(mkinp(vec![press(KeyCode::KeyW, ms)]))?;
        runtime.run_sim_once();
        assert_eq!(runtime.resource::<Player>().walking, true);
        assert_eq!(runtime.resource::<Player>().running, false);
        runtime.remove_resource::<InputEvents>();
        runtime.inject_resource(mkinp(vec![press(KeyCode::ShiftLeft, ms)]))?;
        runtime.run_sim_once();
        assert_eq!(runtime.resource::<Player>().walking, false);
        assert_eq!(runtime.resource::<Player>().running, true);
        runtime.remove_resource::<InputEvents>();
        runtime.inject_resource(mkinp(vec![release(KeyCode::ShiftLeft, ms)]))?;
        runtime.run_sim_once();
        assert_eq!(runtime.resource::<Player>().walking, true);
        assert_eq!(runtime.resource::<Player>().running, false);
        runtime.remove_resource::<InputEvents>();
        runtime.inject_resource(mkinp(vec![release(KeyCode::KeyW, ms)]))?;
        runtime.run_sim_once();
        assert_eq!(runtime.resource::<Player>().walking, false);
        assert_eq!(runtime.resource::<Player>().running, false);
        runtime.remove_resource::<InputEvents>();

        Ok(())
    }

    #[test]
    #[ignore]
    #[allow(clippy::bool_assert_comparison)]
    fn test_exact_modifer_matching() -> Result<()> {
        env_logger::init();
        let mut runtime = prepare()?;
        let mut state = ModifiersState::empty();
        let ms = &mut state;

        // Match modifier planes exactly
        // FIXME: the control does not mask properly because we don't visit it because it's not
        //        part of any binding... which is not quite right.
        runtime.inject_resource(mkinp(vec![
            press(KeyCode::KeyW, ms),
            press(KeyCode::ShiftLeft, ms),
            press(KeyCode::ControlLeft, ms),
        ]))?;
        runtime.run_sim_once();
        println!("MS: {ms:?}");
        assert_eq!(runtime.resource::<Player>().walking, false);
        assert_eq!(runtime.resource::<Player>().running, false);
        runtime.inject_resource(mkinp(vec![release(KeyCode::ControlLeft, ms)]))?;
        runtime.run_sim_once();
        assert_eq!(runtime.resource::<Player>().walking, false);
        assert_eq!(runtime.resource::<Player>().running, true);
        runtime.remove_resource::<InputEvents>();
        runtime.inject_resource(mkinp(vec![release(KeyCode::ShiftLeft, ms)]))?;
        runtime.run_sim_once();
        assert_eq!(runtime.resource::<Player>().walking, true);
        assert_eq!(runtime.resource::<Player>().running, false);
        runtime.remove_resource::<InputEvents>();
        runtime.inject_resource(mkinp(vec![release(KeyCode::KeyW, ms)]))?;
        runtime.run_sim_once();
        assert_eq!(runtime.resource::<Player>().walking, false);
        assert_eq!(runtime.resource::<Player>().running, false);
        runtime.remove_resource::<InputEvents>();

        Ok(())
    }

    /*
    #[test]
    fn test_masking() -> Result<()> {
        let mut runtime = Runtime::default();
        // let mut interpreter = Interpreter::default();
        let player = Arc::new(RwLock::new(Player::default()));
        // interpreter.put_global("player", Value::Module(player.clone()));

        let w_key = Input::KeyboardKey(VirtualKeyCode::W);
        let shift_key = Input::KeyboardKey(VirtualKeyCode::LShift);

        let mut state: State = Default::default();
        let bindings = Bindings::new("test")
            .with_bind("w", "player.walk(pressed)")?
            .with_bind("shift+w", "player.run(pressed)")?;

        state.input_states.insert(w_key, ElementState::Pressed);
        bindings.match_input(
            w_key,
            Some(ElementState::Pressed),
            &mut state,
            &LocalNamespace::empty(),
            &mut runtime.resource_mut::<ScriptHerder>(),
        )?;
        runtime.run_sim_once();
        assert!(player.read().walking);
        assert!(!player.read().running);

        state.input_states.insert(shift_key, ElementState::Pressed);
        state.modifiers_state |= ModifiersState::SHIFT;
        bindings.match_input(
            shift_key,
            Some(ElementState::Pressed),
            &mut state,
            &LocalNamespace::empty(),
            &mut runtime.resource_mut::<ScriptHerder>(),
        )?;
        assert!(player.read().running);
        assert!(!player.read().walking);

        state.input_states.insert(shift_key, ElementState::Released);
        state.modifiers_state -= ModifiersState::SHIFT;
        bindings.match_input(
            shift_key,
            Some(ElementState::Released),
            &mut state,
            &LocalNamespace::empty(),
            &mut runtime.resource_mut::<ScriptHerder>(),
        )?;
        assert!(player.read().walking);
        assert!(!player.read().running);

        state.input_states.insert(shift_key, ElementState::Pressed);
        state.modifiers_state |= ModifiersState::SHIFT;
        bindings.match_input(
            shift_key,
            Some(ElementState::Pressed),
            &mut state,
            &LocalNamespace::empty(),
            &mut runtime.resource_mut::<ScriptHerder>(),
        )?;
        assert!(!player.read().walking);
        assert!(player.read().running);

        state.input_states.insert(w_key, ElementState::Released);
        bindings.match_input(
            w_key,
            Some(ElementState::Released),
            &mut state,
            &LocalNamespace::empty(),
            &mut runtime.resource_mut::<ScriptHerder>(),
        )?;
        assert!(!player.read().walking);
        assert!(!player.read().running);

        state.input_states.insert(w_key, ElementState::Pressed);
        bindings.match_input(
            w_key,
            Some(ElementState::Pressed),
            &mut state,
            &LocalNamespace::empty(),
            &mut runtime.resource_mut::<ScriptHerder>(),
        )?;
        assert!(!player.read().walking);
        assert!(player.read().running);

        Ok(())
    }
     */
}
