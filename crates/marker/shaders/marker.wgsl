// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <orrery/include/orrery.wgsl>
//!include <camera/include/camera.wgsl>

//!bindings [0] <orrery/bindings/orrery.wgsl>
//!bindings [1] <camera/bindings/camera.wgsl>

struct VertexOutput {
    @location(0) color: vec4<f32>,
    @location(1) normal: vec4<f32>,
    @builtin(position) position: vec4<f32>,
}

@vertex
fn main_vert(
    @location(0) position: vec3<f32>,
    @location(1) normal: vec4<f32>,
    @location(2) color: vec4<f32>
) -> VertexOutput {
    if normal.w > 0. {
        return VertexOutput(color, normal, vec4<f32>(position, 1.));
    }
    return VertexOutput(color, normal, camera.perspective_m * vec4<f32>(position, 1.));
}

struct FragmentOutput {
    @location(0) f_color: vec4<f32>,
}

@fragment
fn main_frag(
    @location(0) color: vec4<f32>,
    @location(1) normal: vec4<f32>
) -> FragmentOutput {
    if normal.w > 0. {
        return FragmentOutput(vec4<f32>(color.xyz, 1.));
    }
    let ambient = 0.3 * color.xyz;
    let diffuse = 0.7 * (color.xyz * dot(orrery.sun_direction.xyz, normal.xyz));
    let specular = 1.0 * (vec3(1.)
        * clamp(
           pow(dot(camera.forward.xyz,
                reflect(orrery.sun_direction.xyz, normal.xyz)),
           6.),
          0., 1.));
    let clr = ambient + diffuse + specular;
    return FragmentOutput(vec4<f32>(clr, 1.));
}
