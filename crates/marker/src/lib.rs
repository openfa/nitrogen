// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
mod vertex;

use crate::vertex::{CpuMarkerVertex, GpuMarkerVertex};
use absolute_unit::{scalar, Length, Meters, Pt3};
use anyhow::Result;
use bevy_ecs::prelude::*;
use camera::{CameraStep, ScreenCamera};
use composite::CompositeStep;
use geometry::{
    intersect::{intersect_plane_segment, PlaneSegmentIntersect},
    Aabb3, Arrow, Cylinder, Plane, Primitive, RenderPrimitive, Segment, Sphere, TriMesh,
};
use glam::{DVec3, Vec2, Vec4Swizzles};
use gui::GuiStep;
use log::warn;
use mantle::{CurrentEncoder, Gpu, GpuStep, Window};
use nitrous::{inject_nitrous_resource, NitrousResource};
use orrery::Orrery;
use phase::Frame;
use planck::Color;
use rapier3d_f64::parry::shape::TypedShape;
use runtime::{Extension, Runtime};
use stars::StarsStep;
use std::{f32::consts::TAU, mem, ops::Range};

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum MarkersStep {
    UploadGeometry,
    Render,
}

/// Display points and vectors in the world for debugging purposes.
#[derive(NitrousResource)]
pub struct Markers {
    pipeline: wgpu::RenderPipeline,
    screen_points: Vec<(Pt3<Meters>, f32, Color)>,
    screen_lines: Vec<(Pt3<Meters>, Pt3<Meters>, f32, Color)>,
    cpu_vertices: Vec<CpuMarkerVertex>,
    gpu_vert_buffer: wgpu::Buffer,
    vertex_count: u32,
    cpu_indices: Vec<u32>,
    gpu_indices: wgpu::Buffer,
    index_count: u32,
}

impl Extension for Markers {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.inject_resource(Markers::new(
            runtime.resource::<Orrery>(),
            runtime.resource::<Gpu>(),
        ))?;
        runtime.add_frame_systems((
            Self::sys_upload_geometry
                .in_set(MarkersStep::UploadGeometry)
                .after(GpuStep::CreateCommandEncoder)
                .after(CameraStep::MoveCameraToFrame)
                // Note: pin encoder ordering so we get fixed inter-run behavior on serial systems.
                .after(CameraStep::UploadToGpu)
                .before(GuiStep::EndFrame)
                .before(StarsStep::UpdateHourAngle),
            Self::sys_render_markers
                .in_set(MarkersStep::Render)
                .after(MarkersStep::UploadGeometry)
                .before(CompositeStep::Render)
                .before(GpuStep::SubmitCommands)
                // Note: pin encoder ordering so we get fixed inter-run behavior on serial systems.
                .before(GuiStep::EndFrame)
                .before(StarsStep::UpdateHourAngle),
        ));

        Ok(())
    }
}

#[inject_nitrous_resource]
impl Markers {
    const MAX_VERTICES: usize = 262144;
    const MAX_INDICES: usize = 262144 * 2;

    fn new(orrery: &Orrery, gpu: &Gpu) -> Self {
        let pipeline_layout =
            gpu.device()
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: Some("marker-render-pipeline-layout"),
                    push_constant_ranges: &[],
                    bind_group_layouts: &[
                        orrery.bind_group_layout(),
                        &ScreenCamera::info_bind_group_layout(gpu),
                    ],
                });
        let shader = gpu.compile_shader("marker.wgsl", include_str!("../target/marker.wgsl"));
        let pipeline = gpu
            .device()
            .create_render_pipeline(&wgpu::RenderPipelineDescriptor {
                label: Some("marker-render-pipeline"),
                layout: Some(&pipeline_layout),
                vertex: wgpu::VertexState {
                    module: &shader,
                    entry_point: "main_vert",
                    buffers: &[GpuMarkerVertex::descriptor()],
                    compilation_options: wgpu::PipelineCompilationOptions {
                        constants: &Default::default(),
                        zero_initialize_workgroup_memory: false,
                    },
                },
                fragment: Some(wgpu::FragmentState {
                    module: &shader,
                    entry_point: "main_frag",
                    targets: &[Some(wgpu::ColorTargetState {
                        format: ScreenCamera::COLOR_FORMAT,
                        blend: Some(wgpu::BlendState {
                            color: wgpu::BlendComponent {
                                src_factor: wgpu::BlendFactor::SrcAlpha,
                                dst_factor: wgpu::BlendFactor::OneMinusSrcAlpha,
                                operation: wgpu::BlendOperation::Add,
                            },
                            alpha: wgpu::BlendComponent::REPLACE,
                        }),
                        write_mask: wgpu::ColorWrites::ALL,
                    })],
                    compilation_options: wgpu::PipelineCompilationOptions {
                        constants: &Default::default(),
                        zero_initialize_workgroup_memory: false,
                    },
                }),
                primitive: wgpu::PrimitiveState {
                    topology: wgpu::PrimitiveTopology::TriangleList,
                    strip_index_format: None,
                    front_face: wgpu::FrontFace::Cw,
                    cull_mode: Some(wgpu::Face::Back),
                    unclipped_depth: true,
                    polygon_mode: wgpu::PolygonMode::Fill,
                    conservative: false,
                },
                depth_stencil: Some(wgpu::DepthStencilState {
                    format: ScreenCamera::DEPTH_FORMAT,
                    depth_write_enabled: true,
                    depth_compare: wgpu::CompareFunction::GreaterEqual,
                    stencil: wgpu::StencilState {
                        front: wgpu::StencilFaceState::IGNORE,
                        back: wgpu::StencilFaceState::IGNORE,
                        read_mask: 0,
                        write_mask: 0,
                    },
                    bias: wgpu::DepthBiasState {
                        constant: 0,
                        slope_scale: 0.0,
                        clamp: 0.0,
                    },
                }),
                multisample: wgpu::MultisampleState {
                    count: 1,
                    mask: !0,
                    alpha_to_coverage_enabled: false,
                },
                multiview: None,
            });

        Self {
            pipeline,
            screen_points: Vec::new(),
            screen_lines: Vec::new(),
            cpu_vertices: Vec::new(),
            gpu_vert_buffer: gpu.device().create_buffer(&wgpu::BufferDescriptor {
                label: "markers-vertex-buffer".into(),
                size: (mem::size_of::<GpuMarkerVertex>() * Self::MAX_VERTICES)
                    as wgpu::BufferAddress,
                usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::VERTEX,
                mapped_at_creation: false,
            }),
            vertex_count: 0,
            cpu_indices: Vec::new(),
            gpu_indices: gpu.device().create_buffer(&wgpu::BufferDescriptor {
                label: "markers-index-buffer".into(),
                size: (mem::size_of::<u32>() * Self::MAX_INDICES) as wgpu::BufferAddress,
                usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::INDEX,
                mapped_at_creation: false,
            }),
            index_count: 0,
        }
    }

    pub fn draw_point(&mut self, position: Pt3<Meters>, radius: Length<Meters>, color: Color) {
        self.draw_sphere(&Sphere::from_center_and_radius(position, radius), color);
    }

    pub fn draw_sphere(&mut self, prim: &Sphere<Meters>, color: Color) {
        self.draw_prim_imm(&prim.to_primitive(1), color);
    }

    pub fn draw_mesh(&mut self, prim: &TriMesh<Meters>, color: Color) {
        self.draw_prim_imm(&prim.to_primitive(1), color);
    }

    pub fn draw_local_collision(&mut self, frame: &Frame, shape: &TypedShape, color: Color) {
        let mut mesh = match shape {
            TypedShape::Ball(ball) => TriMesh::from_ball(ball, 5),
            TypedShape::Cuboid(cube) => TriMesh::from_cuboid(cube),
            TypedShape::Capsule(capsule) => TriMesh::from_parry(capsule.to_trimesh(5, 5)),
            TypedShape::HeightField(heightfield) => TriMesh::from_heightfield(heightfield),
            TypedShape::Compound(compound) => {
                for (iso, part) in compound.shapes() {
                    let inner = frame.combine(&iso.into());
                    self.draw_local_collision(&inner, &part.as_typed_shape(), color);
                }
                return;
            }
            _ => {
                println!("don't know how to draw a {:?}", shape);
                warn!("don't know how to draw a {:?}", shape);
                return;
            }
        };
        mesh.transform_by(frame.parts());
        self.draw_mesh(&mesh, color);
    }

    pub fn draw_frame(&mut self, frame: Frame, length: Length<Meters>) {
        let radius = length / scalar!(10);
        let basis = frame.basis();
        let right = Arrow::new(frame.position(), basis.right, length, radius);
        let up = Arrow::new(frame.position(), basis.up, length, radius);
        let fwd = Arrow::new(frame.position(), basis.forward, length, radius);
        self.draw_prim_imm(&right.to_primitive(8), Color::RED);
        self.draw_prim_imm(&up.to_primitive(8), Color::GREEN);
        self.draw_prim_imm(&fwd.to_primitive(8), Color::BLUE);
    }

    pub fn draw_cylinder(
        &mut self,
        color: Color,
        pt1: Pt3<Meters>,
        pt2: Pt3<Meters>,
        radius: Length<Meters>,
    ) {
        let cyl = Cylinder::new(
            pt1,
            pt1.to(pt2).dvec3().normalize(),
            pt1.to(pt2).magnitude(),
            radius,
        );
        self.draw_prim_imm(&cyl.to_primitive(8), color);
    }

    /// Draw an arrow that is already in world space.
    pub fn draw_arrow(&mut self, color: Color, arrow: &Arrow<Meters>) {
        self.draw_prim_imm(&arrow.to_primitive(8), color);
    }

    /// Draw a polygon in world space.
    pub fn draw_polygon(&mut self, color: Color, points: &[Pt3<Meters>], normal: &DVec3) {
        for i in 2..points.len() {
            let js = [0, i - 1, i];
            for j in &js {
                self.cpu_indices.push(self.cpu_vertices.len() as u32);
                self.cpu_vertices
                    .push(CpuMarkerVertex::new(points[*j].dvec3(), *normal, color));
            }
        }
    }

    /// Draw a proxy for a plane, around the given point with the given extent.
    pub fn draw_plane_proxy(
        &mut self,
        color: Color,
        origin: &Pt3<Meters>,
        plane: &Plane<Meters>,
        radius: Length<Meters>,
    ) {
        let center = plane.closest_point_on_plane(origin);
        let (pdv_a, pdv_b) = plane.normal().any_orthonormal_pair();
        let mut pts = [
            plane.closest_point_on_plane(&(center + pdv_a * 2. * radius)),
            plane.closest_point_on_plane(&(center + pdv_b * 2. * radius)),
            plane.closest_point_on_plane(&(center - pdv_a * 2. * radius)),
            plane.closest_point_on_plane(&(center - pdv_b * 2. * radius)),
        ];
        let back_color = color.inverse();
        self.draw_polygon(color, &pts, plane.normal());
        pts.reverse();
        self.draw_polygon(back_color, &pts, plane.normal());
    }

    /// Draw a (local) bounding box, translated into the given world frame.
    pub fn draw_local_bounding_box(
        &mut self,
        color: Color,
        ext_color: Color,
        aabb: &Aabb3<Meters>,
        radius: Length<Meters>,
        frame: &Frame,
    ) {
        let p = frame.position();
        let r = frame.facing();
        let lo = *aabb.lo();
        let hi = *aabb.hi();
        let v000 = p + r * lo;
        let v001 = p + r * Pt3::new(lo.x(), lo.y(), hi.z());
        let v010 = p + r * Pt3::new(lo.x(), hi.y(), lo.z());
        let v011 = p + r * Pt3::new(lo.x(), hi.y(), hi.z());
        let v100 = p + r * Pt3::new(hi.x(), lo.y(), lo.z());
        let v101 = p + r * Pt3::new(hi.x(), lo.y(), hi.z());
        let v110 = p + r * Pt3::new(hi.x(), hi.y(), lo.z());
        let v111 = p + r * hi;
        self.draw_point(v000, radius * scalar!(2), ext_color);
        self.draw_point(v111, radius * scalar!(2), ext_color);
        self.draw_cylinder(color, v000, v001, radius);
        self.draw_cylinder(color, v000, v010, radius);
        self.draw_cylinder(color, v000, v011, radius);
        self.draw_cylinder(color, v000, v100, radius);
        self.draw_cylinder(color, v000, v101, radius);
        self.draw_cylinder(color, v000, v110, radius);
        self.draw_cylinder(color, v001, v010, radius);
        self.draw_cylinder(color, v001, v011, radius);
        self.draw_cylinder(color, v001, v100, radius);
        self.draw_cylinder(color, v001, v101, radius);
        self.draw_cylinder(color, v010, v011, radius);
        self.draw_cylinder(color, v010, v100, radius);
        self.draw_cylinder(color, v010, v110, radius);
        self.draw_cylinder(color, v011, v101, radius);
        self.draw_cylinder(color, v011, v110, radius);
        self.draw_cylinder(color, v100, v101, radius);
        self.draw_cylinder(color, v101, v110, radius);
        self.draw_cylinder(color, v110, v100, radius);
        self.draw_cylinder(color, v111, v001, radius);
        self.draw_cylinder(color, v111, v010, radius);
        self.draw_cylinder(color, v111, v011, radius);
        self.draw_cylinder(color, v111, v100, radius);
        self.draw_cylinder(color, v111, v101, radius);
        self.draw_cylinder(color, v111, v110, radius);
    }

    fn draw_prim_imm(&mut self, prim: &Primitive, color: Color) {
        let base = self.cpu_vertices.len() as u32;
        for vert in &prim.verts {
            self.cpu_vertices
                .push(CpuMarkerVertex::new(vert.position, vert.normal, color));
        }
        for face in &prim.faces {
            self.cpu_indices.push(base + face.index0);
            self.cpu_indices.push(base + face.index1);
            self.cpu_indices.push(base + face.index2);
        }
    }

    /// Draw a circle on the screen to cover a world space coordinate.
    pub fn draw_screen_point(&mut self, position: Pt3<Meters>, radius: f32, color: Color) {
        self.screen_points.push((position, radius, color));
    }

    /// Draw a 2d line on the screen between two world-space coordinates.
    pub fn draw_screen_line(
        &mut self,
        pt1: Pt3<Meters>,
        pt2: Pt3<Meters>,
        radius: f32,
        color: Color,
    ) {
        self.screen_lines.push((pt1, pt2, radius, color));
    }

    fn sys_upload_geometry(
        mut markers: ResMut<Markers>,
        camera: Res<ScreenCamera>,
        gpu: Res<Gpu>,
        window: Res<Window>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) {
        if let Some(encoder) = maybe_encoder.get_encoder_mut() {
            let view = camera.world_to_eye::<Meters>();
            let perspective = camera.perspective::<Meters>();

            // Map cpu to gpu vertices
            let mut gpu_vertices = markers
                .cpu_vertices
                .drain(..)
                .map(|v| GpuMarkerVertex::new(v, &view))
                .collect::<Vec<_>>();

            let mut cpu_indices = Vec::new();
            mem::swap(&mut cpu_indices, &mut markers.cpu_indices);

            // Lower screen points
            let aspect = camera.aspect_ratio() as f32;
            let z_near = camera.z_near::<Meters>().f32();
            for (wrldp, sz, color) in markers.screen_points.drain(..) {
                let eyep = view * wrldp;
                let scrnp = perspective * eyep.dvec4(1.);
                let p0 = (scrnp.xy() / scrnp.w).as_vec2();
                let scrn_index = gpu_vertices.len() as u32;
                gpu_vertices.push(GpuMarkerVertex::new_screen(p0, z_near, color));
                let r = sz / window.width() as f32;
                const CNT: isize = 16;
                for i in 0..CNT {
                    let j = (i + 1) % CNT;
                    let a0 = i as f32 / CNT as f32 * TAU;
                    let a1 = j as f32 / CNT as f32 * TAU;
                    let p1 = Vec2::new(p0.x + r * a0.sin(), p0.y + r * a0.cos() * aspect);
                    let p2 = Vec2::new(p0.x + r * a1.sin(), p0.y + r * a1.cos() * aspect);
                    let base = gpu_vertices.len() as u32;
                    gpu_vertices.push(GpuMarkerVertex::new_screen(p1, z_near, color));
                    gpu_vertices.push(GpuMarkerVertex::new_screen(p2, z_near, color));
                    cpu_indices.push(scrn_index);
                    cpu_indices.push(base);
                    cpu_indices.push(base + 1);
                }
            }

            // Lower screen lines
            let rear = Plane::<Meters>::xy();
            let rear_off = scalar!(0.1) * rear.normal_v3();
            for (pt0, pt1, sz, color) in markers.screen_lines.drain(..) {
                let mut eye0 = view * pt0;
                let mut eye1 = view * pt1;

                // We lose the plot if w goes negative: e.g. we need to clip.
                // For our particular infinite, inverted projection, it is easier
                // to clip in world space. Our only constraint at this point is
                // that everything needs to be in front of the camera.
                let segment = Segment::new(eye0, eye1);
                match intersect_plane_segment(&rear, &segment) {
                    PlaneSegmentIntersect::Below { .. } => {
                        // Skip render if both behind camera
                        continue;
                    }
                    PlaneSegmentIntersect::Intersect {
                        intersect,
                        start_is_in_front,
                        ..
                    } => {
                        // Crosses frustum, so clip to intersection.
                        if start_is_in_front {
                            eye1 = intersect + rear_off;
                        } else {
                            eye0 = intersect + rear_off;
                        }
                    }
                    PlaneSegmentIntersect::Coplanar { .. } => {
                        eye0 = eye0 + rear_off;
                        eye1 = eye1 + rear_off;
                    }
                    _ => {}
                }

                // We can now do perspective divide safely
                let scrn0 = perspective * eye0.dvec4(1.);
                let scrn1 = perspective * eye1.dvec4(1.);
                let p0 = (scrn0.xy() / scrn0.w).as_vec2();
                let p1 = (scrn1.xy() / scrn1.w).as_vec2();

                let r = sz / window.width() as f32;
                let mut perp = (p1 - p0).normalize().perp();
                perp.y *= aspect;
                let base = gpu_vertices.len() as u32;
                gpu_vertices.push(GpuMarkerVertex::new_screen(p0 + perp * r, z_near, color));
                gpu_vertices.push(GpuMarkerVertex::new_screen(p1 + perp * r, z_near, color));
                gpu_vertices.push(GpuMarkerVertex::new_screen(p1 - perp * r, z_near, color));
                gpu_vertices.push(GpuMarkerVertex::new_screen(p0 - perp * r, z_near, color));
                for i in [0, 1, 2, 0, 2, 3] {
                    cpu_indices.push(base + i);
                }
            }

            let vertex_range = 0..gpu_vertices.len().min(Self::MAX_VERTICES);
            markers.vertex_count = vertex_range.end as u32;
            let vertex_range = 0..markers.vertex_count as usize;
            gpu.upload_slice_to(
                &gpu_vertices[vertex_range],
                &markers.gpu_vert_buffer,
                encoder,
            );
            let index_range = 0..cpu_indices.len().min(Self::MAX_INDICES);
            markers.index_count = index_range.end as u32;
            gpu.upload_slice_to(&cpu_indices[index_range], &markers.gpu_indices, encoder);
        }
        markers.cpu_vertices.clear();
        markers.cpu_indices.clear();
    }

    fn sys_render_markers(
        markers: Res<Markers>,
        orrery: Res<Orrery>,
        gpu: Res<Gpu>,
        mut camera: ResMut<ScreenCamera>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) {
        camera.ensure_layer("color", ScreenCamera::COLOR_FORMAT, &gpu);
        camera.ensure_layer("depth", ScreenCamera::DEPTH_FORMAT, &gpu);
        if let Some(encoder) = maybe_encoder.get_encoder_mut() {
            let mut rpass = camera.begin_render_pass("color", Some("depth"), false, encoder);
            if markers.vertex_count > 0 {
                rpass.set_pipeline(&markers.pipeline);
                rpass.set_bind_group(0, orrery.bind_group(), &[]);
                rpass.set_bind_group(1, camera.info_bind_group(), &[]);
                rpass.set_vertex_buffer(0, markers.vertex_buffer());
                rpass.set_index_buffer(markers.index_buffer(), wgpu::IndexFormat::Uint32);
                rpass.draw_indexed(markers.index_span(), 0, 0..1);
            }
        }
    }

    fn index_span(&self) -> Range<u32> {
        0..self.index_count
    }

    fn vertex_buffer(&self) -> wgpu::BufferSlice {
        let sz = self.vertex_count as u64 * mem::size_of::<GpuMarkerVertex>() as u64;
        self.gpu_vert_buffer.slice(0..sz)
    }

    fn index_buffer(&self) -> wgpu::BufferSlice {
        let sz = self.index_count as u64 * mem::size_of::<GpuMarkerVertex>() as u64;
        self.gpu_indices.slice(0..sz)
    }
}
