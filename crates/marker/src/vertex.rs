// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use glam::{DMat4, DVec3, Vec2};
use memoffset::offset_of;
use planck::Color;
use std::mem;
use zerocopy::{FromBytes, Immutable, IntoBytes};

pub struct CpuMarkerVertex {
    position: DVec3,
    normal: DVec3,
    color: [u8; 4],
}

impl CpuMarkerVertex {
    pub(crate) fn new(position: DVec3, normal: DVec3, color: Color) -> Self {
        debug_assert!(normal.is_normalized());
        Self {
            position,
            normal,
            color: color.to_array(),
        }
    }
}

#[repr(C)]
#[derive(IntoBytes, FromBytes, Immutable, Copy, Clone, Debug)]
pub struct GpuMarkerVertex {
    position: [f32; 3],
    normal: [i8; 4],
    color: [u8; 4],
}

impl GpuMarkerVertex {
    #[allow(clippy::unneeded_field_pattern)]
    pub fn descriptor() -> wgpu::VertexBufferLayout<'static> {
        let tmp = wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<Self>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &[
                // position
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Float32x3,
                    offset: 0,
                    shader_location: 0,
                },
                // normal
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Snorm8x4,
                    offset: 12,
                    shader_location: 1,
                },
                // color
                wgpu::VertexAttribute {
                    format: wgpu::VertexFormat::Unorm8x4,
                    offset: 16,
                    shader_location: 2,
                },
            ],
        };

        assert_eq!(
            tmp.attributes[0].offset,
            offset_of!(GpuMarkerVertex, position) as wgpu::BufferAddress
        );
        assert_eq!(
            tmp.attributes[1].offset,
            offset_of!(GpuMarkerVertex, normal) as wgpu::BufferAddress
        );
        assert_eq!(
            tmp.attributes[2].offset,
            offset_of!(GpuMarkerVertex, color) as wgpu::BufferAddress
        );

        tmp
    }

    pub(crate) fn new(v: CpuMarkerVertex, view: &DMat4) -> Self {
        debug_assert!(v.normal.is_normalized());
        Self {
            position: view.transform_point3(v.position).as_vec3().to_array(),
            normal: [
                (v.normal.x * 128.) as i8,
                (v.normal.y * 128.) as i8,
                (v.normal.z * 128.) as i8,
                0,
            ],
            color: v.color,
        }
    }

    pub(crate) fn new_screen(position: Vec2, z_near: f32, color: Color) -> Self {
        Self {
            position: [position.x, position.y, z_near],
            normal: [0, 0, 0, 127],
            color: color.to_array(),
        }
    }
}
