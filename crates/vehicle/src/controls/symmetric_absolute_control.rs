// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::controls::inceptor_position_tick;
use absolute_unit::prelude::*;
use anyhow::Result;
use bevy_ecs::prelude::*;
use nitrous::{inject_nitrous_component, method, NitrousComponent};
use paste::paste;
use runtime::{Extension, Runtime, TimeStep};
use std::ops::RangeInclusive;

// Non-self-centering, symmetrical controls. e.g. for Helicopters.
macro_rules! make_sym {
    ($cls:ident, $name:expr, $up:ident, $down:ident) => {
        paste! {
            #[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
            pub enum [<$cls Step>] {
                Tick,
            }
        }

        #[derive(NitrousComponent, Debug, Copy, Clone)]
        #[component(name = $name)]
        pub struct $cls {
            max_value: f64,
            min_value: f64,
            detent: f64,
            position: f64,        // [min_value, max_value]
            key_move_target: f64, // target of move, depending on what key is held
            #[property]
            key_sensitivity: f64,
        }

        paste! {
            impl Extension for $cls {
                type Opts = ();
                fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
                    runtime.add_sim_system(Self::sys_tick.in_set([<$cls Step>]::Tick));
                    Ok(())
                }
            }
        }

        #[inject_nitrous_component]
        impl $cls {
            pub fn new<U: AngleUnit>(
                min_value: Angle<U>,
                max_value: Angle<U>,
                detent: Angle<U>,
            ) -> Self {
                Self {
                    max_value: radians!(max_value).f64(),
                    min_value: radians!(min_value).f64(),
                    detent: radians!(detent).f64(),
                    position: 0_f64,
                    key_move_target: 0_f64,
                    key_sensitivity: 2_f64,
                }
            }

            #[method]
            pub fn $up(&mut self, pressed: bool) {
                self.key_move_target = if pressed {
                    self.max_value
                } else {
                    self.position
                };
            }

            #[method]
            pub fn $down(&mut self, pressed: bool) {
                self.key_move_target = if pressed {
                    self.min_value
                } else {
                    self.position
                };
            }

            #[method]
            pub fn next_detent(&mut self) {
                // Note: investigate f64::rem_euclid if this is too lossy.
                self.key_move_target = (self.key_move_target + self.detent).min(self.max_value);
            }

            #[method]
            pub fn prev_detent(&mut self) {
                // Note: investigate f64::rem_euclid if this is too lossy.
                self.key_move_target = (self.key_move_target - self.detent).max(self.min_value);
            }

            pub(crate) fn position(&self) -> f64 {
                self.position
            }

            pub(crate) fn min_value(&self) -> f64 {
                self.min_value
            }

            pub(crate) fn max_value(&self) -> f64 {
                self.max_value
            }

            #[method]
            pub fn recenter(&mut self) {
                self.key_move_target = 0.;
            }

            pub fn set_angle<U: AngleUnit>(&mut self, position: Angle<U>) {
                self.position = radians!(position).f64();
            }

            pub fn angle<U: AngleUnit>(&self) -> Angle<U> {
                Angle::<U>::from(&radians!(self.position))
            }

            pub fn range<U: AngleUnit>(&self) -> RangeInclusive<f64> {
                let a = Angle::<U>::from(&radians!(self.min_value)).f64();
                let b = Angle::<U>::from(&radians!(self.max_value)).f64();
                a..=b
            }

            fn sys_tick(timestep: Res<TimeStep>, mut query: Query<&mut $cls>) {
                for mut inceptor in query.iter_mut() {
                    inceptor.position = inceptor_position_tick(
                        inceptor.key_move_target,
                        inceptor.min_value,
                        inceptor.max_value,
                        inceptor.key_sensitivity * timestep.fixed_sim_step().to_seconds(),
                        inceptor.position,
                    );
                }
            }
        }
    };
}

make_sym!(
    ThrustVectorPitchControl,
    "thrust_vector_pitch",
    key_move_front,
    key_move_back
);
make_sym!(
    ThrustVectorYawControl,
    "thrust_vector_yaw",
    key_move_right,
    key_move_left
);
make_sym!(VtolAngleControl, "vtol_angle", key_move_down, key_move_up);
