// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use bevy_ecs::prelude::*;
use nitrous::{inject_nitrous_component, NitrousComponent};

#[derive(Copy, Clone, Debug, Default, Eq, PartialEq)]
pub enum GearKind {
    #[default]
    Nose,
    Tail,
    Skid,
    Car,
    Tank,
}

/// Describes one leg of a landing gear
#[derive(Debug, Clone)]
pub struct GearLeg {
    // Where the wheel/skid/tread exists relative to vehicle center, at minimum deflection
    touch: Pt3<Meters>,

    // The overall length of the leg from body bottom to touch point at minimum deflection
    length: Length<Meters>,

    // The travel length of the oleo. Subtracting this from the length gives the gear leg
    // length at maximum deflection.
    travel: Length<Meters>,

    // The maximum force under which the oleo will not deflect
    min_deflection: Force<Newtons>,

    // The force at which the oleo will suffer permanent damage and stop deflecting.
    max_deflection: Force<Newtons>,

    // The force at which the leg will crumple.
    crumple_force: Force<Newtons>,

    // The side force at which the leg will be ripped off
    detach_force: Force<Newtons>,
}

impl GearLeg {
    pub fn new(
        touch: Pt3<Meters>,
        length: Length<Meters>,
        travel: Length<Meters>,
        min_deflection: Force<Newtons>,
        max_deflection: Force<Newtons>,
        crumple_force: Force<Newtons>,
        detach_force: Force<Newtons>,
    ) -> Self {
        Self {
            touch,
            length,
            travel,
            min_deflection,
            max_deflection,
            crumple_force,
            detach_force,
        }
    }

    pub fn touch(&self) -> Pt3<Meters> {
        self.touch
    }
    pub fn length(&self) -> Length<Meters> {
        self.length
    }
    pub fn travel(&self) -> Length<Meters> {
        self.travel
    }
    pub fn min_deflection(&self) -> Force<Newtons> {
        self.min_deflection
    }
    pub fn max_deflection(&self) -> Force<Newtons> {
        self.max_deflection
    }
    pub fn crumple_force(&self) -> Force<Newtons> {
        self.crumple_force
    }
    pub fn detach_force(&self) -> Force<Newtons> {
        self.detach_force
    }
}

/// Describes the landing gear (or wheels/treads/skids generally), any oleos,
/// their length and forces, gear locations, etc.
#[derive(NitrousComponent, Debug, Clone)]
pub struct GearLayout {
    kind: GearKind,
    legs: Vec<GearLeg>,
}

#[inject_nitrous_component]
impl GearLayout {
    pub fn new(kind: GearKind, legs: Vec<GearLeg>) -> Self {
        Self { kind, legs }
    }

    pub fn kind(&self) -> GearKind {
        self.kind
    }

    pub fn legs(&self) -> &[GearLeg] {
        &self.legs
    }
}
