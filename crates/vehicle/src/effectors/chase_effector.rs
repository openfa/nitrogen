// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    controls::{
        absolute_control::{AirbrakeControlStep, FlapsControlStep},
        symmetric_absolute_control::{
            ThrustVectorPitchControlStep, ThrustVectorYawControlStep, VtolAngleControlStep,
        },
        toggle_control::{BayControlStep, GearControlStep, HookControlStep},
    },
    effectors::effector_chase,
    AirbrakeControl, BayControl, FlapsControl, GearControl, HookControl, ThrustVectorPitchControl,
    ThrustVectorYawControl, VtolAngleControl,
};
use absolute_unit::prelude::*;
use anyhow::Result;
use bevy_ecs::prelude::*;
use nitrous::{inject_nitrous_component, NitrousComponent};
use paste::paste;
use runtime::{Extension, Runtime, TimeStep};
use std::time::Duration;

// Make the effector chase the control position.
// TODO: chase in ways that more closely mimic real hardware and control systems. e.g. should
//       hydraulic control feel the same as servo electric?
macro_rules! make_control_chase {
    ($cls:ident, $name:expr, $control:ident) => {
        paste! {
            #[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
            pub enum [<$cls Step>] {
                Tick,
            }
        }

        /// $cls are a simple effector that chases the control position with some velocity.
        #[derive(NitrousComponent, Debug, Copy, Clone)]
        #[component(name = $name)]
        pub struct $cls {
            position: f64,

            /// The extension time is how long in seconds it takes to go from full up to full down.
            /// This is assumed to be symmetrical for up and down actuation.
            // TODO: allow for up and down at different rates
            // TODO: support non-linear animations, such as supported by Animation
            #[property]
            extension_time: f64,
        }

        paste! {
            impl Extension for $cls {
                type Opts = ();
                fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
                    runtime.add_sim_system(Self::sys_tick.in_set([<$cls Step>]::Tick).after([<$control Step>]::Tick));
                    Ok(())
                }
            }
        }

        #[inject_nitrous_component]
        impl $cls {
            #[inline]
            pub fn new(position: f64, duration: Duration) -> Self {
                Self {
                    position: position.max(0.).min(1.),
                    extension_time: duration.as_secs_f64(),
                }
            }

            #[inline]
            pub fn position(&self) -> f64 {
                self.position
            }

            pub fn angle<U: AngleUnit>(&self) -> Angle<U> {
                Angle::<U>::from(&radians!(self.position))
            }

            fn sys_tick(timestep: Res<TimeStep>, mut query: Query<(&$control, &mut $cls)>) {
                for (control, mut state) in query.iter_mut() {
                    state.position = effector_chase(
                        control.position(),
                        control.min_value(),
                        control.max_value(),
                        timestep.fixed_sim_step().to_seconds() / state.extension_time,
                        state.position,
                    );
                }
            }
        }
    };
}

make_control_chase!(AirbrakeEffector, "airbrake_effector", AirbrakeControl);
make_control_chase!(FlapsEffector, "flaps_effector", FlapsControl);
make_control_chase!(HookEffector, "hook_effector", HookControl);
make_control_chase!(GearEffector, "gear_effector", GearControl);
make_control_chase!(BayEffector, "bay_effector", BayControl);

make_control_chase!(
    ThrustVectorPitchEffector,
    "thrust_vector_pitch_effector",
    ThrustVectorPitchControl
);
make_control_chase!(
    ThrustVectorYawEffector,
    "thrust_vector_yaw_effector",
    ThrustVectorYawControl
);
make_control_chase!(VtolAngleEffector, "vtol_angle_effector", VtolAngleControl);
