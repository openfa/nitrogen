// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
mod controls;
mod effectors;
mod systems;

pub use crate::{
    controls::{
        absolute_control::{AirbrakeControl, FlapsControl},
        symmetric_absolute_control::{
            ThrustVectorPitchControl, ThrustVectorPitchControlStep, ThrustVectorYawControl,
            ThrustVectorYawControlStep, VtolAngleControl, VtolAngleControlStep,
        },
        symmetric_centering_inceptor::{
            PitchInceptor, PitchInceptorStep, RollInceptor, RollInceptorStep, YawInceptor,
            YawInceptorStep,
        },
        throttle_inceptor::{ThrottleInceptor, ThrottlePosition},
        toggle_control::{BayControl, GearControl, HookControl},
    },
    effectors::chase_effector::{
        AirbrakeEffector, AirbrakeEffectorStep, BayEffector, BayEffectorStep, FlapsEffector,
        FlapsEffectorStep, GearEffector, GearEffectorStep, HookEffector, HookEffectorStep,
        ThrustVectorPitchEffector, ThrustVectorPitchEffectorStep, ThrustVectorYawEffector,
        ThrustVectorYawEffectorStep, VtolAngleEffector,
    },
    systems::{
        chassis::Chassis,
        engine::{glider::GliderEngine, Engine, EnginePower},
        fuel::{ConsumeResult, FuelSystem, FuelTank, FuelTankKind},
        gear_layout::{GearKind, GearLayout, GearLeg},
        power::{PowerSystem, PowerSystemStep},
    },
};
