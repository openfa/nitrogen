// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::Value;
use anyhow::{anyhow, ensure, Context, Error, Result};
use indexmap::IndexMap;
use libyaml::{tag, Emitter, Event};
use serde_yaml::Mapping;
use std::{fmt, time::Duration};

/// A value type that can contain structured data.
#[derive(Debug, Default, Clone, Eq, PartialEq)]
pub struct Dict {
    storage: IndexMap<String, Value>,
}

impl Dict {
    pub fn insert<S: ToString>(&mut self, key: S, value: Value) {
        self.storage.insert(key.to_string(), value);
    }

    pub fn get<S: AsRef<str>>(&self, key: S) -> Option<&Value> {
        self.storage.get(key.as_ref())
    }

    pub fn try_get<S: AsRef<str>>(&self, key: S) -> Result<&Value> {
        self.storage
            .get(key.as_ref())
            .ok_or_else(|| anyhow!("expected {}", key.as_ref()))
    }

    pub fn get_mut<S: AsRef<str>>(&mut self, key: S) -> Option<&mut Value> {
        self.storage.get_mut(key.as_ref())
    }

    pub fn try_get_mut<S: AsRef<str>>(&mut self, key: S) -> Result<&mut Value> {
        self.storage
            .get_mut(key.as_ref())
            .ok_or_else(|| anyhow!("expected mut {}", key.as_ref()))
    }

    pub fn expect_str<S: AsRef<str>>(&self, key: S, kind: &str) -> Result<&str> {
        let key = key.as_ref();
        self.get(key)
            .ok_or_else(|| anyhow!("not a {kind} struct, missing {key}"))?
            .to_str()
            .with_context(|| format!("not a {kind} struct, {key} not a string"))
    }

    pub fn expect_float<S: AsRef<str>>(&self, key: S, kind: &str) -> Result<f64> {
        let key = key.as_ref();
        self.get(key)
            .ok_or_else(|| anyhow!("not a {kind} struct, missing {key}"))?
            .to_float()
            .with_context(|| format!("not a {kind} struct, {key} not a float"))
    }

    pub fn expect_int<S: AsRef<str>>(&self, key: S, kind: &str) -> Result<i64> {
        let key = key.as_ref();
        self.get(key)
            .ok_or_else(|| anyhow!("not a {kind} struct, missing {key}"))?
            .to_int()
            .with_context(|| format!("not a {kind} struct, {key} not an int"))
    }

    pub fn iter(&self) -> impl Iterator<Item = (&str, &Value)> {
        self.storage.iter().map(|(k, v)| (k.as_str(), v))
    }

    pub fn keys(&self) -> impl Iterator<Item = &str> {
        self.storage.keys().map(|k| k.as_str())
    }

    pub fn contains_key<S: AsRef<str>>(&self, key: S) -> bool {
        self.storage.contains_key(key.as_ref())
    }

    fn column_sizes(&self) -> (usize, usize) {
        let mut max_c0 = 0;
        let mut max_c1 = 0;
        for (key, value) in self.storage.iter() {
            if key.len() > max_c0 {
                max_c0 = key.len();
            }
            if let Value::Dict(nested) = value {
                let (a, b) = nested.column_sizes();
                let c1 = a + b;
                if c1 > max_c1 {
                    max_c1 = c1;
                }
            }
        }
        (max_c0, max_c1)
    }

    pub fn emit_yaml(&self, emitter: &mut Emitter) -> Result<()> {
        emitter.emit(Event::MappingStart {
            anchor: None,
            tag: Some(tag::OMAP.to_string()),
            implicit: true,
            style: None,
        })?;
        for (key, value) in self.storage.iter() {
            emitter.emit(Event::Scalar {
                anchor: None,
                tag: Some(tag::STR.to_string()),
                value: key.to_owned(),
                plain_implicit: true,
                quoted_implicit: true,
                style: None,
            })?;
            value.emit_yaml(emitter)?;
        }
        emitter.emit(Event::MappingEnd)?;
        Ok(())
    }

    pub(crate) fn deserialize_yaml(map: &Mapping) -> Result<Self> {
        let mut out = Dict::default();
        for (ykey, yvalue) in map.iter() {
            out.insert(
                Value::deserialize_yaml(ykey)?.to_str()?,
                Value::deserialize_yaml(yvalue)?,
            );
        }
        Ok(out)
    }
}

impl fmt::Display for Dict {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut key_order = self.keys().collect::<Vec<_>>();
        key_order.sort();
        let (c0, c1) = self.column_sizes();
        for key in &key_order {
            write!(f, "{key: <c0$} ")?;
            let val = self.storage.get(key.to_owned()).unwrap().to_string();
            if val.contains('\n') {
                writeln!(f)?;
                for line in val.lines() {
                    writeln!(f, "    {line}")?;
                }
            } else {
                writeln!(f, "{val: <c1$}")?;
            }
        }
        Ok(())
    }
}

impl From<Duration> for Dict {
    fn from(value: Duration) -> Self {
        let mut out = Dict::default();
        out.insert("type", Value::from_str("Duration"));
        out.insert("seconds", Value::from_float(value.as_secs_f64()));
        out
    }
}

impl TryFrom<&Dict> for Duration {
    type Error = Error;

    fn try_from(value: &Dict) -> Result<Self> {
        let ty = value.expect_str("type", "Duration")?;
        let secs = value.expect_str("seconds", "Duration")?;
        ensure!(ty == "Duration");
        Ok(Duration::from_secs_f64(
            secs.parse::<f64>()
                .with_context(|| "duration must have a float seconds")?,
        ))
    }
}
