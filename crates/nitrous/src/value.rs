// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    memory::{
        CallResult, ComponentLookup, ComponentSlot, LocalSlot, ResourceLookup, ResourceSlot,
        RustCallbackFunc, WorldIndex,
    },
    Dict, HeapMut, HeapRef, List, ScriptComponent, ScriptResource,
};
use anyhow::{anyhow, bail, Result};
use bevy_ecs::{prelude::*, system::Resource};
use futures::Future;
use itertools::Itertools;
use libyaml::{tag, Emitter, Event, ScalarStyle, VersionDirective};
use log::error;
use ordered_float::{OrderedFloat, Pow};
use parking_lot::RwLock;
use serde::Deserialize;
use serde_yaml::Value as YValue;
use std::{
    fmt::{self, Debug, Formatter},
    num::TryFromIntError,
    pin::Pin,
    sync::Arc,
};

pub type FutureValue = Pin<Box<dyn Future<Output = Value> + Send + Sync + Unpin + 'static>>;

#[derive(Clone)]
pub enum Value {
    // Primitives
    Boolean(bool),
    Integer(i64),
    Float(OrderedFloat<f64>),
    String(String),

    // Containers
    Dict(Dict),
    List(List),

    // Slots
    Resource(ResourceLookup),
    Entity(Entity),
    Component(Entity, ComponentLookup),
    ResourceMethod(ResourceLookup, String), // TODO: atoms?
    ComponentMethod(Entity, ComponentLookup, String), // TODO: atoms?
    LocalSlot(LocalSlot),
    ResourceSlot(ResourceSlot),
    ComponentSlot(ComponentSlot),

    // Weirdos
    RustMethod(Arc<RustCallbackFunc>),
    Future(Arc<RwLock<FutureValue>>),
}

impl Debug for Value {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::Future(_) => write!(f, "FutureValue"),
            _ => write!(f, "{self}"),
        }
    }
}

impl Value {
    #[allow(non_snake_case)]
    pub fn True() -> Self {
        Self::Boolean(true)
    }

    #[allow(non_snake_case)]
    pub fn False() -> Self {
        Self::Boolean(false)
    }

    pub(crate) fn new_resource(lookup: &ResourceLookup) -> Self {
        Self::Resource(lookup.to_owned())
    }

    pub(crate) fn new_entity(entity: Entity) -> Self {
        Self::Entity(entity)
    }

    pub(crate) fn new_component(entity: Entity, lookup: &ComponentLookup) -> Self {
        Value::Component(entity, lookup.to_owned())
    }

    pub fn from_bool(v: bool) -> Self {
        Self::Boolean(v)
    }

    pub fn from_int(v: i64) -> Self {
        Self::Integer(v)
    }

    pub fn from_float(v: f64) -> Self {
        Self::Float(OrderedFloat(v))
    }

    #[allow(clippy::should_implement_trait)]
    pub fn from_str<S: ToString>(v: S) -> Self {
        Self::String(v.to_string())
    }

    // Read through any slots so we have a physical value instead of a reference.
    pub fn deref_self(&mut self, heap: HeapRef) -> Result<()> {
        match self {
            Self::LocalSlot(slot) => *self = slot.inner()?,
            Self::ResourceSlot(slot) => *self = slot.inner(heap)?,
            Self::ComponentSlot(slot) => *self = slot.inner(heap)?,
            _ => {}
        };
        Ok(())
    }

    pub fn to_bool(&self) -> Result<bool> {
        if let Self::Boolean(b) = self {
            return Ok(*b);
        }
        bail!("not a boolean value: {}", self)
    }

    pub fn to_int(&self) -> Result<i64> {
        if let Self::Integer(i) = self {
            return Ok(*i);
        }
        bail!("not an integer value: {}", self)
    }

    pub fn to_float(&self) -> Result<f64> {
        if let Self::Float(f) = self {
            return Ok(f.0);
        }
        bail!("not a float value: {}", self)
    }

    pub fn to_str(&self) -> Result<&str> {
        if let Self::String(s) = self {
            return Ok(s);
        }
        bail!("not a string value: {}", self)
    }

    pub fn to_dict(&self) -> Result<&Dict> {
        if let Self::Dict(d) = self {
            return Ok(d);
        }
        bail!("not a dict value: {}", self)
    }

    pub fn to_dict_mut(&mut self) -> Result<&mut Dict> {
        if let Self::Dict(d) = self {
            return Ok(d);
        }
        bail!("not a dict value: {}", self)
    }

    pub fn to_list(&self) -> Result<&List> {
        if let Self::List(l) = self {
            return Ok(l);
        }
        bail!("not a list value: {}", self)
    }

    pub fn make_resource_method<T>(name: &str) -> Self
    where
        T: Resource + ScriptResource + 'static,
    {
        Self::ResourceMethod(ResourceLookup::new::<T>(), name.to_owned())
    }

    pub fn make_component_method<T>(entity: Entity, name: &str) -> Self
    where
        T: Component + ScriptComponent + 'static,
    {
        Self::ComponentMethod(entity, ComponentLookup::new::<T>(), name.to_owned())
    }

    pub fn to_future(&self) -> Result<Arc<RwLock<FutureValue>>> {
        if let Self::Future(f) = self {
            return Ok(f.clone());
        }
        bail!("not a future value: {self}")
    }

    pub fn is_numeric(&self) -> bool {
        matches!(self, Self::Float(_) | Self::Integer(_))
    }

    pub fn to_numeric(&self) -> Result<f64> {
        Ok(match self {
            Self::Float(f) => f.0,
            Self::Integer(i) => *i as f64,
            _ => bail!("not numeric"),
        })
    }

    pub fn attr(&self, name: &str, heap: HeapRef) -> Result<Value> {
        match self {
            Value::Resource(lookup) => Ok(Value::ResourceSlot(ResourceSlot::new(
                lookup.clone(),
                name.to_owned(),
            ))),
            Value::Entity(entity) => {
                // TODO: there's almost certainly a smarter way to do this.
                if name == "list" {
                    #[allow(unstable_name_collisions)]
                    let msg: Value = heap
                        .resource::<WorldIndex>()
                        .entity_component_names_by_id(*entity)
                        .map(|v| v.intersperse("\n").collect())
                        .unwrap_or_else(|| "error: unknown entity name".to_owned())
                        .into();
                    Ok(Value::RustMethod(Arc::new(move |_, _| Ok(msg.clone()))))
                } else {
                    heap.maybe_component_value(*entity, name)
                        .ok_or_else(|| anyhow!("no such component {name} on entity {entity:?}"))
                }
            }
            Value::Component(entity, lookup) => Ok(Value::ComponentSlot(ComponentSlot::new(
                *entity,
                lookup.clone(),
                name.to_owned(),
            ))),
            Value::Dict(dict) => dict
                .get(name)
                .cloned()
                .ok_or_else(|| anyhow!("no such key {name} on dict")),
            Value::LocalSlot(slot) => slot.inner()?.attr(name, heap),
            Value::ResourceSlot(slot) => slot.inner(heap)?.attr(name, heap),
            Value::ComponentSlot(slot) => slot.inner(heap)?.attr(name, heap),
            _ => bail!(
                "attribute base must be a resource, entity, component, or dict, not {:?}",
                self
            ),
        }
    }

    pub fn call_method(&mut self, mut args: Vec<Value>, heap: HeapMut) -> Result<Value> {
        for arg in &mut args {
            arg.deref_self(heap.as_ref())?;
        }
        match self {
            Value::ResourceMethod(lookup, method_name) => {
                Ok(match lookup.call_method(method_name, &args, heap)? {
                    CallResult::Val(v) => v,
                    CallResult::Selfish => Value::Resource(lookup.to_owned()),
                })
            }
            Value::ComponentMethod(entity, lookup, method_name) => Ok(
                match lookup.call_method(*entity, method_name, &args, heap)? {
                    CallResult::Val(v) => v,
                    CallResult::Selfish => Value::Component(*entity, lookup.to_owned()),
                },
            ),
            Value::RustMethod(method) => method(&args, heap),
            Value::LocalSlot(slot) => slot.inner()?.call_method(args, heap),
            Value::ResourceSlot(slot) => slot.inner(heap.as_ref())?.call_method(args, heap),
            Value::ComponentSlot(slot) => slot.inner(heap.as_ref())?.call_method(args, heap),
            _ => {
                error!("attempting to call non-method value: {}", self);
                bail!("attempting to call non-method value: {}", self);
            }
        }
    }

    pub fn to_yaml(&self) -> Result<String> {
        let mut out = Vec::new();
        {
            let mut emitter = Emitter::new(Box::new(&mut out))?;
            emitter.emit(Event::StreamStart { encoding: None })?;
            emitter.emit(Event::DocumentStart {
                implicit: true,
                tags: vec![],
                version: Some(VersionDirective(1, 2)),
            })?;
            self.emit_yaml(&mut emitter)?;
            emitter.emit(Event::DocumentEnd { implicit: true })?;
            emitter.emit(Event::StreamEnd)?;
            emitter.flush()?;
        }
        Ok(std::str::from_utf8(&out)?.to_owned())
    }

    pub fn from_yaml(input: &str) -> Result<Self> {
        let de = serde_yaml::Deserializer::from_str(input);
        let value = YValue::deserialize(de)?;
        Self::deserialize_yaml(&value)
    }

    pub(crate) fn emit_yaml(&self, emitter: &mut Emitter) -> Result<()> {
        match self {
            Value::Integer(i) => emitter.emit(Event::Scalar {
                anchor: None,
                tag: Some(tag::INT.to_string()),
                value: i.to_string(),
                plain_implicit: true,
                quoted_implicit: true,
                style: None,
            })?,
            Value::Float(OrderedFloat(f)) => emitter.emit(Event::Scalar {
                anchor: None,
                tag: Some(tag::FLOAT.to_string()),
                value: format!("{f:#?}"),
                plain_implicit: true,
                quoted_implicit: true,
                style: None,
            })?,
            Value::String(s) => {
                let mut style = None;
                if s.is_empty() {
                    style = Some(ScalarStyle::DoubleQuoted);
                }
                emitter.emit(Event::Scalar {
                    anchor: None,
                    tag: Some(tag::STR.to_string()),
                    value: s.to_owned(),
                    plain_implicit: true,
                    quoted_implicit: true,
                    style,
                })?
            }
            Value::Boolean(b) => emitter.emit(Event::Scalar {
                anchor: None,
                tag: Some(tag::BOOL.to_string()),
                value: b.to_string(),
                plain_implicit: true,
                quoted_implicit: true,
                style: None,
            })?,
            Value::Dict(d) => d.emit_yaml(emitter)?,
            Value::List(l) => l.emit_yaml(emitter)?,
            _ => bail!("unsupported node in emit_yaml {self:?}"),
        }
        Ok(())
    }

    pub(crate) fn deserialize_yaml(value: &YValue) -> Result<Self> {
        Ok(match value {
            YValue::Bool(b) => Value::Boolean(*b),
            YValue::Number(n) => {
                if n.is_i64() {
                    Value::Integer(n.as_i64().unwrap())
                } else if n.is_u64() {
                    Value::Integer(i64::try_from(n.as_u64().unwrap())?)
                } else {
                    assert!(n.is_f64());
                    Value::Float(OrderedFloat(n.as_f64().unwrap()))
                }
            }
            YValue::String(s) => Value::String(s.to_owned()),
            YValue::Sequence(seq) => Value::List(List::deserialize_yaml(seq)?),
            YValue::Mapping(map) => Value::Dict(Dict::deserialize_yaml(map)?),
            _ => bail!("unsupported type in yaml {value:?}"),
        })
    }
}

macro_rules! impl_integer_value_cast {
    ($($ty:ty),*) => {
        $(
        impl From<$ty> for Value {
            fn from(n: $ty) -> Self {
                Self::Integer(n as i64)
            }
        }
        impl From<&$ty> for Value {
            fn from(n: &$ty) -> Self {
                Self::Integer(*n as i64)
            }
        }
        )*
    };
}
macro_rules! impl_value_integer_cast {
    ($($ty:ty),*) => {
        $(
        impl TryFrom<&Value> for $ty {
            type Error = anyhow::Error;
            fn try_from(value: &Value) -> anyhow::Result<Self> {
                value
                    .to_int()?
                    .try_into()
                    .map_err(|e: TryFromIntError| e.into())
            }
        }
        )*
    }
}
impl_integer_value_cast!(u8, i8, u16, i16, u32, i32, i64, isize);
impl_value_integer_cast!(u8, i8, u16, i16, u32, i32);

macro_rules! impl_float_value_cast {
    ($($ty:ty),*) => {
        $(
        impl From<$ty> for Value {
            fn from(n: $ty) -> Self {
                Self::Float(OrderedFloat(n as f64))
            }
        }
        impl From<&$ty> for Value {
            fn from(n: &$ty) -> Self {
                Self::Float(OrderedFloat(*n as f64))
            }
        }
        )*
    };
}
macro_rules! impl_value_float_cast {
    ($($ty:ty),*) => {
        $(
        impl TryFrom<&Value> for $ty {
            type Error = anyhow::Error;
            fn try_from(value: &Value) -> anyhow::Result<Self> {
                Ok(value.to_float()? as $ty)
            }
        }
        )*
    }
}
impl_float_value_cast!(f32, f64);
impl_value_float_cast!(f32, f64);

impl From<usize> for Value {
    fn from(n: usize) -> Self {
        assert!(n <= i64::MAX as usize);
        Self::Integer(n as i64)
    }
}

impl From<u64> for Value {
    fn from(n: u64) -> Self {
        assert!(n <= i64::MAX as u64);
        Self::Integer(n as i64)
    }
}

impl From<String> for Value {
    fn from(s: String) -> Self {
        Self::String(s)
    }
}

impl From<&String> for Value {
    fn from(s: &String) -> Self {
        Self::String(s.to_owned())
    }
}

impl From<&str> for Value {
    fn from(s: &str) -> Self {
        Self::String(s.to_owned())
    }
}

impl From<Option<String>> for Value {
    fn from(value: Option<String>) -> Self {
        (&value).into()
    }
}

impl From<&Option<String>> for Value {
    fn from(value: &Option<String>) -> Self {
        let mut map = Dict::default();
        if let Some(s) = value {
            map.insert("value", Value::String(s.into()));
        } else {
            map.insert("is_none", Value::Boolean(true));
        }
        Self::Dict(map)
    }
}

impl TryFrom<&Value> for Option<String> {
    type Error = anyhow::Error;
    fn try_from(value: &Value) -> Result<Option<String>> {
        let map = value.to_dict()?;
        Ok(if Some(&Value::Boolean(true)) == map.get("is_none") {
            None
        } else if let Some(v) = map.get("value") {
            Some(v.to_str()?.to_owned())
        } else {
            bail!("not an Option<String> value {value}")
        })
    }
}

impl From<bool> for Value {
    fn from(b: bool) -> Self {
        Self::Boolean(b)
    }
}

impl From<&bool> for Value {
    fn from(b: &bool) -> Self {
        Self::Boolean(*b)
    }
}

impl TryFrom<&Value> for bool {
    type Error = anyhow::Error;
    fn try_from(value: &Value) -> Result<bool> {
        value.to_bool()
    }
}

impl From<Dict> for Value {
    fn from(value: Dict) -> Self {
        Value::Dict(value)
    }
}

impl From<List> for Value {
    fn from(value: List) -> Self {
        Value::List(value)
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Boolean(v) => write!(f, "{v}"),
            Self::Integer(v) => write!(f, "{v}"),
            Self::Float(v) => write!(f, "{v}"),
            Self::String(v) => write!(f, "\"{v}\""),
            Self::Dict(d) => write!(f, "{d}"),
            Self::List(l) => write!(f, "{l:?}"),
            Self::Resource(_) => write!(f, "<resource>"),
            Self::Entity(ent) => write!(f, "@{ent:?}"),
            Self::Component(ent, _) => write!(f, "@[{ent:?}].<lookup>"),
            Self::ResourceMethod(_, name) => {
                write!(f, "<resource>.{name}")
            }
            Self::ComponentMethod(ent, _, name) => write!(f, "@[{ent:?}].<lookup>.{name}"),
            Self::LocalSlot(slot) => write!(f, "${}", slot.name()),
            Self::ResourceSlot(slot) => write!(f, "<resource>.{}", slot.name()),
            Self::ComponentSlot(slot) => write!(f, "<component>.{}", slot.name()),
            Self::RustMethod(_) => write!(f, "<callback>"),
            Self::Future(_) => write!(f, "Future"),
        }
    }
}

// FIXME: this needs a heap
impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        match self {
            Self::Boolean(a) => match other {
                Self::Boolean(b) => a == b,
                _ => false,
            },
            Self::Integer(a) => match other {
                Self::Integer(b) => a == b,
                _ => false,
            },
            Self::Float(a) => match other {
                Self::Float(b) => a == b,
                _ => false,
            },
            Self::String(a) => match other {
                Self::String(b) => a == b,
                _ => false,
            },
            Self::Dict(a) => match other {
                Self::Dict(b) => a == b,
                _ => false,
            },
            Self::List(a) => match other {
                Self::List(b) => a == b,
                _ => false,
            },
            Self::Entity(a) => match other {
                Self::Entity(b) => a == b,
                _ => false,
            },
            Self::Resource(_) => false,
            Self::Component(_, _) => false,
            Self::ComponentMethod(_, _, _) => false,
            Self::ResourceMethod(_, _) => false,
            Self::LocalSlot(slot) => slot.inner().expect("local").eq(other),
            Self::ResourceSlot(_slot) => false,
            Self::ComponentSlot(_slot) => false,
            Self::RustMethod(_) => false,
            Self::Future(_) => false,
        }
    }
}

impl Eq for Value {}

impl Value {
    pub fn impl_negate(self, heap: HeapRef) -> Result<Self> {
        Ok(match self {
            Value::Integer(i) => Value::Integer(-i),
            Value::Float(f) => Value::Float(-f),
            Value::LocalSlot(slot) => slot.inner()?.impl_negate(heap)?,
            Value::ResourceSlot(slot) => slot.inner(heap)?.impl_negate(heap)?,
            Value::ComponentSlot(slot) => slot.inner(heap)?.impl_negate(heap)?,
            _ => bail!("cannot negate this type of value"),
        })
    }

    pub fn impl_positive(self, heap: HeapRef) -> Result<Self> {
        Ok(match self {
            Value::Integer(i) => Value::Integer(i),
            Value::Float(f) => Value::Float(f),
            Value::LocalSlot(slot) => slot.inner()?.impl_positive(heap)?,
            Value::ResourceSlot(slot) => slot.inner(heap)?.impl_positive(heap)?,
            Value::ComponentSlot(slot) => slot.inner(heap)?.impl_positive(heap)?,
            _ => bail!("cannot positive this type of value"),
        })
    }

    pub fn impl_not(self, heap: HeapRef) -> Result<Self> {
        Ok(match self {
            Value::Boolean(b) => Value::Boolean(!b),
            Value::LocalSlot(slot) => slot.inner()?.impl_not(heap)?,
            Value::ResourceSlot(slot) => slot.inner(heap)?.impl_not(heap)?,
            Value::ComponentSlot(slot) => slot.inner(heap)?.impl_not(heap)?,
            _ => bail!("cannot boolean-not this type of value"),
        })
    }

    pub fn impl_multiply(self, other: Self, heap: HeapRef) -> Result<Self> {
        match other {
            Value::LocalSlot(rhs) => return self.impl_multiply(rhs.inner()?, heap),
            Value::ResourceSlot(rhs) => return self.impl_multiply(rhs.inner(heap)?, heap),
            Value::ComponentSlot(rhs) => return self.impl_multiply(rhs.inner(heap)?, heap),
            _ => {}
        }
        Ok(match self {
            Value::Integer(lhs) => match other {
                Value::Integer(rhs) => Value::Integer(lhs * rhs),
                Value::Float(rhs) => Value::Float(OrderedFloat(lhs as f64) * rhs),
                _ => bail!("invalid rhs type for multiply with integer"),
            },
            Value::Float(lhs) => match other {
                Value::Integer(rhs) => Value::Float(lhs * OrderedFloat(rhs as f64)),
                Value::Float(rhs) => Value::Float(lhs * rhs),
                _ => bail!("invalid rhs type for multiply with float"),
            },
            Value::String(lhs) => match other {
                Value::Integer(rhs) => Value::String(lhs.repeat(rhs.max(0) as usize)),
                Value::Float(rhs) => Value::String(lhs.repeat(rhs.floor().max(0f64) as usize)),
                _ => bail!("invalid rhs type for multiply with string"),
            },
            Value::LocalSlot(slot) => slot.inner()?.impl_multiply(other, heap)?,
            Value::ResourceSlot(slot) => slot.inner(heap)?.impl_multiply(other, heap)?,
            Value::ComponentSlot(slot) => slot.inner(heap)?.impl_multiply(other, heap)?,
            _ => bail!("cannot do arithmetic with this type of value"),
        })
    }

    pub fn impl_divide(self, other: Self, heap: HeapRef) -> Result<Self> {
        match other {
            Value::LocalSlot(rhs) => return self.impl_divide(rhs.inner()?, heap),
            Value::ResourceSlot(rhs) => return self.impl_divide(rhs.inner(heap)?, heap),
            Value::ComponentSlot(rhs) => return self.impl_divide(rhs.inner(heap)?, heap),
            _ => {}
        }
        Ok(match self {
            Value::Integer(lhs) => match other {
                Value::Integer(rhs) => Value::Integer(lhs / rhs),
                Value::Float(rhs) => Value::Float(OrderedFloat(lhs as f64) / rhs),
                _ => bail!("invalid rhs type for divide from integer"),
            },
            Value::Float(lhs) => match other {
                Value::Integer(rhs) => Value::Float(lhs / OrderedFloat(rhs as f64)),
                Value::Float(rhs) => Value::Float(lhs / rhs),
                _ => bail!("invalid rhs type for divide from float"),
            },
            Value::LocalSlot(slot) => slot.inner()?.impl_divide(other, heap)?,
            Value::ResourceSlot(slot) => slot.inner(heap)?.impl_divide(other, heap)?,
            Value::ComponentSlot(slot) => slot.inner(heap)?.impl_divide(other, heap)?,
            _ => bail!("cannot divide from this type of value"),
        })
    }

    pub fn impl_add(self, other: Self, heap: HeapRef) -> Result<Self> {
        match other {
            Value::LocalSlot(rhs) => return self.impl_add(rhs.inner()?, heap),
            Value::ResourceSlot(rhs) => return self.impl_add(rhs.inner(heap)?, heap),
            Value::ComponentSlot(rhs) => return self.impl_add(rhs.inner(heap)?, heap),
            _ => {}
        }
        Ok(match self {
            Value::Integer(lhs) => match other {
                Value::Integer(rhs) => Value::Integer(lhs + rhs),
                Value::Float(rhs) => Value::Float(OrderedFloat(lhs as f64) + rhs),
                _ => bail!("invalid rhs type for add to integer"),
            },
            Value::Float(lhs) => match other {
                Value::Integer(rhs) => Value::Float(lhs + OrderedFloat(rhs as f64)),
                Value::Float(rhs) => Value::Float(lhs + rhs),
                _ => bail!("invalid rhs type for add to float"),
            },
            Value::String(lhs) => match other {
                Value::String(rhs) => Value::String(lhs + &rhs),
                _ => bail!("invalid rhs type for add to string"),
            },
            Value::LocalSlot(slot) => slot.inner()?.impl_add(other, heap)?,
            Value::ResourceSlot(slot) => slot.inner(heap)?.impl_add(other, heap)?,
            Value::ComponentSlot(slot) => slot.inner(heap)?.impl_add(other, heap)?,
            _ => bail!("cannot add to this type of value"),
        })
    }

    pub fn impl_subtract(self, other: Self, heap: HeapRef) -> Result<Self> {
        match other {
            Value::LocalSlot(rhs) => return self.impl_subtract(rhs.inner()?, heap),
            Value::ResourceSlot(rhs) => return self.impl_subtract(rhs.inner(heap)?, heap),
            Value::ComponentSlot(rhs) => return self.impl_subtract(rhs.inner(heap)?, heap),
            _ => {}
        }
        Ok(match self {
            Value::Integer(lhs) => match other {
                Value::Integer(rhs) => Value::Integer(lhs - rhs),
                Value::Float(rhs) => Value::Float(OrderedFloat(lhs as f64) - rhs),
                _ => bail!("invalid rhs type for subtract from integer"),
            },
            Value::Float(lhs) => match other {
                Value::Integer(rhs) => Value::Float(lhs - OrderedFloat(rhs as f64)),
                Value::Float(rhs) => Value::Float(lhs - rhs),
                _ => bail!("invalid rhs type for subtract from float"),
            },
            Value::LocalSlot(slot) => slot.inner()?.impl_subtract(other, heap)?,
            Value::ResourceSlot(slot) => slot.inner(heap)?.impl_subtract(other, heap)?,
            Value::ComponentSlot(slot) => slot.inner(heap)?.impl_subtract(other, heap)?,
            _ => bail!("cannot subtract from this type of value"),
        })
    }

    pub fn impl_modulo(self, other: Self, heap: HeapRef) -> Result<Self> {
        match other {
            Value::LocalSlot(rhs) => return self.impl_modulo(rhs.inner()?, heap),
            Value::ResourceSlot(rhs) => return self.impl_modulo(rhs.inner(heap)?, heap),
            Value::ComponentSlot(rhs) => return self.impl_modulo(rhs.inner(heap)?, heap),
            _ => {}
        }
        Ok(match self {
            Value::Integer(lhs) => match other {
                Value::Integer(rhs) => Value::Integer(lhs % rhs),
                Value::Float(rhs) => Value::Float(OrderedFloat(lhs as f64) % rhs),
                _ => bail!("invalid rhs type for modulo from integer"),
            },
            Value::Float(lhs) => match other {
                Value::Integer(rhs) => Value::Float(lhs % OrderedFloat(rhs as f64)),
                Value::Float(rhs) => Value::Float(lhs % rhs),
                _ => bail!("invalid rhs type for modulo from float"),
            },
            Value::LocalSlot(slot) => slot.inner()?.impl_modulo(other, heap)?,
            Value::ResourceSlot(slot) => slot.inner(heap)?.impl_modulo(other, heap)?,
            Value::ComponentSlot(slot) => slot.inner(heap)?.impl_modulo(other, heap)?,
            _ => bail!("cannot modulo from this type of value"),
        })
    }

    pub fn impl_exponentiate(self, other: Self, heap: HeapRef) -> Result<Self> {
        match other {
            Value::LocalSlot(rhs) => return self.impl_exponentiate(rhs.inner()?, heap),
            Value::ResourceSlot(rhs) => return self.impl_exponentiate(rhs.inner(heap)?, heap),
            Value::ComponentSlot(rhs) => return self.impl_exponentiate(rhs.inner(heap)?, heap),
            _ => {}
        }
        Ok(match self {
            Value::Integer(lhs) => match other {
                Value::Integer(rhs) => Value::Integer(lhs.saturating_pow(rhs.try_into()?)),
                Value::Float(rhs) => Value::Float(OrderedFloat(lhs as f64).pow(rhs)),
                _ => bail!("invalid rhs type for pow from integer"),
            },
            Value::Float(lhs) => match other {
                Value::Integer(rhs) => Value::Float(lhs.pow(OrderedFloat(rhs as f64))),
                Value::Float(rhs) => Value::Float(lhs.pow(rhs)),
                _ => bail!("invalid rhs type for divide from float"),
            },
            Value::LocalSlot(slot) => slot.inner()?.impl_exponentiate(other, heap)?,
            Value::ResourceSlot(slot) => slot.inner(heap)?.impl_exponentiate(other, heap)?,
            Value::ComponentSlot(slot) => slot.inner(heap)?.impl_exponentiate(other, heap)?,
            _ => bail!("cannot divide from this type of value"),
        })
    }

    pub fn impl_assign(self, other: Self, heap: HeapMut) -> Result<Self> {
        match other {
            Value::LocalSlot(rhs) => return self.impl_assign(rhs.inner()?, heap),
            Value::ResourceSlot(rhs) => return self.impl_assign(rhs.inner(heap.as_ref())?, heap),
            Value::ComponentSlot(rhs) => return self.impl_assign(rhs.inner(heap.as_ref())?, heap),
            _ => {}
        }
        debug_assert!(!matches!(other, Value::LocalSlot(_)));
        debug_assert!(!matches!(other, Value::ResourceSlot(_)));
        debug_assert!(!matches!(other, Value::ComponentSlot(_)));
        match self {
            Value::LocalSlot(mut slot) => {
                slot.impl_assign(other)?;
                Ok(Value::LocalSlot(slot))
            }
            Value::ResourceSlot(mut slot) => {
                slot.impl_assign(other, heap)?;
                Ok(Value::ResourceSlot(slot))
            }
            Value::ComponentSlot(mut slot) => {
                slot.impl_assign(other, heap)?;
                Ok(Value::ComponentSlot(slot))
            }
            _ => bail!("invalid assignment to value '{self}'"),
        }
    }
}
