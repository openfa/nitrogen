// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::lower::{Atom, Instr, NitrousScriptBuilder};
use anyhow::Result;
use ellipse::Ellipse;
use std::{collections::HashMap, fmt};

#[derive(Clone)]
pub struct NitrousScript {
    origin: String,
    code: Vec<Instr>,
    atoms: HashMap<Atom, String>,
}

impl NitrousScript {
    pub fn compile(script: &str) -> Result<Self> {
        let (code, atoms) = NitrousScriptBuilder::compile(script)?.finish()?;
        Ok(Self {
            origin: script.to_owned(),
            code,
            atoms,
        })
    }

    pub fn code(&self) -> &[Instr] {
        &self.code
    }

    pub fn atom(&self, atom: &Atom) -> &str {
        &self.atoms[atom]
    }

    pub fn atoms(&self) -> &HashMap<Atom, String> {
        &self.atoms
    }

    pub fn source(&self) -> &str {
        &self.origin
    }
}

impl From<&NitrousScript> for NitrousScript {
    fn from(script_ref: &NitrousScript) -> Self {
        script_ref.to_owned()
    }
}

impl fmt::Debug for NitrousScript {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (i, instr) in self.code.iter().enumerate() {
            match instr {
                Instr::Push(v) => writeln!(f, "{i:03} <-- Push {v}")?,
                Instr::Pop => writeln!(f, "{i:03} --> Pop")?,
                Instr::LoadResource(atom) => writeln!(
                    f,
                    "{i:03} ==> LoadResource {}",
                    self.atoms.get(atom).unwrap()
                )?,
                Instr::LoadLocal(atom) => {
                    writeln!(f, "{i:03} ==> LoadLocal {}", self.atoms.get(atom).unwrap())?
                }
                Instr::LoadEntity(atom) => writeln!(
                    f,
                    "{i:03} ==> LoadEntity @{}",
                    self.atoms.get(atom).unwrap()
                )?,
                Instr::InitLocal(atom) => {
                    writeln!(f, "{i:03} <== InitLocal {}", &self.atoms.get(atom).unwrap())?
                }
                Instr::BinOp(op) => writeln!(f, "{i:03} <-> {op:?}")?,
                Instr::PrefixOp(op) => writeln!(f, "{i:03} <- {op:?}")?,
                Instr::Call(cnt) => writeln!(f, "{i:03} <-> Call({cnt})")?,
                Instr::Attr(atom) => {
                    writeln!(f, "{i:03} <-> Attr .{}", &self.atoms.get(atom).unwrap())?
                }
            }
        }
        Ok(())
    }
}

impl fmt::Display for NitrousScript {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            self.origin
                .trim_start()
                .split('\n')
                .next()
                .unwrap_or("")
                .truncate_ellipse(40 - 3)
        )
    }
}
