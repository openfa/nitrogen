// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
#[macro_export]
macro_rules! make_partial_test_part {
    ($test:ident, $total:expr, $cnt:expr) => {
        $crate::paste! {
            #[test]
            fn [<test_ $test _ $cnt _of_ $total>]() -> Result<()> {
                $test($cnt - 1, $total)
            }
        }
    };
}
#[macro_export]
macro_rules! make_partial_test_inner {
    ($test:ident, $total:expr, 20) => {
        $crate::make_partial_test_part!($test, $total, 20);
        $crate::make_partial_test_inner!($test, $total, 19);
    };
    ($test:ident, $total:expr, 19) => {
        $crate::make_partial_test_part!($test, $total, 19);
        $crate::make_partial_test_inner!($test, $total, 18);
    };
    ($test:ident, $total:expr, 18) => {
        $crate::make_partial_test_part!($test, $total, 18);
        $crate::make_partial_test_inner!($test, $total, 17);
    };
    ($test:ident, $total:expr, 17) => {
        $crate::make_partial_test_part!($test, $total, 17);
        $crate::make_partial_test_inner!($test, $total, 16);
    };
    ($test:ident, $total:expr, 16) => {
        $crate::make_partial_test_part!($test, $total, 16);
        $crate::make_partial_test_inner!($test, $total, 15);
    };
    ($test:ident, $total:expr, 15) => {
        $crate::make_partial_test_part!($test, $total, 15);
        $crate::make_partial_test_inner!($test, $total, 14);
    };
    ($test:ident, $total:expr, 14) => {
        $crate::make_partial_test_part!($test, $total, 14);
        $crate::make_partial_test_inner!($test, $total, 13);
    };
    ($test:ident, $total:expr, 13) => {
        $crate::make_partial_test_part!($test, $total, 13);
        $crate::make_partial_test_inner!($test, $total, 12);
    };
    ($test:ident, $total:expr, 12) => {
        $crate::make_partial_test_part!($test, $total, 12);
        $crate::make_partial_test_inner!($test, $total, 11);
    };
    ($test:ident, $total:expr, 11) => {
        $crate::make_partial_test_part!($test, $total, 11);
        $crate::make_partial_test_inner!($test, $total, 10);
    };
    ($test:ident, $total:expr, 10) => {
        $crate::make_partial_test_part!($test, $total, 10);
        $crate::make_partial_test_inner!($test, $total, 9);
    };
    ($test:ident, $total:expr, 9) => {
        $crate::make_partial_test_part!($test, $total, 9);
        $crate::make_partial_test_inner!($test, $total, 8);
    };
    ($test:ident, $total:expr, 8) => {
        $crate::make_partial_test_part!($test, $total, 8);
        $crate::make_partial_test_inner!($test, $total, 7);
    };
    ($test:ident, $total:expr, 7) => {
        $crate::make_partial_test_part!($test, $total, 7);
        $crate::make_partial_test_inner!($test, $total, 6);
    };
    ($test:ident, $total:expr, 6) => {
        $crate::make_partial_test_part!($test, $total, 6);
        $crate::make_partial_test_inner!($test, $total, 5);
    };
    ($test:ident, $total:expr, 5) => {
        $crate::make_partial_test_part!($test, $total, 5);
        $crate::make_partial_test_inner!($test, $total, 4);
    };
    ($test:ident, $total:expr, 4) => {
        $crate::make_partial_test_part!($test, $total, 4);
        $crate::make_partial_test_inner!($test, $total, 3);
    };
    ($test:ident, $total:expr, 3) => {
        $crate::make_partial_test_part!($test, $total, 3);
        $crate::make_partial_test_inner!($test, $total, 2);
    };
    ($test:ident, $total:expr, 2) => {
        $crate::make_partial_test_part!($test, $total, 2);
        $crate::make_partial_test_inner!($test, $total, 1);
    };
    ($test:ident, $total:expr, 1) => {
        $crate::make_partial_test_part!($test, $total, 1);
        $crate::make_partial_test_inner!($test, $total, 0);
    };
    ($test:ident, $total:expr, 0) => {};
}
#[macro_export]
macro_rules! make_partial_test {
    ($test:ident, $total:tt) => {
        $crate::make_partial_test_inner!($test, $total, $total);
    };
}

pub fn segment_tests<T>(section: usize, total_sections: usize, all: &[T]) -> &[T] {
    let per_section = all.len() / total_sections;
    if section < total_sections - 1 {
        &all[section * per_section..(section + 1) * per_section]
    } else {
        &all[section * per_section..]
    }
}
