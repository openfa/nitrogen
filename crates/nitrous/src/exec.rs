// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    lower::Instr,
    memory::{LocalNamespace, LocalSlot, WorldIndex},
    parse::{NitrousBinOp, NitrousPrefixOp},
    HeapMut, NitrousScript, Value,
};
use anyhow::{anyhow, bail, Result};
use parking_lot::{Mutex, MutexGuard};
use std::sync::Arc;

/// Store current execution state of some specific script.
/// Note: this state must always be used with the same script.
#[derive(Clone, Debug)]
pub struct ExecutionContext {
    locals: Arc<Mutex<LocalNamespace>>,
    stack: Vec<Value>,
    script: NitrousScript,
    counter: usize,
}

impl ExecutionContext {
    pub fn new(locals: Arc<Mutex<LocalNamespace>>, script: NitrousScript) -> Self {
        Self {
            locals,
            stack: Vec::new(),
            script,
            counter: 0,
        }
    }

    pub fn script(&self) -> &NitrousScript {
        &self.script
    }

    pub fn has_started(&self) -> bool {
        self.counter != 0
    }

    pub fn locals_mut(&mut self) -> MutexGuard<LocalNamespace> {
        self.locals.lock()
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum YieldState {
    Yielded,
    Finished(Value),
}

/// Executing scripts requires some state and the world.
pub struct NitrousExecutor<'a> {
    state: &'a mut ExecutionContext,
    heap: HeapMut<'a>,
}

impl<'a> NitrousExecutor<'a> {
    pub fn new(state: &'a mut ExecutionContext, heap: HeapMut<'a>) -> Self {
        Self { state, heap }
    }

    fn push(&mut self, value: Value) {
        self.state.stack.push(value);
    }

    fn pop(&mut self, ctx: &str) -> Result<Value> {
        self.state
            .stack
            .pop()
            .ok_or_else(|| anyhow!("empty stack at pop: {}", ctx))
    }

    pub fn run_until_yield(mut self) -> Result<YieldState> {
        use NitrousBinOp as BinOp;
        // println!("at script: {}", self.state.script);
        for pc in self.state.counter..self.state.script.code().len() {
            let instr = self.state.script.code()[pc].to_owned();
            // println!("@{pc:03}: {instr:?}; st: {:?}", self.state.stack);
            match instr {
                Instr::Push(value) => self.state.stack.push(value.to_owned()),
                Instr::Pop => {
                    self.state.stack.pop();
                }
                Instr::LoadResource(atom) => {
                    let name = self.state.script.atom(&atom);
                    if let Some(resource) = self.heap.maybe_resource_value_by_name(name) {
                        self.push(resource);
                    } else {
                        // Note: automatic lifetime extension of the guard returned by the lock
                        //       means that we cannot push (mutably) in the block if the guard is
                        //       in the `if`, even though we clone the value out of it.
                        let local = self.state.locals.lock().get(name).to_owned();
                        if let Some(value) = local {
                            self.push(value);
                        } else {
                            bail!("unknown local or resource variable: {}", name);
                        }
                    }
                }
                Instr::LoadLocal(atom) => {
                    let name = self.state.script.atom(&atom);
                    self.push(Value::LocalSlot(LocalSlot::new(
                        self.state.locals.clone(),
                        name.to_owned(),
                    )));
                }
                Instr::LoadEntity(atom) => {
                    let name = self.state.script.atom(&atom);
                    let entity = self
                        .heap
                        .resource::<WorldIndex>()
                        .lookup_entity(name)
                        .ok_or_else(|| anyhow!("no such entity: @{}", name))?;
                    self.push(entity);
                }
                Instr::InitLocal(atom) => {
                    let value = self.pop("assigned")?;
                    let target = self.state.script.atom(&atom);
                    self.state.locals.lock().put(target, value);
                }

                // Unary Prefix Op
                Instr::PrefixOp(op) => {
                    let rhs = self.pop("rhs")?;
                    self.push(match op {
                        NitrousPrefixOp::Negate => rhs.impl_negate(self.heap.as_ref())?,
                        NitrousPrefixOp::Positive => rhs.impl_positive(self.heap.as_ref())?,
                        NitrousPrefixOp::Not => rhs.impl_not(self.heap.as_ref())?,
                    });
                }

                // Binary Op
                Instr::BinOp(op) => {
                    let rhs = self.pop("rhs")?;
                    let lhs = self.pop("lhs")?;
                    let rv = match op {
                        BinOp::Multiply => lhs.impl_multiply(rhs, self.heap.as_ref())?,
                        BinOp::Divide => lhs.impl_divide(rhs, self.heap.as_ref())?,
                        BinOp::Add => lhs.impl_add(rhs, self.heap.as_ref())?,
                        BinOp::Subtract => lhs.impl_subtract(rhs, self.heap.as_ref())?,
                        BinOp::Modulo => lhs.impl_modulo(rhs, self.heap.as_ref())?,
                        BinOp::Exponentiate => lhs.impl_exponentiate(rhs, self.heap.as_ref())?,
                        BinOp::MultiplyAssign => {
                            let result = lhs.clone().impl_multiply(rhs, self.heap.as_ref())?;
                            lhs.impl_assign(result, self.heap.as_mut())?
                        }
                        BinOp::DivideAssign => {
                            let result = lhs.clone().impl_divide(rhs, self.heap.as_ref())?;
                            lhs.impl_assign(result, self.heap.as_mut())?
                        }
                        BinOp::AddAssign => {
                            let result = lhs.clone().impl_add(rhs, self.heap.as_ref())?;
                            lhs.impl_assign(result, self.heap.as_mut())?
                        }
                        BinOp::SubtractAssign => {
                            let result = lhs.clone().impl_subtract(rhs, self.heap.as_ref())?;
                            lhs.impl_assign(result, self.heap.as_mut())?
                        }
                        BinOp::ModuloAssign => {
                            let result = lhs.clone().impl_modulo(rhs, self.heap.as_ref())?;
                            lhs.impl_assign(result, self.heap.as_mut())?
                        }
                        BinOp::ExponentiateAssign => {
                            let result = lhs.clone().impl_exponentiate(rhs, self.heap.as_ref())?;
                            lhs.impl_assign(result, self.heap.as_mut())?
                        }
                        BinOp::Assign => lhs.impl_assign(rhs, self.heap.as_mut())?,
                        BinOp::Attr => {
                            bail!("unexpected attr binop in exec: should be handled as Attr instr")
                        }
                        _ => unimplemented!(),
                    };
                    self.push(rv);
                }
                Instr::Call(arg_cnt) => {
                    let mut base = self.pop("call target")?;
                    // TODO: use smallvec<4> here
                    let mut args = Vec::with_capacity(arg_cnt as usize);
                    for _ in 0..arg_cnt {
                        args.push(self.pop("arg")?);
                    }
                    let result = base.call_method(args, self.heap.as_mut())?;
                    self.push(result);
                }
                Instr::Attr(atom) => {
                    let base = self.pop("attr base")?;
                    let name = self.state.script.atom(&atom);
                    let result = base.attr(name, self.heap.as_ref())?;
                    self.push(result);
                }
            }
            // println!("    after stack: {:?}", self.state.stack);
        }
        Ok(YieldState::Finished(if self.state.stack.is_empty() {
            Value::True()
        } else {
            self.pop("return value")?
        }))
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::heap::Heap;

    fn run_test(s: &str) -> Result<Value> {
        let mut heap = Heap::default();
        let locals = Arc::new(Mutex::new(LocalNamespace::default()));
        let script = NitrousScript::compile(s)?;
        let mut ctx = ExecutionContext::new(locals, script);
        let exec = NitrousExecutor::new(&mut ctx, heap.as_mut());
        match exec.run_until_yield()? {
            YieldState::Yielded => bail!("did not expect yield in test"),
            YieldState::Finished(v) => Ok(v),
        }
    }

    #[test]
    fn test_exec_empty() -> Result<()> {
        let out = run_test("")?;
        assert_eq!(out, Value::True());
        Ok(())
    }

    #[test]
    fn test_exec_simple() -> Result<()> {
        let tests = [
            ("True", Value::True()),
            ("False", Value::False()),
            ("!True", Value::False()),
            ("!False", Value::True()),
            ("2", Value::Integer(2)),
            ("-2", Value::Integer(-2)),
            ("2+2", Value::Integer(4)),
            ("2*3", Value::Integer(6)),
            ("2-3", Value::Integer(-1)),
            ("6/2", Value::Integer(3)),
            ("6/2;False", Value::False()),
            ("6/2;False;", Value::True()),
            ("let $a <- 42; $a", Value::Integer(42)),
            ("let $a <- 42; $a / 7", Value::Integer(6)),
            ("let $a <- 42; $a <- 7", Value::Integer(7)),
            ("let $a <- 42; $a <- 7;", Value::True()),
            ("let $a <- 42; $a <- 7; $a", Value::Integer(7)),
            ("let $a <- 42; $a +<- 7; $a", Value::Integer(49)),
            ("let $a <- 42; $a -<- 7; $a", Value::Integer(35)),
            ("let $a <- 42; $a *<- 7; $a", Value::Integer(294)),
            ("let $a <- 42; $a /<- 7; $a", Value::Integer(6)),
            ("let $a <- 42; $a %<- 7; $a", Value::Integer(0)),
            ("let $a <- 42; $a ^<- 7; $a", Value::Integer(230539333248)),
            // ("let $a <- 7'ft; $a *<- 7'ft; $a", Value::from(feet2!(42))),
        ];
        for (test, expect) in &tests {
            println!("test: {test}");
            let out = run_test(test)?;
            assert_eq!(out, *expect);
        }
        Ok(())
    }

    #[test]
    fn test_exec_error() -> Result<()> {
        let tests = [
            ("+True", "positive"),
            ("-False", "negate"),
            ("a<-6/2;a", "unknown"),
            ("let foo <- 1", "$foo"),
            ("2<-1", "assign"),
        ];
        for (test, expect) in &tests {
            let out = run_test(test);
            assert!(out.is_err(), "ok: {:#?}", out);
            let err_str = out.unwrap_err().to_string();
            assert!(
                err_str.contains(expect),
                "expect: {}, actual: {}",
                expect,
                err_str
            );
        }
        Ok(())
    }
}
