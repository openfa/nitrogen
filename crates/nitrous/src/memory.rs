// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    heap::{HeapMut, HeapRef},
    value::Value,
};
use anyhow::{anyhow, bail, ensure, Result};
use bevy_ecs::{prelude::*, system::Resource};
use parking_lot::Mutex;
use std::{collections::HashMap, fmt::Debug, sync::Arc};

/// Enable the call to return self for chaining.
pub enum CallResult {
    Val(Value),
    Selfish,
}

impl CallResult {
    pub fn as_value(self) -> Result<Value> {
        match self {
            Self::Val(v) => Ok(v),
            Self::Selfish => bail!("not a value"),
        }
    }
}

/// Use #[derive(NitrousResource)] to implement this trait. The derived implementation
/// will expect the struct to have an impl block annotated with #[inject_nitrous]. This
/// second macro will use #[method] tags to populate lookups for the various operations.
pub trait ScriptResource: Resource + 'static {
    fn resource_name(&self) -> &'static str;
    fn resource_type_name(&self) -> &'static str;
    fn call_method(&mut self, name: &str, args: &[Value], heap: HeapMut) -> Result<CallResult>;
    fn put(&mut self, name: &str, value: Value) -> Result<()>;
    fn get(&self, name: &str) -> Result<Value>;
    fn method_metadata(&self) -> Vec<MethodMetadata>;
}

/// Bridges from a name (as in a script) to ScriptResouce. Effectively it stores the T
/// for us so that we don't have to do TypeId and pointer hyjinx.
type ResourceLookupRefFunc =
    dyn Fn(&World) -> Option<&(dyn ScriptResource + 'static)> + Send + Sync + 'static;
type ResourceLookupMutFunc =
    dyn Fn(&mut World) -> Option<&mut (dyn ScriptResource + 'static)> + Send + Sync + 'static;
type ResourceCallMethodFunc = dyn Fn(&str, &[Value], HeapMut) -> Result<CallResult> + Send + Sync;

#[derive(Clone)]
pub struct ResourceLookup {
    ref_func: Arc<ResourceLookupRefFunc>,
    mut_func: Arc<ResourceLookupMutFunc>,
    call_func: Arc<ResourceCallMethodFunc>,
}

impl ResourceLookup {
    pub fn new<T>() -> Self
    where
        T: Resource + ScriptResource + 'static,
    {
        Self {
            ref_func: Arc::new(move |world| {
                world.get_resource::<T>().map(|resource| {
                    let rto: &(dyn ScriptResource + 'static) = resource;
                    rto
                })
            }),
            mut_func: Arc::new(move |world| {
                world.get_resource_mut::<T>().map(|resource| {
                    let rto: &mut (dyn ScriptResource + 'static) = resource.into_inner();
                    rto
                })
            }),
            call_func: Arc::new(|method_name, args, mut heap| {
                let mut resource = heap.world_mut().remove_resource::<T>().unwrap();
                let rv = resource.call_method(method_name, args, heap.as_mut());
                heap.world_mut().insert_resource(resource);
                rv
            }),
        }
    }

    pub fn as_ref<'a>(&self, world: &'a World) -> &'a dyn ScriptResource {
        (self.ref_func)(world).expect("resource present")
    }

    pub fn get_ref<'a>(&self, world: &'a World) -> Option<&'a dyn ScriptResource> {
        (self.ref_func)(world)
    }

    pub fn as_mut<'a>(&mut self, world: &'a mut World) -> &'a mut dyn ScriptResource {
        (self.mut_func)(world).expect("resource present")
    }

    pub fn get_mut<'a>(&mut self, world: &'a mut World) -> Option<&'a mut dyn ScriptResource> {
        (self.mut_func)(world)
    }

    pub fn call_method(
        &self,
        method_name: &str,
        args: &[Value],
        heap: HeapMut,
    ) -> Result<CallResult> {
        (self.call_func)(method_name, args, heap)
    }
}

// A reference to a value in a resource.
#[derive(Clone)]
pub struct ResourceSlot {
    lookup: ResourceLookup,
    name: String,
}

impl ResourceSlot {
    pub(crate) fn new(lookup: ResourceLookup, name: String) -> Self {
        Self { lookup, name }
    }

    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn inner(&self, heap: HeapRef) -> Result<Value> {
        self.lookup.as_ref(heap.world()).get(&self.name)
    }

    pub fn impl_assign(&mut self, mut other: Value, mut heap: HeapMut) -> Result<()> {
        other.deref_self(heap.as_ref())?;
        self.lookup
            .as_mut(heap.world_mut())
            .put(&self.name, other)?;
        Ok(())
    }
}

/// Use #[derive(NitrousComponent)] to implement this trait. The derived implementation
/// will expect the struct to have an impl block annotated with #[inject_nitrous]. This
/// second macro will use #[method] tags to populate lookups for the various operations.
pub trait ScriptComponent: Send + Sync + 'static {
    fn component_name(&self) -> &'static str;
    fn component_type_name(&self) -> &'static str;
    fn call_method(&mut self, name: &str, args: &[Value], heap: HeapMut) -> Result<CallResult>;
    fn put(&mut self, entity: Entity, name: &str, value: Value) -> Result<()>;
    fn get(&self, entity: Entity, name: &str) -> Result<Value>;
    fn method_metadata(&self) -> Vec<MethodMetadata>;
}

type ComponentLookupRefFunc =
    dyn Fn(Entity, &World) -> Option<&(dyn ScriptComponent + 'static)> + Send + Sync + 'static;
type ComponentLookupMutFunc = dyn Fn(Entity, &mut World) -> Option<&mut (dyn ScriptComponent + 'static)>
    + Send
    + Sync
    + 'static;
type ComponentCallMethodFunc =
    dyn Fn(Entity, &str, &[Value], HeapMut) -> Result<CallResult> + Send + Sync;

#[derive(Clone)]
pub struct ComponentLookup {
    ref_func: Arc<ComponentLookupRefFunc>,
    mut_func: Arc<ComponentLookupMutFunc>,
    call_func: Arc<ComponentCallMethodFunc>,
}

impl ComponentLookup {
    pub fn new<T>() -> Self
    where
        T: Component + ScriptComponent + 'static,
    {
        Self {
            ref_func: Arc::new(move |entity, world| {
                world.get::<T>(entity).map(|ptr| {
                    let cto: &(dyn ScriptComponent + 'static) = ptr;
                    cto
                })
            }),
            mut_func: Arc::new(move |entity, world| {
                world.get_mut::<T>(entity).map(|ptr| {
                    let cto: &mut (dyn ScriptComponent + 'static) = ptr.into_inner();
                    cto
                })
            }),
            call_func: Arc::new(|entity, method_name, args, mut heap| {
                let mut component = heap.world_mut().entity_mut(entity).take::<T>().unwrap();
                let rv = component.call_method(method_name, args, heap.as_mut());
                heap.world_mut().entity_mut(entity).insert(component);
                rv
            }),
        }
    }

    pub fn as_ref<'a>(&self, entity: Entity, world: &'a World) -> &'a dyn ScriptComponent {
        (self.ref_func)(entity, world).expect("resource present")
    }

    pub fn get_ref<'a>(&self, entity: Entity, world: &'a World) -> Option<&'a dyn ScriptComponent> {
        (self.ref_func)(entity, world)
    }

    pub fn as_mut<'a>(
        &mut self,
        entity: Entity,
        world: &'a mut World,
    ) -> &'a mut dyn ScriptComponent {
        (self.mut_func)(entity, world).expect("resource present")
    }

    pub fn get_mut<'a>(
        &mut self,
        entity: Entity,
        world: &'a mut World,
    ) -> Option<&'a mut dyn ScriptComponent> {
        (self.mut_func)(entity, world)
    }

    pub fn call_method(
        &self,
        entity: Entity,
        method_name: &str,
        args: &[Value],
        heap: HeapMut,
    ) -> Result<CallResult> {
        (self.call_func)(entity, method_name, args, heap)
    }
}

// A reference to a value in a resource.
#[derive(Clone)]
pub struct ComponentSlot {
    entity: Entity,
    lookup: ComponentLookup,
    name: String,
}

impl ComponentSlot {
    pub(crate) fn new(entity: Entity, lookup: ComponentLookup, name: String) -> Self {
        Self {
            entity,
            lookup,
            name,
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn inner(&self, heap: HeapRef) -> Result<Value> {
        self.lookup
            .as_ref(self.entity, heap.world())
            .get(self.entity, &self.name)
    }

    pub fn impl_assign(&mut self, mut other: Value, mut heap: HeapMut) -> Result<()> {
        other.deref_self(heap.as_ref())?;
        self.lookup
            .as_mut(self.entity, heap.world_mut())
            .put(self.entity, &self.name, other)?;
        Ok(())
    }
}

/// An inline function that can be stuffed into a Value, where needed.
pub type RustCallbackFunc = dyn Fn(&[Value], HeapMut) -> Result<Value> + Send + Sync + 'static;

struct ResourceMetadata {
    lookup: ResourceLookup,
    attrs: HashMap<String, AttrMetadata>,
}

#[derive(Default)]
struct EntityMetadata {
    name: String,
    components: HashMap<String, ComponentMetadata>,
}

impl EntityMetadata {
    fn new(name: String) -> Self {
        Self {
            name,
            components: Default::default(),
        }
    }

    fn component_names(&self) -> impl Iterator<Item = &str> {
        self.components.keys().map(|s| s.as_str())
    }
}

struct ComponentMetadata {
    lookup: ComponentLookup,
    attrs: HashMap<String, AttrMetadata>,
}

pub struct MethodMetadata {
    pub nitrous_name: &'static str,
    pub rust_name: &'static str,
    pub help: &'static str,
}

#[derive(Debug, Default)]
struct AttrMetadata {
    // TODO: help text, arg lists, etc.
}

/// A map from names to pointers into World.
#[derive(Default, Resource)]
pub struct WorldIndex {
    resource_ptrs: HashMap<String, ResourceMetadata>,
    named_entities: HashMap<String, Entity>,
    entity_metadata: HashMap<Entity, EntityMetadata>,
    numbered_entity_offsets: HashMap<String, usize>,
}

impl WorldIndex {
    pub fn empty() -> Self {
        Self::default()
    }

    pub fn find_numbered_name(&mut self, base: &str) -> String {
        if let Some(n) = self.numbered_entity_offsets.get_mut(base) {
            *n += 1;
            format!("{base}-{}", *n)
        } else {
            self.numbered_entity_offsets.insert(base.to_owned(), 0);
            base.to_owned()
        }
    }

    pub fn insert_named_resource<T>(&mut self, value: T, world: &mut World) -> Result<()>
    where
        T: Resource + ScriptResource + 'static,
    {
        let name = value.resource_name().to_owned();
        let methods = value.method_metadata();
        assert!(
            !self.resource_ptrs.contains_key(&name),
            "attempting to insert duplicate resource {name}"
        );
        let mut attrs = HashMap::new();
        for method in &methods {
            attrs.insert(method.nitrous_name.to_owned(), AttrMetadata {});
        }
        self.resource_ptrs.insert(
            name,
            ResourceMetadata {
                lookup: ResourceLookup::new::<T>(),
                attrs,
            },
        );
        world.insert_resource(value);
        Ok(())
    }

    pub fn remove_named_resource<T>(&mut self, name: &str)
    where
        T: Resource + ScriptResource + 'static,
    {
        assert!(
            self.resource_ptrs.contains_key(name),
            "attempting to remove non-existing resource {name}"
        );
        self.resource_ptrs.remove(name);
    }

    pub fn lookup_resource(&self, name: &str) -> Option<&ResourceLookup> {
        self.resource_ptrs
            .get(name)
            .map(|metadata| &metadata.lookup)
    }

    pub fn resource_names(&self) -> impl Iterator<Item = &str> {
        self.resource_ptrs.keys().map(|s| s.as_str())
    }

    pub fn resource_attr_names_by_name(&self, name: &str) -> Option<impl Iterator<Item = &str>> {
        self.resource_ptrs
            .get(name)
            .map(|metadata| metadata.attrs.keys().map(|k| k.as_ref()))
    }

    pub fn insert_named_entity<S>(&mut self, entity_name: S, entity: Entity) -> Result<()>
    where
        S: Into<String>,
    {
        let entity_name = entity_name.into();
        ensure!(
            !self.named_entities.contains_key(&entity_name),
            "duplicate entity name: {}",
            entity_name
        );
        self.named_entities.insert(entity_name.clone(), entity);
        self.entity_metadata
            .insert(entity, EntityMetadata::new(entity_name));
        Ok(())
    }

    pub fn remove_entity(&mut self, entity: &Entity) {
        if let Some(meta) = self.entity_metadata.remove(entity) {
            self.named_entities.remove(&meta.name);
            if let Some(&offset) = self.numbered_entity_offsets.get(&meta.name) {
                if offset == 0 {
                    self.numbered_entity_offsets.remove(&meta.name);
                }
            }
        }
    }

    pub fn insert_named_component(
        &mut self,
        entity: Entity,
        component_name: &str,
        method_metadata: Vec<MethodMetadata>,
        lookup: ComponentLookup,
    ) -> Result<()> {
        ensure!(self.entity_metadata.contains_key(&entity));
        let meta = self
            .entity_metadata
            .get_mut(&entity)
            .ok_or_else(|| anyhow!("entity {:?} is not a script entity", entity))?;
        ensure!(
            !meta.components.contains_key(component_name),
            format!("duplicate component name: {component_name}")
        );
        let mut attrs = HashMap::new();
        for method in &method_metadata {
            attrs.insert(method.nitrous_name.to_owned(), AttrMetadata {});
        }
        meta.components.insert(
            component_name.to_owned(),
            ComponentMetadata { lookup, attrs },
        );
        Ok(())
    }

    pub fn remove_named_component(&mut self, entity: Entity, component_name: &str) {
        if let Some(meta) = self.entity_metadata.get_mut(&entity) {
            meta.components.remove(component_name);
        }
    }

    pub fn entity_names(&self) -> impl Iterator<Item = &str> {
        self.named_entities.keys().map(|s| s.as_str())
    }

    pub fn entity_component_names_by_id(
        &self,
        entity: Entity,
    ) -> Option<impl Iterator<Item = &str>> {
        self.entity_metadata
            .get(&entity)
            .map(|components| components.component_names())
    }

    pub fn entity_component_names_by_name(
        &self,
        entity_name: &str,
    ) -> Option<impl Iterator<Item = &str>> {
        self.named_entities.get(entity_name).and_then(|entity| {
            self.entity_metadata
                .get(entity)
                .map(|components| components.component_names())
        })
    }

    pub fn entity_component_attr_names_by_name(
        &self,
        entity_name: &str,
        component_name: &str,
    ) -> Option<impl Iterator<Item = &str>> {
        self.named_entities.get(entity_name).and_then(|entity| {
            self.entity_metadata.get(entity).and_then(|metadata| {
                metadata
                    .components
                    .get(component_name)
                    .map(|metadata| metadata.attrs.keys().map(|k| k.as_ref()))
            })
        })
    }

    pub fn get_entity(&self, name: &str) -> Option<Entity> {
        self.named_entities.get(name).cloned()
    }

    // Panics if new_name is already set or old_name is not present.
    pub fn rename_entity<S: ToString>(&mut self, old_name: &str, new_name: S) {
        let new_name = new_name.to_string();
        assert!(self.named_entities.contains_key(old_name));
        assert!(!self.named_entities.contains_key(&new_name));
        let id = self.named_entities.remove(old_name).unwrap();
        self.named_entities.insert(new_name, id);
    }

    /// Look up a named entity in the index.
    pub fn lookup_entity(&self, name: &str) -> Option<Value> {
        self.named_entities
            .get(name)
            .map(|entity| Value::new_entity(*entity))
    }

    pub fn lookup_entity_name(&self, lookup: Entity) -> Option<String> {
        for (name, id) in self.named_entities.iter() {
            if *id == lookup {
                return Some(name.to_owned());
            }
        }
        None
    }

    /// Look up a named component within an entity.
    pub fn lookup_component(&self, entity: &Entity, name: &str) -> Option<Value> {
        self.entity_metadata.get(entity).and_then(|metadata| {
            metadata
                .components
                .get(name)
                .map(|metadata| Value::new_component(*entity, &metadata.lookup))
        })
    }

    pub fn entity_components(&self, entity: &Entity) -> Option<impl Iterator<Item = &str>> {
        self.entity_metadata
            .get(entity)
            .map(|comps| comps.components.keys().map(|k| k.as_ref()))
    }

    pub fn component_attrs(&self, entity: &Entity) -> Vec<&str> {
        self.entity_metadata
            .get(entity)
            .map(|comps| {
                comps
                    .components
                    .keys()
                    .map(|v| v.as_str())
                    .collect::<Vec<&str>>()
            })
            .unwrap_or_default()
    }
}

/// A simple name <-> value map
#[derive(Clone, Debug, Default)]
pub struct LocalNamespace {
    memory: HashMap<String, Value>,
}

impl From<HashMap<String, Value>> for LocalNamespace {
    fn from(memory: HashMap<String, Value>) -> Self {
        Self { memory }
    }
}

impl From<HashMap<&str, Value>> for LocalNamespace {
    fn from(mut memory: HashMap<&str, Value>) -> Self {
        memory
            .drain()
            .map(|(k, v)| (k.to_owned(), v))
            .collect::<HashMap<String, Value>>()
            .into()
    }
}

impl LocalNamespace {
    #[inline]
    pub fn put_if_absent(&mut self, name: &str, value: Value) -> &mut Self {
        if !self.memory.contains_key(name) {
            self.memory.insert(name.to_owned(), value);
        }
        self
    }

    #[inline]
    pub fn put<S: Into<String>>(&mut self, name: S, value: Value) -> &mut Self {
        self.memory.insert(name.into(), value);
        self
    }

    #[inline]
    pub fn get(&self, name: &str) -> Option<Value> {
        self.memory.get(name).cloned()
    }

    #[inline]
    pub fn get_mut(&mut self, name: &str) -> Option<&mut Value> {
        self.memory.get_mut(name)
    }

    #[inline]
    pub fn contains(&self, name: &str) -> bool {
        self.memory.contains_key(name)
    }

    #[inline]
    pub fn remove(&mut self, name: &str) -> Option<Value> {
        self.memory.remove(name)
    }
}

// A reference to a slot with it's namespace.
#[derive(Clone)]
pub struct LocalSlot {
    namespace: Arc<Mutex<LocalNamespace>>,
    name: String,
}

impl LocalSlot {
    pub(crate) fn new(namespace: Arc<Mutex<LocalNamespace>>, name: String) -> Self {
        Self { namespace, name }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn inner(&self) -> Result<Value> {
        self.namespace
            .lock()
            .get(&self.name)
            .ok_or_else(|| anyhow!("live reference to dead slot ${}", self.name))
    }

    pub fn impl_assign(&mut self, other: Value) -> Result<()> {
        self.namespace.lock().put(&self.name, other);
        Ok(())
    }
}
