// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::tokenize::{NitrousToken, NitrousTokenInfo, NitrousTokenizer};
use anyhow::{anyhow, bail, ensure, Context, Result};
use ordered_float::OrderedFloat;
use std::fmt::{self, Display, Formatter};

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum NitrousTerminal {
    ResourceRef(String), // bare symbol in terminal position
    EntityRef(String),   // @symbol
    LocalRef(String),    // $symbol
    Float(OrderedFloat<f64>),
    Integer(i64),
    String(String),
    Boolean(bool),
}

impl Display for NitrousTerminal {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::ResourceRef(s) => write!(f, "{s}"),
            Self::EntityRef(s) => write!(f, "@{s}"),
            Self::LocalRef(s) => write!(f, "${s}"),
            Self::Float(v) => write!(f, "{v}"),
            Self::Integer(i) => write!(f, "{i}"),
            Self::Boolean(b) => write!(f, "{b}"),
            Self::String(s) => write!(f, "\"{s}\""),
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum NitrousBinOp {
    // R-Valued Arithmetic
    Multiply,
    Divide,
    Add,
    Subtract,
    Modulo,
    Exponentiate,

    // R-Valued Logical
    And,
    Or,
    Equals,
    Less,
    LessOrEqual,
    Greater,
    GreaterOrEqual,

    // R-Valued Special
    MakeUnit,
    Attr,

    // L-Valued
    Assign,
    MultiplyAssign,
    DivideAssign,
    AddAssign,
    SubtractAssign,
    ModuloAssign,
    ExponentiateAssign,
}

impl fmt::Display for NitrousBinOp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            // Logical
            Self::And => "&&",
            Self::Or => "||",
            Self::Equals => "=",
            Self::Less => "<",
            Self::LessOrEqual => "<=",
            Self::Greater => ">",
            Self::GreaterOrEqual => ">=",

            // Mutating
            Self::Assign => "<-",
            Self::MultiplyAssign => "*<-",
            Self::DivideAssign => "/<-",
            Self::AddAssign => "+<-",
            Self::SubtractAssign => "-<-",
            Self::ModuloAssign => "%<-",
            Self::ExponentiateAssign => "^<-",

            // Binary
            Self::Multiply => "*",
            Self::Divide => "/",
            Self::Add => "+",
            Self::Subtract => "-",
            Self::Modulo => "%",
            Self::Exponentiate => "^",
            Self::MakeUnit => "`",
            Self::Attr => ".",
        };
        write!(f, "{s}")
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum NitrousPrefixOp {
    Not,
    Negate,
    Positive,
}

impl fmt::Display for NitrousPrefixOp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            Self::Not => "!",
            Self::Negate => "-",
            Self::Positive => "+",
        };
        write!(f, "{s}")
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum NitrousPostfixOp {
    Call,
    Subscript,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum NitrousExpr {
    BinaryOp(Box<NitrousExpr>, NitrousBinOp, Box<NitrousExpr>),
    UnaryOp(NitrousPrefixOp, Box<NitrousExpr>),
    Call(Box<NitrousExpr>, Vec<NitrousExpr>),
    Subscript(Box<NitrousExpr>, Box<NitrousExpr>),
    Term(NitrousTerminal),
}

impl NitrousExpr {
    pub fn parenthesize(&self) -> String {
        match self {
            Self::BinaryOp(a, op, b) => format!("({}{op}{})", a.parenthesize(), b.parenthesize()),
            Self::UnaryOp(op, a) => format!("({op}{})", a.parenthesize()),
            Self::Term(t) => t.to_string(),
            Self::Call(f, args) => format!(
                "({}({}))",
                f.parenthesize(),
                args.iter()
                    .map(|a| a.parenthesize())
                    .collect::<Vec<_>>()
                    .join(",")
            ),
            Self::Subscript(base, index) => {
                format!("({}[{}]", base.parenthesize(), index.parenthesize())
            }
        }
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum NitrousStatement {
    Empty,
    LetAssign(String, NitrousExpr),
    Expr(NitrousExpr),
    ExprOut(NitrousExpr), // an expression without a trailing semicolon
}

impl NitrousStatement {
    #[cfg(test)]
    pub fn parenthesize(&self) -> String {
        match self {
            Self::Empty => "".to_string(),
            Self::LetAssign(s, e) => format!("let {s} = {}", e.parenthesize()),
            Self::Expr(e) => format!("{};", e.parenthesize()),
            Self::ExprOut(e) => e.parenthesize(),
        }
    }
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct NitrousAst {
    statements: Vec<NitrousStatement>,
}

impl NitrousAst {
    pub fn statements(&self) -> impl Iterator<Item = &NitrousStatement> {
        self.statements.iter()
    }

    #[cfg(test)]
    pub fn parenthesize(&self) -> String {
        self.statements
            .iter()
            .map(|v| v.parenthesize())
            .collect::<Vec<_>>()
            .join("")
    }
}

#[derive(Clone, Debug)]
pub struct NitrousParser<'a> {
    tokens: Vec<NitrousTokenInfo<'a>>,
    cursor: usize,
}

impl<'a> NitrousParser<'a> {
    pub fn new(program: &'a str) -> Result<Self> {
        let tokens = NitrousTokenizer::tokenize(program)?;
        Ok(Self { tokens, cursor: 0 })
    }

    pub fn parse(&mut self) -> Result<NitrousAst> {
        if self.peek().is_err() {
            return Ok(NitrousAst::default());
        }

        let mut statements = Vec::new();
        while self.peek().is_ok() {
            statements.push(self.parse_statement()?);
        }
        Ok(NitrousAst { statements })
    }

    pub fn parse_statement(&mut self) -> Result<NitrousStatement> {
        use NitrousToken as TT;
        Ok(match self.peek()?.token() {
            // Generally, keywords introduce a statement
            TT::Let => {
                self.next()?;
                let next = self.next()?;
                let name = match next.token() {
                    TT::DollarSymbol(s) => s.to_owned(),
                    TT::BareSymbol(s) => bail!(
                        "local names defined with let must start with a $.\nDid you mean ${s} at {next}",
                    ),
                    _ => bail!("let variable must be a dollar-symbol at {}", next),
                };
                let assign = self.next()?;
                ensure!(
                    assign.token() == TT::Assignment,
                    "expected an assignment operator after let at {assign}",
                );
                let expr = self.parse_expr_bp(0)?;
                NitrousStatement::LetAssign(name, expr)
            }
            // Duplicate semicolons are fine and can be ignored.
            TT::Semicolon => {
                self.next()?;
                NitrousStatement::Empty
            }
            // Everything else can be treated as an expression.
            _ => {
                let expr = self.parse_expr_bp(0)?;
                if let Ok(next) = self.peek() {
                    if next.token() == TT::Semicolon {
                        self.next()?;
                        NitrousStatement::Expr(expr)
                    } else {
                        NitrousStatement::ExprOut(expr)
                    }
                } else {
                    NitrousStatement::ExprOut(expr)
                }
            }
        })
    }

    fn parse_expr_bp(&mut self, min_bp: u8) -> Result<NitrousExpr> {
        use NitrousExpr as Ast;
        use NitrousPrefixOp as PrefixOp;
        use NitrousTerminal as T;
        use NitrousToken as TT;

        let info = self.next()?;
        let mut lhs = match info.token() {
            TT::BareSymbol(s) => Ast::Term(T::ResourceRef(s.to_owned())),
            TT::AtSymbol(s) => Ast::Term(T::EntityRef(s.to_owned())),
            TT::DollarSymbol(s) => Ast::Term(T::LocalRef(s.to_owned())),
            TT::Float(f) => Ast::Term(T::Float(f)),
            TT::Integer(i) => Ast::Term(T::Integer(i)),
            TT::String(s) => Ast::Term(T::String(s.to_owned())),
            TT::Boolean(b) => Ast::Term(T::Boolean(b)),
            TT::LParen => {
                let lhs = self.parse_expr_bp(0)?; // new primary expr
                ensure!(
                    self.next()?.token() == TT::RParen,
                    "expected ( and ) to match"
                );
                lhs
            }
            TT::Minus | TT::Plus | TT::Exclamation => {
                let op = match info.token() {
                    TT::Minus => PrefixOp::Negate,
                    TT::Plus => PrefixOp::Positive,
                    TT::Exclamation => PrefixOp::Not,
                    _ => unreachable!(),
                };
                let ((), r_bp) = Self::prefix_binding_power(op);
                let rhs = self.parse_expr_bp(r_bp)?;
                NitrousExpr::UnaryOp(op, Box::new(rhs))
            }
            _ => bail!("Expected a terminal or prefix operator at"),
        };

        loop {
            if let Ok(op) = self.peek_binary_operator() {
                let (l_bp, r_bp) = Self::infix_binding_power(op);
                if l_bp < min_bp {
                    break;
                }

                // Consume the token, but only if we can bind to it
                self.next()?;
                let rhs = self.parse_expr_bp(r_bp)?;
                lhs = NitrousExpr::BinaryOp(Box::new(lhs), op, Box::new(rhs));
            } else if let Ok(op) = self.peek_postfix_call_operator() {
                let (l_bp, ()) = Self::postfix_binding_power(op);
                if l_bp < min_bp {
                    break;
                }

                // Consume the token, but only if we can bind to it
                self.next()?;
                let rhs = self.parse_call_args_list()?;
                lhs = NitrousExpr::Call(Box::new(lhs), rhs);
            } else if let Ok(op) = self.peek_postfix_subscript_operator() {
                let (l_bp, ()) = Self::postfix_binding_power(op);
                if l_bp < min_bp {
                    break;
                }

                // Consume the token, but only if we can bind to it
                self.next()?;
                let rhs = self.parse_expr_bp(0)?; // new top-level within []
                let close = self.next()?;
                ensure!(
                    close.token() == TT::RSquare,
                    "expected close bracket at {}",
                    close
                );
                lhs = NitrousExpr::Subscript(Box::new(lhs), Box::new(rhs));
            } else {
                // Fall through to a bare expression-statement
                break;
            };
        }

        Ok(lhs)
    }

    fn parse_call_args_list(&mut self) -> Result<Vec<NitrousExpr>> {
        use NitrousToken as TT;
        let mut args = Vec::new();
        loop {
            // We normally expect to end processing down below by consuming a );
            // however, if the comma list ends in a comma, allow that and end args here instead.
            if self.peek()?.token() == TT::RParen {
                self.next()?;
                return Ok(args);
            }

            // At the start of args, we expect an expression
            let arg = self.parse_expr_bp(0)?;
            args.push(arg);

            // We then expect either a comma or closing paren
            let next = self.next()?;
            match next.token() {
                TT::RParen => {
                    return Ok(args);
                }
                TT::Comma => {}
                _ => bail!("expected either a comma or closing paren at {}", next),
            }
        }
    }

    fn peek_binary_operator(&mut self) -> Result<NitrousBinOp> {
        use NitrousBinOp as Op;
        use NitrousToken as TT;
        let info = self.peek().context("looking for binary operator")?;
        Ok(match info.token() {
            // Logical
            TT::DoubleAnd => Op::And,
            TT::DoubleBar => Op::Or,
            TT::Equal => Op::Equals,
            TT::Less => Op::Less,
            TT::LessEqual => Op::LessOrEqual,
            TT::Greater => Op::Greater,
            TT::GreaterEqual => Op::GreaterOrEqual,

            // Mutating
            TT::Assignment => Op::Assign,
            TT::StarAssignment => Op::MultiplyAssign,
            TT::SlashAssignment => Op::DivideAssign,
            TT::PlusAssignment => Op::AddAssign,
            TT::MinusAssignment => Op::SubtractAssign,
            TT::PercentAssignment => Op::ModuloAssign,
            TT::CaretAssignment => Op::ExponentiateAssign,

            // Binary
            TT::Star => Op::Multiply,
            TT::Slash => Op::Divide,
            TT::Plus => Op::Add,
            TT::Minus => Op::Subtract,
            TT::Percent => Op::Modulo,
            TT::Caret => Op::Exponentiate,
            TT::Backtick => Op::MakeUnit,
            TT::Dot => Op::Attr,

            _ => bail!("not a binary operator {:?}", info),
        })
    }

    fn peek_postfix_call_operator(&mut self) -> Result<NitrousPostfixOp> {
        use NitrousPostfixOp as Op;
        use NitrousToken as TT;
        let info = self.peek().context("looking for binary operator")?;
        Ok(match info.token() {
            TT::LParen => Op::Call,
            _ => bail!("not a postfix operator {:?}", info),
        })
    }

    fn peek_postfix_subscript_operator(&mut self) -> Result<NitrousPostfixOp> {
        use NitrousPostfixOp as Op;
        let info = self.peek().context("looking for binary operator")?;
        Ok(match info.token() {
            NitrousToken::LSquare => Op::Subscript,
            _ => bail!("not a postfix operator {:?}", info),
        })
    }

    fn postfix_binding_power(op: NitrousPostfixOp) -> (u8, ()) {
        match op {
            NitrousPostfixOp::Call | NitrousPostfixOp::Subscript => (130, ()),
        }
    }

    fn prefix_binding_power(op: NitrousPrefixOp) -> ((), u8) {
        match op {
            NitrousPrefixOp::Negate | NitrousPrefixOp::Positive | NitrousPrefixOp::Not => ((), 121),
        }
    }

    fn infix_binding_power(op: NitrousBinOp) -> (u8, u8) {
        use NitrousBinOp as Op;
        match op {
            Op::Attr => (140, 141),
            Op::MakeUnit => (110, 111),
            Op::Exponentiate => (101, 100),
            Op::Multiply | Op::Divide | Op::Modulo => (90, 91),
            Op::Add | Op::Subtract => (80, 81),
            Op::Equals | Op::Less | Op::LessOrEqual | Op::Greater | Op::GreaterOrEqual => (70, 71),
            Op::And => (60, 61),
            Op::Or => (50, 51),

            Op::Assign
            | Op::MultiplyAssign
            | Op::DivideAssign
            | Op::AddAssign
            | Op::SubtractAssign
            | Op::ModuloAssign
            | Op::ExponentiateAssign => (41, 40),
        }
    }

    fn skip(t: NitrousToken) -> bool {
        matches!(
            t,
            NitrousToken::Whitespace(_)
                | NitrousToken::SingleLineComment(_)
                | NitrousToken::MultiLineComment(_)
        )
    }

    fn next(&mut self) -> Result<NitrousTokenInfo> {
        let out = *self
            .tokens
            .get(self.cursor)
            .ok_or_else(|| anyhow!("reached end of input"))?;
        self.cursor += 1;
        if Self::skip(out.token()) {
            return self.next();
        }
        Ok(out)
    }

    fn peek(&mut self) -> Result<NitrousTokenInfo> {
        let mut offset = 0;
        loop {
            let out = *self
                .tokens
                .get(self.cursor + offset)
                .ok_or_else(|| anyhow!("reached end of input"))?;
            if !Self::skip(out.token()) {
                return Ok(out);
            }
            offset += 1;
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parse_empty() -> Result<()> {
        let rv = NitrousParser::new("")?.parse()?;
        assert!(rv.statements.is_empty());

        let rv = NitrousParser::new("// just a comment")?.parse()?;
        assert!(rv.statements.is_empty());

        let rv = NitrousParser::new("/* just a comment */")?.parse()?;
        assert!(rv.statements.is_empty());

        let rv = NitrousParser::new("   /* just some whitespace \n */ \n\n\n\n\t")?.parse()?;
        assert!(rv.statements.is_empty());

        Ok(())
    }

    #[test]
    fn test_parse_basic() -> Result<()> {
        let rv = NitrousParser::new("2+2")?.parse()?;
        assert_eq!(rv.parenthesize(), "(2+2)");

        assert_eq!(
            NitrousParser::new(" /* here */ 2 / 2 // more")?
                .parse()?
                .parenthesize(),
            "(2/2)"
        );

        Ok(())
    }

    #[test]
    fn test_parse_binop() -> Result<()> {
        let tests = [
            // Left assoc
            ("2+3+4", "((2+3)+4)"),
            ("2-3-4", "((2-3)-4)"),
            ("2*3*4", "((2*3)*4)"),
            ("2/3/4", "((2/3)/4)"),
            ("2/3/4", "((2/3)/4)"),
            // Mul over add
            ("2*3+4", "((2*3)+4)"),
            ("2+3*4", "(2+(3*4))"),
            // Pow over mul/div
            ("2*3^4", "(2*(3^4))"),
            ("2^3/4", "((2^3)/4)"),
            // ^ is right assoc
            ("2^3^4", "(2^(3^4))"),
            // add over and
            ("2+3&&4", "((2+3)&&4)"),
            ("2&&3+4", "(2&&(3+4))"),
            // and over or
            ("2||3&&4", "(2||(3&&4))"),
            // anything over assign, right assoc
            ("2<-3||4", "(2<-(3||4))"),
            ("2<-3||4", "(2<-(3||4))"),
            ("2<-3<-4", "(2<-(3<-4))"),
            ("2<-3+<-4", "(2<-(3+<-4))"),
            ("2<-3-<-4", "(2<-(3-<-4))"),
            ("2<-3/<-4", "(2<-(3/<-4))"),
            ("2<-3*<-4", "(2<-(3*<-4))"),
            // units bind most strongly, left assoc
            ("2*3`ft2", "(2*(3`ft2))"),
            ("2^3`ft2", "(2^(3`ft2))"),
            ("2`ft2^3", "((2`ft2)^3)"),
            ("2`ft2`m2", "((2`ft2)`m2)"),
            ("a<-3`m2", "(a<-(3`m2))"),
        ];
        for (test, expect) in &tests {
            assert_eq!(NitrousParser::new(test)?.parse()?.parenthesize(), *expect);
        }
        Ok(())
    }

    #[test]
    fn test_parse_left_unary() -> Result<()> {
        let tests = [
            ("!a||b", "((!a)||b)"),
            ("a||!b", "(a||(!b))"),
            ("!a&&b", "((!a)&&b)"),
            ("a&&!b", "(a&&(!b))"),
            ("a--b", "(a-(-b))"),
            ("-a--b", "((-a)-(-b))"),
            ("a++b", "(a+(+b))"),
            ("+a++b", "((+a)+(+b))"),
            ("2^-3", "(2^(-3))"),
        ];
        for (test, expect) in &tests {
            assert_eq!(NitrousParser::new(test)?.parse()?.parenthesize(), *expect);
        }
        Ok(())
    }

    #[test]
    fn test_parse_parens() -> Result<()> {
        let tests = [
            ("2+(3+4)", "(2+(3+4))"),
            ("2-(3-4)", "(2-(3-4))"),
            ("2*(3*4)", "(2*(3*4))"),
            ("2/(3/4)", "(2/(3/4))"),
            ("(2/3)/4", "((2/3)/4)"),
            ("2*(3+4)", "(2*(3+4))"),
            ("(2+3)*4", "((2+3)*4)"),
            ("(2*3)^4", "((2*3)^4)"),
            ("2^(3/4)", "(2^(3/4))"),
            ("(2^3)^4", "((2^3)^4)"),
            ("(2||3)&&4", "((2||3)&&4)"),
            ("(2<-3)||4", "((2<-3)||4)"),
            ("(2+<-3)/<-4", "((2+<-3)/<-4)"),
            ("(2*3)`ft2", "((2*3)`ft2)"),
            ("(2^3)`ft2", "((2^3)`ft2)"),
            ("((((0))))", "0"),
        ];
        for (test, expect) in &tests {
            assert_eq!(NitrousParser::new(test)?.parse()?.parenthesize(), *expect);
        }
        Ok(())
    }

    #[test]
    fn test_parse_attr() -> Result<()> {
        let tests = [
            ("foo.bar", "(foo.bar)"),
            ("foo.bar.baz", "((foo.bar).baz)"),
            ("foo.(bar.baz)", "(foo.(bar.baz))"),
            ("foo.bar<-a", "((foo.bar)<-a)"),
        ];
        for (test, expect) in &tests {
            assert_eq!(NitrousParser::new(test)?.parse()?.parenthesize(), *expect);
        }
        Ok(())
    }

    #[test]
    fn test_parse_call() -> Result<()> {
        let tests = [
            ("@foo()", "(@foo())"),
            ("$foo(a)", "($foo(a))"),
            ("foo.bar(a,b)", "((foo.bar)(a,b))"),
            ("@foo.bar(a,b,)", "((@foo.bar)(a,b))"),
        ];
        for (test, expect) in &tests {
            assert_eq!(NitrousParser::new(test)?.parse()?.parenthesize(), *expect);
        }
        Ok(())
    }

    #[test]
    fn test_parse_statement() -> Result<()> {
        let tests = [("a;b", "a;b"), (";", ""), (";;;;", "")];
        for (test, expect) in &tests {
            assert_eq!(NitrousParser::new(test)?.parse()?.parenthesize(), *expect);
        }
        Ok(())
    }

    #[test]
    fn test_parse_error() -> Result<()> {
        let tests = [
            ("((a)", "reached end"),
            ("let e;", "must start with a"),
            ("let $e;", "expected an assignment"),
        ];
        for (test, expect) in &tests {
            let out = NitrousParser::new(test)?.parse();
            assert!(out.is_err(), "ok: {:#?}", out);
            let err_str = out.unwrap_err().to_string();
            assert!(
                err_str.contains(expect),
                "expect: {}, actual: {}",
                expect,
                err_str
            );
        }
        Ok(())
    }
}
