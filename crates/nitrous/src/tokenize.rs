// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::{bail, Result};
use once_cell::sync::Lazy;
use ordered_float::OrderedFloat;
use regex::{Captures, Regex};
use std::fmt;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum NitrousToken<'a> {
    Whitespace(&'a str),

    // Comments
    MultiLineComment(&'a str),
    SingleLineComment(&'a str),

    // Literals
    Float(OrderedFloat<f64>),
    Integer(i64),
    String(&'a str),
    Boolean(bool),

    // Symbols
    BareSymbol(&'a str),
    AtSymbol(&'a str),
    DollarSymbol(&'a str),

    // Keywords
    Let,

    // Operators
    DoubleAnd,
    DoubleBar,
    Assignment,
    StarAssignment,
    SlashAssignment,
    PlusAssignment,
    MinusAssignment,
    PercentAssignment,
    CaretAssignment,
    Equal,
    LessEqual,
    GreaterEqual,
    Star,
    Slash,
    Plus,
    Minus,
    Percent,
    Caret,
    Less,
    Greater,
    Backtick,
    Exclamation,
    Dot,
    LParen,
    RParen,
    LSquare,
    RSquare,
    LBracket,
    RBracket,

    // Statement glue
    Comma,
    Semicolon,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct NitrousTokenInfo<'a> {
    token: NitrousToken<'a>,
    line: usize,
    column: usize,
}

impl<'a> NitrousTokenInfo<'a> {
    pub fn token(&self) -> NitrousToken<'a> {
        self.token
    }

    pub fn line(&self) -> usize {
        self.line
    }

    pub fn column(&self) -> usize {
        self.column
    }
}

impl fmt::Display for NitrousTokenInfo<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "at line {}, col {}: {:?}",
            self.line(),
            self.column(),
            self.token()
        )
    }
}

#[derive(Debug)]
pub struct NitrousTokenizer;

impl NitrousTokenizer {
    pub fn tokenize(script: &str) -> Result<Vec<NitrousTokenInfo>> {
        static TBL: Lazy<
            [(
                Regex,
                Box<
                    dyn for<'b> Fn(&Captures<'b>) -> Result<NitrousToken<'b>>
                        + Send
                        + Sync
                        + 'static,
                >,
            ); 44],
        > = Lazy::new(|| {
            use NitrousToken as TT;
            fn r(s: &str) -> Regex {
                Regex::new(s).unwrap()
            }
            [
                // Whitespace
                (
                    r(r"^(?-u:\s)+"),
                    Box::new(|cap| Ok(TT::Whitespace(cap.extract::<0>().0))),
                ),
                // Single Line Comment
                (
                    r(r"^//([^\n]*)(\n|$)"),
                    Box::new(|cap| Ok(TT::SingleLineComment(cap.extract::<2>().1[0]))),
                ),
                /* Multi Line Comment */
                (
                    r(r"^/\*(([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\**)\*/"),
                    Box::new(|cap| Ok(TT::MultiLineComment(cap.get(1).unwrap().as_str()))),
                ),
                // Operators
                (r(r"^&&"), Box::new(|_| Ok(TT::DoubleAnd))),
                (r(r"^\|\|"), Box::new(|_| Ok(TT::DoubleBar))),
                (r(r"^<-"), Box::new(|_| Ok(TT::Assignment))),
                (r(r"^\*<-"), Box::new(|_| Ok(TT::StarAssignment))),
                (r(r"^/<-"), Box::new(|_| Ok(TT::SlashAssignment))),
                (r(r"^\+<-"), Box::new(|_| Ok(TT::PlusAssignment))),
                (r(r"^-<-"), Box::new(|_| Ok(TT::MinusAssignment))),
                (r(r"^%<-"), Box::new(|_| Ok(TT::PercentAssignment))),
                (r(r"^\^<-"), Box::new(|_| Ok(TT::CaretAssignment))),
                (r(r"^<="), Box::new(|_| Ok(TT::LessEqual))),
                (r(r"^>="), Box::new(|_| Ok(TT::GreaterEqual))),
                (r(r"^="), Box::new(|_| Ok(TT::Equal))),
                (r(r"^\*"), Box::new(|_| Ok(TT::Star))),
                (r(r"^/"), Box::new(|_| Ok(TT::Slash))),
                (r(r"^\+"), Box::new(|_| Ok(TT::Plus))),
                (r(r"^-"), Box::new(|_| Ok(TT::Minus))),
                (r(r"^%"), Box::new(|_| Ok(TT::Percent))),
                (r(r"^\^"), Box::new(|_| Ok(TT::Caret))),
                (r(r"^<"), Box::new(|_| Ok(TT::Less))),
                (r(r"^>"), Box::new(|_| Ok(TT::Greater))),
                (r(r"^!"), Box::new(|_| Ok(TT::Exclamation))),
                (r(r"^`"), Box::new(|_| Ok(TT::Backtick))),
                // Statement Glue
                (r(r"^\."), Box::new(|_| Ok(TT::Dot))),
                (r(r"^,"), Box::new(|_| Ok(TT::Comma))),
                (r(r"^;"), Box::new(|_| Ok(TT::Semicolon))),
                (r(r"^\("), Box::new(|_| Ok(TT::LParen))),
                (r(r"^\)"), Box::new(|_| Ok(TT::RParen))),
                (r(r"^\["), Box::new(|_| Ok(TT::LSquare))),
                (r(r"^\]"), Box::new(|_| Ok(TT::RSquare))),
                (r(r"^\{"), Box::new(|_| Ok(TT::LBracket))),
                (r(r"^\}"), Box::new(|_| Ok(TT::RBracket))),
                // Float
                (
                    r(r"^([+-]?[0-9]+[.]([0-9]*)?)|([+-]?[0-9]+[eE][+-]?[0-9]+)"),
                    Box::new(|cap| {
                        Ok(TT::Float(OrderedFloat(
                            cap.get(0).expect("base match").as_str().parse::<f64>()?,
                        )))
                    }),
                ),
                // Integer
                (
                    r(r"^[+-]?[0-9]+"),
                    Box::new(|cap| Ok(TT::Integer(cap.extract::<0>().0.parse::<i64>()?))),
                ),
                // 'String'
                (
                    r(r"^'([^']*)'"),
                    Box::new(|cap| Ok(TT::String(cap.extract::<1>().1[0]))),
                ),
                // "String"
                (
                    r(r#"^"([^"]*)""#),
                    Box::new(|cap| Ok(TT::String(cap.extract::<1>().1[0]))),
                ),
                // Keywords
                (r(r"^True"), Box::new(|_| Ok(TT::Boolean(true)))),
                (r(r"^False"), Box::new(|_| Ok(TT::Boolean(false)))),
                (r(r"^let"), Box::new(|_| Ok(TT::Let))),
                // bare_symbol
                (
                    r(r"^[_a-zA-Z][_a-zA-Z0-9]*"),
                    Box::new(|cap| Ok(TT::BareSymbol(cap.extract::<0>().0))),
                ),
                // @symbol
                (
                    r(r"^@([_a-zA-Z][_a-zA-Z0-9]*)"),
                    Box::new(|cap| Ok(TT::AtSymbol(cap.extract::<1>().1[0]))),
                ),
                // $symbol
                (
                    r(r"^\$([_a-zA-Z][_a-zA-Z0-9]*)"),
                    Box::new(|cap| Ok(TT::DollarSymbol(cap.extract::<1>().1[0]))),
                ),
            ]
        });

        let mut out = vec![];
        let mut s = script;
        let mut line = 1;
        let mut column = 1;
        'top: while !s.is_empty() {
            // A found match continues back to the top.
            for (re, action) in TBL.iter() {
                if let Some(cap) = re.captures(s) {
                    // Get the exact capture size before anything else.
                    let count = cap[0].len();

                    // Adjust the line position data.
                    let next_line = line + cap[0].matches('\n').count();
                    let next_column = if let Some(offset) = cap[0].rfind('\n') {
                        count - offset
                    } else {
                        column + count
                    };

                    // Produce a token from our capture.
                    let token = action(&cap)?;
                    out.push(NitrousTokenInfo {
                        token,
                        line,
                        column,
                    });

                    // Forward by the captured size
                    s = &s[count..];

                    // Update cursor
                    line = next_line;
                    column = next_column;

                    // We found a match, so continue at the top matcher
                    continue 'top;
                }
            }

            // If we do not find a match, we fall through to here.
            bail!(
                "unrecognized token at ...'{}'...\ncontext: {:?}",
                &s[..s.len().min(16)],
                out
            );
        }

        Ok(out)
    }

    #[cfg(test)]
    fn tokenize_bare(program: &str) -> Result<Vec<NitrousToken>> {
        Ok(Self::tokenize(program)?.iter().map(|t| t.token()).collect())
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use ordered_float::OrderedFloat;

    #[test]
    fn test_tok_empty() -> Result<()> {
        let rv = NitrousTokenizer::tokenize_bare("")?;
        assert_eq!(rv, vec![]);
        Ok(())
    }

    #[test]
    fn test_tok_invalid() -> Result<()> {
        let tests = [
            "@",
            "$",
            "1209348761209384761928374691827651205698123764918273641203409238",
            "-1209348761209384761928374691827651205698123764918273641203409238",
        ];
        for test in &tests {
            let rv = NitrousTokenizer::tokenize_bare(test);
            assert!(rv.is_err(), "{:#?}", rv);
        }
        Ok(())
    }

    #[test]
    fn test_tok_cursor() -> Result<()> {
        use NitrousToken as TT;
        let tests = [
            (
                "a.b",
                vec![
                    (TT::BareSymbol("a"), 1, 1),
                    (TT::Dot, 1, 2),
                    (TT::BareSymbol("b"), 1, 3),
                ],
            ),
            (
                "a.b \n c.d",
                vec![
                    (TT::BareSymbol("a"), 1, 1),
                    (TT::Dot, 1, 2),
                    (TT::BareSymbol("b"), 1, 3),
                    (TT::Whitespace(" \n "), 1, 4),
                    (TT::BareSymbol("c"), 2, 2),
                    (TT::Dot, 2, 3),
                    (TT::BareSymbol("d"), 2, 4),
                ],
            ),
            (
                "a.b/* \n */c.d",
                vec![
                    (TT::BareSymbol("a"), 1, 1),
                    (TT::Dot, 1, 2),
                    (TT::BareSymbol("b"), 1, 3),
                    (TT::MultiLineComment(" \n "), 1, 4),
                    (TT::BareSymbol("c"), 2, 4),
                    (TT::Dot, 2, 5),
                    (TT::BareSymbol("d"), 2, 6),
                ],
            ),
        ];
        for (test, raw) in tests {
            let expect = raw
                .into_iter()
                .map(|(token, line, column)| NitrousTokenInfo {
                    token,
                    line,
                    column,
                })
                .collect::<Vec<_>>();
            let rv = NitrousTokenizer::tokenize(test)?;
            assert_eq!(rv, expect);
        }
        Ok(())
    }

    #[test]
    fn test_tok_single_line_comment() -> Result<()> {
        let rv = NitrousTokenizer::tokenize_bare("    ")?;
        assert_eq!(rv, vec![NitrousToken::Whitespace("    ")]);

        let rv = NitrousTokenizer::tokenize_bare(" \t // hello\n")?;
        assert_eq!(
            rv,
            vec![
                NitrousToken::Whitespace(" \t "),
                NitrousToken::SingleLineComment(" hello")
            ]
        );

        let rv = NitrousTokenizer::tokenize_bare("// hello\n")?;
        assert_eq!(rv, vec![NitrousToken::SingleLineComment(" hello")]);

        let rv = NitrousTokenizer::tokenize_bare("// hello")?;
        assert_eq!(rv, vec![NitrousToken::SingleLineComment(" hello")]);

        let rv = NitrousTokenizer::tokenize_bare("// hello\n    // goodbye")?;
        assert_eq!(
            rv,
            vec![
                NitrousToken::SingleLineComment(" hello"),
                NitrousToken::Whitespace("    "),
                NitrousToken::SingleLineComment(" goodbye")
            ]
        );

        Ok(())
    }

    #[test]
    fn test_tok_multi_line_comment() -> Result<()> {
        let rv = NitrousTokenizer::tokenize_bare(" \t /* hello\n*/")?;
        assert_eq!(
            rv,
            vec![
                NitrousToken::Whitespace(" \t "),
                NitrousToken::MultiLineComment(" hello\n")
            ]
        );

        let rv = NitrousTokenizer::tokenize_bare("/* hello*/\n")?;
        assert_eq!(
            rv,
            vec![
                NitrousToken::MultiLineComment(" hello"),
                NitrousToken::Whitespace("\n")
            ]
        );

        let rv = NitrousTokenizer::tokenize_bare("/* hello*/")?;
        assert_eq!(rv, vec![NitrousToken::MultiLineComment(" hello")]);

        let rv = NitrousTokenizer::tokenize_bare("/* hello\n    goodbye */")?;
        assert_eq!(
            rv,
            vec![NitrousToken::MultiLineComment(" hello\n    goodbye "),]
        );

        Ok(())
    }

    #[test]
    fn test_tok_float() -> Result<()> {
        let tests = [
            ("0.5", 0.5),
            ("5.", 5.),
            ("5e2", 500.),
            ("5e+2", 500.),
            ("5e-2", 0.05),
        ];
        for (case, expect) in &tests {
            let rv = NitrousTokenizer::tokenize_bare(case)?;
            assert_eq!(rv, vec![NitrousToken::Float(OrderedFloat(*expect))]);
        }
        let tests = [
            ("-0.5", NitrousToken::Minus, 0.5),
            ("+0.5", NitrousToken::Plus, 0.5),
            ("-5.", NitrousToken::Minus, 5.),
            ("+5.", NitrousToken::Plus, 5.),
            ("+5e+2", NitrousToken::Plus, 500.),
            ("-5e2", NitrousToken::Minus, 500.),
            ("-5e+2", NitrousToken::Minus, 500.),
            ("+5e-2", NitrousToken::Plus, 0.05),
            ("-5e-2", NitrousToken::Minus, 0.05),
        ];
        for (case, sign, expect) in &tests {
            let rv = NitrousTokenizer::tokenize_bare(case)?;
            assert_eq!(rv, vec![*sign, NitrousToken::Float(OrderedFloat(*expect))]);
        }
        Ok(())
    }

    #[test]
    fn test_tok_integer() -> Result<()> {
        let rv = NitrousTokenizer::tokenize_bare("5")?;
        assert_eq!(rv, vec![NitrousToken::Integer(5)]);
        let tests = [
            ("+5", NitrousToken::Plus, 5),
            ("-5", NitrousToken::Minus, 5),
        ];
        for (case, sign, expect) in &tests {
            let rv = NitrousTokenizer::tokenize_bare(case)?;
            assert_eq!(rv, vec![*sign, NitrousToken::Integer(*expect)]);
        }
        Ok(())
    }

    #[test]
    fn test_tok_string() -> Result<()> {
        let tests = [
            (r#"'hello'"#, "hello"),
            (r#""hello""#, "hello"),
            (r#"'  // hello'"#, "  // hello"),
            (r#""  // hello""#, "  // hello"),
            (r#""""#, ""),
            (r#"''"#, ""),
        ];
        for (case, expect) in &tests {
            let rv = NitrousTokenizer::tokenize_bare(case)?;
            assert_eq!(rv, vec![NitrousToken::String(expect)]);
        }
        Ok(())
    }

    #[test]
    fn test_tok_symbol() -> Result<()> {
        let rv = NitrousTokenizer::tokenize_bare("hello")?;
        assert_eq!(rv, vec![NitrousToken::BareSymbol("hello")]);

        let rv = NitrousTokenizer::tokenize_bare("_hello")?;
        assert_eq!(rv, vec![NitrousToken::BareSymbol("_hello")]);

        let rv = NitrousTokenizer::tokenize_bare("_hell0")?;
        assert_eq!(rv, vec![NitrousToken::BareSymbol("_hell0")]);

        let rv = NitrousTokenizer::tokenize_bare("0hello")?;
        assert_eq!(
            rv,
            vec![NitrousToken::Integer(0), NitrousToken::BareSymbol("hello")]
        );

        let rv = NitrousTokenizer::tokenize_bare("@hello")?;
        assert_eq!(rv, vec![NitrousToken::AtSymbol("hello")]);

        let rv = NitrousTokenizer::tokenize_bare("@_hello")?;
        assert_eq!(rv, vec![NitrousToken::AtSymbol("_hello")]);

        let rv = NitrousTokenizer::tokenize_bare("@_hello8")?;
        assert_eq!(rv, vec![NitrousToken::AtSymbol("_hello8")]);

        let rv = NitrousTokenizer::tokenize_bare("@8hello");
        assert!(rv.is_err());

        let rv = NitrousTokenizer::tokenize_bare("$hello")?;
        assert_eq!(rv, vec![NitrousToken::DollarSymbol("hello")]);

        let rv = NitrousTokenizer::tokenize_bare("$_hello")?;
        assert_eq!(rv, vec![NitrousToken::DollarSymbol("_hello")]);

        let rv = NitrousTokenizer::tokenize_bare("$_hello8")?;
        assert_eq!(rv, vec![NitrousToken::DollarSymbol("_hello8")]);

        let rv = NitrousTokenizer::tokenize_bare("$8hello");
        assert!(rv.is_err());

        Ok(())
    }

    #[test]
    fn test_tok_bool_literal() -> Result<()> {
        let rv = NitrousTokenizer::tokenize_bare("True")?;
        assert_eq!(rv, vec![NitrousToken::Boolean(true)]);
        let rv = NitrousTokenizer::tokenize_bare("False")?;
        assert_eq!(rv, vec![NitrousToken::Boolean(false)]);
        Ok(())
    }

    #[test]
    fn test_tok_op_bin() -> Result<()> {
        use NitrousToken as TT;
        let tests = [
            ("&&", TT::DoubleAnd),
            ("||", TT::DoubleBar),
            ("<-", TT::Assignment),
            ("*<-", TT::StarAssignment),
            ("/<-", TT::SlashAssignment),
            ("+<-", TT::PlusAssignment),
            ("-<-", TT::MinusAssignment),
            ("%<-", TT::PercentAssignment),
            ("^<-", TT::CaretAssignment),
            ("<=", TT::LessEqual),
            (">=", TT::GreaterEqual),
            ("=", TT::Equal),
            ("*", TT::Star),
            ("/", TT::Slash),
            ("+", TT::Plus),
            ("-", TT::Minus),
            ("%", TT::Percent),
            ("^", TT::Caret),
            ("<", TT::Less),
            (">", TT::Greater),
            ("`", TT::Backtick),
            ("!", TT::Exclamation),
        ];
        for (case, expect) in &tests {
            let program = format!("a{}3", case);
            let rv = NitrousTokenizer::tokenize_bare(&program)?;
            assert_eq!(rv, vec![TT::BareSymbol("a"), *expect, TT::Integer(3)]);
        }
        Ok(())
    }

    #[test]
    fn test_tok_op_unary() -> Result<()> {
        use NitrousToken as TT;
        let tests = [("+", TT::Plus), ("-", TT::Minus), ("!", TT::Exclamation)];
        for (case, expect) in &tests {
            let program = format!("{}a", case);
            let rv = NitrousTokenizer::tokenize_bare(&program)?;
            assert_eq!(rv, vec![*expect, TT::BareSymbol("a")]);
        }
        Ok(())
    }

    #[test]
    fn test_tok_statement() -> Result<()> {
        use NitrousToken as TT;
        let program = "@foo.bar(3,4).unit;True";
        let rv = NitrousTokenizer::tokenize_bare(program)?;
        assert_eq!(
            rv,
            vec![
                TT::AtSymbol("foo"),
                TT::Dot,
                TT::BareSymbol("bar"),
                TT::LParen,
                TT::Integer(3),
                TT::Comma,
                TT::Integer(4),
                TT::RParen,
                TT::Dot,
                TT::BareSymbol("unit"),
                TT::Semicolon,
                TT::Boolean(true)
            ]
        );
        Ok(())
    }

    /*

    #[test]
    fn script_property_read() -> Result<()> {
        let rv = NitrousAst::parse(r#"@foo.bar.bat;"#)?;
        assert_eq!(
            rv.stmts,
            vec![Box::new(Stmt::Expr(Box::new(Expr::Attr(
                Box::new(Expr::Attr(
                    Box::new(Expr::Term(Term::AtSymbol("foo".to_owned()))),
                    Term::Symbol("bar".to_owned())
                )),
                Term::Symbol("bat".to_owned())
            ))))]
        );
        Ok(())
    }

    #[test]
    fn script_component_property_assign() -> Result<()> {
        let rv = NitrousAst::parse(r#"@foo.bar.bat <- "cat";"#)?;
        assert_eq!(
            rv.stmts,
            vec![Box::new(Stmt::Expr(Box::new(Expr::AssignAttr(
                Box::new(Expr::Attr(
                    Box::new(Expr::Term(Term::AtSymbol("foo".to_owned()))),
                    Term::Symbol("bar".to_owned())
                )),
                Term::Symbol("bat".to_owned()),
                Box::new(Expr::Term(Term::String("cat".to_owned())))
            ))))]
        );
        Ok(())
    }

    #[test]
    fn script_resource_property_assign() -> Result<()> {
        let rv = NitrousAst::parse(r#"camera.exposure <- 0.001;"#)?;
        assert_eq!(
            rv.stmts,
            vec![Box::new(Stmt::Expr(Box::new(Expr::AssignAttr(
                Box::new(Expr::Term(Term::Symbol("camera".to_owned()))),
                Term::Symbol("exposure".to_owned()),
                Box::new(Expr::Term(Term::Float(OrderedFloat(0.001)))),
            ))))]
        );
        Ok(())
    }
     */
}
