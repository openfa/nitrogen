// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    bindle::Bindle,
    memory::{ScriptComponent, ScriptResource, WorldIndex},
    value::Value,
};
use anyhow::{anyhow, Result};
use bevy_ecs::{
    prelude::*,
    query::{QueryData, QueryFilter},
    system::Resource,
    world::{EntityRef, EntityWorldMut},
};

#[derive(Component, Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct EntityName(String);

impl EntityName {
    pub fn name(&self) -> &str {
        &self.0
    }
}

/// Wraps an EntityMut to provide named creation methods
pub struct NamedEntityMut<'w> {
    entity: EntityWorldMut<'w>,
}

impl NamedEntityMut<'_> {
    pub fn id(&self) -> Entity {
        self.entity.id()
    }

    pub fn inject<T>(&mut self, values: T) -> Result<&mut Self>
    where
        T: Bindle + Bundle,
    {
        // Capture info abound the Bindle
        let mut infos = values.script_component_infos();

        // Record the Bundle in the store.
        self.entity.insert(values);

        // Index the component in the script engine.
        // Safety: this is safe because NamedEntityEntity contains a field borrowed from World.
        //         As such, you cannot get to a NamedEntityEntity from just the world, so we
        //         cannot be entering here through some world-related path, such as a system.
        let entity = self.entity.id(); // Copy to avoid double-borrow
        let mut idx = unsafe { self.entity.world_mut() }.resource_mut::<WorldIndex>();
        for (name, metadata, lookup) in infos.drain(..) {
            idx.insert_named_component(entity, name, metadata, lookup)?;
        }
        Ok(self)
    }

    pub fn remove<T>(&mut self) -> Result<&mut Self>
    where
        T: Component + ScriptComponent + 'static,
    {
        // Take the component so we can look up the name to remove.
        if let Some(component) = self.entity.take::<T>() {
            let id = self.id();
            unsafe { self.entity.world_mut() }
                .get_resource_mut::<WorldIndex>()
                .unwrap()
                .remove_named_component(id, component.component_name());
        }
        Ok(self)
    }

    pub fn take<T>(&mut self) -> Option<T>
    where
        T: Component + ScriptComponent + 'static,
    {
        if let Some(component) = self.entity.take::<T>() {
            let id = self.id();
            unsafe { self.entity.world_mut() }
                .get_resource_mut::<WorldIndex>()
                .unwrap()
                .remove_named_component(id, component.component_name());
            Some(component)
        } else {
            None
        }
    }

    pub fn rename(&mut self, target_name: &str) {
        let id = self.id();
        let mut index = unsafe { self.entity.world_mut() }.resource_mut::<WorldIndex>();
        let own_name = index.lookup_entity_name(id).unwrap();
        index.rename_entity(&own_name, target_name);
    }

    pub fn rename_numbered(&mut self, base_name: &str) {
        let id = self.id();
        let mut index = unsafe { self.entity.world_mut() }.resource_mut::<WorldIndex>();
        let mut i = 0;
        loop {
            let target_name = format!("{base_name}{i}");
            i += 1;
            if index.get_entity(&target_name).is_some() {
                continue;
            }
            let own_name = index.lookup_entity_name(id).unwrap();
            index.rename_entity(&own_name, target_name);
            break;
        }
    }
}

macro_rules! impl_immutable_heap_methods {
    () => {
        #[inline]
        pub fn world(&self) -> &World {
            &self.world
        }

        // Component Access
        #[inline]
        pub fn get_by_id<T: Component + 'static>(&self, entity: Entity) -> &T {
            self.world.get::<T>(entity).expect("entity not found")
        }

        #[inline]
        pub fn maybe_get_by_id<T: Component + 'static>(&self, entity: Entity) -> Option<&T> {
            self.world.get::<T>(entity)
        }

        #[inline]
        pub fn get<T: Component + 'static>(&self, name: &str) -> &T {
            let entity = self
                .resource::<WorldIndex>()
                .get_entity(name)
                .expect("named entity not found");
            self.get_by_id::<T>(entity)
        }

        #[inline]
        pub fn maybe_get<T: Component + 'static>(&self, name: &str) -> Option<&T> {
            if let Some(entity) = self.resource::<WorldIndex>().get_entity(name) {
                self.maybe_get_by_id::<T>(entity)
            } else {
                None
            }
        }

        #[inline]
        pub fn maybe_component_value(&self, entity: Entity, name: &str) -> Option<Value> {
            self.resource::<WorldIndex>()
                .lookup_component(&entity, name)
        }

        // Entity Access
        //   name to id
        #[inline]
        pub fn entity(&self, name: &str) -> Entity {
            if let Some(Value::Entity(entity)) = self.resource::<WorldIndex>().lookup_entity(name) {
                entity
            } else {
                panic!("no entity named {}", name)
            }
        }

        #[inline]
        pub fn maybe_entity(&self, name: &str) -> Option<Entity> {
            if let Some(Value::Entity(entity)) = self.resource::<WorldIndex>().lookup_entity(name) {
                Some(entity)
            } else {
                None
            }
        }

        //    entity to wrappers
        #[inline]
        pub fn entity_by_id(&self, entity: Entity) -> EntityRef {
            self.world.entity(entity)
        }

        #[inline]
        pub fn maybe_resource<T: Resource>(&self) -> Option<&T> {
            self.world.get_resource()
        }

        #[inline]
        pub fn resource<T: Resource>(&self) -> &T {
            self.world.get_resource().expect("unset resource")
        }

        #[inline]
        pub fn maybe_resource_by_name(&self, name: &str) -> Option<&dyn ScriptResource> {
            self.resource::<WorldIndex>()
                .lookup_resource(name)
                .map(|lookup| lookup.get_ref(self.world()))
                .flatten()
        }

        #[inline]
        pub fn maybe_resource_value_by_name(&self, name: &str) -> Option<Value> {
            self.resource::<WorldIndex>()
                .lookup_resource(name)
                .map(|lookup| Value::new_resource(lookup))
        }

        #[inline]
        pub fn non_send_resource<T: 'static>(&self) -> &T {
            self.world.non_send_resource()
        }

        // FIXME: this should be immutable
        #[inline]
        pub fn resource_by_name(&self, name: &str) -> &dyn ScriptResource {
            self.maybe_resource_by_name(name)
                .expect("missing named resource")
        }
    };
}

macro_rules! impl_mutable_heap_methods {
    () => {
        #[inline]
        pub fn world_mut(&mut self) -> &mut World {
            &mut self.world
        }

        #[inline]
        pub fn spawn_numbered<S>(&mut self, base_name: S) -> Result<NamedEntityMut>
        where
            S: AsRef<str>,
        {
            let base = base_name.as_ref();
            let name = self
                .world
                .get_resource_mut::<WorldIndex>()
                .expect("world index")
                .find_numbered_name(base);
            self.spawn(name)
        }

        #[inline]
        pub fn spawn<S>(&mut self, name: S) -> Result<NamedEntityMut>
        where
            S: Into<String>,
        {
            let name = name.into();

            // World is borrowed mutably here, so we can't reborrow anything in either
            // world or self, annoyingly.
            let mut ent_mut = self.world.spawn(EntityName(name.clone()));
            let entity = ent_mut.id();

            // But we can go through ent_mut to get the already-borrowed world, as long as
            // we know it's safe to do so. Which it is as the unsafe share is the one above.
            unsafe { ent_mut.world_mut() }
                .get_resource_mut::<WorldIndex>()
                .unwrap()
                .insert_named_entity(name, entity)?;
            Ok(NamedEntityMut { entity: ent_mut })
        }

        #[inline]
        pub fn despawn_by_id(&mut self, entity: Entity) {
            self.resource_mut::<WorldIndex>().remove_entity(&entity);
            self.world.despawn(entity);
        }

        #[inline]
        pub fn despawn<S>(&mut self, name: S)
        where
            S: AsRef<str>,
        {
            if let Some(entity) = self.maybe_entity(name.as_ref()) {
                self.despawn_by_id(entity);
            }
        }

        #[inline]
        pub fn entity_mut<S: AsRef<str>>(&mut self, name: S) -> Result<NamedEntityMut> {
            let entity = self
                .resource::<WorldIndex>()
                .get_entity(name.as_ref())
                .ok_or_else(|| anyhow!("named entity not found: {}", name.as_ref()))?;
            self.entity_by_id_mut(entity)
        }

        #[inline]
        pub fn entity_by_id_mut(&mut self, entity: Entity) -> Result<NamedEntityMut> {
            let entity = self.world.get_entity_mut(entity)?;
            Ok(NamedEntityMut { entity })
        }

        // Manage components
        #[inline]
        pub fn get_mut<T>(&mut self, name: &str) -> Result<Mut<T>>
        where
            T: Component + ScriptComponent + 'static,
        {
            let entity = self
                .resource::<WorldIndex>()
                .get_entity(name)
                .ok_or_else(|| anyhow!("named entity not found '{name}'"))?;
            self.get_by_id_mut(entity)
        }

        #[inline]
        pub fn get_by_id_mut<T>(&mut self, entity: Entity) -> Result<Mut<T>>
        where
            T: Component + ScriptComponent + 'static,
        {
            self.world
                .get_mut::<T>(entity)
                .ok_or_else(|| anyhow!("entity not found: `{entity:?}`"))
        }

        #[inline]
        pub fn maybe_get_by_id_mut<T>(&mut self, entity: Entity) -> Result<Mut<T>>
        where
            T: Component + ScriptComponent + 'static,
        {
            self.world
                .get_mut::<T>(entity)
                .ok_or_else(|| anyhow!("entity not found: `{entity:?}`"))
        }

        #[inline]
        pub fn maybe_get_mut<T: Component + ScriptComponent + 'static>(
            &mut self,
            name: &str,
        ) -> Result<Mut<T>> {
            let entity = self
                .resource::<WorldIndex>()
                .get_entity(name)
                .expect("named entity not found");
            self.maybe_get_by_id_mut::<T>(entity)
        }

        // Resource Management
        #[inline]
        pub fn inject_resource<T>(&mut self, value: T) -> Result<&mut Self>
        where
            T: ScriptResource + 'static,
        {
            self.world
                .resource_scope(|world, mut index: Mut<WorldIndex>| {
                    index.insert_named_resource(value, world)
                })?;
            Ok(self)
        }

        #[inline]
        pub fn insert_non_send_resource<T: 'static>(&mut self, value: T) -> &mut Self {
            self.world.insert_non_send_resource(value);
            self
        }

        #[inline]
        pub fn non_send_resource_mut<T: 'static>(&mut self) -> Mut<T> {
            self.world.non_send_resource_mut()
        }

        #[inline]
        pub fn resource_mut<T: Resource>(&mut self) -> Mut<T> {
            self.world.get_resource_mut().expect("unset resource")
        }

        #[inline]
        pub fn maybe_resource_mut<T: Resource>(&mut self) -> Option<Mut<T>> {
            self.world.get_resource_mut()
        }

        #[inline]
        pub fn remove_resource<T: ScriptResource>(&mut self) -> Option<T> {
            if let Some(value) = self.world.remove_resource::<T>() {
                self.world
                    .resource_mut::<WorldIndex>()
                    .remove_named_resource::<T>(value.resource_name());
                Some(value)
            } else {
                None
            }
        }

        #[inline]
        pub fn resource_scope<T: Resource, U>(
            &mut self,
            f: impl FnOnce(HeapMut, Mut<T>) -> U,
        ) -> U {
            self.world
                .resource_scope(|world, t: Mut<T>| f(HeapMut::wrap(world), t))
        }

        #[inline]
        pub fn query<Q>(&mut self) -> QueryState<Q, ()>
        where
            Q: QueryData,
        {
            self.world.query::<Q>()
        }

        #[inline]
        pub fn query_filtered<Q, F>(&mut self) -> QueryState<Q, F>
        where
            Q: QueryData,
            F: QueryFilter,
        {
            self.world.query_filtered::<Q, F>()
        }
    };
}

/// An immutable wrapper around World that provides better and name-based accessors.
#[derive(Copy, Clone)]
pub struct HeapRef<'a> {
    world: &'a World,
}

impl<'a> HeapRef<'a> {
    #[inline]
    pub fn wrap(world: &'a World) -> Self {
        Self { world }
    }

    impl_immutable_heap_methods!();
}

/// A mutable wrapper around World that provides named-based scriptable creation methods.
pub struct HeapMut<'a> {
    world: &'a mut World,
}

impl<'a> HeapMut<'a> {
    #[inline]
    pub fn wrap(world: &'a mut World) -> Self {
        Self { world }
    }

    #[inline]
    pub fn as_ref(&self) -> HeapRef {
        HeapRef::wrap(self.world)
    }

    #[inline]
    pub fn as_mut(&mut self) -> HeapMut {
        HeapMut::wrap(self.world)
    }

    impl_immutable_heap_methods!();
    impl_mutable_heap_methods!();
}

/// A wrapper around world that provides name-based scriptable creation methods.
pub struct Heap {
    world: World,
}

impl Default for Heap {
    fn default() -> Self {
        let mut world = World::default();
        world.insert_resource(WorldIndex::default());
        Self { world }
    }
}

impl Heap {
    impl_immutable_heap_methods!();
    impl_mutable_heap_methods!();

    pub fn as_mut(&mut self) -> HeapMut {
        HeapMut::wrap(&mut self.world)
    }
}
