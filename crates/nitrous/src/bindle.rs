// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

use crate::{memory::ComponentLookup, MethodMetadata, ScriptComponent};
use bevy_ecs::component::Component;
use bevy_utils::all_tuples;

/// A Bindle is a nitrous' version of Bevy's Bundle.
///
/// # Safety
///     the info must be returned in the declaration order and the same order
///     as Bundle iterates over components.
pub unsafe trait Bindle: Send + Sync + 'static {
    fn script_component_infos(&self) -> Vec<(&'static str, Vec<MethodMetadata>, ComponentLookup)>;
}

unsafe impl<C: ScriptComponent + Component> Bindle for C {
    fn script_component_infos(&self) -> Vec<(&'static str, Vec<MethodMetadata>, ComponentLookup)> {
        vec![(
            self.component_name(),
            self.method_metadata(),
            ComponentLookup::new::<C>(),
        )]
    }
}

macro_rules! tuple_impl {
    ($($name: ident),*) => {
        unsafe impl<$($name: Bindle),*> Bindle for ($($name,)*) {
            #[allow(unused_variables, unused_mut)]
            fn script_component_infos(&self) -> Vec<(&'static str, Vec<MethodMetadata>, ComponentLookup)> {
                let mut out = Vec::new();
                #[allow(non_snake_case)]
                let ($($name,)*) = self;
                $(
                    for info in $name.script_component_infos().drain(..) {
                        out.push(info);
                    }
                )*
                out
            }
        }
    }
}

all_tuples!(tuple_impl, 0, 15, B);
