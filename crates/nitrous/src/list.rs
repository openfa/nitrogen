// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::Value;
use anyhow::{anyhow, ensure, Result};
use libyaml::{tag, Emitter, Event, SequenceStyle};
use serde_yaml::Sequence;
use std::fmt::Debug;

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct List {
    v: Vec<Value>,
}

impl List {
    pub fn from_slice<T: Into<Value> + Copy>(values: &[T]) -> Self {
        let mut out = Self::default();
        for &v in values.iter() {
            out.push(v.into());
        }
        out
    }

    pub fn to_array<'a, T: TryFrom<&'a Value> + Copy + Default, const N: usize>(
        &'a self,
    ) -> Result<[T; N]>
    where
        <T as TryFrom<&'a Value>>::Error: std::fmt::Display + 'static,
    {
        let mut out = [T::default(); N];
        for (i, pv) in self.v.iter().enumerate() {
            ensure!(i < N, "too many coords");
            let decoded = T::try_from(pv).map_err(|e| anyhow!("try_from value failed: {e}"))?;
            out[i] = decoded;
        }
        Ok(out)
    }

    pub fn push(&mut self, val: Value) {
        self.v.push(val);
    }

    pub fn iter(&self) -> impl Iterator<Item = &Value> {
        self.v.iter()
    }

    pub(crate) fn emit_yaml(&self, emitter: &mut Emitter) -> Result<()> {
        let mut style = None;
        if self.v.len() < 5 && self.v.iter().all(|v| v.is_numeric()) {
            style = Some(SequenceStyle::Flow);
        }

        emitter.emit(Event::SequenceStart {
            anchor: None,
            tag: Some(tag::SEQ.to_string()),
            implicit: true,
            style,
        })?;
        for value in self.v.iter() {
            value.emit_yaml(emitter)?;
        }
        emitter.emit(Event::SequenceEnd)?;
        Ok(())
    }

    pub(crate) fn deserialize_yaml(seq: &Sequence) -> Result<Self> {
        let mut l = List::default();
        for yval in seq {
            l.push(Value::deserialize_yaml(yval)?);
        }
        Ok(l)
    }
}
