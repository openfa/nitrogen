// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
mod dynamics;

pub use crate::dynamics::{
    PhysicsEvent, PhysicsMailbox, RigidBodyDynamics, SatelliteDynamics, SatelliteDynamicsStep,
    VehicleDynamics,
};

use absolute_unit::prelude::*;
use anyhow::Result;
use arcball::ArcBallStep;
use bevy_ecs::prelude::*;
use geodb::{GeoDb, GeoDbStep};
use geodesy::{GeodeBB, GeodeticBB};
use log::warn;
use marker::{Markers, MarkersStep};
use nitrous::{inject_nitrous_resource, NitrousResource};
use phase::{BodyMotion, CollisionImpostor, Frame};
use planck::{rgb, Color};
use rapier3d_f64::{na::Vector3, parry::shape::SharedShape, prelude::*};
use runtime::{report_errors, Extension, Runtime, TimeStep};
use std::collections::HashSet;
use terrain::TerrainStep;
use vehicle::GearLayout;

/*
"Physics" is a hard problem [citation needed]. We tackle it here by letting rapier do everything.

This roughly breaks down as:
    Phase 1: Feed all of our Component state into rapier
    ... Phase 1n: Run dynamics customization systems that add forces each frame to the rapier state
    Phase 2: Let rapier do its work
    Phase 3: Read out position, motion, and touch data back from rapier into Components
    ... Phase 3n: Systems that need to be aware of touch events (like damage) read the mailboxes

Some stuff is "hard" so we have to handle it carefully.

Terrain collision.
  Right now, for each mover, we get a height field each frame under that collider,
  and delete it after. This is slow and awful, but we can at least avoid adding the
  slice to the collision set if it's far enough away from its mover.

Undercarriage / tires:
  The joint system is perfect for this: it's a prismatic joint with positional motor with
  appropriate spring constants. This requires an n'th rigidbody for each strut and tire.
  If we do it this way, I don't think we need to model gear forces?

Groups:
  Rapier has 31 groups to optimize collision detection. The deal here is that a collider can be
  in one or more of these groups and can set a "filter" to collide with only some other groups.

  We have the following groups:
    GROUP1 - static objects; buildings and such in the terrain
    GROUP2 - movers; planes, cars, tanks, missiles, etc
    GROUP3 - terrain; terrain slices that we put under each mover

  Ideally we'd be able to put each terrain slice in a group with

Tunneling:
  Movers need to use continuous collision detection. We've turned it on, but it doesn't
  appear to be working.

Required Components:
  Static: CollisionImpostor, Frame
  // Dynamic: ^Static^ + BodyMotion
 */

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum PhysicsStep {
    // Sim
    PullComponents,
    Step,
    RestoreComponents,

    PrepareMailboxes,
    CollideTerrain,

    // Frame
    DisplayDebug,
}

#[derive(NitrousResource)]
pub struct Physics {
    // Mapped to and from Components
    bodies: RigidBodySet,
    colliders: ColliderSet,

    // Ground management state
    terrain_collision_handles: Vec<ColliderHandle>,

    // Random physics state that we have to keep around.
    islands: IslandManager,
    broad_phase: DefaultBroadPhase,
    narrow_phase: NarrowPhase,
    impulse_joints: ImpulseJointSet,
    multibody_joints: MultibodyJointSet,
    ccd_solver: CCDSolver,
    query_pipeline: QueryPipeline,
    parameters: IntegrationParameters,

    // The actual physics pipeline that uses the above state.
    physics_pipeline: PhysicsPipeline,
}

impl Extension for Physics {
    type Opts = ();

    fn init(runtime: &mut Runtime, _: Self::Opts) -> Result<()> {
        let physics = Physics::new(runtime.resource::<TimeStep>());
        runtime.inject_resource(physics)?;

        runtime.add_sim_system(
            Self::sys_pull_components_into_sets
                .pipe(report_errors)
                .in_set(PhysicsStep::PullComponents)
                .after(SatelliteDynamicsStep::Simulate)
                .after(ArcBallStep::ApplyInput),
        );
        runtime.add_sim_system(
            Self::sys_physics_step
                .pipe(report_errors)
                .in_set(PhysicsStep::Step)
                .after(PhysicsStep::PullComponents),
        );
        runtime.add_sim_system(
            Self::sys_push_sets_to_components
                .pipe(report_errors)
                .in_set(PhysicsStep::RestoreComponents)
                .after(PhysicsStep::Step),
        );

        // runtime.add_sim_system(
        //     Self::sys_empty_collision_mailboxes
        //         .in_set(PhysicsStep::PrepareMailboxes)
        //         .before(PhysicsStep::CollideTerrain),
        // );
        // runtime.add_sim_system(
        //     Self::sys_collide_entity_vs_terrain
        //         .pipe(report_errors)
        //         .in_set(PhysicsStep::CollideTerrain)
        //         .before(ArcBallStep::ApplyInput)
        //         .before(RigidBodyDynamicsStep::Simulate)
        //         .ambiguous_with(SatelliteDynamicsStep::Simulate),
        // );
        //
        runtime.add_frame_system(
            Self::sys_show_colliders
                .in_set(PhysicsStep::DisplayDebug)
                .before(MarkersStep::UploadGeometry)
                .ambiguous_with(TerrainStep::OptimizePatches)
                .ambiguous_with(GeoDbStep::CheckDownloads),
        );

        Ok(())
    }
}

#[inject_nitrous_resource]
impl Physics {
    const STATIC_GROUP: Group = Group::GROUP_1;
    const MOVER_GROUP: Group = Group::GROUP_2;
    const TERRAIN_GROUP: Group = Group::GROUP_3;

    fn new(step: &TimeStep) -> Self {
        let parameters = IntegrationParameters {
            dt: step.fixed_sim_step().to_seconds(),
            min_ccd_dt: step.fixed_sim_step().to_seconds() / 100.,
            length_unit: 1.,
            ..Default::default()
        };

        Self {
            bodies: RigidBodySet::default(),
            colliders: ColliderSet::default(),
            terrain_collision_handles: Vec::new(),
            islands: IslandManager::default(),
            broad_phase: DefaultBroadPhase::new(),
            narrow_phase: NarrowPhase::new(),
            impulse_joints: ImpulseJointSet::new(),
            multibody_joints: MultibodyJointSet::new(),
            ccd_solver: CCDSolver::new(),
            query_pipeline: QueryPipeline::new(),
            parameters,
            physics_pipeline: PhysicsPipeline::new(),
        }
    }

    // Assumptions:
    //   * The collision will not change.
    //   * The collision is attached to the rigidbody immediately
    fn sys_pull_components_into_sets(
        mut physics: ResMut<Physics>,
        step: Res<TimeStep>,
        geodb: Res<GeoDb>,
        buildings: Query<(&mut CollisionImpostor, &Frame), Without<VehicleDynamics>>,
        vehicles: Query<(
            &mut VehicleDynamics,
            Option<&GearLayout>,
            &CollisionImpostor,
            &Frame,
            &BodyMotion,
        )>,
        bodies: Query<(
            &mut RigidBodyDynamics,
            Option<&CollisionImpostor>,
            &Frame,
            Option<&BodyMotion>,
        )>,
        movers: Query<(&CollisionImpostor, &Frame, &BodyMotion)>,
    ) -> Result<()> {
        physics.pull_components_into_sets(&step, &geodb, buildings, vehicles, bodies, movers)
    }

    fn pull_components_into_sets(
        &mut self,
        step: &TimeStep,
        geodb: &GeoDb,
        _buildings: Query<(&mut CollisionImpostor, &Frame), Without<VehicleDynamics>>,
        _vehicles: Query<(
            &mut VehicleDynamics,
            Option<&GearLayout>,
            &CollisionImpostor,
            &Frame,
            &BodyMotion,
        )>,
        mut bodies: Query<(
            &mut RigidBodyDynamics,
            Option<&CollisionImpostor>,
            &Frame,
            Option<&BodyMotion>,
        )>,
        movers: Query<(&CollisionImpostor, &Frame, &BodyMotion)>,
    ) -> Result<()> {
        // Clear out last step's terrain collision and rebuild it
        for hdl in self.terrain_collision_handles.drain(..) {
            self.colliders
                .remove(hdl, &mut self.islands, &mut self.bodies, false);
        }

        // Create the ground that our movers walk on.
        for (collision, frame, motion) in movers.iter() {
            // Transform the box out of aabb into world and compute the minimum box height
            // and map space extents of the covered region.
            let corners = collision
                .shape()
                .local_aabb()
                .vertices()
                .map(|p| frame.local_to_world(p.into()));
            let gabb = GeodeticBB::from_world_space_points(&corners);

            let move_distance = motion.cg_velocity() * step.fixed_sim_step_time() * scalar!(1.5);

            // Use the map space extents to get a coverage height field.
            // Note that the height field is in _local_ coordinates.
            let Some(grid) =
                geodb.get_best_sample_grid(&GeodeBB::from_geodetic_bb(&gabb), &move_distance)
            else {
                warn!("skipping ground collision because we couldn't find the ground yet");
                continue;
            };

            // Check if there is any possibility of a collision with the height field.
            // Don't make the collision engine deal with it if it can't actually collide.
            let max_terrain_height = grid.maximum_height();
            let min_shape_height = frame.geodetic().asl()
                - meters!(collision.shape().local_bounding_sphere().radius())
                - move_distance;
            if max_terrain_height + meters!(10) < min_shape_height {
                // skip terrain slice addition because there's no possibility of a collision
                continue;
            }

            // Add the height field to our collision database
            let (mut heightfield, iframe) = grid.to_height_field();
            heightfield.set_flags(HeightFieldFlags::FIX_INTERNAL_EDGES);
            let builder = ColliderBuilder::new(SharedShape::new(heightfield))
                .position(iframe)
                .collision_groups(InteractionGroups {
                    memberships: Self::TERRAIN_GROUP,
                    filter: Self::MOVER_GROUP,
                })
                .user_data(rgb!(0x00FF00).to_u32() as u128);
            let hdl = self.colliders.insert(builder);
            self.terrain_collision_handles.push(hdl);
        }

        // Collect current bodies.
        let mut defunct_bodies = HashSet::new();
        for (hdl, _) in self.bodies.iter() {
            defunct_bodies.insert(hdl);
        }

        // Upload new rigid bodies.
        for (mut body, collision, frame, motion) in bodies.iter_mut() {
            if let Some(hdl) = body.handle() {
                // Touch our body to keep it alive.
                defunct_bodies.remove(&hdl);
            } else {
                // Create the new body.
                let mut builder = match motion {
                    Some(motion) => RigidBodyBuilder::dynamic()
                        .linvel(motion.linear_velocity_na())
                        .angvel(motion.angular_velocity_na())
                        .ccd_enabled(true)
                        .soft_ccd_prediction(15.),
                    None => RigidBodyBuilder::fixed(),
                };
                builder = builder.position(frame.isometry3());
                let hdl = self.bodies.insert(builder);

                if let Some(collision) = collision {
                    let shared = SharedShape::compound(collision.shape().shapes().to_vec());
                    let (memberships, filter) = match motion {
                        Some(_) => (Self::MOVER_GROUP, Group::all()),
                        None => (Self::STATIC_GROUP, Self::MOVER_GROUP),
                    };
                    // TODO: expose all of the collision options somehow
                    let builder = ColliderBuilder::new(shared)
                        .friction(0.5)
                        .collision_groups(InteractionGroups {
                            memberships,
                            filter,
                        })
                        .contact_skin(0.1)
                        .user_data(rgb!(0xFF00FF).to_u32() as u128);
                    self.colliders
                        .insert_with_parent(builder, hdl, &mut self.bodies);
                }

                // Track the rigidbody handle -- no need to track the collider handle for these
                // as delete of the rigid body will cascade automatically.
                body.set_handle(hdl);
            }
        }

        // Clear out any bodies that are no longer live.
        for hdl in defunct_bodies.drain() {
            self.bodies.remove(
                hdl,
                &mut self.islands,
                &mut self.colliders,
                &mut self.impulse_joints,
                &mut self.multibody_joints,
                true,
            );
        }

        Ok(())
    }

    fn sys_physics_step(mut physics: ResMut<Physics>) -> Result<()> {
        physics.physics_step()
    }

    fn physics_step(&mut self) -> Result<()> {
        // Manually apply the correct gravity force to each mover
        for (_hdl, body) in self.bodies.iter_mut() {
            let gravity = body.position().translation.inverse().vector.normalize() * 9.8;
            body.add_force(gravity, false);
        }

        // TODO: get out physics events
        let physics_hooks = ();
        let event_handler = ();
        self.physics_pipeline.step(
            &Vector3::zeros(),
            &self.parameters,
            &mut self.islands,
            &mut self.broad_phase,
            &mut self.narrow_phase,
            &mut self.bodies,
            &mut self.colliders,
            &mut self.impulse_joints,
            &mut self.multibody_joints,
            &mut self.ccd_solver,
            Some(&mut self.query_pipeline),
            &physics_hooks,
            &event_handler,
        );

        Ok(())
    }

    // Read back rigid body position updates into components on Entity
    fn sys_push_sets_to_components(
        physics: Res<Physics>,
        update: Query<(
            &RigidBodyDynamics,
            Option<&CollisionImpostor>,
            &mut Frame,
            Option<&mut BodyMotion>,
        )>,
    ) -> Result<()> {
        physics.push_sets_to_components(update)
    }

    fn push_sets_to_components(
        &self,
        mut update: Query<(
            &RigidBodyDynamics,
            Option<&CollisionImpostor>,
            &mut Frame,
            Option<&mut BodyMotion>,
        )>,
    ) -> Result<()> {
        update
            .par_iter_mut()
            .for_each(|(body, _collision, mut frame, motion)| {
                let hdl = body.handle().expect("inserted above");
                let body = self.bodies.get(hdl).expect("inserted above");
                *frame = Frame::from_isometry3(*body.position());
                if let Some(mut motion) = motion {
                    motion.set_angular_velocity_na(body.angvel());
                    motion.set_linear_velocity_na(body.linvel());
                }
            });

        Ok(())
    }

    // Clean up mailboxes at the start of every sim step.
    // fn sys_empty_collision_mailboxes(mut query: Query<&mut PhysicsMailbox>) {
    //     query.par_iter_mut().for_each(|mut mailbox| {
    //         mailbox.clear();
    //     });
    // }

    // fn sys_collide_entity_vs_terrain(
    //     step: Res<TimeStep>,
    //     geodb: Res<GeoDb>,
    //     mut query: Query<(&CollisionImpostor, &Frame, &BodyMotion, &mut PhysicsMailbox)>,
    // ) -> Result<()> {
    //     // for (collider, frame, motion, mut mailbox) in query.iter_mut() {
    //     query
    //         .par_iter_mut()
    //         .for_each(|(collider, frame, motion, mut mailbox)| {
    //             // Transform the box out of aabb into world and compute the minimum box height
    //             // and map space extents of the covered region.
    //             let corners = collider
    //                 .shape()
    //                 .local_aabb()
    //                 .vertices()
    //                 .map(|p| frame.local_to_world(p.into()));
    //             let gabb = GeodeticBB::from_world_space_points(&corners);
    //
    //             // Use the map space extents to get a coverage height field.
    //             // Note that the height field is in _local_ coordinates.
    //             let Some((heightfield, iframe)) =
    //                 geodb.get_best_height_field(&GeodeBB::from_geodetic_bb(&gabb), &meters!(1))
    //             else {
    //                 warn!("skipping ground collision because we couldn't find the ground");
    //                 return;
    //             };
    //
    //             // Get the motion in world space
    //             let motion_in_frame = frame.facing() * *motion.world_space_velocity().dvec3();
    //
    //             // Note: do the collision in world space for now. It might be more efficient to do the
    //             // collision in a terrain local space using f32, but for now this simpler.
    //             let maybe_collision = cast_shapes(
    //                 // Heightfield
    //                 &iframe,
    //                 &Vector3::<f64>::zeros(), // motion
    //                 &heightfield,
    //                 // Mover
    //                 &frame.isometry3(),
    //                 &motion_in_frame.into(),
    //                 collider.shape(),
    //                 // Options
    //                 ShapeCastOptions {
    //                     max_time_of_impact: step.fixed_sim_step().to_seconds(),
    //                     target_distance: 0.01, // cm precision
    //                     stop_at_penetration: false,
    //                     compute_impact_geometry_on_penetration: true,
    //                 },
    //             );
    //             if let Ok(Some(collision)) = maybe_collision {
    //                 mailbox.insert(PhysicsEvent::TerrainCollision {
    //                     time_of_impact: collision.time_of_impact,
    //                     terrain_position: Pt3::<Meters>::new_dvec3(
    //                         iframe
    //                             .translation
    //                             .transform_point(&collision.witness1)
    //                             .into(),
    //                     ),
    //                     terrain_normal: iframe.rotation.transform_vector(&collision.normal1).into(),
    //                     object_position: Pt3::<Meters>::new_dvec3(collision.witness2.into()),
    //                     object_normal: collision.normal2.into(),
    //                 });
    //             }
    //             // if let Some(collision) = maybe_collision {}
    //
    //             // Apply kinematics?
    //         });
    //     Ok(())
    // }

    fn sys_show_colliders(
        physics: Res<Physics>,
        // geodb: Res<GeoDb>,
        // query_movers: Query<(&CollisionImpostor, &Frame, &BodyMotion, &PhysicsMailbox)>,
        // query_buildings: Query<(&CollisionImpostor, &Frame), Without<BodyMotion>>,
        mut markers: ResMut<Markers>,
    ) {
        for (_hdl, collider) in physics.colliders.iter() {
            let clr = Color::rgba(collider.user_data as u32);
            markers.draw_local_collision(
                &collider.position().into(),
                &collider.shape().as_typed_shape(),
                clr,
            );
        }

        // for (collider, frame, _, mailbox) in query_movers.iter() {
        //     // Transform the box out of aabb into world and compute the minimum box height
        //     // and map space extents of the covered region.
        //     let corners = collider
        //         .shape()
        //         .local_aabb()
        //         .vertices()
        //         .map(|p| frame.local_to_world(p.into()));
        //     let gabb = GeodeticBB::from_world_space_points(&corners);
        //
        //     // Use the map space extents to get a coverage heightmap
        //     let Some((heightfield, iframe)) =
        //         geodb.get_best_height_field(&gabb.geode_bb(), &meters!(1))
        //     else {
        //         continue;
        //     };
        //
        //     // Draw the 2 colliders
        //     markers.draw_local_collision(frame, collider, rgb!(0xFF00FF));
        //     markers.draw_local_collision(
        //         &Frame::identity(),
        //         &CollisionImpostor::new(Compound::new(vec![(
        //             iframe,
        //             SharedShape::new(heightfield),
        //         )])),
        //         rgb!(0x00FF00),
        //     );
        //
        //     for event in mailbox.events() {
        //         match event {
        //             PhysicsEvent::TerrainCollision {
        //                 terrain_position,
        //                 terrain_normal,
        //                 ..
        //             } => {
        //                 markers.draw_arrow(
        //                     rgb!(0x0000FF),
        //                     &Arrow::new(
        //                         *terrain_position,
        //                         *terrain_normal,
        //                         meters!(10),
        //                         meters!(0.75),
        //                     ),
        //                 );
        //             }
        //         }
        //     }
        // }
        //
        // for (collider, frame) in query_buildings.iter() {
        //     markers.draw_local_collision(frame, collider, rgb!(0xDD00FF));
        // }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use mantle::Core;

    #[test]
    fn it_can_boot() -> Result<()> {
        let mut runtime = Core::for_test()?;
        runtime.load_extension::<Physics>()?;
        Ok(())
    }
}
