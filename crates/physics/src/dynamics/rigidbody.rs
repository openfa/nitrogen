// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use bevy_ecs::prelude::*;
use nitrous::{inject_nitrous_component, NitrousComponent};
use rapier3d_f64::dynamics::RigidBodyHandle;

/// Falls and responds to collisions.
///  TODO: figure out how to sleep when not in motion, despite the constant force of gravity
#[derive(Debug, Default, NitrousComponent)]
#[component(name = "dynamics")]
pub struct RigidBodyDynamics {
    handle: Option<RigidBodyHandle>,
}

#[inject_nitrous_component]
impl RigidBodyDynamics {
    pub fn handle(&self) -> Option<RigidBodyHandle> {
        self.handle
    }

    pub fn set_handle(&mut self, handle: RigidBodyHandle) {
        self.handle = Some(handle);
    }
}
