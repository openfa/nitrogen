// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
mod rigidbody;
mod satellite;
mod vehicle;

pub use rigidbody::RigidBodyDynamics;
pub use satellite::{SatelliteDynamics, SatelliteDynamicsStep};
pub use vehicle::VehicleDynamics;

use absolute_unit::prelude::*;
use bevy_ecs::prelude::*;
use glam::DVec3;
use nitrous::{inject_nitrous_component, NitrousComponent};

#[derive(Clone, Debug)]
pub enum PhysicsEvent {
    TerrainCollision {
        // Between 0 and step.fixed_sim_step().to_seconds(); e.g. within the next step
        time_of_impact: f64,

        // World space position and normal of collision.
        terrain_position: Pt3<Meters>,
        terrain_normal: DVec3,

        // Object space position and normal on the collidee.
        object_position: Pt3<Meters>,
        object_normal: DVec3,
    },
}

#[derive(Debug, Default, NitrousComponent)]
pub struct PhysicsMailbox {
    inbox: Vec<PhysicsEvent>,
}

#[inject_nitrous_component]
impl PhysicsMailbox {
    pub fn insert(&mut self, event: PhysicsEvent) {
        self.inbox.push(event);
    }

    pub fn events(&self) -> impl Iterator<Item = &PhysicsEvent> {
        self.inbox.iter()
    }

    pub fn clear(&mut self) {
        self.inbox.clear();
    }
}
