// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::PhysicsStep;
use absolute_unit::prelude::*;
use anyhow::Result;
use arcball::ArcBallStep;
use bevy_ecs::prelude::*;
use glam::{DQuat, DVec3};
use hifitime::Epoch;
use nitrous::{inject_nitrous_component, NitrousComponent};
use orrery::Orrery;
use phase::Frame;
use runtime::{report_errors, Extension, Runtime};
use satellite_catalog::SatelliteCatalog;
use sgp4::chrono;

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum SatelliteDynamicsStep {
    Simulate,
}

/// Follow a satellite trajectory based on orbital parameters.
/// Does not model atmosphere in any way, so make sure the orbit is high enough.
#[derive(Debug, NitrousComponent)]
pub struct SatelliteDynamics {
    name: String,
}

impl Extension for SatelliteDynamics {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.add_sim_system(
            Self::sys_update_position
                .pipe(report_errors)
                .in_set(SatelliteDynamicsStep::Simulate)
                .before(ArcBallStep::ApplyInput)
                .before(PhysicsStep::Step),
        );
        Ok(())
    }
}

#[inject_nitrous_component]
impl SatelliteDynamics {
    pub fn new(body_name: &str) -> Self {
        Self {
            name: body_name.to_owned(),
        }
    }

    fn sys_update_position(
        orrery: Res<Orrery>,
        mut query: Query<(&SatelliteDynamics, &mut Frame)>,
    ) -> Result<()> {
        let now = Self::now_to_chrono(orrery.local_now());
        query.par_iter_mut().for_each(|(satellite, mut frame)| {
            if let Ok(next) = satellite.compute_position_chrono(now) {
                *frame = next;
            }
        });
        Ok(())
    }

    fn now_to_chrono(now: &Epoch) -> chrono::NaiveDateTime {
        let (year, month, day, hour, minute, second, nanos) = now.to_gregorian_utc();
        chrono::NaiveDateTime::new(
            chrono::NaiveDate::from_ymd_opt(year, month as u32, day as u32).unwrap(),
            chrono::NaiveTime::from_hms_nano_opt(hour as u32, minute as u32, second as u32, nanos)
                .unwrap(),
        )
    }

    pub fn compute_position(&self, now: &Epoch) -> Result<Frame> {
        let now = Self::now_to_chrono(now);
        self.compute_position_chrono(now)
    }

    fn compute_position_chrono(&self, now: chrono::NaiveDateTime) -> Result<Frame> {
        let entry = SatelliteCatalog::elements(&self.name)?;
        let elements: sgp4::Elements = entry.into();
        let constants = sgp4::Constants::from_elements(&elements)?;
        let epoch_mins = elements.datetime_to_minutes_since_epoch(&now)?;
        let prediction = constants.propagate(epoch_mins)?;

        let pt = Pt3::new(
            meters!(kilometers!(prediction.position[0])),
            meters!(kilometers!(prediction.position[1])),
            meters!(kilometers!(prediction.position[2])),
        );

        // TODO: Assuming only prograde with look-down for now.
        let up = pt.dvec3().normalize();
        let forward = DVec3::from_array(prediction.velocity).normalize();
        let side = up.cross(forward);
        let to_fwd = DQuat::from_rotation_arc(DVec3::Z, forward);
        let fake_up = to_fwd * DVec3::Y;
        let to_up = DQuat::from_rotation_arc(fake_up, up);
        let to_lookdown = DQuat::from_axis_angle(side, radians!(degrees!(45)).f64());
        let facing = to_lookdown * to_up * to_fwd;

        Ok(Frame::new(pt, facing))
    }
}
