// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use anyhow::{anyhow, bail, Result};
use jiff::Zoned;
use std::{collections::HashMap, sync::OnceLock};

/// Provide built-in (no network) access to the celestrack (active) satellite catalog
/// as downloaded on 11 Sept 24.
///
/// Refresh by curling the following url:
///     https://celestrak.org/NORAD/elements/gp.php?GROUP=active&FORMAT=csv

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
#[repr(u8)]
pub enum EphemerisType {
    Sgp = 0,
    Sgp4 = 2,
    Ppt3 = 3,
    Sgp4Xp = 4,
    SpecialPerturbations = 6,
}

impl TryFrom<u8> for EphemerisType {
    type Error = anyhow::Error;
    fn try_from(value: u8) -> Result<Self> {
        Ok(match value {
            0 => Self::Sgp,
            2 => Self::Sgp4,
            3 => Self::Ppt3,
            4 => Self::Sgp4Xp,
            6 => Self::SpecialPerturbations,
            _ => bail!("invalid EPHEMERIS_TYPE value {value}"),
        })
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
#[repr(u8)]
pub enum ClassificationType {
    Unclassified = b'U',
    Secret = b'S',
}

impl TryFrom<char> for ClassificationType {
    type Error = anyhow::Error;
    fn try_from(value: char) -> Result<Self> {
        Ok(match value {
            'U' => Self::Unclassified,
            'S' => Self::Secret,
            _ => bail!("invalid CLASSIFICATION_TYPE value {value}"),
        })
    }
}

/// As defined by Orbital Mean-Elements Message
#[derive(Clone, Debug)]
pub struct OrbitalElements {
    // OBJECT_NAME
    object_name: String,

    // OBJECT_ID
    object_id: String,

    // EPOCH
    epoch: Zoned,

    // MEAN_MOTION: required by SGP4 in lieu of semi-major; in units of Revolutions per Day
    // TODO: implement angle units for Revolution and time units for Day so we can Unitize this
    // TODO: AngularVelocity<Revolutions, Days>
    mean_motion: f64,

    // ECCENTRICITY
    eccentricity: Angle<Radians>,

    // INCLINATION
    inclination: Angle<Degrees>,

    // RA_OF_ASC_NODE
    ra_of_asc_node: Angle<Degrees>,

    // ARG_OF_PERICENTER
    arg_of_pericenter: Angle<Degrees>,

    // MEAN_ANOMALY
    mean_anomaly: Angle<Degrees>,

    // EPHEMERIS_TYPE
    ephemeris_type: EphemerisType,

    // CLASSIFICATION_TYPE
    classification_type: ClassificationType,

    // NORAD_CAT_ID
    norad_cat_id: u64,

    // ELEMENT_SET_NO
    element_set_no: u64,

    // REV_AT_EPOCH
    // TODO: Angle<Revolutions>
    rev_at_epoch: u64,

    // BSTAR
    // TODO: units are I/Revolutions; worth creating a unit type.
    b_star: f64,

    // MEAN_MOTION_DOT
    // First derivative of the mean_motion
    // TODO: AngularAcceleration<Revolutions, Days>
    mean_motion_dot: f64,

    // MEAN_MOTION_DDOT
    // Second derivative of the mean_motion
    // TODO: AngularJerk<Revolutions, Days>
    mean_motion_ddot: f64,
}

impl OrbitalElements {
    pub fn object_name(&self) -> &str {
        &self.object_name
    }

    pub fn object_id(&self) -> &str {
        &self.object_id
    }

    pub fn epoch(&self) -> &Zoned {
        &self.epoch
    }

    pub fn mean_motion(&self) -> f64 {
        self.mean_motion
    }

    pub fn eccentricity(&self) -> Angle<Radians> {
        self.eccentricity
    }

    pub fn inclination(&self) -> Angle<Degrees> {
        self.inclination
    }

    pub fn ra_of_asc_node(&self) -> Angle<Degrees> {
        self.ra_of_asc_node
    }

    pub fn arg_of_pericenter(&self) -> Angle<Degrees> {
        self.arg_of_pericenter
    }

    pub fn mean_anomaly(&self) -> Angle<Degrees> {
        self.mean_anomaly
    }

    pub fn ephemeris_type(&self) -> EphemerisType {
        self.ephemeris_type
    }

    pub fn classification_type(&self) -> ClassificationType {
        self.classification_type
    }

    pub fn norad_cat_id(&self) -> u64 {
        self.norad_cat_id
    }

    pub fn element_set_no(&self) -> u64 {
        self.element_set_no
    }

    pub fn rev_at_epoch(&self) -> u64 {
        self.rev_at_epoch
    }

    pub fn b_star(&self) -> f64 {
        self.b_star
    }

    pub fn mean_motion_dot(&self) -> f64 {
        self.mean_motion_dot
    }

    pub fn mean_motion_ddot(&self) -> f64 {
        self.mean_motion_ddot
    }
}

impl From<&OrbitalElements> for sgp4::Elements {
    fn from(entry: &OrbitalElements) -> Self {
        let epoch = entry.epoch();
        sgp4::Elements {
            object_name: None,
            international_designator: None,
            norad_id: entry.norad_cat_id(),
            classification: sgp4::Classification::Unclassified,
            datetime: sgp4::chrono::NaiveDate::from_ymd_opt(
                epoch.year() as i32,
                epoch.month() as u32,
                epoch.day() as u32,
            )
            .unwrap()
            .and_hms_nano_opt(
                epoch.hour() as u32,
                epoch.minute() as u32,
                epoch.second() as u32,
                epoch.subsec_nanosecond() as u32,
            )
            .unwrap(),
            mean_motion_dot: entry.mean_motion_dot(),
            mean_motion_ddot: entry.mean_motion_ddot(),
            drag_term: entry.b_star(),
            element_set_number: entry.element_set_no(),
            inclination: entry.inclination().f64(),
            right_ascension: entry.ra_of_asc_node().f64(),
            eccentricity: entry.eccentricity().f64(),
            argument_of_perigee: entry.arg_of_pericenter().f64(),
            mean_anomaly: entry.mean_anomaly().f64(),
            mean_motion: entry.mean_motion(),
            revolution_number: entry.rev_at_epoch(),
            ephemeris_type: entry.ephemeris_type() as u8,
        }
    }
}

// Direct inline include of a bz2 compressed csv.
const ACTIVE_CSV_XZ: &[u8] = include_bytes!("../assets/active.csv.xz");

/// Provide orbital elements for active bodies in orbit around earth.
pub struct SatelliteCatalog;
impl SatelliteCatalog {
    pub fn elements(name: &str) -> Result<&OrbitalElements> {
        static PLANET_ORBITS: OnceLock<HashMap<String, OrbitalElements>> = OnceLock::new();
        let orbits = PLANET_ORBITS.get_or_init(|| {
            fn float(s: &str, name: &str, field: &str) -> f64 {
                s.parse::<f64>()
                    .unwrap_or_else(|_| panic!("malformed satellite field {field} at {name}"))
            }
            fn byte(s: &str, name: &str, field: &str) -> u8 {
                s.parse::<u8>()
                    .unwrap_or_else(|_| panic!("malformed satellite field {field} at {name}"))
            }
            fn integer(s: &str, name: &str, field: &str) -> u64 {
                s.parse::<u64>()
                    .unwrap_or_else(|_| panic!("malformed satellite field {field} at {name}"))
            }

            let decompressed = xz_wrapper::decompress(ACTIVE_CSV_XZ).unwrap();
            // let mut decompressed = Vec::with_capacity(1_502_989);
            // #[allow(clippy::useless_asref)]
            // lzma_rs::xz_decompress(&mut (ACTIVE_CSV_XZ.as_ref()), &mut decompressed).unwrap();

            let mut map = HashMap::new();
            let mut rdr = csv::ReaderBuilder::new().from_reader(decompressed.as_slice());
            for line in rdr.records() {
                let record = line.expect("malformed csv");
                let name = &record[0];
                let elements = OrbitalElements {
                    object_name: record[0].to_owned(),
                    object_id: record[1].to_owned(),
                    // FIXME: find a way to use TAI here
                    epoch: Zoned::strptime(
                        "%Y-%m-%dT%H:%M:%S.%f[%V]",
                        format!("{}[UTC]", &record[2]),
                    )
                    .unwrap(),
                    mean_motion: float(&record[3], name, "mean_motion"),
                    eccentricity: radians!(float(&record[4], name, "eccentricity")),
                    inclination: degrees!(float(&record[5], name, "inclination")),
                    ra_of_asc_node: degrees!(float(&record[6], name, "ra_of_asc_node")),
                    arg_of_pericenter: degrees!(float(&record[7], name, "arg_of_pericenter")),
                    mean_anomaly: degrees!(float(&record[8], name, "mean_anomaly")),
                    ephemeris_type: byte(&record[9], name, "ephemeris_type").try_into().unwrap(),
                    classification_type: record[10].chars().nth(0).unwrap().try_into().unwrap(),
                    norad_cat_id: integer(&record[11], name, "norad_cat_id"),
                    element_set_no: integer(&record[12], name, "element_set_no"),
                    rev_at_epoch: integer(&record[13], name, "rev_at_epoch"),
                    b_star: float(&record[14], name, "b_star"),
                    mean_motion_dot: float(&record[14], name, "mean_motion_dot"),
                    mean_motion_ddot: float(&record[14], name, "mean_motion_ddot"),
                };
                map.insert(elements.object_name.clone(), elements);
            }
            map
        });
        orbits
            .get(name)
            .ok_or_else(|| anyhow!("no such satellite {name}"))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() -> Result<()> {
        let orbit = SatelliteCatalog::elements("ISS (ZARYA)")?;
        assert_eq!(&orbit.object_id, "1998-067A");
        Ok(())
    }
}
