// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::offscreen::OffscreenRender;
use camera::ScreenCamera;
use mantle::Gpu;

#[derive(Debug)]
pub(crate) struct NormalAccumulator {
    spherical_pipeline: wgpu::RenderPipeline,
}

impl NormalAccumulator {
    pub(crate) const LAYER_NAME: &'static str = "terrain-acc-sph-normals";
    pub(crate) const NORMAL_ACCUMULATION_FORMAT: wgpu::TextureFormat =
        wgpu::TextureFormat::Rg32Float;

    pub(crate) fn new(
        layout_height_tiles: &wgpu::BindGroupLayout,
        offscreen_layout: &wgpu::BindGroupLayout,
        gpu: &Gpu,
    ) -> Self {
        let pipeline_layout =
            gpu.device()
                .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                    label: Some("terrain-accumulate-normals-pipeline-layout"),
                    push_constant_ranges: &[],
                    bind_group_layouts: &[
                        layout_height_tiles,
                        offscreen_layout,
                        &ScreenCamera::info_bind_group_layout(gpu),
                    ],
                });
        let shader = gpu.compile_shader(
            "acc_spherical_normals.wgsl",
            include_str!("../target/acc_spherical_normals.wgsl"),
        );

        let spherical_pipeline =
            gpu.device()
                .create_render_pipeline(&wgpu::RenderPipelineDescriptor {
                    label: Some("terrain-acc-colors-sph-pipeline"),
                    layout: Some(&pipeline_layout),
                    vertex: wgpu::VertexState {
                        module: &shader,
                        entry_point: "main_vert",
                        buffers: &[],
                        compilation_options: wgpu::PipelineCompilationOptions {
                            constants: &Default::default(),
                            zero_initialize_workgroup_memory: false,
                        },
                    },
                    fragment: Some(wgpu::FragmentState {
                        module: &shader,
                        entry_point: "main_frag",
                        targets: &[Some(wgpu::ColorTargetState {
                            format: Self::NORMAL_ACCUMULATION_FORMAT,
                            blend: None,
                            write_mask: wgpu::ColorWrites::ALL,
                        })],
                        compilation_options: wgpu::PipelineCompilationOptions {
                            constants: &Default::default(),
                            zero_initialize_workgroup_memory: false,
                        },
                    }),
                    primitive: wgpu::PrimitiveState {
                        topology: wgpu::PrimitiveTopology::TriangleList,
                        strip_index_format: None,
                        front_face: wgpu::FrontFace::Ccw,
                        cull_mode: Some(wgpu::Face::Back),
                        unclipped_depth: false,
                        polygon_mode: wgpu::PolygonMode::Fill,
                        conservative: false,
                    },
                    depth_stencil: None,
                    multisample: wgpu::MultisampleState {
                        count: 1,
                        mask: !0,
                        alpha_to_coverage_enabled: false,
                    },
                    multiview: None,
                });

        Self { spherical_pipeline }
    }

    pub(crate) fn accumulate_normals(
        &self,
        height_tiles_bind_group: &wgpu::BindGroup,
        gpu: &Gpu,
        camera: &mut ScreenCamera,
        encoder: &mut wgpu::CommandEncoder,
    ) {
        camera.ensure_layer(Self::LAYER_NAME, Self::NORMAL_ACCUMULATION_FORMAT, gpu);

        let mut rpass = camera.begin_render_pass(Self::LAYER_NAME, None, true, encoder);
        rpass.set_pipeline(&self.spherical_pipeline);
        rpass.set_bind_group(0, height_tiles_bind_group, &[]);
        rpass.set_bind_group(1, camera.bind_group(OffscreenRender::BINDING_NAME), &[]);
        rpass.set_bind_group(2, camera.info_bind_group(), &[]);
        rpass.draw(0..3, 0..1);
    }
}
