// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use mantle::Gpu;
use shader_shared::{binding, layout, ComputePipelineBuilder};

// GPU resources to displace the fully prepared vertex buffer from Patches by the fully prepared
// heights coming from Tiles.
#[derive(Debug)]
pub(crate) struct HeightDisplacer {
    bind_group_layout: wgpu::BindGroupLayout,
    bind_group: wgpu::BindGroup,
    vertex_count: u32,
    pipeline: wgpu::ComputePipeline,
}

impl HeightDisplacer {
    pub fn new(
        heights_bind_group_layout: &wgpu::BindGroupLayout,
        vertex_buffer: &wgpu::Buffer,
        _vertex_buffer_size: wgpu::BufferAddress,
        vertex_count: u32,
        gpu: &Gpu,
    ) -> Self {
        let (pipeline, mut layouts) =
            ComputePipelineBuilder::new("terrain-displace-height-pipeline", 2, gpu.device())
                .with_shader_wgsl(
                    "subdivide_displace_height.wgsl",
                    include_str!("../target/subdivide_displace_height.wgsl"),
                )
                .with_binding_entry(0, layout::comp::buffer_rw_for(0, vertex_buffer))
                .with_existing_layout(1, heights_bind_group_layout)
                .build();
        let bind_group_layout = layouts.remove(0);
        let bind_group = gpu.device().create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("terrain-geo-displace-height-bind-group"),
            layout: &bind_group_layout,
            entries: &[binding::buffer(0, vertex_buffer)],
        });

        Self {
            bind_group_layout,
            bind_group,
            vertex_count,
            pipeline,
        }
    }

    pub(crate) fn displace_heights(
        &self,
        heights_bind_group: &wgpu::BindGroup,
        encoder: &mut wgpu::CommandEncoder,
    ) {
        let mut cpass = encoder.begin_compute_pass(&wgpu::ComputePassDescriptor {
            label: Some("terrain-displace-height-compute-pass"),
            timestamp_writes: None,
        });

        // Displace vertices according to the height map
        cpass.set_pipeline(&self.pipeline);
        cpass.set_bind_group(0, &self.bind_group, &[]);
        cpass.set_bind_group(1, heights_bind_group, &[]);
        const WORKGROUP_WIDTH: u32 = 65536;
        let wg_x = (self.vertex_count % WORKGROUP_WIDTH).max(1);
        let wg_y = (self.vertex_count / WORKGROUP_WIDTH).max(1);
        cpass.dispatch_workgroups(wg_x, wg_y, 1);
    }

    pub(crate) fn bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        &self.bind_group_layout
    }

    pub(crate) fn bind_group(&self) -> &wgpu::BindGroup {
        &self.bind_group
    }
}
