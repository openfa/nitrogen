// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
mod accumulate_colors;
mod accumulate_normals;
mod common;
mod displace_heights;
mod offscreen;
mod patch;
mod tables;
mod tile;

pub use crate::patch::{PatchWinding, TerrainVertex};

use crate::{
    accumulate_colors::ColorAccumulator, accumulate_normals::NormalAccumulator,
    common::OptimizeCamera, displace_heights::HeightDisplacer, offscreen::OffscreenRender,
    patch::PatchManager, tile::TileManager,
};
use anyhow::Result;
use bevy_ecs::prelude::*;
use camera::{CameraStep, ScreenCamera};
use geodb::{GeoDb, GeoDbStep};
use gui::GuiStep;
use mantle::{CpuDetailLevel, CurrentEncoder, Gpu, GpuDetailLevel, GpuStep, Window};
use marker::{Markers, MarkersStep};
use nitrous::{inject_nitrous_resource, method, NitrousResource};
use orrery::OrreryStep;
use runtime::{report_errors, Extension, Runtime, RuntimeResource};
use shader_shared::binding;
use stars::StarsStep;
use std::ops::Range;

#[allow(unused)]
const DBG_COLORS_BY_LEVEL: [[f32; 3]; 19] = [
    [0.75, 0.25, 0.25],
    [0.25, 0.75, 0.75],
    [0.75, 0.42, 0.25],
    [0.25, 0.58, 0.75],
    [0.75, 0.58, 0.25],
    [0.25, 0.42, 0.75],
    [0.75, 0.75, 0.25],
    [0.25, 0.25, 0.75],
    [0.58, 0.75, 0.25],
    [0.42, 0.25, 0.75],
    [0.58, 0.25, 0.75],
    [0.42, 0.75, 0.25],
    [0.25, 0.75, 0.25],
    [0.75, 0.25, 0.75],
    [0.25, 0.75, 0.42],
    [0.75, 0.25, 0.58],
    [0.25, 0.75, 0.58],
    [0.75, 0.25, 0.42],
    [0.10, 0.75, 0.72],
];

#[derive(Debug)]
pub struct TerrainOpts {
    geo_max_cpu_level: usize,
    geo_target_refinement: f64,
    geo_desired_patch_count: usize,
    geo_gpu_subdivisions: usize,

    tile_max_level: usize,
    tile_target_refinement: f64,
    tile_desired_patch_count: usize,
    tile_cache_size: u32,
}

impl TerrainOpts {
    pub fn from_detail(cpu: CpuDetailLevel, gpu: GpuDetailLevel) -> Self {
        let (
            geo_max_cpu_level,
            geo_target_refinement,
            geo_desired_patch_count,
            tile_max_level,
            tile_target_refinement,
            tile_desired_patch_count,
        ) = match cpu {
            CpuDetailLevel::Low => (14, 0.03, 200, 9, 0.03, 200),
            CpuDetailLevel::Medium => (15, 0.03, 300, 10, 0.03, 200),
            CpuDetailLevel::High => (16, 0.03, 400, 10, 0.03, 200),
        };
        println!("GPU: {gpu:?}");
        let (geo_gpu_subdivisions, tile_cache_size) = match gpu {
            GpuDetailLevel::Low => (4, 256),
            GpuDetailLevel::Medium => (5, 384),
            GpuDetailLevel::High => (6, 768),
        };
        Self {
            geo_max_cpu_level,
            geo_target_refinement,
            geo_desired_patch_count,
            geo_gpu_subdivisions,
            tile_max_level,
            tile_target_refinement,
            tile_desired_patch_count,
            tile_cache_size,
        }
    }
}

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum TerrainStep {
    // Pre-encoder
    HandleDisplayChange,
    OptimizePatches,

    // Encoder
    EncodeUploads,
    Tessellate,
    RenderDeferredTexture,
    AccumulateNormalsAndColor,

    // Post encoder
    CaptureSnapshots,
}

#[derive(Debug, NitrousResource)]
pub struct Terrain {
    patch_manager: PatchManager,
    tile_manager: TileManager,
    height_displacer: HeightDisplacer,
    offscreen_render: OffscreenRender,
    normal_accumulator: NormalAccumulator,
    color_accumulator: ColorAccumulator,

    toggle_pin_camera: bool,
    pinned_camera: Option<OptimizeCamera>,

    #[property]
    show_patch_visibility: bool,

    #[property]
    show_tile_visibility: bool,

    acc_extent: wgpu::Extent3d,

    gbuffer_bind_group_layout: wgpu::BindGroupLayout,
}

impl Extension for Terrain {
    type Opts = TerrainOpts;

    fn init(runtime: &mut Runtime, opts: TerrainOpts) -> Result<()> {
        let terrain = Terrain::new(&opts, runtime.resource::<Gpu>())?;
        runtime.inject_resource(terrain)?;

        runtime.add_frame_system(
            Self::sys_handle_display_config_change
                .in_set(TerrainStep::HandleDisplayChange)
                .after(GpuStep::HandleDisplayChange)
                .before(GpuStep::CreateCommandEncoder)
                .before(GpuStep::CreateTargetSurface),
        );

        runtime.add_frame_system(
            Self::sys_optimize_patches
                .in_set(TerrainStep::OptimizePatches)
                .after(TerrainStep::HandleDisplayChange)
                .after(CameraStep::HandleDisplayChange)
                .after(CameraStep::MoveCameraToFrame)
                .after(GeoDbStep::CheckDownloads)
                .before(MarkersStep::UploadGeometry)
                // Non-conflicting usage of Camera
                .ambiguous_with(GpuStep::CreateTargetSurface),
        );

        runtime.add_frame_system(
            Self::sys_encode_uploads
                .in_set(TerrainStep::EncodeUploads)
                .after(CameraStep::UploadToGpu)
                .after(TerrainStep::OptimizePatches)
                .after(GpuStep::CreateCommandEncoder)
                .before(GpuStep::SubmitCommands)
                // Note: ambiguous, but pin the encode order for consistency
                .after(OrreryStep::UploadToGpu)
                .before(GuiStep::EndFrame)
                .before(MarkersStep::UploadGeometry)
                .before(StarsStep::UpdateHourAngle),
        );
        runtime.add_frame_system(
            Self::sys_terrain_tesselate
                .in_set(TerrainStep::Tessellate)
                .after(TerrainStep::EncodeUploads)
                .after(GpuStep::CreateCommandEncoder)
                .before(GpuStep::SubmitCommands)
                // Note: ambiguous, but pin the encode order for consistency
                .after(OrreryStep::UploadToGpu)
                .before(GuiStep::EndFrame)
                .before(MarkersStep::UploadGeometry)
                .before(StarsStep::UpdateHourAngle),
        );
        runtime.add_frame_system(
            Self::sys_deferred_texture
                .in_set(TerrainStep::RenderDeferredTexture)
                .after(TerrainStep::Tessellate)
                .after(GpuStep::CreateCommandEncoder)
                .before(GpuStep::SubmitCommands)
                // Note: ambiguous, but pin the encode order for consistency
                .after(OrreryStep::UploadToGpu)
                .before(GuiStep::EndFrame)
                .before(MarkersStep::UploadGeometry)
                .before(StarsStep::UpdateHourAngle),
        );
        runtime.add_frame_system(
            Self::sys_accumulate_normal_and_color
                .in_set(TerrainStep::AccumulateNormalsAndColor)
                .after(TerrainStep::RenderDeferredTexture)
                .after(GpuStep::CreateCommandEncoder)
                .before(GpuStep::SubmitCommands)
                // Note: ambiguous, but pin the encode order for consistency
                .after(OrreryStep::UploadToGpu)
                .before(GuiStep::EndFrame)
                .before(MarkersStep::UploadGeometry)
                .before(StarsStep::UpdateHourAngle),
        );

        runtime.add_frame_system(
            Self::sys_handle_capture_snapshot
                .pipe(report_errors)
                .in_set(TerrainStep::CaptureSnapshots)
                .after(GpuStep::PresentTargetSurface)
                // Note: non-conflicting use of Gpu (separate queue)
                .ambiguous_with(MarkersStep::UploadGeometry),
        );

        Ok(())
    }
}

#[inject_nitrous_resource]
impl Terrain {
    pub const GBUFFER_BINDING_NAME: &'static str = "terrain-composite-bind-group";
    pub const OFFSCREEN_BINDING_NAME: &'static str = OffscreenRender::BINDING_NAME;
    pub const COLOR_LAYER_NAME: &'static str = ColorAccumulator::LAYER_NAME;
    pub const DEPTH_LAYER_NAME: &'static str = OffscreenRender::DEPTH_LAYER_NAME;
    pub const COLOR_ACCUMULATION_FORMAT: wgpu::TextureFormat =
        ColorAccumulator::COLOR_ACCUMULATION_FORMAT;

    pub fn new(opts: &TerrainOpts, gpu: &Gpu) -> Result<Self> {
        let tile_manager = TileManager::new(
            opts.tile_max_level,
            opts.tile_target_refinement,
            opts.tile_desired_patch_count,
            opts.tile_cache_size,
            gpu,
        )?;
        let patch_manager = PatchManager::new(
            opts.geo_max_cpu_level,
            opts.geo_target_refinement,
            opts.geo_desired_patch_count,
            opts.geo_gpu_subdivisions,
            gpu,
        )?;
        let height_displacer = HeightDisplacer::new(
            tile_manager.heights_bind_group_layout(),
            patch_manager.target_vertex_buffer(),
            patch_manager.target_vertex_buffer_size(),
            patch_manager.target_vertex_count(),
            gpu,
        );
        let offscreen_render = OffscreenRender::new(gpu);
        let normal_accumulator = NormalAccumulator::new(
            tile_manager.heights_bind_group_layout(),
            offscreen_render.bind_group_layout(),
            gpu,
        );
        let color_accumulator = ColorAccumulator::new(
            tile_manager.colors_bind_group_layout(),
            offscreen_render.bind_group_layout(),
            gpu,
        );

        // A bind group layout with readonly, filtered access to accumulator buffers for
        // compositing all of it together to the screen.
        let gbuffer_bind_group_layout = ScreenCamera::make_bind_group_layout(
            &[
                OffscreenRender::DEFERRED_TEXTURE_FORMAT,
                Gpu::DEPTH_FORMAT,
                ColorAccumulator::COLOR_ACCUMULATION_FORMAT,
                NormalAccumulator::NORMAL_ACCUMULATION_FORMAT,
            ],
            false,
            gpu,
        );

        Ok(Self {
            patch_manager,
            tile_manager,
            height_displacer,
            offscreen_render,
            normal_accumulator,
            color_accumulator,
            toggle_pin_camera: false,
            pinned_camera: None,
            show_patch_visibility: false,
            show_tile_visibility: false,
            acc_extent: gpu.attachment_extent(),
            gbuffer_bind_group_layout,
        })
    }

    #[method]
    pub fn reload_tiles(&mut self) {
        self.tile_manager.reload_tiles();
    }

    #[method]
    fn toggle_pin_camera(&mut self) {
        self.toggle_pin_camera = true;
    }

    #[method]
    fn toggle_show_culling(&mut self) {
        self.patch_manager.toggle_show_culling();
    }

    pub fn sys_handle_display_config_change(
        window: Res<Window>,
        gpu: Res<Gpu>,
        mut terrain: ResMut<Terrain>,
    ) {
        if window.updated_config().is_some() {
            terrain
                .handle_render_extent_changed(&gpu)
                .expect("Terrain::handle_render_extent_changed")
        }
    }

    fn handle_render_extent_changed(&mut self, gpu: &Gpu) -> Result<()> {
        self.acc_extent = gpu.attachment_extent();
        Ok(())
    }

    #[allow(clippy::too_many_arguments)]
    fn _make_composite_bind_group(
        device: &wgpu::Device,
        bind_group_layout: &wgpu::BindGroupLayout,
        deferred_texture: &wgpu::TextureView,
        deferred_depth: &wgpu::TextureView,
        color_acc: &wgpu::TextureView,
        normal_acc: &wgpu::TextureView,
    ) -> wgpu::BindGroup {
        device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("terrain-composite-bind-group"),
            layout: bind_group_layout,
            entries: &[
                binding::texture(0, deferred_texture),
                binding::texture(1, deferred_depth),
                binding::texture(2, color_acc),
                binding::texture(3, normal_acc),
            ],
        })
    }

    fn _make_accumulate_common_bind_group(
        device: &wgpu::Device,
        bind_group_layout: &wgpu::BindGroupLayout,
        deferred_texture: &wgpu::TextureView,
        deferred_depth: &wgpu::TextureView,
        color_acc: &wgpu::TextureView,
        normal_acc: &wgpu::TextureView,
        sampler: &wgpu::Sampler,
    ) -> wgpu::BindGroup {
        device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("terrain-accumulate-bind-group"),
            layout: bind_group_layout,
            entries: &[
                binding::texture(0, deferred_texture),
                binding::texture(1, deferred_depth),
                binding::texture(2, color_acc),
                binding::texture(3, normal_acc),
                binding::sampler(4, sampler),
            ],
        })
    }

    fn sys_optimize_patches(
        camera: Res<ScreenCamera>,
        rt: Res<RuntimeResource>,
        gpu: Res<Gpu>,
        mut geodb: ResMut<GeoDb>,
        mut terrain: ResMut<Terrain>,
        mut markers: ResMut<Markers>,
    ) {
        terrain.optimize_patches(&camera, &rt, &gpu, &mut geodb, &mut markers);
    }

    // Given the new camera position, update our internal CPU tracking.
    fn optimize_patches(
        &mut self,
        camera: &ScreenCamera,
        rt: &RuntimeResource,
        gpu: &Gpu,
        geodb: &mut GeoDb,
        markers: &mut Markers,
    ) {
        let camera = OptimizeCamera::from(camera);
        if self.toggle_pin_camera {
            self.toggle_pin_camera = false;
            self.pinned_camera = match self.pinned_camera {
                None => Some(camera.to_owned()),
                Some(_) => None,
            };
        }
        let optimize_camera = if let Some(camera) = &self.pinned_camera {
            camera
        } else {
            &camera
        };

        // Upload patches and capture visibility regions.
        self.patch_manager.track_state_changes(
            &camera,
            optimize_camera,
            &mut if self.show_patch_visibility {
                Some(markers)
            } else {
                None
            },
        );
        self.tile_manager.track_state_changes(
            optimize_camera,
            rt,
            gpu,
            geodb,
            &mut if self.show_tile_visibility {
                Some(markers)
            } else {
                None
            },
        );
    }

    fn sys_encode_uploads(
        mut terrain: ResMut<Terrain>,
        gpu: Res<Gpu>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) {
        if let Some(encoder) = maybe_encoder.get_encoder_mut() {
            terrain.patch_manager.encode_uploads(&gpu, encoder);
            terrain.tile_manager.encode_uploads(&gpu, encoder);
        }
    }

    fn sys_terrain_tesselate(terrain: ResMut<Terrain>, mut maybe_encoder: ResMut<CurrentEncoder>) {
        if let Some(encoder) = maybe_encoder.get_encoder_mut() {
            terrain.patch_manager.tessellate(encoder);
            terrain
                .height_displacer
                .displace_heights(terrain.tile_manager.heights_bind_group(), encoder);
        }
    }

    /// Draw the tessellated and height-displaced patch geometry to an offscreen buffer colored
    /// with the texture coordinates. This is the only geometry pass. All other terrain passes
    /// work in the screen space that we create here.
    fn sys_deferred_texture(
        terrain: Res<Terrain>,
        gpu: Res<Gpu>,
        mut camera: ResMut<ScreenCamera>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) {
        if let Some(encoder) = maybe_encoder.get_encoder_mut() {
            terrain.offscreen_render.deferred_texture(
                &terrain.patch_manager,
                &gpu,
                &mut camera,
                encoder,
            );
        }
    }

    /// Use the offscreen texcoord buffer to build offscreen color and normals buffers.
    /// These offscreen buffers will get fed into the `world` renderer with the atmosphere,
    /// clouds, shadowmap, etc, to composite a final "world" image.
    fn sys_accumulate_normal_and_color(
        terrain: Res<Terrain>,
        gpu: Res<Gpu>,
        mut camera: ResMut<ScreenCamera>,
        mut maybe_encoder: ResMut<CurrentEncoder>,
    ) {
        if let Some(encoder) = maybe_encoder.get_encoder_mut() {
            terrain.accumulate_normals(&gpu, &mut camera, encoder);
            terrain.accumulate_colors(&gpu, &mut camera, encoder);
            camera.ensure_bind_group(
                Self::GBUFFER_BINDING_NAME,
                &[
                    OffscreenRender::COLOR_LAYER_NAME,
                    OffscreenRender::DEPTH_LAYER_NAME,
                    ColorAccumulator::LAYER_NAME,
                    NormalAccumulator::LAYER_NAME,
                ],
                false,
                &gpu,
            );
        }
    }

    fn accumulate_normals(
        &self,
        gpu: &Gpu,
        camera: &mut ScreenCamera,
        encoder: &mut wgpu::CommandEncoder,
    ) {
        self.normal_accumulator.accumulate_normals(
            self.tile_manager.heights_bind_group(),
            gpu,
            camera,
            encoder,
        );
    }

    fn accumulate_colors(
        &self,
        gpu: &Gpu,
        camera: &mut ScreenCamera,
        encoder: &mut wgpu::CommandEncoder,
    ) {
        self.color_accumulator.accumulate_colors(
            self.tile_manager.colors_bind_group(),
            gpu,
            camera,
            encoder,
        );
    }

    #[method]
    pub fn request_dump_indices(&mut self) -> Result<()> {
        self.tile_manager.request_dump_indices()
    }

    #[method]
    pub fn request_dump_atlases(&mut self) -> Result<()> {
        self.tile_manager.request_dump_atlases()
    }

    fn sys_handle_capture_snapshot(
        mut terrain: ResMut<Terrain>,
        mut gpu: ResMut<Gpu>,
    ) -> Result<()> {
        terrain.tile_manager.maybe_dump(&mut gpu)?;
        Ok(())
    }

    pub fn gbuffer_bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        &self.gbuffer_bind_group_layout
    }

    pub fn offscreen_bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        self.offscreen_render.bind_group_layout()
    }

    pub fn mesh_bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        self.height_displacer.bind_group_layout()
    }
    pub fn mesh_bind_group(&self) -> &wgpu::BindGroup {
        self.height_displacer.bind_group()
    }
    pub fn mesh_vertex_count(&self) -> u32 {
        self.patch_manager.target_vertex_count()
    }

    pub fn num_patches(&self) -> i32 {
        self.patch_manager.num_patches()
    }

    pub fn vertex_buffer(&self) -> wgpu::BufferSlice {
        self.patch_manager.vertex_buffer()
    }

    pub fn patch_vertex_buffer_offset(&self, patch_number: i32) -> i32 {
        self.patch_manager.patch_vertex_buffer_offset(patch_number)
    }

    pub fn patch_winding(&self, patch_number: i32) -> PatchWinding {
        self.patch_manager.patch_winding(patch_number)
    }

    pub fn wireframe_index_buffer(&self, winding: PatchWinding) -> wgpu::BufferSlice {
        self.patch_manager.wireframe_index_buffer(winding)
    }

    pub fn wireframe_index_range(&self, winding: PatchWinding) -> Range<u32> {
        self.patch_manager.wireframe_index_range(winding)
    }
}

#[cfg(test)]
mod test {
    use crate::patch::icosahedron::Icosahedron;
    use absolute_unit::prelude::*;
    use anyhow::Result;
    use approx::assert_relative_eq;
    use arcball::ArcBallController;
    use camera::ScreenCamera;
    use geodesy::{Bearing, Geodetic, PitchCline};
    use glam::{Vec4, Vec4Swizzles};
    use hifitime::Epoch;
    use mantle::{Core, Gpu, PhysicalSize};
    use planck::EARTH_RADIUS;

    #[test]
    fn test_built_index_lut() {
        for i in 0..300 {
            let patch_id = i / 3;
            let offset = i % 3;
            assert_eq!(i, patch_id * 3 + offset);
        }
    }

    #[test]
    fn test_roundtrip_coords() -> Result<()> {
        let runtime = Core::for_test()?;
        let mut camera = ScreenCamera::new(
            "test",
            degrees!(90),
            PhysicalSize::new(1920, 1080),
            meters!(0.5),
            runtime.resource::<Gpu>(),
        );
        let mut arcball = ArcBallController::default();
        arcball.set_target_imm_unsafe(Geodetic::new(
            degrees!(27.988),
            degrees!(86.92),
            meters!(8000),
        ));
        arcball.set_bearing(Bearing::new(radians!(-4.066617157146773)));
        arcball.set_pitch(PitchCline::new(radians!(1.3962634015954636)));
        arcball.set_distance(meters!(6978491.29099145))?;

        let frame = arcball.world_space_frame();
        camera.update_frame(&frame, &Epoch::now()?);

        let view_cpu = camera.world_to_eye::<Meters>();
        let inv_view_cpu = view_cpu.inverse();
        let proj_cpu = camera.perspective::<Meters>();
        let inv_proj_cpu = proj_cpu.inverse();
        let inv_view_gpu = inv_view_cpu.as_mat4();
        let proj_gpu = proj_cpu.as_mat4();
        let inv_proj_gpu = inv_proj_cpu.as_mat4();

        let sphere = Icosahedron::new();
        for vert in &sphere.verts {
            let wrld0 = *vert * *EARTH_RADIUS;
            let eye0 = view_cpu * wrld0;
            let eye0_gpu = eye0.dvec3().as_vec3();
            let clip0_gpu = proj_gpu * Vec4::from((eye0_gpu, 1.));
            let ndc0_gpu = clip0_gpu / clip0_gpu.w;

            // now invert it on the gpu
            let w = 0.5 / ndc0_gpu.z;
            let clip1_gpu = Vec4::new(ndc0_gpu.x * w, ndc0_gpu.y * w, 0.5, w);
            let eye1_gpu = (inv_proj_gpu * clip1_gpu).xyz();
            let wrld1_gpu = (inv_view_gpu * Vec4::from((eye1_gpu, 1.))).xyz();

            assert_relative_eq!(clip0_gpu, clip1_gpu, epsilon = 1.);
            assert_relative_eq!(eye0_gpu, eye1_gpu, epsilon = 1.);
            assert_relative_eq!(wrld0.dvec3().as_vec3(), wrld1_gpu, epsilon = 10.);
        }

        Ok(())
    }
}
