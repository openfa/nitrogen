// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::tile::{
    tile_atlas::SlotPos, tile_index_metadata::IndexMetadata,
    tile_index_paint_vertex::TileIndexPaintVertex,
};
use anyhow::Result;
use geodb::{GeoDb, MapKind, MapName, MAP_SIZE};
use log::trace;
use mantle::{texture_format_size, Gpu};
use shader_shared::{binding, layout};
use std::{env, mem, ops::Range, path::PathBuf};

// This class is purely about tracking the GPU resources and painting the allocations
// that have been collected and ordered for us by TileAtlas.
pub(crate) struct AtlasResources {
    _kind: MapKind,
    slots_size: u32,

    // Index
    index_extent: wgpu::Extent3d,
    _index_texture: wgpu::Texture,
    index_texture_view: wgpu::TextureView,
    paint_tris: Vec<TileIndexPaintVertex>,
    index_paint_range: Range<u32>,
    index_paint_vert_buffer: wgpu::Buffer,
    index_paint_pipeline: wgpu::RenderPipeline,
    maybe_snapshot_index: Option<PathBuf>,

    // Atlas
    uploads: Vec<(SlotPos, wgpu::Buffer)>,
    _atlas_extent: wgpu::Extent3d,
    atlas_format: wgpu::TextureFormat,
    atlas_texture: wgpu::Texture,
    maybe_snapshot_atlas: Option<PathBuf>,

    // Bind group
    bind_group: wgpu::BindGroup,
}

impl AtlasResources {
    pub(crate) fn new(
        kind: MapKind,
        slots_size: u32,
        bind_group_layout: &wgpu::BindGroupLayout,
        geodb: &GeoDb,
        gpu: &Gpu,
    ) -> Self {
        let atlas_format = Self::atlas_texture_format(kind);
        // Note: dense packing; we use manual sampling in the shader so no border needed.
        let atlas_extent = wgpu::Extent3d {
            width: slots_size * MAP_SIZE,
            height: slots_size * MAP_SIZE,
            depth_or_array_layers: 1, // array support is questionable on webgpu
        };
        let atlas_texture = gpu.device().create_texture(&wgpu::TextureDescriptor {
            label: Some("terrain-tile-atlas-texture"),
            size: atlas_extent,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: atlas_format,
            usage: wgpu::TextureUsages::COPY_SRC
                | wgpu::TextureUsages::COPY_DST
                | wgpu::TextureUsages::TEXTURE_BINDING,
            view_formats: &[atlas_format],
        });
        let atlas_texture_view = atlas_texture.create_view(&wgpu::TextureViewDescriptor {
            label: Some("terrain-tile-atlas-texture-view"),
            ..Default::default()
        });
        // Note: linear sampling will read past the edges of tiles, so we do our own
        let atlas_texture_sampler = gpu.device().create_sampler(&wgpu::SamplerDescriptor {
            label: Some("terrain-tile-atlas-sampler"),
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Nearest,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            lod_min_clamp: 0.0,
            lod_max_clamp: 0.0,
            compare: None,
            anisotropy_clamp: 1,
            border_color: None,
        });

        // Index setup
        // Note: pixels in the COG are *area*, so the logical extent is half of the highest
        // resolution pixel off from start. Our mapping from index to slot only cares about
        // tile position; the metadata configures how we map graticules to index positions,
        // and thus the meaning of the tile metadata.
        let geotiff = geodb.tiff(kind).base_layer();
        let index_extent = wgpu::Extent3d {
            width: geotiff.width().div_ceil(MAP_SIZE),
            height: geotiff.height().div_ceil(MAP_SIZE),
            depth_or_array_layers: 1,
        };

        // Not all of the index is "used", in that not all tiles are fully used around the edge.
        let level_0_px_degrees = geodb.tiff(kind).base_layer().scale();
        let scale_x = geotiff.width() as f32 / MAP_SIZE as f32;
        let scale_y = geotiff.height() as f32 / MAP_SIZE as f32;
        let index_metadata = IndexMetadata::new(
            *geotiff.tie_start(),
            geotiff.tie_extent_lat(),
            geotiff.tie_extent_lon(),
            [scale_x, scale_y],
            [atlas_extent.width as f32, atlas_extent.height as f32],
            level_0_px_degrees.f32(),
        );
        trace!("Uploading index metadata: {index_metadata:?}");
        let index_metadata = gpu.push_data(
            "index-metadata",
            &index_metadata,
            wgpu::BufferUsages::STORAGE,
        );
        let index_paint_range = 0u32..(6 * slots_size * slots_size);
        let index_paint_vert_buffer = gpu.device().create_buffer(&wgpu::BufferDescriptor {
            label: Some("tile-index-paint-vert-buffer"),
            size: (TileIndexPaintVertex::mem_size() * 6 * slots_size as usize * slots_size as usize)
                as wgpu::BufferAddress,
            usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::VERTEX,
            mapped_at_creation: false,
        });
        let index_paint_shader = gpu.compile_shader(
            "tile_index_paint.wgsl",
            include_str!("../../target/tile_index_paint.wgsl"),
        );
        // Tile x, tile y, and level
        let index_format = wgpu::TextureFormat::Rgba8Uint;
        let index_texture = gpu.device().create_texture(&wgpu::TextureDescriptor {
            label: Some("terrain-tile-index-texture"),
            size: index_extent,
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: index_format,
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT
                | wgpu::TextureUsages::TEXTURE_BINDING
                | wgpu::TextureUsages::COPY_SRC,
            view_formats: &[index_format],
        });
        let index_texture_view = index_texture.create_view(&wgpu::TextureViewDescriptor {
            label: Some("terrain-tile-index-texture-view"),
            format: None,
            dimension: None,
            aspect: wgpu::TextureAspect::All,
            base_mip_level: 0,
            mip_level_count: None,
            base_array_layer: 0,
            array_layer_count: None,
        });
        let index_texture_sampler = gpu.device().create_sampler(&wgpu::SamplerDescriptor {
            label: Some("terrain-tile-index-sampler"),
            address_mode_u: wgpu::AddressMode::ClampToEdge,
            address_mode_v: wgpu::AddressMode::ClampToEdge,
            address_mode_w: wgpu::AddressMode::ClampToEdge,
            mag_filter: wgpu::FilterMode::Nearest,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            lod_min_clamp: 0.0,
            lod_max_clamp: 0.0,
            compare: None,
            anisotropy_clamp: 1,
            border_color: None,
        });
        let index_paint_pipeline =
            gpu.device()
                .create_render_pipeline(&wgpu::RenderPipelineDescriptor {
                    label: Some("terrain-tile-index-paint-pipeline"),
                    layout: Some(&gpu.device().create_pipeline_layout(
                        &wgpu::PipelineLayoutDescriptor {
                            label: Some("terrain-tile-index-paint-pipeline-layout"),
                            push_constant_ranges: &[],
                            bind_group_layouts: &[],
                        },
                    )),
                    vertex: wgpu::VertexState {
                        module: &index_paint_shader,
                        entry_point: "main_vert",
                        buffers: &[TileIndexPaintVertex::descriptor()],
                        compilation_options: wgpu::PipelineCompilationOptions {
                            constants: &Default::default(),
                            zero_initialize_workgroup_memory: false,
                        },
                    },
                    fragment: Some(wgpu::FragmentState {
                        module: &index_paint_shader,
                        entry_point: "main_frag",
                        targets: &[Some(wgpu::ColorTargetState {
                            format: index_format,
                            blend: None,
                            write_mask: wgpu::ColorWrites::ALL,
                        })],
                        compilation_options: wgpu::PipelineCompilationOptions {
                            constants: &Default::default(),
                            zero_initialize_workgroup_memory: false,
                        },
                    }),
                    primitive: wgpu::PrimitiveState {
                        topology: wgpu::PrimitiveTopology::TriangleList,
                        strip_index_format: None,
                        front_face: wgpu::FrontFace::Cw,
                        cull_mode: Some(wgpu::Face::Back),
                        unclipped_depth: false,
                        polygon_mode: wgpu::PolygonMode::Fill,
                        conservative: false,
                    },
                    depth_stencil: None,
                    multisample: wgpu::MultisampleState {
                        count: 1,
                        mask: !0,
                        alpha_to_coverage_enabled: false,
                    },
                    multiview: None,
                });

        // Bind group
        let bind_group = gpu.device().create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("terrain-tile-bind-group"),
            layout: bind_group_layout,
            entries: &[
                // Index
                binding::texture(0, &index_texture_view),
                binding::sampler(1, &index_texture_sampler),
                // Tile Atlas
                binding::texture(2, &atlas_texture_view),
                binding::sampler(3, &atlas_texture_sampler),
                // Index Metadata
                binding::buffer(4, &index_metadata),
            ],
        });

        Self {
            _kind: kind,
            slots_size,
            index_extent,
            _index_texture: index_texture,
            index_texture_view,
            paint_tris: Vec::new(),
            index_paint_range,
            index_paint_vert_buffer,
            index_paint_pipeline,
            maybe_snapshot_index: None,
            uploads: Vec::new(),
            _atlas_extent: atlas_extent,
            atlas_format,
            atlas_texture,
            maybe_snapshot_atlas: None,
            bind_group,
        }
    }

    pub(crate) fn atlas_sample_type(kind: MapKind) -> wgpu::TextureSampleType {
        match kind {
            MapKind::Heights => wgpu::TextureSampleType::Sint,
            MapKind::Colors => wgpu::TextureSampleType::Float { filterable: false },
        }
    }

    pub(crate) fn atlas_texture_format(kind: MapKind) -> wgpu::TextureFormat {
        match kind {
            MapKind::Heights => wgpu::TextureFormat::R16Sint,
            MapKind::Colors => wgpu::TextureFormat::Rgba8Unorm,
        }
    }

    pub(crate) fn make_bind_group_layout(
        kind: MapKind,
        device: &wgpu::Device,
    ) -> wgpu::BindGroupLayout {
        let visibility = wgpu::ShaderStages::COMPUTE | wgpu::ShaderStages::FRAGMENT;
        device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: Some("terrain-tile-bind-group-layout"),
            // TODO: remove COMPUTE stage
            entries: &[
                // Index Texture
                layout::texture_uint(0, visibility),
                layout::sampler_nearest(1, visibility),
                // Atlas Textures, as referenced by the above index
                layout::texture_any(2, visibility, Self::atlas_sample_type(kind)),
                layout::sampler_nearest(3, visibility),
                // Index metadata
                layout::buffer::<IndexMetadata>(4, visibility),
            ],
        })
    }

    pub(crate) fn upload_tile(&mut self, pos: SlotPos, buffer: wgpu::Buffer) {
        self.uploads.push((pos, buffer));
    }

    pub(crate) fn encode_uploads(
        &mut self,
        maps: &[(MapName, SlotPos)],
        gpu: &Gpu,
        encoder: &mut wgpu::CommandEncoder,
    ) {
        // Queue uploads into the GPU texture
        for (pos, buffer) in self.uploads.drain(..) {
            encoder.copy_buffer_to_texture(
                wgpu::ImageCopyBuffer {
                    buffer: &buffer,
                    layout: wgpu::ImageDataLayout {
                        offset: 0,
                        bytes_per_row: Some(MAP_SIZE * texture_format_size(self.atlas_format)),
                        rows_per_image: Some(MAP_SIZE),
                    },
                },
                wgpu::ImageCopyTexture {
                    texture: &self.atlas_texture,
                    mip_level: 0,
                    origin: wgpu::Origin3d {
                        x: pos.x32(self.slots_size) * MAP_SIZE,
                        y: pos.y32(self.slots_size) * MAP_SIZE,
                        z: 0,
                    },
                    aspect: wgpu::TextureAspect::All,
                },
                wgpu::Extent3d {
                    width: MAP_SIZE,
                    height: MAP_SIZE,
                    depth_or_array_layers: 1,
                },
            );
        }

        // Paint where our maps will land on the index for lookup
        self.paint_tris.clear();
        for (map, pos) in maps {
            // Offset is from top-left of the tile.
            let size_px = map.level().extent_factor();
            let offset_x_px = map.position().x() as f32 * size_px;
            let offset_y_px = map.position().y() as f32 * size_px;
            // println!(
            //     "EXTENT: {} @ {}x{}",
            //     size_px,
            //     map.position().x(),
            //     map.position().y()
            // );
            // Map to pixel offsets on bottom left of the tile to top right.
            let x0_px = offset_x_px;
            let x1_px = x0_px + size_px;
            let y0_px = self.index_extent.height as f32 - offset_y_px - size_px;
            let y1_px = y0_px + size_px;
            // Map pixel extents to [-1,1] for polygons
            let x0 = (x0_px / self.index_extent.width as f32) * 2. - 1.;
            let x1 = (x1_px / self.index_extent.width as f32) * 2. - 1.;
            let y0 = (y0_px / self.index_extent.height as f32) * 2. - 1.;
            let y1 = (y1_px / self.index_extent.height as f32) * 2. - 1.;
            let c = [
                pos.x8(self.slots_size),
                pos.y8(self.slots_size),
                map.level().level(), // overview / tiff level, max res at 0
                255,
            ];

            // TODO: this could easily be indexed, saving us a bit of bandwidth.
            self.paint_tris.push(TileIndexPaintVertex::new([x0, y0], c));
            self.paint_tris.push(TileIndexPaintVertex::new([x0, y1], c));
            self.paint_tris.push(TileIndexPaintVertex::new([x1, y0], c));
            self.paint_tris.push(TileIndexPaintVertex::new([x1, y0], c));
            self.paint_tris.push(TileIndexPaintVertex::new([x0, y1], c));
            self.paint_tris.push(TileIndexPaintVertex::new([x1, y1], c));
        }
        while self.paint_tris.len() < self.index_paint_range.end as usize {
            self.paint_tris
                .push(TileIndexPaintVertex::new([0f32, 0f32], [0; 4]));
        }
        gpu.upload_slice_to(&self.paint_tris, &self.index_paint_vert_buffer, encoder);

        let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: Some("terrain-tile-paint-atlas-index-render-pass"),
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &self.index_texture_view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color::TRANSPARENT),
                    store: wgpu::StoreOp::Store,
                },
            })],
            depth_stencil_attachment: None,
            timestamp_writes: None,
            occlusion_query_set: None,
        });
        rpass.set_pipeline(&self.index_paint_pipeline);
        rpass.set_vertex_buffer(0, self.index_paint_vert_buffer.slice(..));
        rpass.draw(self.index_paint_range.clone(), 0..1);
    }

    pub(crate) fn request_dump_index(&mut self, path: &str) -> Result<()> {
        let mut buf = env::current_dir()?;
        buf.push("__dump__");
        buf.push(path);
        buf.set_extension("png");
        self.maybe_snapshot_index = Some(buf);
        Ok(())
    }

    pub(crate) fn maybe_dump_index(&mut self, gpu: &mut Gpu) -> Result<()> {
        if self.maybe_snapshot_index.is_some() {
            let mut path = None;
            mem::swap(&mut path, &mut self.maybe_snapshot_index);
            self.capture_and_save_index_snapshot(path.unwrap(), gpu)?;
        }
        Ok(())
    }

    pub(crate) fn capture_and_save_index_snapshot(
        &mut self,
        _path: PathBuf,
        _gpu: &mut Gpu,
    ) -> Result<()> {
        #[cfg(target_arch = "wasm32")]
        return Ok(());
        #[cfg(not(target_arch = "wasm32"))]
        Gpu::dump_texture(
            &self._index_texture,
            self.index_extent,
            wgpu::TextureFormat::Rgba8Uint,
            _gpu,
            Box::new(
                move |extent: wgpu::Extent3d, _: wgpu::TextureFormat, mut data: Vec<u8>| {
                    use image::{ImageBuffer, Rgba};
                    let stride = Gpu::stride_for_row_size(extent.width * 4) / 4;
                    for x in 0..extent.width {
                        for y in 0..extent.height {
                            let p = (y * stride + x) as usize * 4;
                            data[p] = data[p].saturating_mul(10);
                            data[p + 1] = data[p + 1].saturating_mul(10);
                            data[p + 2] = data[p + 2].saturating_mul(10);
                            data[p + 3] = 255;
                        }
                    }
                    let img = ImageBuffer::<Rgba<u8>, _>::from_raw(stride, extent.height, data)
                        .expect("built image");
                    println!("writing to {:?}", _path);
                    img.save(_path).expect("wrote file");
                },
            ),
        )
    }

    pub(crate) fn request_dump_atlas(&mut self, path: &str) -> Result<()> {
        let mut buf = env::current_dir()?;
        buf.push("__dump__");
        buf.push(path);
        buf.set_extension("png");
        self.maybe_snapshot_atlas = Some(buf);
        Ok(())
    }

    pub(crate) fn maybe_dump_atlas(&mut self, gpu: &mut Gpu) -> Result<()> {
        if self.maybe_snapshot_atlas.is_some() {
            let mut path = None;
            mem::swap(&mut path, &mut self.maybe_snapshot_atlas);
            self.capture_and_save_atlas_snapshot(path.unwrap(), gpu)?;
        }
        Ok(())
    }

    pub(crate) fn capture_and_save_atlas_snapshot(
        &mut self,
        _path: PathBuf,
        _gpu: &mut Gpu,
    ) -> Result<()> {
        #[cfg(target_arch = "wasm32")]
        return Ok(());
        #[cfg(not(target_arch = "wasm32"))]
        if self._kind == MapKind::Heights {
            Gpu::dump_texture(
                &self.atlas_texture,
                self._atlas_extent,
                wgpu::TextureFormat::R16Sint,
                _gpu,
                Box::new(
                    move |extent: wgpu::Extent3d, _: wgpu::TextureFormat, data: Vec<u8>| {
                        use image::{ImageBuffer, Luma};
                        let mut d = vec![0u8; (extent.width * extent.height) as usize];
                        let stride = Gpu::stride_for_row_size(extent.width * 2) / 2;
                        for x in 0..extent.width {
                            for y in 0..extent.height {
                                let ps = (y * stride + x) as usize * 2;
                                let pd = (y * extent.width + x) as usize;
                                let h = (data[ps + 1] as u16) << 8 | (data[ps] as u16);
                                d[pd] = ((h as u32 + 2u32) / 12u32).clamp(0, 255) as u8;
                            }
                        }
                        let img = ImageBuffer::<Luma<u8>, _>::from_raw(stride, extent.height, d)
                            .expect("built image");
                        println!("writing {:?}", _path);
                        img.save(_path).expect("wrote file");
                    },
                ),
            )
        } else {
            Gpu::dump_texture(
                &self.atlas_texture,
                self._atlas_extent,
                wgpu::TextureFormat::Rgba8Unorm,
                _gpu,
                Box::new(
                    move |extent: wgpu::Extent3d, _: wgpu::TextureFormat, data: Vec<u8>| {
                        use image::{ImageBuffer, Rgb};
                        let mut d = vec![0u8; (extent.width * extent.height) as usize * 3];
                        let stride = Gpu::stride_for_row_size(extent.width * 4);
                        for x in 0..extent.width {
                            for y in 0..extent.height {
                                let ps = (y * stride + x * 4) as usize;
                                let pd = (y * extent.width + x) as usize * 3;
                                d[pd] = data[ps];
                                d[pd + 1] = data[ps + 1];
                                d[pd + 2] = data[ps + 2];
                            }
                        }
                        let img =
                            ImageBuffer::<Rgb<u8>, _>::from_raw(extent.width, extent.height, d)
                                .expect("built image");
                        println!("writing {:?}", _path);
                        img.save(_path).expect("wrote file");
                    },
                ),
            )
        }
    }

    pub(crate) fn bind_group(&self) -> &wgpu::BindGroup {
        &self.bind_group
    }
}
