// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::tile::{tile_atlas_resource::AtlasResources, tile_index_metadata::IndexMetadata};
use anyhow::{anyhow, Result};
use geodb::{GeoDb, MapKind, MapName};
use log::trace;
use mantle::Gpu;
use shader_shared::binding;
use std::{collections::HashMap, fmt, sync::Arc};

// The tile atlas is a giant GPU pixmap containing the currently loaded set of
// maps (of a given kind), as well as a (slightly) smaller index pixmap that we paint
// with the area covered by each atlas pixmap, with the location of that map.
//
// A request for a certain lat/lon starts by requesting in the index; this
// provides an atlas slot; we then read from the given atlas slot to find
// the required height or color value.

#[derive(Debug)]
enum SlotState {
    Empty,
    Filled { map: MapName, last_use: u64 },
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub(crate) struct SlotPos(u32);

impl fmt::Display for SlotPos {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "S[{}]", self.0)
    }
}

impl SlotPos {
    fn new(x: u32, y: u32, stride: u32) -> Self {
        Self(y * stride + x)
    }

    pub(crate) fn x32(&self, stride: u32) -> u32 {
        self.0 % stride
    }

    pub(crate) fn y32(&self, stride: u32) -> u32 {
        self.0 / stride
    }

    pub(crate) fn x8(&self, stride: u32) -> u8 {
        let x = self.0 % stride;
        debug_assert!(x < 256);
        x as u8
    }

    pub(crate) fn y8(&self, stride: u32) -> u8 {
        let y = self.0 / stride;
        debug_assert!(y < 256);
        y as u8
    }
}

// TileAtlas covers the logical management of allocations into the atlas and index.
// For management of the GPU resources, see the TileAtlasResource struct.
pub(crate) struct TileAtlas {
    // Always use square textures, so all of width, height, and stride.
    slots_size: u32, // number of slots wide or high in the atlas
    kind: MapKind,

    // Allocator
    total_slot_count: usize, // number of slots in the atlas
    slot_map: HashMap<SlotPos, SlotState>,
    slot_allocations: HashMap<MapName, SlotPos>,
    free_slots: Vec<SlotPos>,

    // Create once the geodb has the details we need; by definition before
    // it can send us tiles to upload to the GPU.
    bind_group_layout: wgpu::BindGroupLayout,
    tmp_bind_group: wgpu::BindGroup,
    resources: Option<AtlasResources>,
}

impl TileAtlas {
    pub(crate) fn new(kind: MapKind, slot_count: u32, gpu: &Gpu) -> Self {
        let slots_size = (slot_count as f32).sqrt().ceil() as u32;

        // Start all slots as free
        let mut slot_map = HashMap::new();
        for y in 0..slots_size {
            for x in 0..slots_size {
                let pos = SlotPos::new(x, y, slots_size);
                slot_map.insert(pos, SlotState::Empty);
            }
        }

        // This is painful, but lets us put off booting the atlas up until after
        // we've downloaded the cog metadata so that we don't have to guess at parameters.
        let tmp_index_texture_view = gpu
            .device()
            .create_texture(&wgpu::TextureDescriptor {
                label: Some("terrain-tile-tmp-index-texture"),
                size: wgpu::Extent3d::default(),
                mip_level_count: 1,
                sample_count: 1,
                dimension: wgpu::TextureDimension::D2,
                format: wgpu::TextureFormat::Rgba8Uint,
                usage: wgpu::TextureUsages::all(),
                view_formats: &[wgpu::TextureFormat::Rgba8Uint],
            })
            .create_view(&wgpu::TextureViewDescriptor {
                label: Some("terrain-tile-tmp-index-texture-view"),
                ..Default::default()
            });
        let tmp_atlas_texture_view = gpu
            .device()
            .create_texture(&wgpu::TextureDescriptor {
                label: Some("terrain-tile-tmp-atlas-texture"),
                size: wgpu::Extent3d::default(),
                mip_level_count: 1,
                sample_count: 1,
                dimension: wgpu::TextureDimension::D2,
                format: AtlasResources::atlas_texture_format(kind),
                usage: wgpu::TextureUsages::TEXTURE_BINDING,
                view_formats: &[AtlasResources::atlas_texture_format(kind)],
            })
            .create_view(&wgpu::TextureViewDescriptor {
                label: Some("terrain-tile-tmp-atlas-texture-view"),
                ..Default::default()
            });
        let sampler = gpu.device().create_sampler(&wgpu::SamplerDescriptor {
            label: Some("terrain-tile-tmp-index-sampler"),
            ..Default::default()
        });
        let tmp_index_meta_buffer = gpu.push_data(
            "terrain-tile-tmp-index-metadata-buffer",
            &IndexMetadata::default(),
            wgpu::BufferUsages::STORAGE,
        );
        let bind_group_layout = AtlasResources::make_bind_group_layout(kind, gpu.device());
        let tmp_bind_group = gpu.device().create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("terrain-tile-tmp-bind-group"),
            layout: &bind_group_layout,
            entries: &[
                binding::texture(0, &tmp_index_texture_view),
                binding::sampler(1, &sampler),
                binding::texture(2, &tmp_atlas_texture_view),
                binding::sampler(3, &sampler),
                binding::buffer(4, &tmp_index_meta_buffer),
            ],
        });

        Self {
            slots_size,
            kind,
            total_slot_count: slots_size as usize * slots_size as usize,
            slot_map,
            slot_allocations: HashMap::new(),
            free_slots: Vec::new(),
            bind_group_layout,
            tmp_bind_group,
            resources: None,
        }
    }

    // Remove our current allocations so the map effectively becomes empty and forces
    // re-upload of anything provided in the next frame.
    pub(crate) fn clear(&mut self) {
        self.slot_map
            .values_mut()
            .for_each(|state| *state = SlotState::Empty);
        self.slot_allocations.clear();
        self.free_slots.clear();
    }

    pub(crate) fn num_slots(&self) -> usize {
        self.total_slot_count
    }

    pub(crate) fn provide_maps(
        &mut self,
        maps: &[(MapName, Arc<Vec<u8>>)],
        frame_number: u64,
        geodb: &GeoDb,
        gpu: &Gpu,
    ) {
        // No point processing more maps than will actually fit
        let maps = &maps[0..self.total_slot_count.min(maps.len())];

        // Lazily (re)create our gpu-side resources, as needed.
        if self.resources.is_none() && !maps.is_empty() {
            assert!(geodb.is_ready(self.kind));
            self.resources = Some(AtlasResources::new(
                self.kind,
                self.slots_size,
                &self.bind_group_layout,
                geodb,
                gpu,
            ));
        }

        // The update algorithm looks like this:
        //
        // 1) Visit all maps and mark any that are currently present with the new frame count.
        // 2) Visit all slots and free any that are old.
        // 3) Re-visit the maps and place any missing in free slots.
        //
        // We should never need to evict with this scheme.

        // Mark live maps
        for (name, _) in maps {
            if let Some(pos) = self.slot_allocations.get(name) {
                if let Some(SlotState::Filled { last_use, .. }) = self.slot_map.get_mut(pos) {
                    *last_use = frame_number;
                }
            }
        }

        // Sweep unmarked maps and generate a free list.
        self.free_slots.clear();
        for (pos, slot) in self.slot_map.iter_mut() {
            if let SlotState::Filled { last_use, map } = slot {
                if *last_use < frame_number {
                    self.slot_allocations.remove(map);
                    self.free_slots.push(*pos);
                    *slot = SlotState::Empty;
                }
            } else {
                self.free_slots.push(*pos);
            }
        }

        // Allocate
        for (name, data) in maps {
            if !self.slot_allocations.contains_key(name) {
                self.fill_empty_slot(name, data, frame_number, gpu);
            }
        }

        assert!(self.slot_map.len() <= self.total_slot_count);
        assert!(self.slot_allocations.len() <= self.total_slot_count);
        assert_eq!(self.free_slots.len(), self.total_slot_count - maps.len());
    }

    fn fill_empty_slot(&mut self, name: &MapName, data: &[u8], frame_number: u64, gpu: &Gpu) {
        let pos = self.free_slots.pop().expect("enough slots");
        trace!("Allocating {} for {} in {}", pos, name, frame_number);
        self.slot_allocations.insert(*name, pos);
        self.slot_map.insert(
            pos,
            SlotState::Filled {
                map: *name,
                last_use: frame_number,
            },
        );

        let buffer = gpu.push_buffer("tile-atlas-upload", data, wgpu::BufferUsages::COPY_SRC);
        self.resources.as_mut().unwrap().upload_tile(pos, buffer);
    }

    pub(crate) fn encode_uploads(&mut self, gpu: &Gpu, encoder: &mut wgpu::CommandEncoder) {
        if let Some(resources) = &mut self.resources {
            // Sort the slot map by level for use of painters algorithm on the index.
            let mut filled = self
                .slot_map
                .iter()
                .filter_map(|(pos, state)| match state {
                    SlotState::Empty => None,
                    SlotState::Filled { map, .. } => Some((*map, *pos)),
                })
                .collect::<Vec<_>>();
            filled.sort_unstable_by(|(a, _), (b, _)| b.cmp(a));

            resources.encode_uploads(&filled, gpu, encoder);
        }
    }

    pub(crate) fn request_dump_index(&mut self, path: &str) -> Result<()> {
        self.resources
            .as_mut()
            .ok_or_else(|| anyhow!("still initializing terrain engine"))?
            .request_dump_index(path)
    }

    pub(crate) fn maybe_dump_index(&mut self, gpu: &mut Gpu) -> Result<()> {
        if let Some(resources) = &mut self.resources {
            resources.maybe_dump_index(gpu)?;
        }
        Ok(())
    }

    pub(crate) fn request_dump_atlas(&mut self, path: &str) -> Result<()> {
        self.resources
            .as_mut()
            .ok_or_else(|| anyhow!("still initializing terrain engine"))?
            .request_dump_atlas(path)
    }

    pub(crate) fn maybe_dump_atlas(&mut self, gpu: &mut Gpu) -> Result<()> {
        if let Some(resources) = &mut self.resources {
            resources.maybe_dump_atlas(gpu)?;
        }
        Ok(())
    }

    pub(crate) fn bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        &self.bind_group_layout
    }

    pub(crate) fn bind_group(&self) -> &wgpu::BindGroup {
        if let Some(resources) = &self.resources {
            resources.bind_group()
        } else {
            &self.tmp_bind_group
        }
    }
}
