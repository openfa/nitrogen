// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
pub(crate) mod tile_atlas;
pub(crate) mod tile_atlas_resource;
pub(crate) mod tile_index_metadata;
pub(crate) mod tile_index_paint_vertex;
pub(crate) mod tile_info;
pub(crate) mod tile_manager;
pub(crate) mod tile_tree;

pub(crate) use crate::tile::tile_manager::TileManager;
