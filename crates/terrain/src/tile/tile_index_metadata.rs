// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use absolute_unit::prelude::*;
use geodesy::Geode;
use zerocopy::{FromBytes, Immutable, IntoBytes};

#[repr(C)]
#[derive(IntoBytes, FromBytes, Immutable, Copy, Clone, Default, Debug)]
pub(crate) struct IndexMetadata {
    // The base of the indexed region
    tie_start_ll_rad: [f32; 2],

    // Size of the indexed region
    tie_extent_ll_rad: [f32; 2],

    // The index uses 1 px per highest resolution tile, but the planet doesn't evenly
    // map to tiles, so the last tile is partially filled. We also need to access as
    // pixels. This scale combines both mapping operations from logical uv in tie-point
    // space into the index space.
    scale_tie_to_px: [f32; 2],

    // Size in pixels of x and y of the index.
    atlas_size: [f32; 2],

    // Angular extent of a single pixel at each level.
    level_0_px_degrees: f32,
}

impl IndexMetadata {
    pub(crate) fn new(
        tie_start: Geode,
        tie_extent_lat: Angle<Radians>,
        tie_extent_lon: Angle<Radians>,
        scale_tie_to_px: [f32; 2],
        atlas_size: [f32; 2],
        level_0_px_degrees: f32,
    ) -> Self {
        Self {
            tie_start_ll_rad: tie_start.lat_lon::<Radians>().as_vec2().to_array(),
            tie_extent_ll_rad: [tie_extent_lat.f32(), tie_extent_lon.f32()],
            scale_tie_to_px,
            atlas_size,
            level_0_px_degrees,
        }
    }
}
