// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    common::{Handle, OptimizeCamera, Queue, UnorderedHeap},
    tile::tile_info::Tile,
};
use absolute_unit::prelude::*;
use anyhow::Result;
use geodesy::{Geode, GeodeBB};
use geometry::Plane;
use glam::DVec3;
use log::trace;
use marker::Markers;
use ordered_float::OrderedFloat;
use planck::EARTH_RADIUS;
use std::{cmp::Reverse, fmt::Write, time::Instant};

// Tile layout:
//         max
//  +---+---+
//  | 1 | 2 |
//  +---+---+
//  | 0 | 3 |
//  +---+---+
// min

// It's possible that the current viewpoint just cannot be refined to our target, given
// our other constraints. Since we'll have another chance to optimize next frame, bail
// if we spend too much time optimizing.
const MAX_STUCK_ITERATIONS: usize = 3;

// Adds a sanity checks to the inner loop.
const PARANOIA_MODE: bool = false;

// Everything we need about a tile to look up pixmaps. We need a separate structure because we
// also want to capture this info for nodes for mipmapping.
#[derive(Debug)]
pub(crate) struct LiveRegion {
    solid_angle: OrderedFloat<f64>,
    pub(crate) bounds: GeodeBB,
    pub(crate) level: usize,
}

#[derive(Clone, Copy, Debug, Hash, Ord, Eq, PartialEq, PartialOrd)]
pub(crate) struct TileHeapTag;
pub(crate) type TileHeap = UnorderedHeap<Tile, TileHeapTag>;
pub(crate) type TileHandle = Handle<TileHeapTag>;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
struct Root {
    children: [TileTreeHandle; 8],
}

#[derive(Copy, Clone, Debug)]
enum TreeHolder {
    Children([TileTreeHandle; 4]),
    Tile(TileHandle),
}

#[derive(Clone, Debug)]
pub(crate) struct TileTreeNode {
    holder: TreeHolder,
    parent: Option<TileTreeHandle>,
    min: Geode,
    max: Geode,
    cached_solid_angle: f64,
    level: usize,
}

impl TileTreeNode {
    pub(crate) fn is_leaf(&self) -> bool {
        !self.is_node()
    }

    fn is_node(&self) -> bool {
        matches!(self.holder, TreeHolder::Children(_))
    }

    fn level(&self) -> usize {
        self.level
    }

    pub(crate) fn min(&self) -> &Geode {
        &self.min
    }

    pub(crate) fn max(&self) -> &Geode {
        &self.max
    }

    pub(crate) fn solid_angle(&self) -> f64 {
        self.cached_solid_angle
    }

    pub(crate) fn children(&self) -> &[TileTreeHandle; 4] {
        match self.holder {
            TreeHolder::Children(ref children) => children,
            TreeHolder::Tile(_) => panic!("called children on a leaf node"),
        }
    }

    pub(crate) fn tile_handle(&self) -> TileHandle {
        match self.holder {
            TreeHolder::Children(_) => panic!("called tile_handle on a node"),
            TreeHolder::Tile(tile_handle) => tile_handle,
        }
    }
}

#[derive(Clone, Copy, Debug, Hash, Ord, Eq, PartialEq, PartialOrd)]
pub(crate) struct TileTreeTag;
pub(crate) type TileTreeHeap = UnorderedHeap<TileTreeNode, TileTreeTag>;
pub(crate) type TileTreeHandle = Handle<TileTreeTag>;

// A virtual tree of tiles. The tree recurses until a certain camera-relative "fineness"
// is achieved, defined by the max level, target refinement, and a tile count.
pub(crate) struct TileTree {
    max_level: usize,
    target_refinement: f64,
    desired_tile_count: usize,

    tiles: TileHeap,
    tree: TileTreeHeap,
    root: Root,

    split_queue: Queue<OrderedFloat<f64>, TileTreeHandle>,
    merge_queue: Queue<Reverse<OrderedFloat<f64>>, TileTreeHandle>,

    subdivide_count: usize,
    rejoin_count: usize,

    frame_number: usize,
    cached_viewable_region: [Plane<Meters>; 6],
    cached_eye_position: Pt3<Meters>,
    cached_eye_direction: DVec3,
}

impl TileTree {
    pub(crate) fn new(max_level: usize, target_refinement: f64, desired_tile_count: usize) -> Self {
        let mut tiles = TileHeap::default();
        let mut tree = TileTreeHeap::default();

        let extents = [
            // South West
            (
                Geode::new(degrees!(-90_f64), degrees!(-180_f64)),
                Geode::new(degrees!(0_f64), degrees!(-90_f64)),
            ),
            (
                Geode::new(degrees!(-90_f64), degrees!(-90_f64)),
                Geode::new(degrees!(0_f64), degrees!(0_f64)),
            ),
            // North West
            (
                Geode::new(degrees!(0_f64), degrees!(-180_f64)),
                Geode::new(degrees!(90_f64), degrees!(-90_f64)),
            ),
            (
                Geode::new(degrees!(0_f64), degrees!(-90_f64)),
                Geode::new(degrees!(90_f64), degrees!(0_f64)),
            ),
            // South East
            (
                Geode::new(degrees!(-90_f64), degrees!(0_f64)),
                Geode::new(degrees!(0_f64), degrees!(90_f64)),
            ),
            (
                Geode::new(degrees!(-90_f64), degrees!(90_f64)),
                Geode::new(degrees!(0_f64), degrees!(180_f64)),
            ),
            // North East
            (
                Geode::new(degrees!(0_f64), degrees!(0_f64)),
                Geode::new(degrees!(90_f64), degrees!(90_f64)),
            ),
            (
                Geode::new(degrees!(0_f64), degrees!(90_f64)),
                Geode::new(degrees!(90_f64), degrees!(180_f64)),
            ),
        ];
        let tile_handles = extents.map(|(min, max)| tiles.alloc(Tile::new(min, max)));
        let tree_handles = tile_handles
            .iter()
            .zip(extents)
            .map(|(tile_handle, (min, max))| {
                tree.alloc(TileTreeNode {
                    level: 1,
                    parent: None,
                    holder: TreeHolder::Tile(*tile_handle),
                    min,
                    max,
                    cached_solid_angle: -1f64,
                })
            })
            .collect::<Vec<TileTreeHandle>>();

        let root = Root {
            children: [
                tree_handles[0],
                tree_handles[1],
                tree_handles[2],
                tree_handles[3],
                tree_handles[4],
                tree_handles[5],
                tree_handles[6],
                tree_handles[7],
            ],
        };

        Self {
            max_level,
            target_refinement,
            desired_tile_count,
            tiles,
            tree,
            root,
            subdivide_count: 0,
            rejoin_count: 0,
            frame_number: 0,
            split_queue: Queue::new(),
            merge_queue: Queue::new(),
            cached_viewable_region: [Plane::from_normal_and_distance(DVec3::X, meters!(0f64)); 6],
            cached_eye_position: Pt3::zero(),
            cached_eye_direction: DVec3::X,
        }
    }

    pub(crate) fn get_tile(&self, handle: TileHandle) -> &Tile {
        self.tiles.get(handle)
    }

    fn allocate_leaf(
        &mut self,
        parent_handle: TileTreeHandle,
        level: usize,
        min: Geode,
        max: Geode,
    ) -> TileTreeHandle {
        let tile_handle = self.tiles.alloc(Tile::new(min, max));
        self.tree.alloc(TileTreeNode {
            holder: TreeHolder::Tile(tile_handle),
            parent: Some(parent_handle),
            level,
            min,
            max,
            cached_solid_angle: self.tiles.get(tile_handle).compute_solid_angle(
                &self.cached_viewable_region,
                &self.cached_eye_position,
                &self.cached_eye_direction,
                &mut None,
            ),
        })
    }

    fn free_leaf(&mut self, leaf_handle: TileTreeHandle) {
        assert!(
            self.tree.get(leaf_handle).is_leaf(),
            "trying to free non-leaf"
        );
        self.tiles.free(self.tree.get(leaf_handle).tile_handle());
        self.tree.free(leaf_handle);
    }

    pub(crate) fn solid_angle(&self, tree_handle: TileTreeHandle) -> f64 {
        self.tree.get(tree_handle).solid_angle()
    }

    fn check_queues(&self) {
        self.split_queue.check_invariants();
        self.merge_queue.check_invariants();
        for tree_handle in self.split_queue.iter() {
            assert!(self.is_splittable_node(*tree_handle));
        }
        for tree_handle in self.merge_queue.iter() {
            assert!(self.is_mergeable_node(*tree_handle));
        }
    }

    fn max_splittable(&mut self) -> f64 {
        if self.split_queue.is_empty() {
            return f64::MIN;
        }
        self.split_queue.peek_value().0
    }

    pub(crate) fn is_splittable_node(&self, ti: TileTreeHandle) -> bool {
        let node = self.tree.get(ti);
        node.is_leaf() && node.level < self.max_level
    }

    fn min_mergeable(&mut self) -> f64 {
        if self.merge_queue.is_empty() {
            return f64::MAX;
        }
        self.merge_queue.peek_value().0 .0
    }

    fn is_leaf_node(&self, tree_handle: TileTreeHandle) -> bool {
        let node = self.tree.get(tree_handle);
        node.is_node()
            && node
                .children()
                .iter()
                .all(|child| self.tree.get(*child).is_leaf())
    }

    pub(crate) fn is_mergeable_node(&self, ti: TileTreeHandle) -> bool {
        let node = self.tree.get(ti);
        if node.is_leaf() {
            return false;
        }

        // Check if merging would cause us to violate constraints.
        // All of our peers must be either leafs.
        for &child_handle in node.children() {
            let child = self.tree.get(child_handle);
            if !child.is_leaf() {
                return false;
            }
        }

        true
    }

    pub(crate) fn optimize_for_view(
        &mut self,
        camera: &OptimizeCamera,
        live_regions: &mut Vec<LiveRegion>,
        markers: &mut Option<&mut Markers>,
    ) {
        let reshape_start = Instant::now();

        self.subdivide_count = 0;
        self.rejoin_count = 0;

        self.cached_eye_position = camera.position::<Meters>();
        self.cached_eye_direction = *camera.forward();
        assert!(self.cached_eye_position.is_finite(), "camera 'sploded");

        for (i, f) in camera.world_space_frustum_m().iter().enumerate() {
            self.cached_viewable_region[i] = *f;
        }
        // Replace the rear culling plane with the horizon culling plane
        let horizon_plane_height =
            *EARTH_RADIUS * *EARTH_RADIUS / self.cached_eye_position.length();
        self.cached_viewable_region[5] = Plane::from_normal_and_distance(
            self.cached_eye_position.dvec3().normalize(),
            horizon_plane_height,
        );

        // If f=0 {
        //   Let T = the base triangulation.
        //   Clear Qs, Qm.
        //   Compute priorities for T’s triangles and diamonds, then
        //     insert into Qs and Qm, respectively.
        if self.frame_number == 0 {
            for &child in &self.root.children {
                self.split_queue
                    .insert(self.tree.get(child).solid_angle().into(), child);
            }
        }
        self.frame_number += 1;

        // } otherwise {
        //   Continue processing T=T{f-1}.
        //   Update priorities for all elements of Qs, Qm.
        // }
        self.update_priorities(markers);

        // While T is not the target size/accuracy, or the maximum split priority is greater than the minimum merge priority {
        //   If T is too large or accurate {
        //      Identify lowest-priority (T, TB) in Qm.
        //      Merge (T, TB).
        //      Update queues as follows: {
        //        Remove all merged children from Qs.
        //        Add merge parents T, TB to Qs.
        //        Remove (T, TB) from Qm.
        //        Add all newly-mergeable diamonds to Qm.
        //   } otherwise {
        //     Identify highest-priority T in Qs.
        //     Force-split T.
        //     Update queues as follows: {
        //       Remove T and other split triangles from Qs.
        //       Add any new triangles in T to Qs.
        //       Remove from Qm any diamonds whose children were split.
        //       Add all newly-mergeable diamonds to Qm.
        //     }
        //   }
        // }
        // Set Tf = T.

        let target_tile_count = self.desired_tile_count;
        let mut stuck_iterations = 0;

        // While T is not the target size/accuracy, or the maximum split priority is greater than the minimum merge priority {
        while self.max_splittable() - self.min_mergeable() > self.target_refinement
            || self.tiles.len() < target_tile_count - 4
            || self.tiles.len() > target_tile_count
        {
            if self.tiles.len() >= target_tile_count - 4 && self.tiles.len() <= target_tile_count {
                stuck_iterations += 1;
                if stuck_iterations > MAX_STUCK_ITERATIONS {
                    break;
                }
            }
            if PARANOIA_MODE {
                self.check_tree(None);
                self.check_queues();
            }

            //   If T is too large or accurate {
            if self.tiles.len() >= target_tile_count {
                if self.merge_queue.is_empty() {
                    panic!("would merge but nothing to merge");
                }

                //      Identify lowest-priority (T, TB) in Qm.
                let bottom_key = self.merge_queue.pop();

                //      Merge (T, TB).
                self.rejoin_leaf_tile_into(bottom_key);
            } else {
                if self.split_queue.is_empty() {
                    panic!("would split but nothing to split");
                }

                // Identify highest-priority T in Qs.
                let top_leaf = self.split_queue.pop();

                // Force-split T.
                self.subdivide_leaf(top_leaf);
            }
        }
        assert!(
            self.tiles.len() <= target_tile_count,
            "cached: {} of {}, stuck_iterations: {}, camera: {:?}",
            self.tiles.len(),
            target_tile_count,
            stuck_iterations,
            camera
        );

        // Prepare for next frame.
        self.check_tree(None);
        self.check_queues();

        // Build a view-direction independent tesselation based on the current camera position.
        assert!(live_regions.is_empty());
        self.capture_tiles(live_regions);
        let reshape_time = Instant::now() - reshape_start;

        // Select tiles based on visibility.
        let max_split = self.max_splittable();
        let min_merge = self.min_mergeable();
        trace!(
            "i:{} r:{} qs:{} qm:{} p:{}/{} t:{}/{} | -/+: {}/{} | {:.02}/{:.02} | {:?}",
            stuck_iterations,
            live_regions.len(),
            self.split_queue.len(),
            self.merge_queue.len(),
            self.tiles.len(),
            self.tiles.capacity(),
            self.tree.len(),
            self.tree.capacity(),
            self.rejoin_count,
            self.subdivide_count,
            max_split,
            min_merge,
            reshape_time,
        );
    }

    fn pull_solid_angle(
        &mut self,
        tree_handle: &TileTreeHandle,
        markers: &mut Option<&mut Markers>,
    ) -> f64 {
        let node = self.tree.get(*tree_handle);
        let mut sa = 0f64;
        match node.holder {
            TreeHolder::Tile(tile_handle) => {
                sa += self.tiles.get(tile_handle).compute_solid_angle(
                    &self.cached_viewable_region,
                    &self.cached_eye_position,
                    &self.cached_eye_direction,
                    markers,
                );
            }
            TreeHolder::Children(children) => {
                for child in &children {
                    sa += self.pull_solid_angle(child, markers);
                }
            }
        }
        self.tree.get_mut(*tree_handle).cached_solid_angle = sa;
        sa
    }

    fn update_priorities(&mut self, markers: &mut Option<&mut Markers>) {
        // Visit all tiles to apply the current view state
        let children = self.root.children;
        for tree_handle in &children {
            self.pull_solid_angle(tree_handle, markers);
        }

        // Re-sort split and merge queues, given the updated solid angles in the tree.
        let mut qs = self.split_queue.steal_content();
        for cache in qs.iter_mut() {
            if !self.split_queue.is_tombstoned(&cache.item) {
                cache.cache = self.solid_angle(cache.item).into();
            }
        }
        self.split_queue.restore_content(qs);

        let mut qm = self.merge_queue.steal_content();
        for cache in qm.iter_mut() {
            if !self.merge_queue.is_tombstoned(&cache.item) {
                cache.cache = Reverse(self.solid_angle(cache.item).into());
            }
        }
        self.merge_queue.restore_content(qm);
    }

    fn rejoin_leaf_tile_into(&mut self, tree_handle: TileTreeHandle) {
        self.rejoin_count += 1;
        assert!(self.is_leaf_node(tree_handle));
        let children = *self.tree.get(tree_handle).children();
        let min = self.tree.get(tree_handle).min;
        let max = self.tree.get(tree_handle).max;

        // Free children and remove from Qs.
        for &child in &children {
            self.free_leaf(child);
            //        Remove all merged children from Qs.
            self.split_queue.remove(child);
        }
        //        Remove (T, TB) from Qm.
        self.merge_queue.remove(tree_handle);

        // Replace the current node tile as a leaf tile and free the prior leaf node.
        let tile_handle = self.tiles.alloc(Tile::new(min, max));
        self.tree.get_mut(tree_handle).holder = TreeHolder::Tile(tile_handle);

        //        Add merge parents T, TB to Qs.
        self.split_queue
            .insert(self.solid_angle(tree_handle).into(), tree_handle);

        //        Add all newly-mergeable diamonds to Qm.
        if let Some(parent_handle) = self.tree.get(tree_handle).parent {
            if self.is_mergeable_node(parent_handle) {
                self.merge_queue.insert(
                    Reverse(self.solid_angle(parent_handle).into()),
                    parent_handle,
                );
            }
        }
    }

    fn subdivide_leaf(&mut self, tree_handle: TileTreeHandle) {
        self.subdivide_count += 1;

        let node = self.tree.get(tree_handle);

        assert!(node.is_leaf());
        let current_level = node.level;
        let next_level = current_level + 1;
        assert!(next_level <= self.max_level);
        let min = *node.min();
        let max = *node.max();
        let maybe_parent_handle = node.parent;

        // Get new bounds.
        let mid_lat = (max.lat::<Radians>() + min.lat::<Radians>()) / scalar!(2_f64);
        let mid_lon = (max.lon::<Radians>() + min.lon::<Radians>()) / scalar!(2_f64);
        let cent = Geode::new(mid_lat, mid_lon);
        let c01 = Geode::new(mid_lat, min.lon());
        let c12 = Geode::new(max.lat(), mid_lon);
        let c23 = Geode::new(mid_lat, max.lon());
        let c30 = Geode::new(min.lat(), mid_lon);

        // Allocate geometry to new tiles.
        let children = [
            self.allocate_leaf(tree_handle, next_level, min, cent),
            self.allocate_leaf(tree_handle, next_level, c01, c12),
            self.allocate_leaf(tree_handle, next_level, cent, max),
            self.allocate_leaf(tree_handle, next_level, c30, c23),
        ];

        // Recompute the solid angle of our node, using our new leaves solid angles
        // Note: the parent solid angles will remain wrong. In theory this should help with
        //       stability, but I've no evidence. We could probably just set SA to 0 initially
        //       and still be fine, as it will update next frame.
        let solid_angle = children
            .iter()
            .map(|hdl| self.tree.get(*hdl).solid_angle())
            .sum::<f64>();

        //   Remove T and other split triangles from Qs
        self.split_queue.remove(tree_handle);

        //   Add any new triangles in T to Qs.
        for &child in &children {
            if next_level < self.max_level {
                self.split_queue
                    .insert(self.tree.get(child).solid_angle().into(), child);
            }
        }

        // We can now be merged, since our children are leafs.
        //   Add all newly-mergeable diamonds to Qm.
        self.merge_queue
            .insert(Reverse(solid_angle.into()), tree_handle);

        // Transform our leaf/tile into a node and clobber the old tile.
        self.tiles.free(self.tree.get(tree_handle).tile_handle());
        self.tree.get_mut(tree_handle).holder = TreeHolder::Children(children);

        // We can no longer merge the parent, since we're now a node instead of a leaf.
        //   Remove from Qm any diamonds whose children were split.
        if let Some(parent_handle) = maybe_parent_handle {
            self.merge_queue.remove(parent_handle);
        }
    }

    fn check_tree(&self, split_context: Option<TileTreeHandle>) {
        // self.print_tree();
        let children = self.root.children; // Clone to avoid dual-borrow.
        for i in &children {
            self.check_tree_inner(1, *i, split_context);
        }
    }

    fn check_tree_inner(
        &self,
        level: usize,
        tree_handle: TileTreeHandle,
        split_context: Option<TileTreeHandle>,
    ) {
        let node = self.tree.get(tree_handle);

        if let Some(parent_handle) = node.parent {
            let parent = self.tree.get(parent_handle);
            assert!(parent.is_node());
            assert!(parent
                .children()
                .iter()
                .any(|child_handle| *child_handle == tree_handle));
        }

        if node.is_node() {
            if self.is_mergeable_node(tree_handle) {
                if !self.merge_queue.contains(&tree_handle) {
                    println!(
                        "{:?} is mergeable, so should be in merge_queue",
                        tree_handle
                    );
                    self.print_tree().unwrap();
                }
                assert!(self.merge_queue.contains(&tree_handle));
            }
            for child in node.children().iter() {
                self.check_tree_inner(level + 1, *child, split_context);
            }
        } else {
            assert!(level <= self.max_level);
            if self.is_splittable_node(tree_handle) && level < self.max_level {
                if !(self.split_queue.contains(&tree_handle) || Some(tree_handle) == split_context)
                {
                    println!(
                        "{:?} is splittable, so should be in split_queue; {} < {} w/ ctx {:?}",
                        tree_handle, level, self.max_level, split_context
                    );
                    self.print_tree().unwrap();
                }
                assert!(
                    self.split_queue.contains(&tree_handle) || Some(tree_handle) == split_context
                );
            }
        }
    }

    fn capture_tiles(&self, live_tiles: &mut Vec<LiveRegion>) {
        // Capture breadth first, sorting each level by solid-angle.
        // This lets us just stop when we run out of space each frame.
        assert!(live_tiles.is_empty());
        let children = self.root.children; // Clone to avoid dual-borrow.
        for at_level in 1..self.max_level {
            let start_offset = live_tiles.len();
            for i in &children {
                self.capture_live_tiles_inner(at_level, 1, *i, live_tiles);
            }
            let end_offset = live_tiles.len();

            // Sort in-place in the output vector to avoid extra allocations.
            live_tiles[start_offset..end_offset]
                .sort_unstable_by_key(|cap| Reverse(cap.solid_angle));
        }
    }

    fn capture_live_tiles_inner(
        &self,
        at_level: usize,
        level: usize,
        tree_handle: TileTreeHandle,
        live_tiles: &mut Vec<LiveRegion>,
    ) {
        let node = self.tree.get(tree_handle);

        // Note: we need to add all parents, not just leaves, for mipmaps and
        // full coverage when we cut off early
        if level == at_level {
            assert!(level <= self.max_level);
            live_tiles.push(LiveRegion {
                solid_angle: node.solid_angle().into(),
                bounds: GeodeBB::from_bounds(*node.min(), *node.max()),
                level,
            })
        }
        if node.is_node() {
            for &child in node.children() {
                self.capture_live_tiles_inner(at_level, level + 1, child, live_tiles);
            }
        }
    }

    #[allow(unused)]
    pub(crate) fn print_tree(&self) -> Result<()> {
        println!("{}", self.format_tree_display()?);
        Ok(())
    }

    #[allow(unused)]
    fn format_tree_display(&self) -> Result<String> {
        let mut out = String::new();
        out += "Root\n";
        for child in &self.root.children {
            out += &self.format_tree_display_inner(1, *child)?;
        }
        Ok(out)
    }

    #[allow(unused)]
    fn format_tree_display_inner(&self, lvl: usize, tree_handle: TileTreeHandle) -> Result<String> {
        let node = self.tree.get(tree_handle);
        let mut out = String::new();
        let pad = "  ".repeat(lvl);
        if node.is_node() {
            writeln!(out, "{}Node @{}", pad, node.level())?;
            for child in node.children() {
                out += &self.format_tree_display_inner(lvl + 1, *child)?;
            }
        } else {
            let tile = self.get_tile(node.tile_handle());
            writeln!(
                out,
                "{}Leaf @{} w/ {:0.4} - {} to {}",
                pad, node.level, node.cached_solid_angle, node.min, node.max,
            )?;
        }
        Ok(out)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use absolute_unit::{degrees, meters};
    use anyhow::Result;
    use arcball::ArcBallController;
    use camera::ScreenCamera;
    use geodesy::{Bearing, Geodetic, PitchCline};
    use hifitime::Epoch;
    use mantle::{Core, Gpu, PhysicalSize};

    #[test]
    fn test_pathological() -> Result<()> {
        let runtime = Core::for_test()?;
        let mut tree = TileTree::new(15, 150.0, 300);
        let mut live_tiles = Vec::new();
        let mut camera = ScreenCamera::new(
            "test",
            degrees!(90),
            PhysicalSize::new(1920, 1080),
            meters!(0.1),
            runtime.resource::<Gpu>(),
        );
        let mut arcball = ArcBallController::default();
        arcball.set_bearing(Bearing::north());
        arcball.set_pitch(PitchCline::new(degrees!(89)));
        arcball.set_distance(meters!(4_000_000))?;
        arcball.set_target_imm_unsafe(Geodetic::new(degrees!(0), degrees!(0), meters!(2)));
        camera.update_frame(&arcball.world_space_frame(), &Epoch::now()?);
        live_tiles.clear();
        tree.optimize_for_view(&OptimizeCamera::from(&camera), &mut live_tiles, &mut None);

        arcball.set_target_imm_unsafe(Geodetic::new(degrees!(0), degrees!(180), meters!(2)));
        camera.update_frame(&arcball.world_space_frame(), &Epoch::now()?);
        live_tiles.clear();
        tree.optimize_for_view(&OptimizeCamera::from(&camera), &mut live_tiles, &mut None);

        arcball.set_target_imm_unsafe(Geodetic::new(degrees!(0), degrees!(0), meters!(2)));
        camera.update_frame(&arcball.world_space_frame(), &Epoch::now()?);
        live_tiles.clear();
        tree.optimize_for_view(&OptimizeCamera::from(&camera), &mut live_tiles, &mut None);
        Ok(())
    }

    #[test]
    fn test_zoom_in() -> Result<()> {
        let runtime = Core::for_test()?;
        let mut tree = TileTree::new(15, 150.0, 300);
        let mut live_tiles = Vec::new();
        let mut camera = ScreenCamera::new(
            "test",
            degrees!(90),
            PhysicalSize::new(1920, 1080),
            meters!(0.1),
            runtime.resource::<Gpu>(),
        );
        let mut arcball = ArcBallController::default();
        arcball.set_target_imm_unsafe(Geodetic::new(degrees!(0), degrees!(0), meters!(2)));

        const CNT: i64 = 40;
        for i in 0..CNT {
            arcball.set_bearing(Bearing::north());
            arcball.set_pitch(PitchCline::new(degrees!(89)));
            arcball.set_distance(meters!(4_000_000 - i * (4_000_000 / CNT)))?;
            camera.update_frame(&arcball.world_space_frame(), &Epoch::now()?);
            live_tiles.clear();
            tree.optimize_for_view(&OptimizeCamera::from(&camera), &mut live_tiles, &mut None);
        }

        Ok(())
    }

    #[test]
    fn test_fly_forward() -> Result<()> {
        let runtime = Core::for_test()?;
        let mut tree = TileTree::new(15, 150.0, 300);
        let mut live_tiles = Vec::new();
        let mut camera = ScreenCamera::new(
            "test",
            degrees!(90),
            PhysicalSize::new(1920, 1080),
            meters!(0.1),
            runtime.resource::<Gpu>(),
        );
        let mut arcball = ArcBallController::default();
        arcball.set_target_imm_unsafe(Geodetic::new(degrees!(0), degrees!(0), meters!(1000)));
        arcball.set_bearing(Bearing::new(degrees!(90)));
        arcball.set_pitch(PitchCline::new(degrees!(1)));
        arcball.set_distance(meters!(1_500_000))?;

        const CNT: i64 = 40;
        for i in 0..CNT {
            arcball.set_target_imm_unsafe(Geodetic::new(
                degrees!(0),
                degrees!(4 * i),
                meters!(1000),
            ));
            camera.update_frame(&arcball.world_space_frame(), &Epoch::now()?);
            live_tiles.clear();
            tree.optimize_for_view(&OptimizeCamera::from(&camera), &mut live_tiles, &mut None);
        }

        Ok(())
    }
}
