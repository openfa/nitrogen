// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    common::OptimizeCamera,
    tile::{
        tile_atlas::TileAtlas,
        tile_tree::{LiveRegion, TileTree},
    },
};
use anyhow::Result;
use geodb::{GeoDb, MapKind, MapName, MapState, TileSubdivisionLevel};
use mantle::Gpu;
use marker::Markers;
use runtime::RuntimeResource;
use std::{collections::HashSet, fmt, sync::Arc};

pub(crate) struct TileManager {
    // Configuration
    _max_level: usize,
    _target_refinement: f64,
    desired_patch_count: usize,

    // The frame-coherent optimal patch tree. e.g. the hard part.
    tile_tree: TileTree,

    // Cached allocation of frame-local live tiles
    live_tiles: Vec<LiveRegion>,

    // Cached allocation of frame-local required backing pixmaps
    live_height_maps: Vec<(MapName, Arc<Vec<u8>>)>,
    seen_height_maps: HashSet<MapName>,
    live_color_maps: Vec<(MapName, Arc<Vec<u8>>)>,
    seen_color_maps: HashSet<MapName>,

    // Atlases
    height_atlas: TileAtlas,
    color_atlas: TileAtlas,
}

impl fmt::Debug for TileManager {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "TileManger")
    }
}

impl TileManager {
    pub(crate) fn new(
        max_level: usize,
        target_refinement: f64,
        desired_patch_count: usize,
        tile_cache_size: u32,
        gpu: &Gpu,
    ) -> Result<Self> {
        // FIXME: assert that max_level is less than whatever is provided by geodb
        let tile_tree = TileTree::new(max_level, target_refinement, desired_patch_count);
        let height_atlas = TileAtlas::new(MapKind::Heights, tile_cache_size, gpu);
        let color_atlas = TileAtlas::new(MapKind::Colors, tile_cache_size, gpu);
        Ok(Self {
            _max_level: max_level,
            _target_refinement: target_refinement,
            desired_patch_count,
            tile_tree,
            live_tiles: Vec::new(),
            live_height_maps: Vec::new(),
            seen_height_maps: HashSet::new(),
            live_color_maps: Vec::new(),
            seen_color_maps: HashSet::new(),
            height_atlas,
            color_atlas,
        })
    }

    pub(crate) fn reload_tiles(&mut self) {
        // The beyond frame resources here are the live sets. This will force us to
        // pull anything new to the front if it's not being discarded for some reason.
        self.live_height_maps.clear();
        self.live_color_maps.clear();

        // We don't upload if the thing is already there, so we also have to reset
        // the atlas to get anything to reload if the content changed.
        self.height_atlas.clear();
    }

    fn upsert_map_tree(
        (name, state): (MapName, MapState),
        seen_maps: &mut HashSet<MapName>,
        live_maps: &mut Vec<(MapName, Arc<Vec<u8>>)>,
    ) -> bool {
        // Early exit if we've already processed this path.
        if seen_maps.contains(&name) {
            return true;
        }

        // Since we succeeded in adding the parent, add ourself now, if we are ready.
        if let MapState::Ready { data, .. } = state {
            seen_maps.insert(name);
            live_maps.push((name, data));
            return true;
        }

        false
    }

    fn map_tiles_to_regions(
        (frame_number, geodb, gpu): (u64, &mut GeoDb, &Gpu),
        (kind, atlas, live_tiles): (MapKind, &mut TileAtlas, &[LiveRegion]),
        (seen_maps, live_maps): (&mut HashSet<MapName>, &mut Vec<(MapName, Arc<Vec<u8>>)>),
    ) {
        seen_maps.clear();
        for tile in live_tiles {
            // If we have filled our atlas, stop iterating.
            if live_maps.len() >= (atlas.num_slots() - 1) {
                break;
            }
            // We should expect a tile to regularly map to 4-9 tiles, but most of those maps
            // to be intersecting many tiles.
            let mut maps = geodb.map_region_to_tiles(
                &tile.bounds,
                TileSubdivisionLevel::new(tile.level),
                kind,
                frame_number,
            );
            for (name, state) in maps.drain(..) {
                Self::upsert_map_tree((name, state), seen_maps, live_maps);
            }
        }

        // Push to the GPU
        atlas.provide_maps(live_maps, frame_number, geodb, gpu);

        // Clear arcs to drop mem
        live_maps.clear();
    }

    pub(crate) fn track_state_changes(
        &mut self,
        optimize_camera: &OptimizeCamera,
        rt: &RuntimeResource,
        gpu: &Gpu,
        geodb: &mut GeoDb,
        markers: &mut Option<&mut Markers>,
    ) {
        // Live tiles is breadth-first, sorted by solid-angle within level.
        self.live_tiles.clear();
        self.tile_tree
            .optimize_for_view(optimize_camera, &mut self.live_tiles, markers);
        assert!(self.live_tiles.len() <= self.desired_patch_count * 4);

        // Re-use the existing allocations.
        Self::map_tiles_to_regions(
            (rt.frame_number(), geodb, gpu),
            (MapKind::Heights, &mut self.height_atlas, &self.live_tiles),
            (&mut self.seen_height_maps, &mut self.live_height_maps),
        );
        Self::map_tiles_to_regions(
            (rt.frame_number(), geodb, gpu),
            (MapKind::Colors, &mut self.color_atlas, &self.live_tiles),
            (&mut self.seen_color_maps, &mut self.live_color_maps),
        );
    }

    pub(crate) fn encode_uploads(&mut self, gpu: &Gpu, encoder: &mut wgpu::CommandEncoder) {
        self.height_atlas.encode_uploads(gpu, encoder);
        self.color_atlas.encode_uploads(gpu, encoder);
    }

    pub(crate) fn request_dump_indices(&mut self) -> Result<()> {
        self.height_atlas
            .request_dump_index("terrain/tiles/height-index")?;
        self.color_atlas
            .request_dump_index("terrain/tiles/color-index")?;
        Ok(())
    }

    pub(crate) fn request_dump_atlases(&mut self) -> Result<()> {
        self.height_atlas
            .request_dump_atlas("terrain/tiles/height-atlas")?;
        self.color_atlas
            .request_dump_atlas("terrain/tiles/color-atlas")?;
        Ok(())
    }

    pub(crate) fn maybe_dump(&mut self, gpu: &mut Gpu) -> Result<()> {
        self.height_atlas.maybe_dump_index(gpu)?;
        self.height_atlas.maybe_dump_atlas(gpu)?;
        self.color_atlas.maybe_dump_index(gpu)?;
        self.color_atlas.maybe_dump_atlas(gpu)?;
        Ok(())
    }

    pub(crate) fn heights_bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        self.height_atlas.bind_group_layout()
    }

    pub(crate) fn heights_bind_group(&self) -> &wgpu::BindGroup {
        self.height_atlas.bind_group()
    }

    pub(crate) fn colors_bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        self.color_atlas.bind_group_layout()
    }

    pub(crate) fn colors_bind_group(&self) -> &wgpu::BindGroup {
        self.color_atlas.bind_group()
    }
}
