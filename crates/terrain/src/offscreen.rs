// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::patch::{PatchManager, TerrainVertex};
use camera::ScreenCamera;
use mantle::Gpu;

// Manage the offscreen depth and lat/lon buffers: our sole geometry rendering pass.
#[derive(Debug)]
pub(crate) struct OffscreenRender {
    pipeline: wgpu::RenderPipeline,
    bind_group_layout: wgpu::BindGroupLayout,
}

impl OffscreenRender {
    pub(crate) const COLOR_LAYER_NAME: &'static str = "terrain-color-deferred-gbuffer";
    pub(crate) const DEPTH_LAYER_NAME: &'static str = "terrain-depth-deferred-gbuffer";
    pub(crate) const BINDING_NAME: &'static str = "terrain-deferred-gbuffer-bindings";
    pub(crate) const DEFERRED_TEXTURE_FORMAT: wgpu::TextureFormat =
        wgpu::TextureFormat::Rgba32Float;

    pub(crate) fn new(gpu: &Gpu) -> Self {
        let shader = gpu.compile_shader(
            "draw_deferred_texture.wgsl",
            include_str!("../target/draw_deferred_texture.wgsl"),
        );
        let pipeline = gpu
            .device()
            .create_render_pipeline(&wgpu::RenderPipelineDescriptor {
                label: Some("deferred-texture-pipeline"),
                layout: Some(&gpu.device().create_pipeline_layout(
                    &wgpu::PipelineLayoutDescriptor {
                        label: Some("deferred-texture-pipeline-layout"),
                        push_constant_ranges: &[],
                        bind_group_layouts: &[&ScreenCamera::info_bind_group_layout(gpu)],
                    },
                )),
                vertex: wgpu::VertexState {
                    module: &shader,
                    entry_point: "main_vert",
                    buffers: &[TerrainVertex::descriptor()],
                    compilation_options: wgpu::PipelineCompilationOptions {
                        constants: &Default::default(),
                        zero_initialize_workgroup_memory: false,
                    },
                },
                fragment: Some(wgpu::FragmentState {
                    module: &shader,
                    entry_point: "main_frag",
                    targets: &[Some(wgpu::ColorTargetState {
                        format: Self::DEFERRED_TEXTURE_FORMAT,
                        blend: None,
                        write_mask: wgpu::ColorWrites::ALL,
                    })],
                    compilation_options: wgpu::PipelineCompilationOptions {
                        constants: &Default::default(),
                        zero_initialize_workgroup_memory: false,
                    },
                }),
                primitive: wgpu::PrimitiveState {
                    topology: wgpu::PrimitiveTopology::TriangleStrip,
                    strip_index_format: Some(wgpu::IndexFormat::Uint32),
                    front_face: wgpu::FrontFace::Cw,
                    cull_mode: Some(wgpu::Face::Back),
                    unclipped_depth: false,
                    polygon_mode: wgpu::PolygonMode::Fill,
                    conservative: false,
                },
                depth_stencil: Some(wgpu::DepthStencilState {
                    format: Gpu::DEPTH_FORMAT,
                    depth_write_enabled: true,
                    depth_compare: wgpu::CompareFunction::Greater,
                    stencil: wgpu::StencilState {
                        front: wgpu::StencilFaceState::IGNORE,
                        back: wgpu::StencilFaceState::IGNORE,
                        read_mask: 0,
                        write_mask: 0,
                    },
                    bias: wgpu::DepthBiasState {
                        constant: 0,
                        slope_scale: 0.0,
                        clamp: 0.0,
                    },
                }),
                multisample: wgpu::MultisampleState {
                    count: 1,
                    mask: !0,
                    alpha_to_coverage_enabled: false,
                },
                multiview: None,
            });

        let bind_group_layout = ScreenCamera::make_bind_group_layout(
            &[Self::DEFERRED_TEXTURE_FORMAT, Gpu::DEPTH_FORMAT],
            false,
            gpu,
        );

        Self {
            pipeline,
            bind_group_layout,
        }
    }

    pub(crate) fn bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        &self.bind_group_layout
    }

    pub(crate) fn deferred_texture(
        &self,
        patch_man: &PatchManager,
        gpu: &Gpu,
        camera: &mut ScreenCamera,
        encoder: &mut wgpu::CommandEncoder,
    ) {
        camera.ensure_layer(Self::COLOR_LAYER_NAME, Self::DEFERRED_TEXTURE_FORMAT, gpu);
        camera.ensure_layer(Self::DEPTH_LAYER_NAME, Gpu::DEPTH_FORMAT, gpu);
        camera.ensure_bind_group(
            Self::BINDING_NAME,
            &[Self::COLOR_LAYER_NAME, Self::DEPTH_LAYER_NAME],
            false,
            gpu,
        );

        let mut rpass = camera.begin_render_pass(
            Self::COLOR_LAYER_NAME,
            Some(Self::DEPTH_LAYER_NAME),
            true,
            encoder,
        );

        rpass.set_pipeline(&self.pipeline);
        rpass.set_bind_group(0, camera.info_bind_group(), &[]);
        rpass.set_vertex_buffer(0, patch_man.vertex_buffer());
        rpass.set_index_buffer(patch_man.tristrip_index_buffer(), wgpu::IndexFormat::Uint32);

        if gpu.has_multi_draw_indirect() {
            rpass.multi_draw_indexed_indirect(
                patch_man.draw_indirect_buffer(),
                0,
                patch_man.num_patches() as u32,
            );
        } else {
            for cmd in patch_man.draw_indirect_commands() {
                rpass.draw_indexed(
                    cmd.base_index..cmd.base_index + cmd.index_count,
                    cmd.vertex_offset,
                    cmd.base_instance..cmd.base_instance + cmd.instance_count,
                )
            }
        }
    }
}
