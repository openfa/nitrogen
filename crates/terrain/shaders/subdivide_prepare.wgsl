// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <terrain/include/terrain_subdivision_context.wgsl>
//!include <terrain/include/terrain_vertex.wgsl>

struct TerrainUploadVertex {
    position: array<f32,3u>,
    normal: array<f32,3u>,
    graticule: array<f32,2u>,
}

@group(0) @binding(0) var<uniform> context: SubdivisionContext;
@group(0) @binding(1) var<storage, read_write> target_vertices: array<TerrainVertex>;
@group(0) @binding(2) var<storage> patch_upload_vertices: array<TerrainUploadVertex>;

var<private> PATCH_UPLOAD_STRIDE: u32 = 3u;

@compute @workgroup_size(64, 1, 1)
fn main(@builtin(global_invocation_id) idx: vec3<u32>) {
    let i = idx.x;

    // Find our patch offset and our offset within the uploaded patch.
    let patch_id = i / PATCH_UPLOAD_STRIDE;
    let offset = i % PATCH_UPLOAD_STRIDE;

    // Project our input into the target patch.
    // Note: No need to set the position yet
    let voff = patch_id * context.target_stride + offset;
    target_vertices[voff].surface_position = patch_upload_vertices[i].position;
    //target_vertices[voff].position = patch_upload_vertices[i].position;
    target_vertices[voff].normal = patch_upload_vertices[i].normal;
    target_vertices[voff].graticule = patch_upload_vertices[i].graticule;

    return;
}
