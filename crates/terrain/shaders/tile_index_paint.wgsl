// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

struct VertexOutput {
    @location(0) color: vec4<u32>,
    @builtin(position) position: vec4<f32>,
}

@vertex
fn main_vert(@location(0) position: vec2<f32>, @location(1) color: vec4<u32>) -> VertexOutput {
    return VertexOutput(color, vec4(position, 0., 1.));
}

struct FragmentOutput {
    @location(0) color: vec4<u32>,
}

@fragment
fn main_frag(@location(0) color: vec4<u32>) -> FragmentOutput {
    return FragmentOutput(color);
}
