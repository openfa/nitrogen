// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <shader_shared/include/buffer_helpers.wgsl>
//!include <terrain/include/terrain_subdivision_context.wgsl>
//!include <terrain/include/terrain_vertex.wgsl>

var<private> WORKGROUP_WIDTH: u32 = 65536u;

@group(0) @binding(0) var<uniform> context: SubdivisionContext;
@group(0) @binding(1) var<uniform> expand: SubdivisionExpandContext;
@group(0) @binding(2) var<storage, read_write> target_vertices: array<TerrainVertex>;
@group(0) @binding(3) var<storage> index_dependency_lut: array<u32>;

@compute @workgroup_size(64, 2, 1)
fn main(@builtin(global_invocation_id) global_idx: vec3<u32>) {
    // The iteration vector is over expand.compute_vertices_in_patch * num_patches.
    let i = global_idx.x + global_idx.y * WORKGROUP_WIDTH;

    // Find our patch offset and our offset within the current work set.
    let patch_id = i / expand.compute_vertices_in_patch;
    let relative_offset = i % expand.compute_vertices_in_patch;

    // To get the buffer offset we find our base patch offset, skip the prior computed vertices, then offset.
    let patch_base = context.target_stride * patch_id;
    let patch_offset = expand.skip_vertices_in_patch + relative_offset;
    let offset = patch_base + patch_offset;

    // There are two dependencies per input, uploaded sequentially. Note that the deps are per-patch.
    let dep_a = patch_base + index_dependency_lut[patch_offset * 2u + 0u];
    let dep_b = patch_base + index_dependency_lut[patch_offset * 2u + 1u];

    // Load vertices.
    var tva: TerrainVertex = target_vertices[dep_a];
    var tvb: TerrainVertex = target_vertices[dep_b];

    // Note: patch edges that show up as A->B->C on one patch may be wound as C->B->A on an adjacent patch. There is no
    // way to fix this at the patch level (for the same reason we need to note the adjacent patch's edge in our peers).
    // While it should not actually matter if we process these as A-B vs B-A, in practice numerical precision issues
    // crop up in tall and steep slopes, leading to obvious gaps between patches, even with correctly wound strips.
    // HACK: process all pairs in the same order by sorting on x; edges lying exactly in the y-z
    //       plane are unlikely enough in combination that we can get away with a single compare.
    if (tva.surface_position[0] > tvb.surface_position[0]) {
        let tmp = tva;
        tva = tvb;
        tvb = tmp;
    }

    // Do normal interpolation the normal way.
    let na = arr_to_vec3_(tva.normal);
    let nb = arr_to_vec3_(tvb.normal);
    let tmp = na + nb;
    let nt = tmp / length(tmp);
    // Note clamp to 1 to avoid NaN from acos.
    let w = acos(min(1., dot(na, nt)));

    // Use the haversine geodesic midpoint method to compute graticule.
    // j/k => a/b
    let phi_a = tva.graticule[0];
    let theta_a = tva.graticule[1];
    let phi_b = tvb.graticule[0];
    let theta_b = tvb.graticule[1];
    // bx = cos(φk) · cos(θk−θj)
    let beta_x = cos(phi_b) * cos(theta_b - theta_a);
    // by = cos(φk) · sin(θk−θj)
    let beta_y = cos(phi_b) * sin(theta_b - theta_a);
    // φi = atan2(sin(φj) + sin(φk), √((cos(φj) + bx)^2 + by^2))
    let cpa_beta_x = cos(phi_a) + beta_x;
    let phi_t = atan2(
        sin(phi_a) + sin(phi_b),
        sqrt(cpa_beta_x * cpa_beta_x + beta_y * beta_y)
    );
    // θi = θj + atan2(by, cos(φj) + bx)
    let theta_t = theta_a + atan2(beta_y, cos(phi_a) + beta_x);

    // Use the clever tan method from figure 35.
    let pa = arr_to_vec3_(tva.surface_position);
    let pb = arr_to_vec3_(tvb.surface_position);
    let x = length(pb - pa) / 2.;
    // Note that the angle we get is not the same as the opposite-over-adjacent angle we want.
    // It seems to be related to that angle though, by being 2x that angle; thus, divide by 2.
    let y = x * tan(w / 2.);
    let midpoint = (pa + pb) / 2.;
    let pt = midpoint + y * nt;

    target_vertices[offset].surface_position[0] = pt.x;
    target_vertices[offset].surface_position[1] = pt.y;
    target_vertices[offset].surface_position[2] = pt.z;
    target_vertices[offset].normal[0] = nt.x;
    target_vertices[offset].normal[1] = nt.y;
    target_vertices[offset].normal[2] = nt.z;
    target_vertices[offset].graticule[0] = phi_t;
    target_vertices[offset].graticule[1] = theta_t;
    return;
}
