// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <camera/include/camera.wgsl>
//!bindings [0] <camera/bindings/camera.wgsl>

struct VertexOutput {
    @location(0) color: vec4<f32>,
    @builtin(position) position: vec4<f32>,
}

@vertex
fn main_vert(
    @location(0) surface_position_eye: vec3<f32>,
    @location(1) position_eye: vec3<f32>,
    @location(2) normal_eye: vec3<f32>,
    @location(3) graticule: vec2<f32>
) -> VertexOutput {
    // Note: we upload positions in eye space instead of world space for precision reasons.
    // Note: multiply by perspective to get to a clip space coordinate
    let position = camera.perspective_m * vec4(position_eye, 1.);

    // Normals are uploaded in eye space so that they can displace the eye-space verticies as we
    // build the vertices. We want to invert the normal to world space for storage in the deferred
    // texture.
    let normal_wrld = (camera.inverse_view_km * vec4(normalize(normal_eye), 0.)).xyz;
    let color = vec4(graticule, normal_wrld.x, normal_wrld.z);

    return VertexOutput(color, position);
}

struct FragmentOutput {
    @location(0) color: vec4<f32>,
}

@fragment
fn main_frag(@location(0) color: vec4<f32>) -> FragmentOutput {
    // Note: relying here on automatic depth writes.
    // 0 is unwritten, 1 is near camera, smaller is farther away
    return FragmentOutput(color);
}
