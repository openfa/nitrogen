// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <camera/include/camera.wgsl>
//!include <shader_shared/include/buffer_helpers.wgsl>
//!include <shader_shared/include/fullscreen.wgsl>
//!include <terrain/include/terrain_color_tile_access.wgsl>

@group(0) @binding(0) var color_index_texture: texture_2d<u32>;
@group(0) @binding(1) var color_index_sampler: sampler;
@group(0) @binding(2) var color_atlas_texture: texture_2d<f32>;
@group(0) @binding(3) var color_atlas_sampler: sampler;
@group(0) @binding(4) var<storage> color_index_info: IndexMetadata;

//!bindings [1] <terrain/bindings/offscreen_deferred.wgsl>
//!bindings [2] <camera/bindings/camera.wgsl>

struct VertexOutput {
    @location(0) texcoord: vec2<f32>,
    @builtin(position) position: vec4<f32>,
}

@vertex
fn main_vert(@builtin(vertex_index) i: u32) -> VertexOutput {
    let input = fullscreen_input(i);
    return VertexOutput(input.frame, input.clip);
}

struct FragOut {
    @location(0) color: vec4<f32>,
}

@fragment
fn main_frag(@location(0) uv: vec2<f32>) -> FragOut {
    let coord = vec2<i32>(uv * vec2(f32(camera.render_width), f32(camera.render_height)));
    let depth = textureLoad(terrain_deferred_depth, coord, 0);
    var out = vec4(0.);
    if (depth > 0.) {
        let grat_ll = textureLoad(terrain_deferred_texture, coord, 0).xy;
        out = terrain_sample_color_linear(
            grat_ll,
            color_index_texture,
            color_atlas_texture,
            color_index_info
        );
    }
    return FragOut(out);
}
