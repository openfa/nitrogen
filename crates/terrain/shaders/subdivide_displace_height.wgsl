// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <shader_shared/include/buffer_helpers.wgsl>
//!include <terrain/include/terrain_vertex.wgsl>
//!include <terrain/include/terrain_height_tile_access.wgsl>

var<private> WORKGROUP_WIDTH: u32 = 65536u;

@group(0) @binding(0) var<storage, read_write> vertices: array<TerrainVertex>;
@group(1) @binding(0) var height_index_texture: texture_2d<u32>;
@group(1) @binding(1) var height_index_sampler: sampler;
@group(1) @binding(2) var height_atlas_texture: texture_2d<i32>;
@group(1) @binding(3) var height_atlas_sampler: sampler;
@group(1) @binding(4) var<storage> height_index_info: IndexMetadata;

@compute @workgroup_size(64, 2, 1)
fn main(@builtin(global_invocation_id) global_idx: vec3<u32>) {
    // One invocation per vertex.
    let i = global_idx.x + global_idx.y * WORKGROUP_WIDTH;

    let grat_ll = arr_to_vec2_(vertices[i].graticule);
    let height = terrain_sample_height_linear(
        grat_ll,
        height_index_texture,
        height_atlas_texture,
        height_index_info
    );

    let v_normal = arr_to_vec3_(vertices[i].normal);
    let v_position = arr_to_vec3_(vertices[i].surface_position);
    vertices[i].position = vec3_to_arr(v_position + (height * v_normal));

    return;
}
