// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

struct TerrainVertex {
    // Note that we cannot use vec3 here as that packs into vec4 in a struct storage buffer context,
    // unlike in a vertex context where it packs properly.
    surface_position: array<f32,3u>,
    position: array<f32,3u>,
    normal: array<f32,3u>,
    graticule: array<f32,2u>,
}
