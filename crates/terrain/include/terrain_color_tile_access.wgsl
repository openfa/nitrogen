// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <terrain/include/terrain_tile_access.wgsl>

fn _terrain_sample_color_oob(
    grat_ll: vec2<f32>,
    color_index_texture: texture_2d<u32>,
    color_atlas_texture: texture_2d<f32>,
    color_index_info: IndexMetadata
) -> vec4<f32> {
    let info = terrain_sample_index(grat_ll, color_index_texture, color_index_info);
    // Note the lack of a 0.5 factor: we are already offset from the parent algorithm.
    let tile_px = vec2<i32>(floor(info.tile_px_f));
    return textureLoad(color_atlas_texture, info.atlas_base_px + tile_px, 0);
}

struct ColorGatherResult {
    c00: vec4<f32>,
    c01: vec4<f32>,
    c10: vec4<f32>,
    c11: vec4<f32>,
    f: vec2<f32>,
    level: u32,
}

fn _terrain_gather_colors_2x2(
    grat_ll: vec2<f32>,
    color_index_texture: texture_2d<u32>,
    color_atlas_texture: texture_2d<f32>,
    color_index_info: IndexMetadata,
) -> ColorGatherResult {
    let info = terrain_sample_index(
        grat_ll,
        color_index_texture,
        color_index_info,
    );

    // Map from index to atlas sampling
    let tile_px_f = info.tile_px_f;
    let f = fract(tile_px_f - vec2(0.5));
    let tile_px = vec2<i32>(floor(tile_px_f - vec2(0.5)));
    var i0 = tile_px.x; // pixel grid
    var i1 = tile_px.x + 1;
    var j0 = tile_px.y;
    var j1 = tile_px.y + 1;

    // If all samples are within the tile, use cheap sampling.
    // Otherwise, compute the lat-lon of the samples and sample
    // by doing the full index->atlas traversal for each pixel.
    let is_edge = i0 < 0 || j0 < 0|| i1 >= i32(TILE_SIZE) || j1 >= i32(TILE_SIZE);
    if (!is_edge) {
        var c00 = textureLoad(color_atlas_texture, info.atlas_base_px + vec2(i0, j0), 0);
        var c01 = textureLoad(color_atlas_texture, info.atlas_base_px + vec2(i0, j1), 0);
        var c10 = textureLoad(color_atlas_texture, info.atlas_base_px + vec2(i1, j0), 0);
        var c11 = textureLoad(color_atlas_texture, info.atlas_base_px + vec2(i1, j1), 0);
        return ColorGatherResult(c00, c01, c10, c11, f, info.level);
    } else {
        // Compute grat equivalent for i/j for lookup in other tiles, if needed.
        // this is all TIFF level; max resolution at level 0
        let pixel_rad = radians(color_index_info.level_0_px_degrees * f32(1u << info.level));

        let dx = pixel_rad;
        let dy = -pixel_rad;
        let base_x = -pixel_rad * f.x;
        let base_y = pixel_rad * f.y;
        let ori = vec2(grat_ll.x + base_y, grat_ll.y + base_x);
        let grat00 = vec2(ori.x,      ori.y);
        let grat01 = vec2(ori.x,      ori.y + dx);
        let grat10 = vec2(ori.x + dy, ori.y);
        let grat11 = vec2(ori.x + dy, ori.y + dx);

        var c00 = _terrain_sample_color_oob(grat00, color_index_texture, color_atlas_texture, color_index_info);
        var c01 = _terrain_sample_color_oob(grat10, color_index_texture, color_atlas_texture, color_index_info);
        var c10 = _terrain_sample_color_oob(grat01, color_index_texture, color_atlas_texture, color_index_info);
        var c11 = _terrain_sample_color_oob(grat11, color_index_texture, color_atlas_texture, color_index_info);

        return ColorGatherResult(c00, c01, c10, c11, f, info.level);
    }
}

fn terrain_sample_color_linear(
    grat_ll: vec2<f32>,
    color_index_texture: texture_2d<u32>,
    color_atlas_texture: texture_2d<f32>,
    color_index_info: IndexMetadata,
) -> vec4<f32> {
    let s = _terrain_gather_colors_2x2(
        grat_ll,
        color_index_texture,
        color_atlas_texture,
        color_index_info,
    );
    let f = s.f;
    let fp = 1. - f;
    let t00 = fp.x * fp.y * s.c00;
    let t10 =  f.x * fp.y * s.c10;
    let t01 = fp.x *  f.y * s.c01;
    let t11 =  f.x *  f.y * s.c11;
    return t00 + t10 + t01 + t11;
}
