// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

fn terrain_ground_diffuse_color(
    terrain_color_acc_texture: texture_2d<f32>,
    tc_idx: vec2<i32>
) -> vec4<f32> {
    // Load the accumulated color at the current screen position.
    return textureLoad(terrain_color_acc_texture, tc_idx, 0);
}

fn terrain_ground_normal_local(
    terrain_normal_acc_texture: texture_2d<f32>,
    tc_idx: vec2<i32>
) -> vec3<f32> {
    var m = textureLoad(terrain_normal_acc_texture, tc_idx, 0).xy;

    // x**2 + y**2 + z**2 = 1
    // z**2 = 1 - x**2 - y**2
    var y2 = 1. - length(m);
    let y = sqrt(clamp(y2, 0., 1.));
    let ground_normal_local = normalize(vec3(
        m.x,
        y,
        m.y
    ));

    return ground_normal_local;
}

fn terrain_ground_normal_world(
    terrain_deferred_texture: texture_2d<f32>,
    terrain_normal_acc_texture: texture_2d<f32>,
    tc_idx: vec2<i32>
) -> vec3<f32> {
    let ground_normal_local = terrain_ground_normal_local(terrain_normal_acc_texture, tc_idx);

    // Translate the normal's local coordinates into global coordinates by using the lat/lon.
    let latlon = textureLoad(terrain_deferred_texture, tc_idx, 0).xy;
    let lat = latlon.x;
    let lon = latlon.y;
    let r_lon = quat_from_axis_angle(vec3(0., 1., 0.), lon);
    let lat_axis = quat_rotate(r_lon, vec3(-1., 0., 0.)).v.xyz;
    let r_lat = quat_from_axis_angle(lat_axis, (PI / 2.) - lat);
    var ground_normal_w = quat_rotate(r_lat, quat_rotate(r_lon, ground_normal_local).v.xyz).v.xyz;
    ground_normal_w.z = -ground_normal_w.z;

    return ground_normal_w;
}
