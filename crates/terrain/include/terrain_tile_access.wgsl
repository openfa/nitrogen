// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

var<private> TILE_SIZE: f32 = 512.0;

struct IndexMetadata {
    // The base of the indexed region
    // Note: tie_start at upper-left corner, rather than lower-left
    tie_start_ll_rad: array<f32,2u>,

    // Size of the indexed region
    tie_extent_ll_rad: array<f32,2u>,

    // The index uses 1 px per highest resolution tile, but the planet doesn't evenly
    // map to tiles, so the last tile is partially filled. We also need to access as
    // pixels. This scale combines both mapping operations from logical uv in tie-point
    // space into the index space.
    scale_tie_to_px: array<f32,2u>,

    // Size in pixels of x and y of the index.
    atlas_size: array<f32,2u>,

    // Angular extent of a single pixel at each level.
    level_0_px_degrees: f32,
}

// Input: the index_info for the tile set we are looking at.
// Output: the tie_start value in x/y
fn terrain_index_tie_start_xy(index_info: IndexMetadata) -> vec2<f32> {
    return arr_to_vec2_(index_info.tie_start_ll_rad).yx;
}

// Input: the index_info for the tile set we are looking at.
// Output: the tie_extent value in x/y
fn terrain_index_tie_extent_xy(index_info: IndexMetadata) -> vec2<f32> {
    return arr_to_vec2_(index_info.tie_extent_ll_rad).yx;
}

// Input: the graticule in lat/lon and the index_extent
// Output: the graticule's offset from tie_start, in x/y
fn terrain_index_tie_to_grat_xy(grat_ll: vec2<f32>, index_info: IndexMetadata) -> vec2<f32> {
    // [-60,60]x[-180,180] - [60,-180] => [-120,0]x[0,360]
    return grat_ll.yx - terrain_index_tie_start_xy(index_info);
}

// Input: the graticule in lat/lon and the index_extent
// Output: the scaling factor in x/y
fn terrain_index_scale_tie_to_px_xy(index_info: IndexMetadata) -> vec2<f32> {
    return arr_to_vec2_(index_info.scale_tie_to_px);
}

// Compute the tile atlas base for our level, in index space so we can preserve uv.
fn terrain_atlas_tile_px_from_index_px(level: u32, index_px_f: vec2<f32>) -> vec2<f32> {
    let tile_px_in_index = f32(1u << level);
    let index_tile_internal_px_f = index_px_f % vec2<f32>(tile_px_in_index);
    let atlas_tile_uv = index_tile_internal_px_f / vec2<f32>(tile_px_in_index);
    let atlas_tile_px_f = atlas_tile_uv * TILE_SIZE;
    return atlas_tile_px_f;
}

// Return the index slot for the given graticule and index
struct SlotInfo {
    // Float coordinate of the sample in index space.
    index_px_f: vec2<f32>,

    // Float coordinate of the sample in tile space.
    tile_px_f: vec2<f32>,

    // Transform of the index lookup into atlas pixel lookup.
    // Per the atlas tile of the lookup performed via index_px_f.
    atlas_base_px: vec2<i32>,

    // The level looked up at index_px_f.
    level: u32,
}

fn terrain_sample_index(
    grat_ll_rad: vec2<f32>,
    index_texture: texture_2d<u32>,
    index_info: IndexMetadata,
) -> SlotInfo {
    // FIXME: WAT?
    // Our wgsl projection disagrees with our Rust projection by one pixel at max resolution.
    // There is an off-by-one somewhere. Account for that discrepancy in the renderer for now
    // because all of our physics is based around the other interpretation. The constant here
    // is the max-res pixel size (e.g. of the top tie in the geotiff), in radians.
    let grat_ll = vec2<f32>(grat_ll_rad.x + 0.00000969627362219072, grat_ll_rad.y);

    // These are stored in f64 and truncated to f32 immediately before uploading.
    // In theory, 24 bits of precision is only 16M increments, whereas the earth's surface
    // is 40M meters circumference. So we've only got like 4 meters precision in f32.
    // This is enough to select a texel, even at 10m, but not enough for subsampling,
    // e.g. for stable alpha/beta computation.
    let tie_extent_xy = terrain_index_tie_extent_xy(index_info);

    // Map from the vertex position in graticule radians into the logical position within
    // the index based on the tie points from the COG.
    let tie_to_grat_xy = terrain_index_tie_to_grat_xy(grat_ll, index_info);
    var logical_uv = tie_to_grat_xy / tie_extent_xy;

    // Wrap around longitude lookups; clamp latitude lookups
    logical_uv.x %= 1.;
    logical_uv.y = saturate(logical_uv.y);

    // The index is 1-px per tile, but the actual sizes don't line up with tiles,
    // so we pass in the exact floating extent to map into, combined with expansion
    // into pixel space (such that we can truncate to pixels to get the actual position).
    let scale_tie_to_px = terrain_index_scale_tie_to_px_xy(index_info);

    // It is important that we compute our atlas uv in index space. We scaled
    // by the index size above to get a uv within the pixel. If we were to
    // compute the uv in world/atlas space, we'd be using a different scaling,
    // which might land in a different pixel within the error bars. However,
    // we've already selected the atlas slot, so this kind of error wraps and
    // tells us to select atlas pixels from the wrong side of the tile.
    let index_px_f = logical_uv * scale_tie_to_px;

    // Get the index pixel.
    let index_px_i = vec2<i32>(floor(index_px_f));
    let slot_info = textureLoad(index_texture, index_px_i, 0);

    // Compute the offset to the atlas base pixel.
    let atlas_base_px = vec2<i32>(slot_info.xy) * i32(TILE_SIZE);
    let level = slot_info.z;

    // Compute the tile pixel in fractional units.
    // Note: calling fract on this is potentially exciting, given we're near precision limits.
    let tile_px_f = terrain_atlas_tile_px_from_index_px(level, index_px_f);

    return SlotInfo(index_px_f, tile_px_f, atlas_base_px, level);
}
