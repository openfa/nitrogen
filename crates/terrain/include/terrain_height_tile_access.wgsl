// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.

//!include <shader_shared/include/consts.wgsl>
//!include <terrain/include/terrain_tile_access.wgsl>

fn _terrain_sample_height_oob(
    grat_ll: vec2<f32>,
    height_index_texture: texture_2d<u32>,
    height_atlas_texture: texture_2d<i32>,
    height_index_info: IndexMetadata
) -> i32 {
    let info = terrain_sample_index(grat_ll, height_index_texture, height_index_info);
    // Note the lack of a 0.5 offset: this has already been applied by the parent algorithm.
    let tile_px = vec2<i32>(floor(info.tile_px_f));
    return textureLoad(height_atlas_texture, info.atlas_base_px + tile_px, 0).r;
}

struct HeightGatherResult {
    samples: vec4<i32>,
    f: vec2<f32>,
    level: u32
}

fn _terrain_gather_heights_2x2_(
    grat_ll: vec2<f32>,
    height_index_texture: texture_2d<u32>,
    height_atlas_texture: texture_2d<i32>,
    height_index_info: IndexMetadata,
) -> HeightGatherResult {
    let info = terrain_sample_index(
        grat_ll,
        height_index_texture,
        height_index_info,
    );
    var level = info.level;

    // Map from index to atlas
    let subpx = 0.0;
    let tile_px_f = info.tile_px_f;
    let f = fract(tile_px_f - vec2(subpx)); // sub-pixel offsets
    let tile_px = vec2<i32>(floor(tile_px_f - vec2(subpx))); // top left pixel
    var i0 = tile_px.x; // pixel grid
    var i1 = tile_px.x + 1;
    var j0 = tile_px.y;
    var j1 = tile_px.y + 1;

    // If all samples are within the tile, use cheap sampling.
    // Otherwise, compute the lat-lon of the samples and sample
    // by doing the full index->atlas traversal for each pixel.
    let is_edge = i0 < 0 || j0 < 0|| i1 >= i32(TILE_SIZE) || j1 >= i32(TILE_SIZE);
    if (!is_edge) {
        let p00 = textureLoad(height_atlas_texture, info.atlas_base_px + vec2(i0, j0), 0).r;
        let p01 = textureLoad(height_atlas_texture, info.atlas_base_px + vec2(i0, j1), 0).r;
        let p10 = textureLoad(height_atlas_texture, info.atlas_base_px + vec2(i1, j0), 0).r;
        let p11 = textureLoad(height_atlas_texture, info.atlas_base_px + vec2(i1, j1), 0).r;
        return HeightGatherResult(vec4(p00, p01, p10, p11), f, level);
    } else {
        // Compute grat equivalent for i/j for lookup in other tiles, if needed.
        // Note `level` is all TIFF level; max resolution at level 0
        let pixel_rad = radians(height_index_info.level_0_px_degrees * f32(1u << level));
        let dx = pixel_rad;
        let dy = -pixel_rad;
        let base_x = -pixel_rad * f.x;
        let base_y = pixel_rad * f.y;
        let ori = vec2(grat_ll.x + base_y, grat_ll.y + base_x);
        let grat00 = vec2(ori.x,      ori.y);
        let grat01 = vec2(ori.x,      ori.y + dx);
        let grat10 = vec2(ori.x + dy, ori.y);
        let grat11 = vec2(ori.x + dy, ori.y + dx);

        let p00 = _terrain_sample_height_oob(grat00, height_index_texture, height_atlas_texture, height_index_info);
        let p01 = _terrain_sample_height_oob(grat10, height_index_texture, height_atlas_texture, height_index_info);
        let p10 = _terrain_sample_height_oob(grat01, height_index_texture, height_atlas_texture, height_index_info);
        let p11 = _terrain_sample_height_oob(grat11, height_index_texture, height_atlas_texture, height_index_info);
        return HeightGatherResult(vec4(p00, p01, p10, p11), f, level);
    }
}

fn terrain_sample_height_linear(
    grat_ll: vec2<f32>,
    height_index_texture: texture_2d<u32>,
    height_atlas_texture: texture_2d<i32>,
    height_index_info: IndexMetadata
) -> f32 {
    let rv = _terrain_gather_heights_2x2_(
        grat_ll,
        height_index_texture,
        height_atlas_texture,
        height_index_info,
    );
    let f = rv.f;
    let fp = vec2(1.) - f;
    let level = rv.level;
    let p = rv.samples;
    // Note: vertical units are already meters
    let p00 = f32(p.x);
    let p01 = f32(p.y);
    let p10 = f32(p.z);
    let p11 = f32(p.w);

    let t00 = fp.x * fp.y * p00;
    let t10 =  f.x * fp.y * p10;
    let t01 = fp.x *  f.y * p01;
    let t11 =  f.x *  f.y * p11;
    return t00 + t10 + t01 + t11;
}

// This is not going to be as good as averaging face normals,
// but is it good enough for now.
fn _compute_slope_linear(
    center: f32,
    left: f32,
    right: f32,
    top: f32,
    bottom: f32,
    pixel_m: vec2<f32>
) -> vec2<f32> {
    return vec2(
        (right - left) / pixel_m.x,
        (bottom - top) / pixel_m.y
    );
}

fn terrain_sample_slope2(
    grat_ll: vec2<f32>,
    height_index_texture: texture_2d<u32>,
    height_atlas_texture: texture_2d<i32>,
    height_index_info: IndexMetadata
) -> vec2<f32> {
    let slot = terrain_sample_index(
        grat_ll,
        height_index_texture,
        height_index_info,
    );

    // Map from index to atlas and compute alpha/beta at the same time.
    let tile_px_f = slot.tile_px_f;
    let f = fract(tile_px_f - vec2(0.5));
    let fp = vec2(1.) - f;
    let tile_px = vec2<i32>(floor(tile_px_f - vec2(0.5))); // top left pixel

    // We need to sample 5 heights to compute a single normal and we have to lerp between
    // 4 normals, so we'd naively have to sample 20 heights. We can obvious cut this down,
    // as most samples are shared. The pattern we need to sample is a tic-tac-toe grid.
    // We want to find the normals at the `x`, using heights at `x` and `*` to find the
    // interpolated normal eventually at for `^`.
    //
    //  i1j0  i2j0
    //     *  *
    //  *  x  x  * i3j1
    //  *  x ^x  * i3j2
    //     *  *
    //  i1j3  i2j3
    //
    // As for our height and color interpolations, tile_px is the top left corner above
    // the ^, in this case an x. This gives us sample coords as:
    var i0 = tile_px.x - 1; // pixel grid
    var i1 = tile_px.x;
    var i2 = tile_px.x + 1;
    var i3 = tile_px.x + 2;
    var j0 = tile_px.y - 1;
    var j1 = tile_px.y;
    var j2 = tile_px.y + 1;
    var j3 = tile_px.y + 2;

    var h10 = 0.;
    var h20 = 0.;
    var h01 = 0.;
    var h11 = 0.;
    var h21 = 0.;
    var h31 = 0.;
    var h02 = 0.;
    var h12 = 0.;
    var h22 = 0.;
    var h32 = 0.;
    var h13 = 0.;
    var h23 = 0.;

    // As for other samplers, the pixel size in radians for re-sampling to grats.
    let pixel_rad = radians(height_index_info.level_0_px_degrees * f32(1u << slot.level));
    let pixel_m = vec2(
        EARTH_RADIUS_M * pixel_rad, // * cos(grat_ll[0]),
        EARTH_RADIUS_M * pixel_rad
    );

    let is_edge = i0 < 0 || j0 < 0 || i3 >= i32(TILE_SIZE) || j3 >= i32(TILE_SIZE);
    if (!is_edge) {
        // Note: vertical units are already meters
        h10 = f32(textureLoad(height_atlas_texture, slot.atlas_base_px + vec2(i1, j0), 0).r);
        h20 = f32(textureLoad(height_atlas_texture, slot.atlas_base_px + vec2(i2, j0), 0).r);
        h01 = f32(textureLoad(height_atlas_texture, slot.atlas_base_px + vec2(i0, j1), 0).r);
        h11 = f32(textureLoad(height_atlas_texture, slot.atlas_base_px + vec2(i1, j1), 0).r);
        h21 = f32(textureLoad(height_atlas_texture, slot.atlas_base_px + vec2(i2, j1), 0).r);
        h31 = f32(textureLoad(height_atlas_texture, slot.atlas_base_px + vec2(i3, j1), 0).r);
        h02 = f32(textureLoad(height_atlas_texture, slot.atlas_base_px + vec2(i0, j2), 0).r);
        h12 = f32(textureLoad(height_atlas_texture, slot.atlas_base_px + vec2(i1, j2), 0).r);
        h22 = f32(textureLoad(height_atlas_texture, slot.atlas_base_px + vec2(i2, j2), 0).r);
        h32 = f32(textureLoad(height_atlas_texture, slot.atlas_base_px + vec2(i3, j2), 0).r);
        h13 = f32(textureLoad(height_atlas_texture, slot.atlas_base_px + vec2(i1, j3), 0).r);
        h23 = f32(textureLoad(height_atlas_texture, slot.atlas_base_px + vec2(i2, j3), 0).r);
    } else {
        // Unlike for other samplers, 1x1 is our baseline and we don't need a complete grid.
        // For grat these are lat/lon, so inverse for x/y orientation.
        let dx = pixel_rad;
        let dy = -pixel_rad;
        let base_x = -pixel_rad * f.x;
        let base_y = pixel_rad * f.y;
        let ori = vec2(grat_ll.x + base_y, grat_ll.y + base_x);

        let grat01 = vec2(ori.x - dy, ori.y);
        let grat02 = vec2(ori.x - dy, ori.y + dx);
        let grat10 = vec2(ori.x, ori.y - dx);
        let grat11 = vec2(ori.x, ori.y);
        let grat12 = vec2(ori.x, ori.y + dx);
        let grat13 = vec2(ori.x, ori.y + dx + dx);
        let grat20 = vec2(ori.x + dy, ori.y - dx);
        let grat21 = vec2(ori.x + dy, ori.y);
        let grat22 = vec2(ori.x + dy, ori.y + dx);
        let grat23 = vec2(ori.x + dy, ori.y + dx + dx);
        let grat31 = vec2(ori.x + dy + dy, ori.y);
        let grat32 = vec2(ori.x + dy + dy, ori.y + dx);

        // Sample the full grid, use x/y naming for outputs, inverted from the lat/lon inputs.
        // Note: vertical units are already meters
        h10 = f32(_terrain_sample_height_oob(grat01, height_index_texture, height_atlas_texture, height_index_info));
        h20 = f32(_terrain_sample_height_oob(grat02, height_index_texture, height_atlas_texture, height_index_info));
        h01 = f32(_terrain_sample_height_oob(grat10, height_index_texture, height_atlas_texture, height_index_info));
        h11 = f32(_terrain_sample_height_oob(grat11, height_index_texture, height_atlas_texture, height_index_info));
        h21 = f32(_terrain_sample_height_oob(grat12, height_index_texture, height_atlas_texture, height_index_info));
        h31 = f32(_terrain_sample_height_oob(grat13, height_index_texture, height_atlas_texture, height_index_info));
        h02 = f32(_terrain_sample_height_oob(grat20, height_index_texture, height_atlas_texture, height_index_info));
        h12 = f32(_terrain_sample_height_oob(grat21, height_index_texture, height_atlas_texture, height_index_info));
        h22 = f32(_terrain_sample_height_oob(grat22, height_index_texture, height_atlas_texture, height_index_info));
        h32 = f32(_terrain_sample_height_oob(grat23, height_index_texture, height_atlas_texture, height_index_info));
        h13 = f32(_terrain_sample_height_oob(grat31, height_index_texture, height_atlas_texture, height_index_info));
        h23 = f32(_terrain_sample_height_oob(grat32, height_index_texture, height_atlas_texture, height_index_info));
    }

    let n11 = _compute_slope_linear(h11, h01, h21, h10, h12, pixel_m);
    let n21 = _compute_slope_linear(h21, h11, h31, h20, h22, pixel_m);
    let n12 = _compute_slope_linear(h12, h02, h22, h11, h13, pixel_m);
    let n22 = _compute_slope_linear(h22, h12, h32, h21, h23, pixel_m);

    let t11 = fp.x * fp.y * n11;
    let t21 =  f.x * fp.y * n21;
    let t12 = fp.x *  f.y * n12;
    let t22 =  f.x *  f.y * n22;
    return t11 + t21 + t12 + t22;
}
