// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::Result;
use bevy_ecs::prelude::*;
use egui_plot::{Line, Plot, PlotPoints};
use gui::{Gui, GuiStep};
use mantle::GpuStep;
use nitrous::{inject_nitrous_resource, method, NitrousResource};
use ringbuffer::{AllocRingBuffer, RingBuffer};
use runtime::{Extension, Runtime, TimeStep};

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum DashboardStep {
    CollectFrameTime,
    Show,
}

/// Visibility for things like frame time, memory usage, system info.
#[derive(NitrousResource)]
pub struct Dashboard {
    visible: bool,
    frame_times: AllocRingBuffer<[f64; 2]>,
}

impl Extension for Dashboard {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.inject_resource(Dashboard::default())?;
        runtime.add_frame_system(
            Self::sys_collect_frame_time
                .in_set(DashboardStep::CollectFrameTime)
                .after(GpuStep::PresentTargetSurface),
        );
        runtime.add_frame_system(
            Self::sys_show_dashboard
                .in_set(DashboardStep::Show)
                .after(GuiStep::StartFrame)
                .before(GuiStep::EndFrame),
        );
        Ok(())
    }
}

impl Default for Dashboard {
    fn default() -> Self {
        let mut frame_times = AllocRingBuffer::new(Self::NSAMPLES);
        for _ in 0..Self::NSAMPLES {
            frame_times.push([0.; 2]);
        }
        Self {
            visible: false,
            frame_times,
        }
    }
}

#[inject_nitrous_resource]
impl Dashboard {
    const NSAMPLES: usize = 128;

    #[method]
    fn toggle(&mut self) {
        self.visible = !self.visible;
    }

    fn sys_collect_frame_time(mut dashboard: ResMut<Dashboard>, timestep: Res<TimeStep>) {
        dashboard.collect(&timestep);
    }

    fn collect(&mut self, timestep: &TimeStep) {
        self.frame_times.push([
            timestep.real_time_elapsed_since_boot().f64(),
            timestep.prior_frame_real_duration().f64() * 1000.,
        ]);
    }

    fn sys_show_dashboard(mut dashboard: ResMut<Dashboard>, gui: Res<Gui>) {
        dashboard.show(gui.screen().ctx());
    }

    fn show(&mut self, ctx: &egui::Context) {
        let line_points: PlotPoints = self.frame_times.iter().cloned().collect();
        egui::Window::new("🔧 Debug")
            .open(&mut self.visible)
            .show(ctx, |ui| {
                ui.label("Frame times:");
                Plot::new("frame_time")
                    .height(64.0)
                    .show_y(true)
                    .allow_zoom(false)
                    .allow_drag(false)
                    .allow_scroll(false)
                    .allow_boxed_zoom(false)
                    .include_y(0.)
                    .include_y(40.)
                    .auto_bounds(egui::Vec2b::new(true, true))
                    .show(ui, |plot_ui| plot_ui.line(Line::new(line_points)))
                    .response
            });
    }
}
