// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use anyhow::{Context, Result};
use bevy_ecs::prelude::*;
use egui::text::LayoutJob;
use event_mapper::EventMapper;
use gui::{egui_color, Gui, GuiStep};
use nitrous::{
    inject_nitrous_resource, NitrousAst, NitrousBinOp as BinOp, NitrousExpr as Expr, NitrousParser,
    NitrousResource, NitrousStatement as Stmt, NitrousTerminal as Term, Value, WorldIndex,
};
use planck::{rgb, rgba};
use ringbuffer::{AllocRingBuffer, RingBuffer};
use runtime::{
    report_errors, Extension, Runtime, RuntimeSet, ScriptCompletion, ScriptCompletions,
    ScriptHerder, ScriptResult, ScriptRunKind, StartupSet, StdPaths, ERROR_REPORTS,
};
use std::{
    fs::{File, OpenOptions},
    io::{Read, Seek, Write},
    mem,
    path::Path,
};

// Key actions that queue for later processing outside the gui context.
enum TerminalCommand {
    RunScript,
    TabComplete,
    HistoryPrevious,
    HistoryNext,
    MoveToLineStart(egui::Id),
    MoveToLineEnd(egui::Id),
}

#[derive(SystemSet, Clone, Debug, Eq, PartialEq, Hash)]
pub enum TerminalStep {
    ReportStartupCompletions,
    ReportScriptCompletions,
    Show,
    HandleTerminalCommands,
}

/// System interactivity
#[derive(NitrousResource)]
pub struct Terminal {
    backscroll: AllocRingBuffer<LayoutJob>,
    command: String,

    history: Vec<String>,
    history_file: Option<File>,
    history_cursor: usize,

    // Tab press requires us to inspect heap state, so don't constrain when it can run.
    terminal_commands: Vec<TerminalCommand>,
}

impl Extension for Terminal {
    type Opts = ();
    fn init(runtime: &mut Runtime, _: ()) -> Result<()> {
        runtime.inject_resource(Terminal::new(runtime.resource::<StdPaths>().state_dir())?)?;

        runtime.add_startup_system(
            Self::sys_report_script_completions
                .in_set(TerminalStep::ReportStartupCompletions)
                .after(StartupSet::Main),
        );
        runtime.add_frame_system(
            Self::sys_report_script_completions
                .in_set(TerminalStep::ReportScriptCompletions)
                .before(TerminalStep::Show)
                .before(RuntimeSet::ClearCompletions),
        );

        runtime.add_frame_system(
            Self::sys_show_terminal
                .in_set(TerminalStep::Show)
                .after(GuiStep::StartFrame)
                .before(GuiStep::EndFrame),
        );

        runtime.add_frame_system(
            Self::sys_handle_terminal_commands
                .pipe(report_errors)
                .in_set(TerminalStep::HandleTerminalCommands)
                .after(GuiStep::EndFrame),
        );

        Ok(())
    }
}

#[inject_nitrous_resource]
impl Terminal {
    const NUM_LINES: usize = 8_192;

    pub(crate) fn new(state_dir: Option<&Path>) -> Result<Self> {
        let mut backscroll = AllocRingBuffer::new(Self::NUM_LINES);
        for _ in 0..Self::NUM_LINES {
            backscroll.push(LayoutJob::simple(
                "".to_owned(),
                egui::FontId::new(12., egui::FontFamily::Monospace),
                egui_color(rgb!(0x77FF99)),
                f32::INFINITY,
            ));
        }

        // Load command history from state dir
        let mut history_file = if let Some(state_dir) = state_dir {
            let mut history_path = state_dir.to_owned();
            history_path.push("command_history.txt");
            OpenOptions::new()
                .read(true)
                .append(true)
                .truncate(false)
                .create(true)
                .open(&history_path)
                .ok()
        } else {
            None
        };

        let history = if let Some(fp) = history_file.as_mut() {
            fp.rewind()?;
            let mut content = String::new();
            fp.read_to_string(&mut content)
                .with_context(|| "corrupted history")?;
            content.lines().map(|s| s.to_owned()).collect()
        } else {
            vec![]
        };
        let history_cursor = history.len();

        Ok(Self {
            backscroll,
            command: "".to_owned(),
            history,
            history_file,
            history_cursor,
            terminal_commands: Vec::new(),
        })
    }

    fn sys_report_script_completions(
        mut terminal: ResMut<Terminal>,
        completions: Res<ScriptCompletions>,
    ) {
        terminal.report_script_completions(&completions);
    }

    pub fn report_script_completions(&mut self, completions: &ScriptCompletions) {
        for err in ERROR_REPORTS.lock().drain(..) {
            self.println(&err);
        }
        for completion in completions.iter() {
            if completion.meta.kind() == ScriptRunKind::Interactive {
                match &completion.result {
                    ScriptResult::Ok(v) => match v {
                        Value::String(s) => {
                            for line in s.lines() {
                                self.println(line);
                            }
                        }
                        Value::ResourceMethod(_, _) | Value::ComponentMethod(_, _, _) => {
                            self.println(&format!("{v} is a method, did you mean to call it?",));
                            self.println(
                                "Try using up-arrow to go back and add parentheses to the end.",
                            );
                        }
                        _ => {
                            self.println(&v.to_string());
                        }
                    },
                    ScriptResult::Err(error) => {
                        self.show_script_error(completion, error);
                    }
                };
            } else if completion.result.is_error() {
                self.show_script_error(completion, completion.result.error().unwrap());
            }
        }
    }

    pub fn println(&mut self, line: &str) {
        println!("{line}");
        let mut job = LayoutJob::default();
        let fmt = egui::TextFormat {
            color: egui_color(rgb!(0x77FF99)),
            font_id: egui::FontId::new(12., egui::FontFamily::Monospace),
            ..Default::default()
        };
        job.append(line, 0., fmt);
        self.backscroll.push(job);
    }

    fn show_script_error(&mut self, completion: &ScriptCompletion, error: &str) {
        self.show_run_error(&completion.meta.context().script().to_string(), error);
    }

    fn show_run_error(&mut self, command: &str, error: &str) {
        let grey = egui::TextFormat {
            color: egui_color(rgb!(0xCCCCCC)),
            ..Default::default()
        };
        let yellow = egui::TextFormat {
            color: egui_color(rgb!(0xFFFF00)),
            ..Default::default()
        };
        let red = egui::TextFormat {
            color: egui_color(rgb!(0xFF0000)),
            ..Default::default()
        };

        let mut job = LayoutJob::default();
        job.append("Script Failed: ", 0., yellow);
        job.append(command, 0., grey.clone());
        self.backscroll.push(job);

        let mut errors = error.lines();
        let error = errors.next().unwrap();
        let mut job = LayoutJob::default();
        job.append("    Error: ", 0., red.clone());
        job.append(error, 0., grey);
        self.backscroll.push(job);

        for error in errors {
            let mut job = LayoutJob::default();
            job.append("       ", 0., red.clone());
            job.append(&format!("         {error}"), 0., red.clone());
            self.backscroll.push(job);
        }
    }

    fn sys_show_terminal(mut terminal: ResMut<Terminal>, gui: Res<Gui>, mapper: Res<EventMapper>) {
        if mapper.terminal_active() {
            terminal.show(&gui);
        }
    }

    fn show(&mut self, gui: &Gui) {
        egui::TopBottomPanel::top("terminal_panel")
            .frame(egui::Frame::default().fill(egui_color(rgba!(0x202020A0))))
            .resizable(false)
            .show(gui.screen().ctx(), |ui| {
                let text_style = egui::TextStyle::Body;
                let row_height = ui.text_style_height(&text_style);
                let num_rows = Self::NUM_LINES;
                egui::ScrollArea::vertical()
                    .min_scrolled_height(gui.screen().height() / 3.)
                    .max_height(gui.screen().height() / 3.)
                    .auto_shrink([false; 2])
                    .stick_to_bottom(true)
                    .show_rows(ui, row_height, num_rows, |ui, row_range| {
                        for row in row_range {
                            let job = self.backscroll.get(row).unwrap();
                            ui.label(job.to_owned());
                        }
                    });
                ui.horizontal(|ui| {
                    let prompt = ui
                        .label("> ")
                        .on_hover_text("Type help() in the box at right and hit enter, if lost.");
                    let output = egui::TextEdit::singleline(&mut self.command)
                        .frame(false)
                        .code_editor()
                        .desired_width(
                            gui.screen().width()
                                - prompt.rect.width()
                                - ui.spacing().item_spacing.x * 3.,
                        )
                        .show(ui);
                    self.handle_terminal_keys(ui, &output);
                    output.response.request_focus();
                });
            });
    }

    fn handle_terminal_keys(&mut self, ui: &egui::Ui, output: &egui::text_edit::TextEditOutput) {
        assert!(self.terminal_commands.is_empty());

        // Poking at the internals of egui is dangerous because it all accesses locks
        // transparently. This order is very carefully selected to get all the info
        // we need, taking the minimal number of locks without deadlocking. We then
        // queue up events so that we can fully release locks (note: outside the context
        // of building the ui entirely) before acting on the user's requests.
        let lost_focus = output.response.lost_focus();
        let text_edit_id = output.response.id;
        ui.input(|input| {
            if lost_focus {
                if input.key_pressed(egui::Key::Enter) {
                    self.terminal_commands.push(TerminalCommand::RunScript);
                } else if input.key_pressed(egui::Key::Tab) {
                    self.terminal_commands.push(TerminalCommand::TabComplete);
                    self.terminal_commands
                        .push(TerminalCommand::MoveToLineEnd(text_edit_id));
                }
            } else if input.key_pressed(egui::Key::ArrowUp)
                || (input.key_pressed(egui::Key::P) && input.modifiers.ctrl)
            {
                self.terminal_commands
                    .push(TerminalCommand::HistoryPrevious);
                self.terminal_commands
                    .push(TerminalCommand::MoveToLineEnd(text_edit_id));
            } else if input.key_pressed(egui::Key::ArrowDown)
                || (input.key_pressed(egui::Key::N) && input.modifiers.ctrl)
            {
                self.terminal_commands.push(TerminalCommand::HistoryNext);
                self.terminal_commands
                    .push(TerminalCommand::MoveToLineEnd(text_edit_id));
            } else if input.key_pressed(egui::Key::Home)
                || (input.key_pressed(egui::Key::A) && input.modifiers.ctrl)
            {
                self.terminal_commands
                    .push(TerminalCommand::MoveToLineStart(text_edit_id));
            } else if input.key_pressed(egui::Key::End)
                || (input.key_pressed(egui::Key::E) && input.modifiers.ctrl)
            {
                //self.on_end_pressed(ui.ctx(), &output);
                self.terminal_commands
                    .push(TerminalCommand::MoveToLineEnd(text_edit_id));
            }
        });
    }

    fn on_up_pressed(&mut self) {
        if self.history_cursor > 0 {
            self.history_cursor -= 1;
            self.command.clone_from(&self.history[self.history_cursor]);
        }
    }

    fn on_down_pressed(&mut self) {
        if self.history_cursor < self.history.len() {
            self.history_cursor += 1;
            if self.history_cursor < self.history.len() {
                self.command.clone_from(&self.history[self.history_cursor]);
            } else {
                self.command = String::new();
            }
        }
    }

    fn on_end_pressed(&mut self, ctx: &egui::Context, text_edit_id: egui::Id) {
        if let Some(mut state) = egui::TextEdit::load_state(ctx, text_edit_id) {
            let ccursor = egui::text::CCursor::new(self.command.chars().count());
            state
                .cursor
                .set_char_range(Some(egui::text::CCursorRange::one(ccursor)));
            state.store(ctx, text_edit_id);
            ctx.memory_mut(|mem| mem.request_focus(text_edit_id)); // give focus back to the `TextEdit`.
        }
    }

    fn on_home_pressed(&mut self, ctx: &egui::Context, text_edit_id: egui::Id) {
        if let Some(mut state) = egui::TextEdit::load_state(ctx, text_edit_id) {
            let ccursor = egui::text::CCursor::new(0);
            state
                .cursor
                .set_char_range(Some(egui::text::CCursorRange::one(ccursor)));
            state.store(ctx, text_edit_id);
            ctx.memory_mut(|mem| mem.request_focus(text_edit_id)); // give focus back to the `TextEdit`.
        }
    }

    fn on_enter_pressed(&mut self, herder: &mut ScriptHerder) -> Result<()> {
        let mut command = String::new();
        mem::swap(&mut command, &mut self.command);

        self.add_command_to_history(&command)?;

        match herder.run_interactive(&command) {
            Ok(_) => {}
            Err(e) => {
                // Help with some common cases before we show a scary error.
                if Self::is_help_command(&command) {
                    herder.run_interactive("help()")?;
                } else if Self::is_quit_command(&command) {
                    herder.run_interactive("quit()")?;
                } else {
                    self.show_run_error(&command, &e.to_string());
                }
            }
        }

        Ok(())
    }

    fn is_help_command(command: &str) -> bool {
        let cmd = command.trim().to_lowercase();
        cmd.starts_with("help") || cmd.starts_with('?') || cmd.ends_with('?')
    }

    fn is_quit_command(command: &str) -> bool {
        let cmd = command.trim().to_lowercase();
        cmd == "quit" || cmd == "exit" || cmd == "q"
    }

    fn add_command_to_history(&mut self, command: &str) -> Result<()> {
        // Echo the command into the output buffer as a literal.
        self.println(&("> ".to_owned() + command));

        // And save it in our local history so we don't have to re-type it
        self.history.push(command.to_owned());

        // And stream it to our history file
        if let Some(fp) = self.history_file.as_mut() {
            fp.write(format!("{command}\n").as_bytes())
                .with_context(|| "recording history")?;
            fp.sync_data()?;
        }

        // Reset the history cursor
        self.history_cursor = self.history.len();

        Ok(())
    }

    fn try_complete_resource(
        &mut self,
        partial: &NitrousAst,
        index: &WorldIndex,
    ) -> Option<String> {
        if let Stmt::ExprOut(Expr::Term(Term::ResourceRef(sym))) =
            partial.statements().next().cloned().unwrap_or(Stmt::Empty)
        {
            let matching_resources = index
                .resource_names()
                .filter(|&s| s.starts_with(sym.as_str()))
                .collect::<Vec<&str>>();
            match matching_resources.len() {
                0 => self.println("no such resources"),
                1 => return Some(matching_resources[0].to_owned()),
                _ => {
                    for m in &matching_resources {
                        self.println(&format!("  @{m}"));
                    }
                }
            }
        }
        None
    }

    fn try_complete_resource_attrs(
        &mut self,
        partial: &NitrousAst,
        index: &WorldIndex,
    ) -> Option<String> {
        if let Stmt::ExprOut(Expr::BinaryOp(lhs_name_term, BinOp::Attr, rhs_name_term)) =
            partial.statements().next().cloned().unwrap_or(Stmt::Empty)
        {
            if let Expr::Term(Term::ResourceRef(sym)) = rhs_name_term.as_ref() {
                if let Expr::Term(Term::ResourceRef(res_name)) = lhs_name_term.as_ref() {
                    if let Some(attrs) = index.resource_attr_names_by_name(res_name) {
                        let matching_attrs = attrs
                            .filter(|&s| s.starts_with(sym.as_str()))
                            .map(|s| s.to_owned())
                            .collect::<Vec<String>>();
                        match matching_attrs.len() {
                            0 => self.println("no such attrs"),
                            1 => return Some(format!("{}.{}", res_name, matching_attrs[0])),
                            _ => {
                                for m in &matching_attrs {
                                    self.println(&format!("  @{m}"));
                                }
                            }
                        }
                    }
                }
            }
        }
        None
    }

    fn try_complete_entity(&mut self, partial: &NitrousAst, index: &WorldIndex) -> Option<String> {
        if let Stmt::ExprOut(Expr::Term(Term::EntityRef(sym))) =
            partial.statements().next().cloned().unwrap_or(Stmt::Empty)
        {
            let matching_entities = index
                .entity_names()
                .filter(|&s| s.starts_with(sym.as_str()))
                .collect::<Vec<&str>>();
            match matching_entities.len() {
                0 => self.println("no such entity"),
                1 => return Some("@".to_owned() + matching_entities[0]),
                _ => {
                    for m in &matching_entities {
                        self.println(&format!("  @{m}"));
                    }
                }
            }
        }
        None
    }

    fn try_complete_entity_component(
        &mut self,
        partial: &NitrousAst,
        index: &WorldIndex,
    ) -> Option<String> {
        if let Stmt::ExprOut(Expr::BinaryOp(lhs_name_term, BinOp::Attr, rhs_name_term)) =
            partial.statements().next().cloned().unwrap_or(Stmt::Empty)
        {
            if let Expr::Term(Term::ResourceRef(sym)) = rhs_name_term.as_ref() {
                if let Expr::Term(Term::EntityRef(ent_name)) = lhs_name_term.as_ref() {
                    if let Some(attrs) = index.entity_component_names_by_name(ent_name) {
                        let matching_components = attrs
                            .filter(|&s| s.starts_with(sym.as_str()))
                            .collect::<Vec<_>>();
                        match matching_components.len() {
                            0 => self.println("no such component"),
                            1 => return Some(format!("@{}.{}", ent_name, matching_components[0])),
                            _ => {
                                for m in &matching_components {
                                    self.println(&format!("  {m}"));
                                }
                            }
                        }
                    }
                }
            }
        }
        None
    }

    fn try_complete_entity_component_attrs(
        &mut self,
        partial: &NitrousAst,
        index: &WorldIndex,
    ) -> Option<String> {
        if let Stmt::ExprOut(Expr::BinaryOp(attr_term, BinOp::Attr, rhs_attr_term)) =
            partial.statements().next().cloned().unwrap_or(Stmt::Empty)
        {
            if let Expr::Term(Term::ResourceRef(attr_sym)) = rhs_attr_term.as_ref() {
                if let Expr::BinaryOp(ent_term, BinOp::Attr, rhs_comp_term) = attr_term.as_ref() {
                    if let Expr::Term(Term::ResourceRef(comp_sym)) = rhs_comp_term.as_ref() {
                        if let Expr::Term(Term::EntityRef(ent_sym)) = ent_term.as_ref() {
                            if let Some(attrs) =
                                index.entity_component_attr_names_by_name(ent_sym, comp_sym)
                            {
                                let mut matching_attrs = attrs
                                    .filter(|&s| s.starts_with(attr_sym.as_str()))
                                    .collect::<Vec<_>>();
                                matching_attrs.sort();
                                match matching_attrs.len() {
                                    0 => self.println("no such attr"),
                                    1 => {
                                        return Some(format!(
                                            "@{}.{}.{}",
                                            ent_sym, comp_sym, matching_attrs[0]
                                        ));
                                    }
                                    _ => {
                                        for m in &matching_attrs {
                                            self.println(&format!("  {m}"));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        None
    }

    fn try_completion(&mut self, partial: NitrousAst, index: &WorldIndex) -> Option<String> {
        if partial.statements().count() != 1 {
            return None;
        }
        if let Some(s) = self.try_complete_resource(&partial, index) {
            return Some(s);
        }
        if let Some(s) = self.try_complete_resource_attrs(&partial, index) {
            return Some(s);
        }
        if let Some(s) = self.try_complete_entity_component_attrs(&partial, index) {
            return Some(s);
        }
        if let Some(s) = self.try_complete_entity(&partial, index) {
            return Some(s);
        }
        if let Some(s) = self.try_complete_entity_component(&partial, index) {
            return Some(s);
        }
        None
    }

    fn on_tab_pressed(&mut self, index: &WorldIndex) {
        let incomplete = self.command.clone();
        if let Ok(mut parser) = NitrousParser::new(&incomplete) {
            if let Ok(partial) = parser.parse() {
                if let Some(full) = self.try_completion(partial, index) {
                    self.command = full;
                }
            }
        }
    }

    fn sys_handle_terminal_commands(
        mut terminal: ResMut<Terminal>,
        mut herder: ResMut<ScriptHerder>,
        gui: Res<Gui>,
        index: Res<WorldIndex>,
    ) -> Result<()> {
        terminal.handle_terminal_commands(&mut herder, &gui, &index)?;
        Ok(())
    }

    fn handle_terminal_commands(
        &mut self,
        herder: &mut ScriptHerder,
        gui: &Gui,
        index: &WorldIndex,
    ) -> Result<()> {
        let mut empty = Vec::new();
        mem::swap(&mut empty, &mut self.terminal_commands);
        for command in empty.drain(..) {
            match command {
                TerminalCommand::RunScript => self.on_enter_pressed(herder)?,
                TerminalCommand::TabComplete => self.on_tab_pressed(index),
                TerminalCommand::HistoryNext => self.on_down_pressed(),
                TerminalCommand::HistoryPrevious => self.on_up_pressed(),
                TerminalCommand::MoveToLineEnd(id) => self.on_end_pressed(gui.screen().ctx(), id),
                TerminalCommand::MoveToLineStart(id) => {
                    self.on_home_pressed(gui.screen().ctx(), id)
                }
            }
        }
        Ok(())
    }
}
