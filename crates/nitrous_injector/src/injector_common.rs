// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use proc_macro2::{Delimiter, TokenStream as TokenStream2, TokenTree};
use quote::{quote, ToTokens};
use std::borrow::Borrow;
use syn::{
    parse2,
    visit::{self, Visit},
    Arm, Data, DeriveInput, Expr, FnArg, GenericArgument, Ident, ImplItemMethod, ItemFn, ItemImpl,
    Pat, PathArguments, ReturnType, Type, TypePath,
};

pub(crate) fn make_augment_method(item: ItemFn) -> TokenStream2 {
    quote! {
        #[allow(clippy::unnecessary_wraps)]
        #item
    }
}

pub(crate) type Ast = ItemImpl;

pub(crate) struct InjectModel {
    pub(crate) item: ItemImpl,
    pub(crate) methods: Vec<MethodMetadata>,
}

impl InjectModel {
    pub fn new(ast: Ast, visitor: CollectorVisitor) -> Self {
        Self {
            item: ast,
            methods: visitor.methods,
        }
    }
}

pub(crate) struct Ir {
    pub(crate) item: ItemImpl,
    pub(crate) methods: Vec<MethodMetadata>,
    pub(crate) method_arms: Vec<Arm>,
    pub(crate) get_arms: Vec<Arm>,
    pub(crate) list_result_dict_builders: Vec<TokenStream2>,
    pub(crate) metadata_result_dict_builders: Vec<TokenStream2>,
    pub(crate) metadata_rust_items: Vec<TokenStream2>,
}

impl Ir {
    pub(crate) fn new(item: ItemImpl, methods: Vec<MethodMetadata>) -> Self {
        Self {
            item,
            methods,
            method_arms: Vec::new(),
            get_arms: Vec::new(),
            list_result_dict_builders: Vec::new(),
            metadata_result_dict_builders: Vec::new(),
            metadata_rust_items: Vec::new(),
        }
    }
}

impl Ir {
    pub(crate) fn lower_methods<F>(self: &mut Ir, make_get_arm: F)
    where
        F: Fn(&str, &str) -> Arm,
    {
        let type_name = self.item.self_ty.clone().into_token_stream().to_string();
        for meta in &self.methods {
            let rust_name = format!("{}", meta.rust_name);
            self.get_arms.push(make_get_arm(&type_name, &rust_name));
            let arg_exprs = meta
                .args
                .iter()
                .enumerate()
                .map(|(i, arg)| lower_arg(&meta.nitrous_name, i, arg))
                .collect::<Vec<_>>();
            self.method_arms.push(lower_method_call(
                &rust_name,
                &meta.rust_name,
                &arg_exprs,
                &meta.ret,
            ));
            self.list_result_dict_builders.push(meta.lower_for_list());
            self.metadata_result_dict_builders
                .push(meta.lower_to_dict());
            self.metadata_rust_items.push(meta.lower_rust_item());
        }
    }

    pub(crate) fn lower_list<F>(&mut self, make_get_arm: F)
    where
        F: Fn(&str, &str) -> Arm,
    {
        let type_name = self.item.self_ty.clone().into_token_stream().to_string();

        self.method_arms
            .push(parse2(quote! { "list" => { self.__impl_list__() } }).unwrap());
        self.get_arms.push(make_get_arm(&type_name, "list"));

        self.method_arms
            .push(parse2(quote! { "metadata" => { self.__impl_metadata__() } }).unwrap());
        self.get_arms.push(make_get_arm(&type_name, "metadata"));
    }
}

fn lower_arg(method: &Ident, i: usize, arg: &ArgDef) -> Expr {
    match arg.ty {
        Scalar::Boolean => parse2(quote! {
            if let Some(arg) = args.get(#i) {
                arg.to_bool()?
            } else {
                ::nitrous::anyhow::bail!("not enough args")
            }
        })
        .unwrap(),
        Scalar::Integer => parse2(quote! {
            if let Some(arg) = args.get(#i) {
                arg.to_int()?
            } else {
                ::nitrous::anyhow::bail!("not enough args")
            }
        })
        .unwrap(),
        Scalar::Float => parse2(quote! {
            if let Some(arg) = args.get(#i) {
                arg.to_float()?
            } else {
                ::nitrous::anyhow::bail!("not enough args")
            }
        })
        .unwrap(),
        Scalar::StrRef => parse2(quote! {
            if let Some(arg) = args.get(#i) {
                arg.to_str()?
            } else {
                ::nitrous::anyhow::bail!("not enough args")
            }
        })
        .unwrap(),
        Scalar::String => parse2(quote! {
            if let Some(arg) = args.get(#i) {
                arg.to_str()?.to_owned()
            } else {
                ::nitrous::anyhow::bail!("not enough args")
            }
        })
        .unwrap(),
        Scalar::Value => parse2(quote! {
            if let Some(arg) = args.get(#i) {
                arg.clone()
            } else {
                ::nitrous::anyhow::bail!("not enough args")
            }
        })
        .unwrap(),
        Scalar::Other => parse2(quote! {
            if let Some(arg) = args.get(#i) {
                if let Ok(d) = arg.to_dict() {
                    d.try_into()?
                } else {
                    ::nitrous::anyhow::bail!("argument to {} @{} with value '{}' must be a dict", stringify!(#method), #i, arg)
                }
            } else {
                ::nitrous::anyhow::bail!("not enough args")
            }
        })
        .unwrap(),
        Scalar::Unit => parse2(quote! { ::nitrous::Value::True() }).unwrap(),
        Scalar::HeapMut => parse2(quote! { heap }).unwrap(),
        Scalar::HeapRef => parse2(quote! { heap.as_ref() }).unwrap(),
        Scalar::Selfish => parse2(quote! { self }).unwrap(),
    }
}

fn lower_method_call(name: &str, item: &Ident, arg_exprs: &[Expr], ret: &RetType) -> Arm {
    parse2(match ret {
        RetType::Nothing => {
            quote! { #name => { self.#item( #(#arg_exprs),* ); ::nitrous::CallResult::Val(::nitrous::Value::True()) } }
        }
        RetType::Raw(llty) => match llty {
            Scalar::Boolean => {
                quote! { #name => { ::nitrous::CallResult::Val(::nitrous::Value::Boolean(self.#item( #(#arg_exprs),* ))) } }
            }
            Scalar::Integer => {
                quote! { #name => { ::nitrous::CallResult::Val(::nitrous::Value::Integer(self.#item( #(#arg_exprs),* ))) } }
            }
            Scalar::Float => {
                quote! { #name => { ::nitrous::CallResult::Val(::nitrous::Value::Float(::nitrous::ordered_float::OrderedFloat(self.#item( #(#arg_exprs),* )))) } }
            }
            Scalar::String => {
                quote! { #name => { ::nitrous::CallResult::Val(::nitrous::Value::String(self.#item( #(#arg_exprs),* ))) } }
            }
            Scalar::StrRef => {
                quote! { #name => { ::nitrous::CallResult::Val(::nitrous::Value::String(self.#item( #(#arg_exprs),* ).to_owned())) } }
            }
            Scalar::Value => {
                quote! { #name => { ::nitrous::CallResult::Val(self.#item( #(#arg_exprs),* )) } }
            }
            Scalar::Unit => {
                quote! { #name => { self.#item( #(#arg_exprs),* ); ::nitrous::CallResult::Val(::nitrous::Value::True()) } }
            }
            Scalar::HeapMut | Scalar::HeapRef => {
                panic!("invalid return of heap type from method")
            }
            Scalar::Selfish => {
                quote! { #name => { self.#item( #(#arg_exprs),* ); ::nitrous::CallResult::Selfish } }
            }
            Scalar::Other => {
                quote! { #name => { ::nitrous::CallResult::Val(::nitrous::Value::Dict(::nitrous::Dict::from(self.#item( #(#arg_exprs),* )))) } }
            }
        },
        RetType::ResultRaw(llty) => match llty {
            Scalar::Boolean => {
                quote! { #name => { ::nitrous::CallResult::Val(::nitrous::Value::Boolean(self.#item( #(#arg_exprs),* )?)) } }
            }
            Scalar::Integer => {
                quote! { #name => { ::nitrous::CallResult::Val(::nitrous::Value::Integer(self.#item( #(#arg_exprs),* )?)) } }
            }
            Scalar::Float => {
                quote! { #name => { ::nitrous::CallResult::Val(::nitrous::Value::Float(::nitrous::ordered_float::OrderedFloat(self.#item( #(#arg_exprs),* )?))) } }
            }
            Scalar::String => {
                quote! { #name => { ::nitrous::CallResult::Val(::nitrous::Value::String(self.#item( #(#arg_exprs),* )?)) } }
            }
            Scalar::StrRef => {
                quote! { #name => { ::nitrous::CallResult::Val(::nitrous::Value::String(self.#item( #(#arg_exprs),* )?.to_owned())) } }
            }
            Scalar::Value => {
                quote! { #name => { ::nitrous::CallResult::Val(self.#item( #(#arg_exprs),* )?) } }
            }
            Scalar::Unit => {
                quote! { #name => { self.#item( #(#arg_exprs),* )?; ::nitrous::CallResult::Val(::nitrous::Value::True()) } }
            }
            Scalar::HeapMut | Scalar::HeapRef => {
                panic!("invalid return of heap type from method")
            }
            Scalar::Selfish => {
                quote! { #name => { self.#item( #(#arg_exprs),* )?; ::nitrous::CallResult::Selfish } }
            }
            Scalar::Other => {
                quote! { #name => { ::nitrous::CallResult::Val(::nitrous::Value::Dict(::nitrous::Dict::from(self.#item( #(#arg_exprs),* )?))) } }
            }
        },
    })
    .unwrap()
}

pub(crate) fn find_properties_in_struct(ast: &DeriveInput) -> Vec<(Ident, Scalar)> {
    let mut properties = Vec::new();
    if let Data::Struct(data) = &ast.data {
        for field in &data.fields {
            for attr in &field.attrs {
                if attr.path.is_ident("property") {
                    let ident = field.ident.as_ref().unwrap().to_owned();
                    let scalar = Scalar::from_type(&field.ty);
                    properties.push((ident, scalar));
                }
            }
        }
    }
    properties
}

pub(crate) fn parse_derive_metadata(base: &str, ast: &DeriveInput) -> (String, TokenStream2) {
    let mut name = String::new();
    let mut storage = quote!(::bevy_ecs::component::StorageType::Table);
    for attr in &ast.attrs {
        if attr.path.is_ident(base) {
            for tok in attr.tokens.to_token_stream() {
                if let TokenTree::Group(group) = tok {
                    assert_eq!(group.delimiter(), Delimiter::Parenthesis);
                    let stream = group.stream().into_iter().collect::<Vec<_>>();
                    assert_eq!(stream.len(), 3);
                    assert!(matches!(stream[0], TokenTree::Ident(_)));
                    match &stream[0] {
                        TokenTree::Ident(ident) => match ident.to_string().as_ref() {
                            "name" => {
                                name = stream[2]
                                    .to_string()
                                    .strip_prefix('"')
                                    .expect("leading quote")
                                    .strip_suffix('"')
                                    .expect("trailing quote")
                                    .to_string();
                            }
                            "storage" => {
                                storage = match stream[2].to_string().as_ref() {
                                    "Table" => quote!(::bevy_ecs::component::StorageType::Table),
                                    "SparseSet" => {
                                        quote!(::bevy_ecs::component::StorageType::SparseSet)
                                    }
                                    n => panic!(
                                        "unknown storage type {n}, expected Table or SparseSet"
                                    ),
                                };
                            }
                            n => panic!("unknown meta arg name {n}"),
                        },
                        _ => panic!("expected identifier in meta args at {:?}", stream[0].span()),
                    };
                }
            }
        }
    }
    (name, storage)
}

pub(crate) struct MethodMetadata {
    nitrous_name: Ident,
    rust_name: Ident,
    help: String,
    args: Vec<ArgDef>,
    ret: RetType,
}

impl MethodMetadata {
    pub fn from_method_node(node: &ImplItemMethod) -> Self {
        let mut meta = Self {
            nitrous_name: node.sig.ident.clone(),
            rust_name: node.sig.ident.clone(),
            help: Default::default(),
            args: Default::default(),
            ret: Default::default(),
        };
        for attr in &node.attrs {
            if attr.path.is_ident("method") {
                for tok in attr.tokens.to_token_stream() {
                    if let TokenTree::Group(group) = tok {
                        assert_eq!(group.delimiter(), Delimiter::Parenthesis);
                        let stream = group.stream().into_iter().collect::<Vec<_>>();
                        assert_eq!(stream.len(), 3);
                        assert!(matches!(stream[0], TokenTree::Ident(_)));
                        match &stream[0] {
                            TokenTree::Ident(ident) => match ident.to_string().as_ref() {
                                "name" => {
                                    let raw_name = stream[2]
                                        .to_string()
                                        .strip_prefix('"')
                                        .expect("leading quote")
                                        .strip_suffix('"')
                                        .expect("trailing quote")
                                        .to_string();
                                    meta.nitrous_name = Ident::new(&raw_name, ident.span());
                                }
                                n => panic!("unknown meta arg name {n}"),
                            },
                            _ => {
                                panic!("expected identifier in meta args at {:?}", stream[0].span())
                            }
                        };
                    }
                }
            } else if attr.path.is_ident("doc") {
                let toks = attr.tokens.clone().into_iter().collect::<Vec<_>>();
                meta.help = toks[1]
                    .to_string()
                    .strip_prefix('"')
                    .expect("leading quote")
                    .strip_suffix('"')
                    .expect("trailing quote")
                    .trim()
                    .to_string();
            }
        }
        meta.args = node
            .sig
            .inputs
            .iter()
            .filter(|arg| matches!(arg, FnArg::Typed(_)))
            .map(|arg| match arg {
                FnArg::Receiver(_) => panic!("already filtered out receivers"),
                FnArg::Typed(pat_type) => {
                    if let Pat::Ident(ident) = pat_type.pat.borrow() {
                        ArgDef {
                            name: ident.ident.clone(),
                            ty: Scalar::from_type(pat_type.ty.borrow()),
                            hidden: ident.ident == "heap",
                        }
                    } else {
                        panic!("only identifier patterns supported as nitrous method args")
                    }
                }
            })
            .collect::<Vec<_>>();
        meta.ret = RetType::from_return_type(&node.sig.output);
        meta
    }

    pub(crate) fn lower_to_dict(&self) -> TokenStream2 {
        let name = &self.nitrous_name;
        let internal_name = &self.rust_name;
        let ret = self.ret.nitrous_type();
        let help = &self.help;
        quote! {
            let mut item = ::nitrous::Dict::default();
            item.insert("name".to_owned(), ::nitrous::Value::String(stringify!(#name).to_owned()));
            item.insert("internal_name".to_owned(), ::nitrous::Value::String(stringify!(#internal_name).to_owned()));
            item.insert("returns".to_owned(), ::nitrous::Value::String(#ret.to_owned()));
            item.insert("help".to_owned(), ::nitrous::Value::String(#help.to_owned()));
            acc.insert(stringify!(#name).to_owned(), ::nitrous::Value::Dict(item));
        }
    }

    pub(crate) fn lower_for_list(&self) -> TokenStream2 {
        let help = &self.help;
        let key = format!(
            "{}({})",
            self.rust_name,
            self.args
                .iter()
                .filter(|a| !a.hidden)
                .map(|a| format!("{}", a.name))
                .collect::<Vec<String>>()
                .join(", ")
        );
        quote! {
            acc.insert(#key.to_owned(), ::nitrous::Value::String(#help.to_owned()));
        }
    }

    pub(crate) fn lower_rust_item(&self) -> TokenStream2 {
        let nitrous_name = &self.nitrous_name;
        let rust_name = &self.rust_name;
        let help = &self.help;
        quote! {
            ::nitrous::MethodMetadata {
                nitrous_name: stringify!(#nitrous_name),
                rust_name: stringify!(#rust_name),
                help: #help,
            }
        }
    }
}

pub(crate) fn make_property_get_arm(name_str: &str, name: &Ident, ty: &Scalar) -> Arm {
    parse2(match ty {
        Scalar::Boolean => quote! { #name_str => { Ok(::nitrous::Value::Boolean(self.#name)) } },
        Scalar::Integer => quote! { #name_str => { Ok(::nitrous::Value::Integer(self.#name)) } },
        Scalar::Float => quote! { #name_str => { Ok(::nitrous::Value::Float(::nitrous::ordered_float::OrderedFloat(self.#name))) } },
        Scalar::String => quote! { #name_str => { Ok(::nitrous::Value::String(self.#name.clone())) } },
        Scalar::Value => quote! { #name_str => { Ok(self.#name) } },
        _ => panic!("unsupported property type"),
    })
        .unwrap()
}

pub(crate) fn make_property_put_arm(name_str: &str, name: &Ident, ty: &Scalar) -> Arm {
    parse2(match ty {
        Scalar::Boolean => quote! { #name_str => { self.#name = value.to_bool()?; Ok(()) } },
        Scalar::Integer => quote! { #name_str => { self.#name = value.to_int()?; Ok(()) } },
        Scalar::Float => quote! { #name_str => { self.#name = value.to_float()?; Ok(()) } },
        Scalar::String => {
            quote! { #name_str => { self.#name = value.to_str()?.to_owned(); Ok(()) } }
        }
        Scalar::Value => quote! { #name_str => { self.#name = value; Ok(()) } },
        _ => panic!("unsupported property type"),
    })
    .unwrap()
}

#[derive(Debug)]
pub(crate) struct ArgDef {
    pub(crate) name: Ident,
    pub(crate) ty: Scalar,
    pub(crate) hidden: bool,
}

pub(crate) struct CollectorVisitor {
    pub(crate) methods: Vec<MethodMetadata>,
}

impl CollectorVisitor {
    pub(crate) fn new() -> Self {
        Self {
            methods: Vec::new(),
        }
    }
}

impl<'ast> Visit<'ast> for CollectorVisitor {
    fn visit_impl_item_method(&mut self, node: &'ast ImplItemMethod) {
        for attr in &node.attrs {
            if attr.path.is_ident("method") {
                let metadata = MethodMetadata::from_method_node(node);
                self.methods.push(metadata);
                break;
            }
        }
        visit::visit_impl_item_method(self, node);
    }
}

#[derive(Debug)]
pub(crate) enum Scalar {
    Boolean,
    Integer,
    Float,
    String,
    StrRef,
    Value,
    Unit,
    HeapMut,
    HeapRef,
    Selfish, // Covering all of bare, &, mut, and &mut kinds of Self.
    Other,
}

impl Scalar {
    pub(crate) fn from_type(ty: &Type) -> Self {
        if let Type::Path(p) = ty {
            Self::from_type_path(p)
        } else if let Type::Reference(r) = ty {
            match Self::from_type(&r.elem) {
                Scalar::StrRef => Scalar::StrRef,
                Scalar::Selfish => Scalar::Selfish,
                v => panic!("nitrous Scalar only support references to str and Self, not: {v:#?}",),
            }
        } else if let Type::Tuple(tt) = ty {
            if tt.elems.is_empty() {
                Scalar::Unit
            } else {
                panic!("nitrous Scalar only supports the unit tuple type")
            }
        } else {
            panic!("nitrous Scalar only supports path and ref types, not: {ty:#?}",);
        }
    }

    pub(crate) fn type_path_name(p: &TypePath) -> String {
        assert_eq!(
            p.path.segments.len(),
            1,
            "nitrous Scalar paths must have length 1 (e.g. builtins)"
        );
        p.path.segments.first().unwrap().ident.to_string()
    }

    pub(crate) fn from_type_path(p: &TypePath) -> Self {
        match Self::type_path_name(p).as_str() {
            "bool" => Scalar::Boolean,
            "i64" => Scalar::Integer,
            "f64" => Scalar::Float,
            "str" => Scalar::StrRef,
            "String" => Scalar::String,
            "Value" => Scalar::Value,
            "HeapMut" => Scalar::HeapMut,
            "HeapRef" => Scalar::HeapRef,
            "Self" => Scalar::Selfish,
            _ => Scalar::Other,
        }
    }

    pub(crate) fn nitrous_type(&self) -> &'static str {
        match self {
            Self::Boolean => "Boolean",
            Self::Integer => "Integer",
            Self::Float => "Float",
            Self::String => "String",
            Self::StrRef => "StrRef",
            Self::Value => "Value",
            Self::Unit => "Unit",
            Self::HeapMut => "HeapMut",
            Self::HeapRef => "HeapRef",
            Self::Selfish => "Selfish",
            Self::Other => "Other",
        }
    }
}

#[derive(Debug, Default)]
pub(crate) enum RetType {
    #[default]
    Nothing,
    Raw(Scalar),
    ResultRaw(Scalar),
}

impl RetType {
    pub(crate) fn from_return_type(ret_ty: &ReturnType) -> Self {
        match ret_ty {
            ReturnType::Default => RetType::Nothing,
            ReturnType::Type(_, ref ty) => {
                if let Type::Path(p) = ty.borrow() {
                    match Scalar::type_path_name(p).as_str() {
                        "Result" => {
                            if let PathArguments::AngleBracketed(ab_generic_args) =
                                &p.path.segments.first().unwrap().arguments
                            {
                                let fallible_arg = ab_generic_args.args.first().unwrap();
                                if let GenericArgument::Type(ty_inner) = fallible_arg {
                                    RetType::ResultRaw(Scalar::from_type(ty_inner))
                                } else {
                                    panic!("Result parameter must be a type");
                                }
                            } else {
                                panic!("Result must have an angle bracketed argument");
                            }
                        }
                        _ => Self::Raw(Scalar::from_type(ty.borrow())),
                    }
                } else if let Type::Reference(ref ty_ref) = ty.borrow() {
                    if let Type::Path(p) = ty_ref.elem.borrow() {
                        if Scalar::type_path_name(p).as_str() == "Self" {
                            RetType::Raw(Scalar::Selfish)
                        } else if Scalar::type_path_name(p).as_str() == "str" {
                            RetType::Raw(Scalar::StrRef)
                        } else {
                            panic!(
                                "nitrous methods can only return references to Self, not {p:#?}",
                            );
                        }
                    } else {
                        panic!("nitrous methods can only return references to Self");
                    }
                } else {
                    panic!("nitrous RetType only supports Result, None, Value, and basic types, not: {ty:#?}")
                }
            }
        }
    }

    pub(crate) fn nitrous_type(&self) -> &'static str {
        match self {
            Self::Nothing => "",
            Self::Raw(s) => s.nitrous_type(),
            Self::ResultRaw(s) => s.nitrous_type(),
        }
    }
}
