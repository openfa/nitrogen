[![pipeline status](https://gitlab.com/openfa/nitrogen/badges/main/pipeline.svg)](https://gitlab.com/openfa/nitrogen/-/commits/main)
[![Latest
Release](https://gitlab.com/openfa/nitrogen/-/badges/release.svg)](https://gitlab.com/openfa/nitrogen/-/releases)

# Nitrogen
An open-source Flight Sim engine.

Official Repo: [https://gitlab.com/openfa/nitrogen](https://gitlab.com/openfa/nitrogen)

## Project Status
* Graphics / Input Engine
  * [x] Wgpu + Winit
  * [x] Simple frame management and buffer upload system
  * [x] Robust key binding support
  * [x] Basic command system
  * [ ] Data-driven Compositing
  * [ ] Multi-Camera support
  * [ ] VR support
  * [ ] Joystick support
  * [ ] Gamepad support
* Atmospheric Simulation
  * [x] Basic precomputed scattering: Using [Bruneton's method](https://github.com/ebruneton/precomputed_atmospheric_scattering).
  * [ ] Dynamically changing atmospheric conditions
  * [ ] Spatially variable atmospheric parameters
* Weather Simulation
  * [ ] Pick a technique to apply for both global and local scales
* Water Simulation
  * [ ] Pick a research paper to implement
* Cloud Simulation
  * [x] Pick a technique to implement: Using [Horizon Zero Dawn's method](http://advances.realtimerendering.com/s2015/The%20Real-time%20Volumetric%20Cloudscapes%20of%20Horizon%20-%20Zero%20Dawn%20-%20ARTR.pdf)
  * [ ] Noise based cloud layer generation and management
  * [ ] Cloud light scattering implementation
* Shadows
  * [ ] Pick a technique
* Forest Simulation
  * [ ] Pick a technique to implement
      * Candidate: [Bruneton's Real Time Realistic Rendering and Lighting of Forests](https://hal.inria.fr/hal-00650120/file/article.pdf)
* Script Driven Game Engine
  * [x] Pick a good name: Nitrous
  * [x] Basic scripting engine
  * [x] drop-down console
  * [x] command history
  * [x] ECS Driven Memory System
  * [ ] pretty output and entities lists
  * [ ] Scripted Functions
* Model Rendering
  * [x] Select a format: glTF
  * [ ] Load glTF files and upload batches to the GPU
  * [ ] Render glTF shapes
  * [ ] Animate glTF sub-models with control state
* Flight Modeling
  * [x] Pick an algorithm: [Allerton's Principles of Flight Simulation](https://www.wiley.com/en-us/Principles+of+Flight+Simulation-p-9780470754368)
  * [ ] Expose relevant controls and surfaces
  * [ ] Framework for providing forces and moments to the flight model
* Entity/Runtime System
  * [ ] Save/Load support
  * [ ] Replay recording
  * [ ] Network syncing
* Planetary Scale Rendering; Using [Kooima's thesis](https://www.evl.uic.edu/documents/kooima-dissertation-uic.pdf).
  * [x] Patch management
  * [x] Patch tesselation
  * [x] Heightmap generator
  * [x] Colormap generator
  * [ ] L14+ data
  * [ ] polar projection data
  * [x] web hosted data
  * [x] Atmospheric blending
  * [ ] Self shadowing
* Text
  * [x] Layout management
  * [x] TTF loading
  * [x] 2d screen-space rendering
  * [ ] in-world text rendering
* UI
  * [x] Basic UI Framework (egui)
  * [x] Blurred backgrounds
  * [ ] In-world UI elements
* Sound
  * [ ] Pick a framework
  * [ ] Sample management
  * [ ] Channel management and blending
  * [ ] Positional audio
  * [ ] Frequency scaling (e.g. for wind and engine noises)
  * [ ] Doppler effects
* Joystick / Gamepad Support
  * [ ] Pick framework(s)

## Terrain Generation Notes

### SRTM -> COG

* We need to create a tiff first by merging our srtm tiles
* Overviews seem to be automatic... no override options on bare Gdal Tiff driver
* Blocksize must be power of 2, so we can't just directly copy srtm data;
  this doesn't seem to hurt performance much.
* GoogleMaps style tiling gives 256px tiles, so we have to use "custom" layout
* We need to enable sparse aggressively to get reasonable sizes

#### SRTM -> BigGeoTiff
```shell
cd /directory/with/all/NXXEYYY.SRTMGL1.hgt.zip
\time gdal_merge.py -o ../srtm-tiled-sparse-lzma.tif -ot Int16 -v \
  -co NUM_THREADS=ALL_CPUS -co SPARSE_OK=TRUE -co TILED=YES \
  -co BLOCKXSIZE=512 -co BLOCKYSIZE=512 -co COMPRESS=LZMA \
  -co PROFILE=GDALGeoTIFF -co GEOTIFF_VERSION=1.1 -co BIGTIFF=YES *
```

#### SRTM BigGeoTiff -> COG
```shell
\time gdal_translate -of COG -ot Int16 \
  -co BLOCKSIZE=512 -co COMPRESS=LZMA -co OVERVIEW_COMPRESS=LZMA \
  -co NUM_THREADS=ALL_CPUS -co BIGTIFF=YES -co OVERVIEWS=IGNORE_EXISTING \
  -co GEOTIFF_VERSION=1.1 -co SPARSE_OK=TRUE -co ADD_ALPHA=NO \
  srtm-tiled-sparse-lzma.tif srtm-sparse-512-lzma.cog
```

#### BMNG -> BigGeoTiff
The same as for SRTM, but leave off the data type as png includes this.
```shell
cd /directory/with/bmng/world.2004MM.3x21600x21600.XX_geo.tif
\time gdal_merge.py -o ../srtm-tiled-sparse-lzma.tif -v \
  -co NUM_THREADS=ALL_CPUS -co SPARSE_OK=TRUE -co TILED=YES \
  -co BLOCKXSIZE=512 -co BLOCKYSIZE=512 -co COMPRESS=LZMA \
  -co PROFILE=GDALGeoTIFF -co GEOTIFF_VERSION=1.1 -co BIGTIFF=YES *
```

#### BMNG BigGeoTiff -> Cog
```shell
\time gdal_translate -of COG -ot Byte \
  -co BLOCKSIZE=512 -co COMPRESS=LZMA -co OVERVIEW_COMPRESS=LZMA \
  -co NUM_THREADS=ALL_CPUS -co BIGTIFF=YES -co OVERVIEWS=IGNORE_EXISTING \
  -co GEOTIFF_VERSION=1.1 -co SPARSE_OK=TRUE -co ADD_ALPHA=NO \
  bmng-tiled-sparse-lzma.tif bmng-mmm-sparse-512-lzma.cog
```

### GDAL & SRTM

The caspian sea was processed with a different pipeline and GDAL won't touch it.
https://www.opentopodata.org/notes/invalid-srtm-zips/

These tiles can be removed or re-packed to fix the issue:
N37E051.SRTMGL1.hgt.zip
N37E052.SRTMGL1.hgt.zip
N38E050.SRTMGL1.hgt.zip
N38E051.SRTMGL1.hgt.zip
N38E052.SRTMGL1.hgt.zip
N39E050.SRTMGL1.hgt.zip
N39E051.SRTMGL1.hgt.zip
N40E051.SRTMGL1.hgt.zip
N41E050.SRTMGL1.hgt.zip
N41E051.SRTMGL1.hgt.zip
N42E049.SRTMGL1.hgt.zip
N42E050.SRTMGL1.hgt.zip
N43E048.SRTMGL1.hgt.zip
N43E049.SRTMGL1.hgt.zip
N44E048.SRTMGL1.hgt.zip
N44E049.SRTMGL1.hgt.zip
N47W087.SRTMGL1.hgt.zip
